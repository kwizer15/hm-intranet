<?php

namespace JLM\TransmitterBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use JLM\TransmitterBundle\Entity\Ask;
use JLM\TransmitterBundle\Form\Type\AskType;
use JLM\TransmitterBundle\Form\Type\AskDontTreatType;

/**
 * @Route("/ask")
 * @IsGranted("ROLE_OFFICE")
 */
class AskController extends \JLM\OfficeBundle\Controller\AskController
{
    /**
     * Finds and displays a Ask entity.
     *
     * @Route("/{id}/show", name="transmitter_ask_show")
     */
    public function showAction($id)
    {
        $entity = $this->entityManager->getRepository(Ask::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ask entity.');
        }
        $form = $this->createForm(AskDontTreatType::class, $entity);

        return $this->render('@JLMTransmitter/Ask/show.html.twig', [
            'entity'      => $entity,
            'form_donttreat' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to create a new Ask entity.
     *
     * @Route("/new", name="transmitter_ask_new")
     */
    public function newAction()
    {
        $entity = new Ask();
        $entity->setCreation(new \DateTime);
        $form   = $this->createForm(AskType::class, $entity);

        return $this->render('@JLMTransmitter/Ask/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a new Ask entity.
     *
     * @Route("/create", name="transmitter_ask_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $entity  = new Ask();
        $form = $this->createForm(AskType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('transmitter_ask_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMTransmitter/Ask/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Ask entity.
     *
     * @Route("/{id}/edit", name="transmitter_ask_edit")
     */
    public function editAction($id)
    {
        $entity = $this->entityManager->getRepository(Ask::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ask entity.');
        }

        $editForm = $this->createForm(AskType::class, $entity);

        return $this->render('@JLMTransmitter/Ask/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Edits an existing Ask entity.
     *
     * @Route("/{id}/update", name="transmitter_ask_update", methods={"POST"})
     */
    public function updateAction(Request $request, $id)
    {
        $entity = $this->entityManager->getRepository(Ask::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Ask entity.');
        }

        $editForm = $this->createForm(AskType::class, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('transmitter_ask_edit', ['id' => $id]));
        }

        return $this->render('@JLMTransmitter/Ask/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }
    
    protected function getRepositoryName()
    {
        return Ask::class;
    }
    
    /**
     * Lists all Ask entities.
     *
     * @Route("/page/{page}", name="transmitter_ask_page")
     * @Route("/", name="transmitter_ask")
     */
    public function indexAction($page = 1)
    {
        $parms = parent::indexAction($page);
        $parms['pageRoute'] = 'transmitter_ask_page';

        return $this->render('@JLMTransmitter/Ask/index.html.twig', $parms);
    }
    
    /**
     * Lists all treated Ask entities.
     *
     * @Route("/treated", name="transmitter_ask_treated")
     * @Route("/treated/page/{page}", name="transmitter_ask_treated_page")
     */
    public function listtreatedAction($page = 1)
    {
        $parms = parent::listtreatedAction($page);
        $parms['pageRoute'] = 'transmitter_ask_treated_page';

        return $this->render('@JLMTransmitter/Ask/index.html.twig', $parms);
    }
    
    /**
     * Lists all untreated Ask entities.
     *
     * @Route("/untreated", name="transmitter_ask_untreated")
     * @Route("/untreated/page/{page}", name="transmitter_ask_untreated_page")
     */
    public function listuntreatedAction($page = 1)
    {
        $parms = parent::listuntreatedAction($page);
        $parms['pageRoute'] = 'transmitter_ask_untreated_page';

        return $this->render('@JLMTransmitter/Ask/index.html.twig', $parms);
    }
    
    /**
     * Note an Ask entities as Don't treated.
     *
     * @Route("/donttreat/{id}", name="transmitter_ask_donttreat")
     */
    public function donttreatAction(Request $request, $id)
    {
        $entity = $this->getEntity($id);
        $form = $this->createForm(AskDontTreatType::class, $entity);
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }
        }
        return $this->redirect($request->headers->get('referer'));
    }
    
    /**
     * Cancel the no-treatement ok Ask entities.
     *
     * @Route("/canceldonttreat/{id}", name="transmitter_ask_canceldonttreat")
     */
    public function canceldonttreatAction(Request $request, $id)
    {
        return parent::canceldonttreatAction($request, $id);
    }
    
    /**
     * Display Sidebar
     *
     * @Route("/sidebar", name="transmitter_ask_sidebar")
     */
    public function sidebarAction()
    {
        return parent::sidebarAction();
    }
}
