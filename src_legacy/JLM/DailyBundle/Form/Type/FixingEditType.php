<?php

namespace JLM\DailyBundle\Form\Type;

use JLM\ModelBundle\Form\Type\DoorHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\DailyBundle\Entity\Fixing;

class FixingEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('door', DoorHiddenType::class)
            ->add('place', null, [
                'label' => 'Porte',
                'attr' => [
                    'class' => 'input-xlarge',
                ],
            ])
            ->add('askDate', DateTimeType::class, [
                'label' => 'Date de la demande',
                'date_widget' => 'single_text',
                'date_format' => 'dd/MM/yyyy',
            ])
            ->add('askMethod', null, [
                'label' => 'Méthode de la demande',
                'attr' => [
                    'class' => 'input-small',
                ],
            ])
            ->add('reason', null, [
                'label' => 'Raison de l\'intervention',
                'attr' => [
                    'class' => 'input-xxlarge',
                ],
            ])
            ->add('contactName', null, [
                'label' => 'Nom du contact',
                'required' => false,
            ])
            ->add('contactPhones', null, [
                'label' => 'Téléphones',
                'required' => false,
            ])
            ->add('contactEmail', EmailType::class, [
                'label' => 'e-mail',
                'required' => false,
                'attr' => [
                    'class' => 'input-xlarge',
                ],
            ])
            ->add('priority', ChoiceType::class, [
                'label' => 'Priorité',
                'choices' => [
                    1 => 'TRES URGENT',
                    2 => 'Urgent',
                    3 => 'Haute',
                    4 => 'Normal',
                    5 => 'Basse',
                    6 => 'Très basse',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Fixing::class,
            'attr' => [
                'class' => 'interventionForm'
            ],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_dailybundle_fixingedittype';
    }
}
