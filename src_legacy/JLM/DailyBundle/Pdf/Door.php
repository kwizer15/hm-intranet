<?php
namespace JLM\DailyBundle\Pdf;

use \JLM\DefaultBundle\Pdf\FPDFext;
use \JLM\ModelBundle\Entity\Door as ModelDoor;
use \JLM\DailyBundle\Entity\ShiftTechnician;
use JLM;

class Door extends FPDFext
{
    
    public static function get($door, $entities)
    {
        $pdf = new self();
        $pdf->init();
        $pdf->buildHeader($door);
        

        foreach ($entities as $entity) {
            $pdf->show($entity);
        }
        return $pdf->Output('', 'S');
    }
    
    private function init()
    {
        $this->aliasNbPages();
        $this->setFillColor(200);
        $this->addPage('L');
    }
    
    private function buildHeader(ModelDoor $door)
    {
        $this->setFont('Arial', 'B', 18);
        $this->multicell(0, 12, $door->toString(), 1, 1, 'C', true);
        $this->ln(5);
        $this->setFont('Arial', 'B', 11);
        $this->setWidths([24,34,24,8,79,79,29]);
        $this->row(['Date','Contact','Type','Ctr','Raison','Rapport','Technicien'], 6, 1, true);
        $this->setFont('Arial', '', 10);
    }
    
    private function show(ShiftTechnician $entity)
    {
        $shifting = $entity->getShifting();
        $types = [
                'fixing' => 'Dépannage',
                'maintenance' => 'Entretien',
                'work' => 'Travaux',
        ];
        $dayTrans = ['dimanche','lundi','mardi','mercredi','jeudi','vendredi','samedi'];
        $datas[0] = $dayTrans[$entity->getBegin()->format('w')].PHP_EOL.$entity->getBegin()->format('d/m/Y');
        $datas[1] = 'le '.$shifting->getCreation()->format('d/m/Y H:i').PHP_EOL
            .$shifting->getContactName().PHP_EOL
            .$shifting->getContactPhones();
        $datas[2] = $types[$shifting->getType()];
        $datas[3] = ($shifting->getContract() == 'Hors contrat') ? 'HC' : $shifting->getContract();
        $datas[4] = $shifting->getReason();
        if ($shifting instanceof JLM\DailyBundle\Entity\Work) {
            if ($shifting->getQuote() !== null) {
                $datas[4] = 'Travaux selon devis n°'.$shifting->getQuote()->getNumber().PHP_EOL.$datas[4];
            }
        }
        $datas[5] = '';
        if ($entity->getComment()) {
            $datas[5] = 'Technicien :'.PHP_EOL.$entity->getComment().PHP_EOL.PHP_EOL;
        } else {
            $interv = $entity->getShifting();
            if ($interv instanceof JLM\DailyBundle\Entity\Fixing) {
                $datas[5] = 'Constat :'.PHP_EOL.$interv->getObservation().PHP_EOL.PHP_EOL;
            }
            
            $datas[5] .= 'Action menée :'.PHP_EOL.$interv->getReport();
            if ($interv->getRest()) {
                $datas[5] .= PHP_EOL.PHP_EOL.'Reste à faire :'.PHP_EOL.$interv->getRest();
            }
        }
        $datas[6] = $entity->getTechnician().'';
        if ($entity->getEnd()) {
            $datas[6] .= PHP_EOL.$entity->getBegin()->format('H\hi').' - '
                .$entity->getEnd()->format('H\hi').PHP_EOL.$entity->getTime()->format('%h:%I');
        }
        $this->row($datas, 5, 1, false);
    }
}
