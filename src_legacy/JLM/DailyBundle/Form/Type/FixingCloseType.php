<?php

namespace JLM\DailyBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\DailyBundle\Entity\Fixing;

class FixingCloseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('published', CheckboxType::class, [
                'label' => 'Publier',
                'required' => false,
            ])
            ->add('partFamily', PartFamilyType::class, [
                'label' => 'Famille de pièce',
                'attr' => [
                    'class' => 'input-large'
                ]
            ])
            ->add('due', null, [
                'label' => 'Cause',
                'attr' => [
                    'class' => 'input-large'
                ]
            ])
            ->add('done', null, [
                'label' => 'Action',
                'attr' => [
                    'class' => 'input-large'
                ]
            ])
            ->add('observation', null, [
                'label' => 'Constat',
                'attr' => [
                    'class' => 'input-xlarge'
                ]
            ])
            ->add('report', TextareaType::class, [
                'label' => 'Action menée',
                'required' => false,
                'attr' => [
                    'class' => 'input-xlarge'
                ]
            ])
            ->add('rest', TextareaType::class, [
                'label' => 'Reste à faire',
                'required' => false,
                'attr' => [
                    'class' => 'input-xlarge'
                ]
            ])
            ->add('voucher', null, [
                'label' => 'Bon d\'intervention',
                'required' => false,
                'attr' => [
                    'class' => 'input-small'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Fixing::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_dailybundle_fixingclosetype';
    }
}
