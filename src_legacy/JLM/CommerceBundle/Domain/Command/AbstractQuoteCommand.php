<?php

namespace JLM\CommerceBundle\Domain\Command;

use DateTime;
use JLM\CommerceBundle\Model\CustomerInterface;
use JLM\ModelBundle\Entity\Door;
use JLM\ModelBundle\Entity\SiteContact;
use JLM\OfficeBundle\Entity\AskQuote;

abstract class AbstractQuoteCommand
{
    /**
     * @var DateTime
     */
    public $creation;

    /**
     * @var CustomerInterface
     */
    public $trustee;

    /**
     * @var string
     */
    public $trusteeName;

    /**
     * @var string
     */
    public $trusteeAddress;

    /**
     * @var SiteContact
     */
    public $contact;

    /**
     * @var string
     */
    public $contactCp;

    /**
     * @var string
     */
    public $follower;

    /**
     * @var string
     */
    public $followerCp;

    /**
     * @var Door
     */
    public $door;

    /**
     * @var string
     */
    public $doorCp;

    /**
     * @var float
     */
    public $vat;

    /**
     * @var string
     */
    public $description;

    /**
     * @var float
     */
    public $vatTransmitter;

    /**
     * @var AskQuote
     */
    public $ask;
}
