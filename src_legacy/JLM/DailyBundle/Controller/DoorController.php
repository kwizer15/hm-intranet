<?php

namespace JLM\DailyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\DailyBundle\Pdf\Stopped;
use JLM\ModelBundle\Form\Type\DoorTagType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\ModelBundle\Entity\Door;
use JLM\ModelBundle\Entity\DoorStop;
use JLM\ModelBundle\Form\Type\DoorStopEditType;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class DoorController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Finds and displays a Door entity.
     */
    public function showAction(Door $door)
    {
        $codeForm = $this->createCodeForm($door);

        return $this->render('@JLMDaily/Door/show.html.twig', [
            'entity' => $door,
            'quotes' => $this->entityManager->getRepository(Quote::class)->getByDoor($door),
            'codeForm' => $codeForm->createView(),
        ]);
    }
    
    private function createCodeForm(Door $door)
    {
        $form = $this->createForm(
            DoorTagType::class,
            $door,
            ['action'=>$this->generateUrl('model_door_update_code', ['id'=>$door->getId()]),
            'method'=>'POST']
        );
                    
        return $form;
    }
    
    /**
     * Displays Doors stopped
     */
    public function stoppedAction(FormFactoryInterface $formFactory)
    {
        $doors = $this->entityManager->getRepository(Door::class)->getStopped();
        $stopForms = [];
        
        foreach ($doors as $door) {
            $stopForms[] = $formFactory->createNamed(
                'doorStopEdit'.$door->getLastStop()->getId(),
                DoorStopEditType::class,
                $door->getLastStop()
            )->createView();
        }
        
        return $this->render('@JLMDaily/Door/stopped.html.twig', [
            'entities' => $doors,
            'stopForms' => $stopForms,
        ]);
    }
    
    /**
     * Displays Doors stopped
     */
    public function stopupdateAction(FormFactoryInterface $formFactory, Request $request, DoorStop $entity)
    {
        $form = $formFactory->createNamed(
            'doorStopEdit'.$entity->getId(),
            DoorStopEditType::class,
            $entity
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        return $this->stoppedAction();
    }
    
    /**
     * Displays Doors stopped
     */
    public function printstoppedAction()
    {
        $doors = $this->entityManager->getRepository(Door::class)->getStopped();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename=portes-arret.pdf');
        $response->setContent(Stopped::get($doors));

        return $response;
    }
    
    /**
     * Stop door
     */
    public function stopAction(Door $entity)
    {
        if ($entity->getLastStop() === null) {
            $stop = new DoorStop;
            $stop->setBegin(new \DateTime);
            $stop->setReason('À définir');
            $stop->setState('Non traitée');
            $entity->addStop($stop);
            $this->entityManager->persist($stop);
            $this->entityManager->flush();
        }
        return $this->showAction($entity);
    }
    
    /**
     * Unstop door
     */
    public function unstopAction(Door $entity)
    {
        $stop = $entity->getLastStop();
        if ($stop === null) {
            return $this->showAction($entity);
        }
        $stop->setEnd(new \DateTime);
        $this->entityManager->persist($stop);
        $this->entityManager->flush();
        return $this->showAction($entity);
    }
}
