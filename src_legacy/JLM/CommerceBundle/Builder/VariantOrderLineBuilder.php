<?php

namespace JLM\CommerceBundle\Builder;

use JLM\CommerceBundle\Entity\QuoteLine;
use JLM\OfficeBundle\Builder\OrderLineBuilderAbstract;

class VariantOrderLineBuilder extends OrderLineBuilderAbstract
{
    private $l;
    
    public function __construct(QuoteLine $line)
    {
        $this->l = $line;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildReference()
    {
        $this->getLine()->setReference($this->l->getReference());
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildDesignation()
    {
        $this->getLine()->setDesignation($this->l->getDesignation());
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildQuantity()
    {
        $this->getLine()->setQuantity($this->l->getQuantity());
    }
}
