<?php

namespace Tests\JLM\DailyBundle\Entity;

use JLM\DailyBundle\Entity\PartFamily;

class PartFamilyTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Country
     */
    protected $entity;
    
    protected function setUp()
    {
        $this->entity = new PartFamily();
    }
    
    protected function assertPreConditions()
    {
        $this->assertInstanceOf('JLM\DailyBundle\Model\PartFamilyInterface', $this->entity);
        $this->assertNull($this->entity->getId());
        $this->assertNull($this->entity->getName());
    }
    
    /**
     * Valid codes
     * @return array
     */
    public function getNames()
    {
        return [
            ['Guidage', 'Guidage'],
        ];
    }
    
    /**
     * @dataProvider getNames
     */
    public function testName($in, $out)
    {
        $this->assertSame($this->entity, $this->entity->setName($in));
        $this->assertSame($out, $this->entity->getName());
    }
    
    /**
     * @dataProvider getNames
     */
    public function testToString($in, $out)
    {
        $this->assertSame($this->entity, $this->entity->setName($in));
        $this->assertSame($out, $this->entity->__toString());
    }
}
