<?php

namespace JLM\TransmitterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\TransmitterBundle\Entity\Attribution;
use JLM\TransmitterBundle\Entity\Transmitter;
use JLM\TransmitterBundle\Form\Type\TransmitterType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/transmitter")
 * @IsGranted("ROLE_OFFICE")
 */
class TransmitterController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Displays a form to create a new Transmitter entity.
     *
     * @Route("/new/{id}", name="transmitter_new")
     */
    public function newAction(Attribution $attribution)
    {
        $entity = new Transmitter();
        $entity->setAttribution($attribution);
        $form   = $this->createForm(TransmitterType::class, $entity, ['id' => $attribution->getSite()->getId()]);

        return $this->render('@JLMTransmitter/Transmitter/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a new Transmitter entity.
     *
     * @Route("/create/{id}", name="transmitter_create", methods={"POST"})
     */
    public function createAction(Request $request, Attribution $attribution)
    {
        $entity  = new Transmitter();
        $form = $this->createForm(TransmitterType::class, $entity, ['id' => $attribution->getSite()->getId()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            
            // On met à jour la page de base
            return new JsonResponse([]);
        }

        return $this->render('@JLMTransmitter/Transmitter/create.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Transmitter entity.
     *
     * @Route("/{id}/edit", name="transmitter_edit")
     */
    public function editAction(FormFactoryInterface $formFactory, $id)
    {
        $entity = $this->entityManager->getRepository(Transmitter::class)->getById($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transmitter entity.');
        }
        $editForm = $formFactory->createNamed(
            'transmitterEdit'.$id,
            TransmitterType::class,
            $entity,
            ['id' => $entity->getAttribution()->getSite()->getId()]
        );

        return $this->render('@JLMTransmitter/Transmitter/edit.html.twig', [
            'entity'      => $entity,
            'form'   => $editForm->createView(),
        ]);
    }

    /**
     * Edits an existing Transmitter entity.
     *
     * @Route("/{id}/update", name="transmitter_update", methods={"POST"})
     */
    public function updateAction(FormFactoryInterface $formFactory, Request $request, $id)
    {
        $entity = $this->entityManager->getRepository(Transmitter::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Transmitter entity.');
        }

        $editForm = $formFactory->createNamed(
            'transmitterEdit'.$id,
            TransmitterType::class,
            $entity,
            ['id' => $entity->getAttribution()->getSite()->getId()]
        );
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return new JsonResponse();
        }

        return $this->render('@JLMTransmitter/Transmitter/create.html.twig', [
            'entity'      => $entity,
            'form'   => $editForm->createView(),
        ]);
    }
    
    /**
     * Unactive Transmitter entity.
     *
     * @Route("/{id}/unactive", name="transmitter_unactive")
     */
    public function unactiveAction(Request $request, Transmitter $entity)
    {
        $entity->setIsActive(false);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        
        return $this->redirect($request->headers->get('referer'));
    }
    
    /**
     * Reactive Transmitter entity.
     *
     * @Route("/{id}/reactive", name="transmitter_reactive")
     */
    public function reactiveAction(Request $request, Transmitter $entity)
    {
        $entity->setIsActive(true);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
    
        return $this->redirect($request->headers->get('referer'));
    }
}
