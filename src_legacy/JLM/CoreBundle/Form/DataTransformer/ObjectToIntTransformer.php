<?php

namespace JLM\CoreBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

abstract class ObjectToIntTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public function transform($entity)
    {
        return (null === $entity) ? '' : $entity->getId();
    }
    
    /**
     * {@inheritdoc}
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
        $entity = $this->objectManager
          ->getRepository($this->getClass())
          ->find($id)
        ;
        if (null === $entity) {
            throw new TransformationFailedException(sprintf($this->getErrorMessage(), $id));
        }
    
        return $entity;
    }
    
    /**
     * Return class name
     *
     * @return string
     */
    abstract protected function getClass();
    
    /**
     * Get error message
     *
     * @return string
     */
    protected function getErrorMessage()
    {
        return 'A '.$this->getClass().' object with id "%s" does not exist!';
    }
}
