<?php

namespace JLM\DailyBundle\Builder\Email;

class WorkPlannedMailBuilder extends WorkMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Travaux plannifiés');
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'Les travaux'.$this->getSource().' sont plannifiés pour le ...'.PHP_EOL
        .$this->getSignature());
    }
}
