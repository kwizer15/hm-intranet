<?php

namespace JLM\ModelBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\ModelBundle\Entity\Site;
use JLM\ModelBundle\Entity\SiteContact;
use JLM\ModelBundle\Form\Type\SiteContactType;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class SiteContactController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all SiteContact entities.
     */
    public function indexAction()
    {
        $entities = $this->entityManager->getRepository(SiteContact::class)->findAll();

        return $this->render('@JLMModel/SiteContact/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Finds and displays a SiteContact entity.
     */
    public function showAction(SiteContact $entity)
    {
        return $this->render('@JLMModel/SiteContact/show.html.twig', [
            'entity'      => $entity,
        ]);
    }

    /**
     * Displays a form to create a new SiteContact entity.
     */
    public function newAction(Site $site = null)
    {
        $entity = new SiteContact();
        if ($site) {
            $entity->setAdministrator($site);
        }
        $form   = $this->createForm(SiteContactType::class, $entity);

        return $this->render('@JLMModel/SiteContact/new.html.twig', [
            'entity' => $entity,
            'site' => $site,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a new SiteContact entity.
     */
    public function createAction(Request $request, Site $site = null)
    {
        $entity  = new SiteContact();
        $form = $this->createForm(SiteContactType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($entity->getPerson()->getAddress() !== null) {
                $this->entityManager->persist($entity->getPerson()->getAddress());
            }
            $this->entityManager->persist($entity->getPerson());
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('sitecontact_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMModel/SiteContact/new.html.twig', [
            'entity' => $entity,
            'site' => $site,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing SiteContact entity.
     */
    public function editAction($id)
    {
        $entity = $this->entityManager->getRepository(SiteContact::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SiteContact entity.');
        }

        $editForm = $this->createForm(SiteContactType::class, $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@JLMModel/SiteContact/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Edits an existing SiteContact entity.
     */
    public function updateAction(Request $request, $id)
    {
        $entity = $this->entityManager->getRepository(SiteContact::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SiteContact entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(SiteContactType::class, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            if ($entity->getPerson()->getAddress() !== null) {
                $this->entityManager->persist($entity->getPerson()->getAddress());
            }
            //$entity->getPerson()->formatPhones();
            $this->entityManager->persist($entity->getPerson());
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('sitecontact_show', ['id' => $id]));
        }

        return $this->render('@JLMModel/SiteContact/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a SiteContact entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $entity = $this->entityManager->getRepository(SiteContact::class)->find($id);

        if ($entity !== null) {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }
        
        return new RedirectResponse($request->headers->get('referer'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
}
