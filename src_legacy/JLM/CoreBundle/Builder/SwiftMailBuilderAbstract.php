<?php

namespace JLM\CoreBundle\Builder;

abstract class SwiftMailBuilderAbstract extends MailBuilderAbstract
{
    public function create()
    {
        $this->mail = new \Swift_Message();
    }
}
