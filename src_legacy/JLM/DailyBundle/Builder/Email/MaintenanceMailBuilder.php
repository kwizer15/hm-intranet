<?php

namespace JLM\DailyBundle\Builder\Email;

use JLM\DailyBundle\Model\MaintenanceInterface;

abstract class MaintenanceMailBuilder extends InterventionMailBuilder
{
    public function __construct(MaintenanceInterface $maintenance)
    {
        parent::__construct($maintenance);
    }
    
    public function getMaintenance()
    {
        return $this->getIntervention();
    }
}
