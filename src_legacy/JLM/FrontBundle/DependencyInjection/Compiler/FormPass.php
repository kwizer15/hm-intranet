<?php

namespace JLM\FrontBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FormPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $resources = $container->getParameter('twig.form.resources');

        foreach (['askQuoteType'] as $template) {
            $resources[] = '@JLMFront/Form/' . $template . '.html.twig';
        }

        $container->setParameter('twig.form.resources', $resources);
    }
}
