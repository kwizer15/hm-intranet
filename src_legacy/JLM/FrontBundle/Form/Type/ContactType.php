<?php

namespace JLM\FrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\FrontBundle\Entity\Contact;

class ContactType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('subject', TextType::class);
        $builder->add('content', TextareaType::class);
        $builder->add('company', TextType::class, ['required' => false]);
        $builder->add('firstName', TextType::class, ['required' => false]);
        $builder->add('lastName', TextType::class);
        $builder->add('address', TextType::class, ['required' => false]);
        $builder->add('zip', TextType::class);
        $builder->add('city', TextType::class);
        $builder->add('country', TextType::class, ['required' => false]);
        $builder->add('phone', TextType::class);
        $builder->add('email', TextType::class);
    }

    /**
     *
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_front_contacttype';
    }
    
    /**
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => Contact::class,
        ]);
    }
}
