<?php

namespace JLM\ProductBundle\Model;

interface ProductPriceInterface
{
    /**
     * Get quantity
     *
     * @return integer
     */
    public function getQuantity();
    
    /**
     * Get unitPrice
     *
     * @return float
     */
    public function getUnitPrice();
}
