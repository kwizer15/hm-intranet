<?php
namespace JLM\ModelBundle\Form\DataTransformer;

use JLM\ModelBundle\Entity\Trustee;

class TrusteeToIntTransformer extends ObjectToIntTransformer
{
    public function getClass()
    {
        return Trustee::class;
    }
    
    protected function getErrorMessage()
    {
        return 'A trustee with id "%s" does not exist!';
    }
}
