<?php

namespace JLM\ModelBundle\Form\Type;

use JLM\ContactBundle\Form\Type\AddressType;
use JLM\ContactBundle\Form\Type\ContactSelectType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ModelBundle\Entity\Trustee;

class TrusteeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('contact', ContactSelectType::class, ['label' => 'Contact', 'attr' => ['class' => 'input-large']])
            ->add('accountNumber', null, [
                'label' => 'Numéro de compte',
                'required' => false,
                'attr' => ['class' => 'input-small'],
            ])
            ->add('billingLabel', null, ['label' => 'Libélé de facturation', 'required' => false])
            ->add('billingAddress', AddressType::class, [
                'label' => 'Adresse de facturation',
                'required' => false,
            ])// Petite case à cocher + Formulaire
            ->add('billingPhone', null, [
                'label' => 'Téléphone',
                'required' => false,
                'attr' => ['class' => 'input-medium'],
            ])
            ->add('billingFax', null, ['label' => 'Fax', 'required' => false, 'attr' => ['class' => 'input-medium']])
            ->add('billingEmail', EmailType::class, [
                'label' => 'e-mail',
                'required' => false,
                'attr' => ['class' => 'input-xlarge'],
            ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_modelbundle_trusteetype';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => Trustee::class,
            ]);
    }
}
