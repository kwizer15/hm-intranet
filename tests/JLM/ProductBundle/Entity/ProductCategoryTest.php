<?php

namespace Tests\JLM\ProductBundle\Entity;

use JLM\ProductBundle\Entity\Product;
use JLM\ProductBundle\Entity\ProductCategory;
use PHPUnit\Framework\TestCase;
use JLM\ProductBundle\Model\ProductCategoryInterface;

class ProductCategoryTest extends TestCase
{
    /**
     * @var Product
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new ProductCategory;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf(ProductCategoryInterface::class, $this->entity);
        $this->assertNull($this->entity->getId());
    }
    
    public function testParent()
    {
        $parent = $this->createMock(ProductCategoryInterface::class);
        $this->assertSame($this->entity, $this->entity->setParent($parent));
        $this->assertSame($parent, $this->entity->getParent());
    }
    
    public function testChildren()
    {
        $child = $this->createMock(ProductCategoryInterface::class);
        $this->assertCount(0, $this->entity->getChildren());
        $this->assertTrue($this->entity->addChild($child));
        $this->assertCount(1, $this->entity->getChildren());
        $this->assertTrue($this->entity->removeChild($child));
        $this->assertCount(0, $this->entity->getChildren());
    }
    
    public function testName()
    {
        $this->assertSame($this->entity, $this->entity->setName('Foo'));
        $this->assertSame('Foo', $this->entity->getName());
        $this->assertSame('Foo', (string) $this->entity);
    }
}
