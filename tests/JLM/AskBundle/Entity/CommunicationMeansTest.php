<?php

namespace Tests\JLM\AskBundle\Entity;

use JLM\AskBundle\Entity\CommunicationMeans;
use JLM\AskBundle\Model\CommunicationMeansInterface;
use PHPUnit\Framework\TestCase;

class CommunicationMeansTest extends TestCase
{
    /**
     * @var CommunicationMeans
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new CommunicationMeans;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf(CommunicationMeansInterface::class, $this->entity);
        $this->assertNull($this->entity->getId());
    }
    
    public function getGetterSetter()
    {
        return [
            ['Name', 'Foo'],
        ];
    }
    
    /**
     * @dataProvider getGetterSetter
     */
    public function testGetterSetter($attribute, $value)
    {
        $getter = 'get'.$attribute;
        $setter = 'set'.$attribute;
        $this->assertSame($this->entity, $this->entity->$setter($value));
        $this->assertSame($value, $this->entity->$getter());
    }
    
    public function testToString()
    {
        $value = 'Foo';
        $this->entity->setName($value);
        $this->assertSame($value, $this->entity->__toString());
    }
}
