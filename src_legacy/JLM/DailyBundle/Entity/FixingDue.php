<?php

namespace JLM\DailyBundle\Entity;

use JLM\ModelBundle\Entity\StringModel;

/**
 * JLM\DailyBundle\Entity\FixingDue
 * @author Emmanuel Bernaszuk <emmanuel.bernaszuk@kw12er.com>
 */
class FixingDue extends StringModel
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
