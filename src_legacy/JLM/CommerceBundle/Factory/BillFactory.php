<?php

namespace JLM\CommerceBundle\Factory;

use JLM\CommerceBundle\Builder\BillBuilderInterface;

class BillFactory
{
    /**
     *
     * @param BillBuilderInterface $bill
     *
     * @return BillInterface
     */
    public static function create(BillBuilderInterface $builder)
    {
        $builder->create();
        $builder->buildCreation();
        $builder->buildLines();
        $builder->buildCustomer();
        $builder->buildBusiness();
        $builder->buildReference();
        $builder->buildIntro();
        $builder->buildDetails();
        $builder->buildConditions();
        
        return $builder->getBill();
    }
}
