<?php

namespace Tests\JLM\ModelBundle\Entity;

use JLM\ModelBundle\Entity\SiteContact;

class SiteContactTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Country
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new SiteContact;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
    }
    
    public function testPerson()
    {
        $person = $this->createMock('JLM\ContactBundle\Model\PersonInterface');
        $this->assertSame($this->entity, $this->entity->setPerson($person));
        $this->assertSame($person, $this->entity->getPerson());
    }
}
