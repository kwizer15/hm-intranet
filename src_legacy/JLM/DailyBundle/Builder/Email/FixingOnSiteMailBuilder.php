<?php

namespace JLM\DailyBundle\Builder\Email;

class FixingOnSiteMailBuilder extends FixingMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Intervention #'.$this->getFixing()->getId().' - Technicien sur place');
    }
    
    public function buildBody()
    {
        $this->setBody(
            'Bonjour,'.PHP_EOL
            .PHP_EOL
            .'Nous vous informons que notre technicien est actuellement sur place et qu\'il '
            .'procède au dépannage de l\'installation'.PHP_EOL
            .PHP_EOL
            .$this->getFixing()->getInstallationCode().PHP_EOL
            .$this->getFixing()->getPlace().PHP_EOL
            .PHP_EOL
            .'Cordialement'
            .$this->getSignature()
        );
    }
    
    public function buildAttachements()
    {
    }
}
