<?php

namespace JLM\CommerceBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\EarlyPaymentModel;
use JLM\CommerceBundle\Entity\PenaltyModel;
use JLM\CommerceBundle\Entity\PropertyModel;
use JLM\CommerceBundle\Entity\VAT;
use JLM\CoreBundle\Manager\BaseManager as Manager;
use JLM\CommerceBundle\Form\Type\BillType;
use JLM\CommerceBundle\Entity\BillLine;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CoreBundle\Event\FormPopulatingEvent;
use JLM\CommerceBundle\Entity\Bill;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class BillManager extends Manager
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        EventDispatcherInterface $eventDispatcher,
        FormFactoryInterface $formFactory
    ) {
        parent::__construct(
            Bill::class,
            $entityManager,
            $router,
            $formFactory
        );
        $this->eventDispatcher = $eventDispatcher;
    }

    protected function getFormParam($name, $options = [])
    {
        switch ($name) {
            case 'new':
                return [
                    'method' => 'POST',
                    'route' => 'bill_create',
                    'params' => [],
                    'label' => 'Créer',
                    'type'  => BillType::class,
                    'entity' => null,
                ];
            case 'edit':
                return [
                    'method' => 'POST',
                    'route' => 'bill_update',
                    'params' => ['id' => $options['entity']->getId()],
                    'label' => 'Modifier',
                    'type'  => BillType::class,
                    'entity' => $options['entity']
                ];
        }
        
        return parent::getFormParam($name, $options);
    }
    
    /**
     * {@inheritdoc}
     */
    public function populateForm($form, Request $request)
    {
        // Appel des évenements de remplissage du formulaire
        $this->eventDispatcher->dispatch(JLMCommerceEvents::BILL_FORM_POPULATE, new FormPopulatingEvent($form, $request));
        
        // On complète avec ce qui reste vide
        $vat = $this->objectManager->getRepository(VAT::class)->find(1)->getRate();
        $params = [
                'creation' => new \DateTime,
                'vat' => $vat,
                'vatTransmitter' => $vat,
                'penalty' => $this->objectManager->getRepository(PenaltyModel::class)->find(1).'',
                'property' => $this->objectManager->getRepository(PropertyModel::class)->find(1).'',
                'earlyPayment' => $this->objectManager->getRepository(EarlyPaymentModel::class)->find(1).'',
                'maturity' => 30,
                'discount' => 0,
        ];
        foreach ($params as $key => $value) {
            $param = $form->get($key)->getData();
            if (empty($param)) {
                $form->get($key)->setData($value);
            }
        }
        
        // on met un ligne vide si y en a pas
        $lines = $form->get('lines')->getData();
        if (empty($lines)) {
            $form->get('lines')->setData([new BillLine()]);
        }
    
        return parent::populateForm($form, $request);
    }
}
