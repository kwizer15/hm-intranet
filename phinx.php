<?php

use Symfony\Component\Dotenv\Dotenv;

require_once __DIR__.'/vendor/autoload.php';

$dotenv = new Dotenv();
$dotenvFiles = [__DIR__.'/.env'];
if (file_exists(__DIR__.'/.env.local')) {
    $dotenvFiles[] = __DIR__.'/.env.local';
}
$dotenv->load(...$dotenvFiles);

return [
    'paths' => [
        'migrations' =>'%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' =>'%%PHINX_CONFIG_DIR%%/db/seeds',
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'database',
        'database' => [
            'adapter' => 'mysql',
            'host' => getenv('DATABASE_HOST') ?? 'localhost',
            'name' => getenv('DATABASE_NAME'),
            'user' => getenv('DATABASE_USERNAME') ?? 'root',
            'pass' => getenv('DATABASE_PASSWORD') ?? 'root',
            'port' => getenv('DATABASE_PORT') ?? 3306,
            'charset' => 'utf8',
        ],
    ],
    'version_order' => 'creation',
];
