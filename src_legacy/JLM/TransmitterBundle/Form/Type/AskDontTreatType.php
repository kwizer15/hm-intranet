<?php
namespace JLM\TransmitterBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\TransmitterBundle\Entity\Ask;

class AskDontTreatType extends \JLM\OfficeBundle\Form\Type\AskDontTreatType
{
   
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => Ask::class
        ]);
    }
    
    public function getBlockPrefix(): string
    {
        return 'jlm_transmitterbundle_askdonttreattype';
    }
}
