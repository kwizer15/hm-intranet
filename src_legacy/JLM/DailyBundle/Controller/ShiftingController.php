<?php

namespace JLM\DailyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\ModelBundle\Entity\Technician;
use JLM\DailyBundle\Entity\Shifting;
use JLM\DailyBundle\Entity\ShiftTechnician;
use JLM\DailyBundle\Form\Type\AddTechnicianType;
use JLM\DailyBundle\Form\Type\ShiftingEditType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class ShiftingController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * List
     */
    public function listAction(Technician $technician, $page = 1)
    {
        $limit = 10;
        $repository = $this->entityManager->getRepository(ShiftTechnician::class);
        $nb = $repository->getCountWithoutTime($technician);
        $nbPages = ceil($nb/$limit);
        $nbPages = ($nbPages < 1) ? 1 : $nbPages;
        $offset = ($page-1) * $limit;
        if ($page < 1 || $page > $nbPages) {
            throw $this->createNotFoundException('Page inexistante (page '.$page.'/'.$nbPages.')');
        }
        $entities = $repository->getWithoutTime(
            $technician,
            $limit,
            $offset
        );
        
        return $this->render('@JLMDaily/Shifting/list.html.twig', [
                'technician'=>$technician,
                'shiftings' => $entities,
                'page'     => $page,
                'nbPages'  => $nbPages,
        ]);
    }
    
    /**
     * Ajoute un technicien sur une intervention
     */
    public function newAction(FormFactoryInterface $formFactory, Shifting $shifting)
    {
        $entity = new ShiftTechnician();
        
        $entity->setBegin(new \DateTime);
        $form = $formFactory->createNamed(
            'shiftTechNew'.$shifting->getId(),
            AddTechnicianType::class,
            $entity
        );

        return $this->render('@JLMDaily/Shifting/new.html.twig', [
            'shifting' => $shifting,
            'entity' => $entity,
            'form'   => $form->createView(),
            'id' => $shifting->getId(),
        ]);
    }
    
    /**
     * Creates a new ShiftTechnician entity.
     */
    public function createAction(FormFactoryInterface $formFactory, Request $request, Shifting $shifting)
    {
        $entity  = new ShiftTechnician();
        $entity->setShifting($shifting);
        $entity->setCreation(new \DateTime);
        $form = $formFactory->createNamed(
            'shiftTechNew'.$shifting->getId(),
            AddTechnicianType::class,
            $entity
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($shifting);
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
    
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([]);
            }
            return $this->redirect($request->headers->get('referer'));
        }
    
        return $this->render('@JLMDaily/Shifting/create.html.twig', [
            'shifting'=>$shifting,
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Displays a form to edit an existing ShiftTechnician entity.
     */
    public function editAction(FormFactoryInterface $formFactory, ShiftTechnician $entity)
    {
        $editForm = $formFactory->createNamed(
            'shiftTechEdit'.$entity->getId(),
            ShiftingEditType::class,
            $entity
        );

        return $this->render('@JLMDaily/Shifting/edit.html.twig', [
                'entity'      => $entity,
                'form'   => $editForm->createView(),
        ]);
    }
    
    /**
     * Displays a form to edit an existing ShiftTechnician entity.
     *
     * @deprecated Modal system
     */
    public function edittableAction(ShiftTechnician $entity)
    {
        return $this->editAction($entity);
    }
    
    /**
     * Edits an existing InterventionPlanned entity.
     */
    public function updateAction(FormFactoryInterface $formFactory, Request $request, ShiftTechnician $entity)
    {
        $editForm = $formFactory->createNamed(
            'shiftTechEdit'.$entity->getId(),
            ShiftingEditType::class,
            $entity
        );
        $editForm->handleRequest($request);
    
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $request->isXmlHttpRequest()
                ? new JsonResponse([])
                : $this->redirect($request->headers->get('referer'))
            ;
        }
        
        $errors = array_reduce($editForm->getErrors(), function ($carry, $item) {
            $carry[] = $item->getMessage();
            return $carry;
        }, []);
        
        return $request->isXmlHttpRequest()
            ? new JsonResponse(['errors' => $errors])
            : $this->render('@JLMDaily/Shifting/update.html.twig', [
                'entity'      => $entity,
                'form'   => $editForm->createView(),
            ]);
    }
    
    /**
     * Delete an existing ShiftTechnician entity.
     */
    public function deleteAction(ShiftTechnician $entity)
    {
        $intervId = $entity->getShifting()->getId();
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    
        return ($this->redirectToRoute('intervention_redirect', ['id' => $intervId, 'act'=>'show']));
    }
}
