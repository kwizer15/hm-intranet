<?php

namespace JLM\InstallationBundle\Model;

interface InstallationInterface
{

    /**
     * Details to access
     * @return string
     */
    public function getObservations();
    
    /**
     * Get Warranty
     *
     * @return DateTime|null
     */
    public function getEndWarranty();
    
    /**
     * Is Under Warranty
     *
     * @return bool
     */
    public function isUnderWarranty();
    
    /**
     * Get Code installation
     *
     * @return string
     */
    public function getCode();
    
    /**
     * Has Code installation
     *
     * @return bool
     */
    public function hasCode();
    
    /**
     * Width
     * @return int
     */
    public function getWidth();
    
    /**
     * Height
     * @return int
     */
    public function getHeight();
    
    /**
     * Get actual contract
     * @return ContractInterface
     */
    public function getActualContract();
}
