<?php

namespace JLM\DailyBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\DailyBundle\Entity\Intervention;

class ExternalBillType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('externalBill', null, ['label'=>'Numéro de facture'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Intervention::class
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_dailybundle_externalbilltype';
    }
}
