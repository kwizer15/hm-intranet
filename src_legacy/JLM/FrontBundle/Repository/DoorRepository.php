<?php

namespace JLM\FrontBundle\Repository;

use Doctrine\ORM\EntityRepository;

class DoorRepository extends EntityRepository
{
    /**
     * Get by code
     *
     * @return Door
     */
    public function getByCode($code)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.code = ?1')
            ->setParameter(1, strtoupper($code))
        ;
        
        try {
            return $queryBuilder->getQuery()->getSingleResult();
        } catch (\Exception $e) {
        }
        
        return null;
    }
}
