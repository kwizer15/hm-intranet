<?php

namespace JLM\CommerceBundle\Builder;

interface BillLineBuilderInterface
{
    public function create();
    
    public function buildProduct();
    
    public function buildQuantity();
    
    public function buildPrice();
    
    public function getLine();
}
