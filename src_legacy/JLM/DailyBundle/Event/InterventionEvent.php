<?php

namespace JLM\DailyBundle\Event;

use JLM\DailyBundle\Model\InterventionInterface;
use Symfony\Component\EventDispatcher\Event;

class InterventionEvent extends Event
{
    /**
     * @var InterventionInterface
     */
    private $intervention;

    /**
     * @param FormInterface $form
     * @param Request $request
     */
    public function __construct(InterventionInterface $intervention)
    {
        $this->intervention = $intervention;
    }
    
    public function getIntervention()
    {
        return $this->intervention;
    }
}
