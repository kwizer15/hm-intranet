<?php

namespace JLM\TransmitterBundle\Model;

interface AttributionInterface
{
    /**
     * Get ask
     *
     * @return \JLM\OfficeBundle\Entity\Ask
     */
    public function getAsk();
    
    /**
     * Get Site
     *
     * @return \JLM\ModelBundel\Entity\Site
     */
    public function getSite();
    
    /**
     * Get transmitters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTransmitters();
}
