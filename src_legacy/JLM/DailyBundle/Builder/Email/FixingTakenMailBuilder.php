<?php

namespace JLM\DailyBundle\Builder\Email;

class FixingTakenMailBuilder extends FixingMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Intervention #'.$this->getFixing()->getId().' - Demande d\'intervention prise en compte');
    }
    
    public function buildBody()
    {
        $this->setBody(
            'Bonjour,'.PHP_EOL
            .PHP_EOL
            .'La demande d\'intervention du '.$this->getFixing()->getAskDate()->format('d/m/Y à H\hi')
            .' pour l\'installation : '.PHP_EOL
            .$this->getFixing()->getInstallationCode().PHP_EOL
            .$this->getFixing()->getPlace().PHP_EOL
            .PHP_EOL
            .'à bien été prise en compte par nos services.'.PHP_EOL
            .$this->getSignature()
        );
    }
}
