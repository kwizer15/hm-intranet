<?php
namespace JLM\ModelBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\ModelBundle\JLMModelEvents;
use JLM\ModelBundle\Event\DoorEvent;

class EmailSubscriber implements EventSubscriberInterface
{
   
    private $objectManager;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public static function getSubscribedEvents()
    {
        return [
            JLMModelEvents::DOOR_SENDMAIL => 'persistEmailsOnDoor',
        ];
    }
    
    public function persistEmailsOnDoor(DoorEvent $event)
    {
        $door = $event->getDoor();
        $mail = $event->getParam('jlm_core_mail');
        $to = (isset($mail['to'])) ? $mail['to'] : [];
        $cc = (isset($mail['cc'])) ? $mail['cc'] : [];
        $door->setManagerEmails($to);
        $door->setAdministratorEmails($cc);
        $this->objectManager->persist($door);
        $this->objectManager->flush();
    }
}
