<?php

namespace Tests\JLM\CondominiumBundle\Entity;

use JLM\CondominiumBundle\Entity\Manager;

class ManagerTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Person
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->contact = $this->createMock('JLM\ContactBundle\Model\ContactInterface');
        $this->entity = new Manager();
        $this->entity->setContact($this->contact);
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf('JLM\CondominiumBundle\Model\ManagerInterface', $this->entity);
    }
    
    public function getNames()
    {
        return [
            ['Foo', 'Foo'],
        ];
    }
    
    /**
     * @dataProvider getNames
     * @param string $in
     * @param string $out
     */
    public function testName($in, $out)
    {
        $this->contact->expects($this->once())->method('getName')->will($this->returnValue($in));
        $this->assertSame($out, $this->entity->getName());
    }
}
