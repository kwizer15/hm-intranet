<?php

namespace JLM\ProductBundle\Form\Type;

use JLM\CoreBundle\Form\Type\AbstractSelectType;
use JLM\ProductBundle\Form\DataTransformer\ProductToIntTransformer;

class ProductSelectType extends AbstractSelectType
{

    protected function getTransformerClass()
    {
        return ProductToIntTransformer::class;
    }
    
    protected function getTypeName()
    {
        return 'jlm_product_product';
    }
}
