<?php

namespace JLM\ModelBundle\Form\Type;

use Genemu\Bundle\FormBundle\Form\JQuery\Type\Select2EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use JLM\ModelBundle\Entity\Site;

class DoorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('site', Select2EntityType::class, [
                'label' => 'Affaire',
                'class' => Site::class,
                'attr' => ['class' => 'input-xxlarge'],
            ])
            ->add('location', null, ['label' => 'Localisation'])
            ->add('street', null, ['label' => 'Adresses d\'accès', 'required' => false])
            ->add('billingPrelabel', null, ['label' => 'Libélé de facturation', 'required' => false])
            ->add('model', null, ['label' => 'Modèle de porte'])
            ->add('ceNumber', null, ['label' => 'Identification CE'])
            ->add('type', null, ['label' => 'Type de porte'])
            ->add('width', DistanceType::class, ['label' => 'Largeur', 'required' => false])
            ->add('height', DistanceType::class, ['label' => 'Hauteur', 'required' => false])
            ->add('transmitters', null, [
                'label' => 'Type d\'emetteurs',
                'required' => false,
                'attr' => ['class' => 'input-xlarge'],
            ])
            ->add('observations', null, [
                'label' => 'Observations',
                'required' => false,
                'attr' => ['class' => 'input-large'],
            ])
            ->add('latitude', null, [
                'label' => 'Latitude',
                'required' => false,
                'scale' => 7,
                'attr' => ['class' => 'input-large'],
            ])// Précision
            ->add('longitude', null, [
                'label' => 'Latitude',
                'required' => false,
                'scale' => 7,
                'attr' => ['class' => 'input-large'],
            ])// Précision
            ->add('googlemaps', null, [
                'label' => 'Lien Google Maps',
                'required' => false,
                'attr' => ['class' => 'input-xxlarge'],
            ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_modelbundle_doortype';
    }
}
