<?php

namespace JLM\CommerceBundle\Builder;

interface BillBuilderInterface
{
    /**
     * @return BillInterface
     */
    public function getBill();

    public function create();
    
    public function buildCreation();
    
    public function buildLines();
    
    public function buildCustomer();
    
    public function buildBusiness();

    public function buildReference();
    
    public function buildIntro();
    
    public function buildDetails();
    
    public function buildConditions();
}
