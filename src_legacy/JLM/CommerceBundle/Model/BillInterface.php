<?php

namespace JLM\CommerceBundle\Model;

interface BillInterface
{
    /**
     *
     * @param BusinessInterface $siteObject
     * @return self
     *
     * @deprecated Use setBusiness
     */
    public function setSiteObject(BusinessInterface $siteObject = null);
    
    /**
     * @param BusinessInterface $siteObject
     * @return self
     */
    public function setBusiness(BusinessInterface $business = null);
    
    /**
     *
     * @param string $site
     * @return self
     */
    public function setSite($site);
    
    /**
     *
     * @param CustomerInterface $customer
     * @return self
     */
    public function setCustomer(CustomerInterface $customer = null);
    
    /**
     *
     * @param string $name
     * @return self
    */
    public function setCustomerName($name);
    
    /**
     *
     * @param string $address
     * @return self
    */
    public function setCustomerAddress($address);
    
    /**
     *
     * @param CustomerInterface $customer
     * @return self
     * @deprecated Use setCustomer
     */
    public function setTrustee(CustomerInterface $customer = null);
    
    /**
     *
     * @param string $name
     * @return self
     * @deprecated Use setCustomerName
     */
    public function setTrusteeName($name);
    
    /**
     *
     * @param string $address
     * @return self
     * @deprecated Use setCustomerAddress
     */
    public function setTrusteeAddress($address);
    
    /**
     * @return string
     */
    public function getNumber();
    
    /**
     * @return array
     */
    public function getBoostContacts();
}
