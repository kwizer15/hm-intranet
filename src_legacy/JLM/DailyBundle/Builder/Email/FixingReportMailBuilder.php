<?php

namespace JLM\DailyBundle\Builder\Email;

class FixingReportMailBuilder extends FixingMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Intervention #'.$this->getFixing()->getId().' Compte-rendu');
    }
    
    public function buildBody()
    {
        $fixing = $this->getFixing();
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'Suite à notre intervention du '.$fixing->getLastDate()->format('d/m/Y').' sur l\'installation :'.PHP_EOL
        .PHP_EOL
        .$fixing->getInstallationCode().PHP_EOL
        .$fixing->getDoor()->getType().' / '.$fixing->getDoor()->getLocation().PHP_EOL
        .$fixing->getDoor()->getSite()->getAddress().PHP_EOL
        .PHP_EOL
        .trim($fixing->getCustomerReport().PHP_EOL
        .$fixing->getCustomerActions().PHP_EOL
        .$fixing->getCustomerState().PHP_EOL
        .$fixing->getCustomerProcess().PHP_EOL)
        .$this->getSignature());
    }
    
    public function buildAttachements()
    {
    }
}
