<?php

namespace JLM\TransmitterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\EarlyPaymentModel;
use JLM\CommerceBundle\Entity\PenaltyModel;
use JLM\CommerceBundle\Entity\PropertyModel;
use JLM\CommerceBundle\Entity\VAT;
use JLM\CommerceBundle\Factory\BillFactory;
use JLM\CoreBundle\Response\PdfResponse;
use JLM\CoreBundle\Service\Paginator;
use JLM\ProductBundle\Entity\Product;
use JLM\TransmitterBundle\Builder\AttributionBillBuilder;
use JLM\TransmitterBundle\Entity\Attribution;
use JLM\TransmitterBundle\Entity\Ask;
use JLM\TransmitterBundle\Form\Type\AttributionType;
use JLM\TransmitterBundle\Pdf\AttributionCourrier;
use JLM\TransmitterBundle\Pdf\AttributionList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/attribution")
 * @IsGranted("ROLE_OFFICE")
 */
class AttributionController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Attribution entities.
     *
     * @Route("/", name="transmitter_attribution")
     */
    public function indexAction(Paginator $paginator, Request $request)
    {
        return $this->render('@JLMTransmitter/Attribution/index.html.twig', $paginator->paginator(
            $request,
            $this->entityManager->getRepository(Attribution::class),
            ['sort' => '!date']
        ));
    }

    /**
     * Finds and displays a Attribution entity.
     *
     * @Route("/{id}/show", name="transmitter_attribution_show")
     */
    public function showAction(Attribution $entity)
    {
        return $this->render('@JLMTransmitter/Attribution/show.html.twig', [
            'entity' => $entity,
        ]);
    }

    /**
     * Displays a form to create a new Attribution entity.
     *
     * @Route("/new/{id}", name="transmitter_attribution_new")
     */
    public function newAction(Ask $ask)
    {
        $entity = new Attribution();
        $entity->setCreation(new \DateTime);
        $entity->setAsk($ask);
        $form = $this->createForm(AttributionType::class, $entity);

        return $this->render('@JLMTransmitter/Attribution/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a new Attribution entity.
     *
     * @Route("/create", name="transmitter_attribution_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $entity = new Attribution();
        $form = $this->createForm(AttributionType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $this->redirectToRoute('transmitter_attribution_show', ['id' => $entity->getId()]);
        }

        return $this->render('@JLMTransmitter/Attribution/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Attribution entity.
     *
     * @Route("/{id}/edit", name="transmitter_attribution_edit")
     */
    public function editAction(Attribution $entity)
    {
        $editForm = $this->createForm(AttributionType::class, $entity);

        return $this->render('@JLMTransmitter/Attribution/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Edits an existing Attribution entity.
     *
     * @Route("/{id}/update", name="transmitter_attribution_update", methods={"POST"})
     */
    public function updateAction(Request $request, Attribution $entity)
    {
        $editForm = $this->createForm(AttributionType::class, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $this->redirectToRoute('transmitter_attribution_show', ['id' => $entity->getId()]);
        }

        return $this->render('@JLMTransmitter/Attribution/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * Imprime la liste d'attribution
     *
     * @Route("/{id}/printlist", name="transmitter_attribution_printlist")
     */
    public function printlistAction(Attribution $entity)
    {
        // Retrier les bips par Groupe puis par numéro
        $transmitters = $entity->getTransmitters();
        $resort = [];
        foreach ($transmitters as $transmitter) {
            $index = $transmitter->getUserGroup()->getName();
            if (!isset($resort[$index])) {
                $resort[$index] = [];
            }
            $resort[$index][] = $transmitter;
        }

        $final = array_merge([], ...$resort);

        return new PdfResponse('attribution-' . $entity->getId() . '.pdf', AttributionList::get($entity, $final, true));
    }

    /**
     * Imprime le courrier
     *
     * @Route("/{id}/printcourrier", name="transmitter_attribution_printcourrier")
     */
    public function printcourrierAction(Attribution $entity)
    {
        return new PdfResponse(
            'attribution-courrier-' . $entity->getId() . '.pdf',
            AttributionCourrier::get($entity)
        );
    }

    /**
     * Generate bill
     *
     * @Route("/{id}/bill", name="transmitter_attribution_bill")
     */
    public function billAction(Attribution $entity)
    {
        if ($entity->getBill() !== null) {
            return ($this->redirectToRoute('bill_edit', ['id' => $entity->getBill()->getId()]));
        }
        // @todo trouver un autre solution que le codage brut
        $options = [
            'port' => $this->entityManager->getRepository(Product::class)->find(134),
            'earlyPayment' => (string) $this->entityManager->getRepository(EarlyPaymentModel::class)->find(1),
            'penalty' => (string) $this->entityManager->getRepository(PenaltyModel::class)->find(1),
            'property' => (string) $this->entityManager->getRepository(PropertyModel::class)->find(1),
        ];
        $bill = BillFactory::create(new AttributionBillBuilder(
            $entity,
            $this->entityManager->getRepository(VAT::class)->find(1)->getRate(),
            $options
        ));
        $this->entityManager->persist($bill);
        $entity->setBill($bill);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $this->redirectToRoute('bill_edit', ['id' => $bill->getId()]);
    }
}
