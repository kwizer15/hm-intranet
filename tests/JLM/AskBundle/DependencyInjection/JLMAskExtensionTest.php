<?php

namespace Tests\JLM\AskBundle\DependencyInjection;

use JLM\AskBundle\DependencyInjection\JLMAskExtension;

class JLMAskExtensionTest extends \PHPUnit\Framework\TestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->ext = new JLMAskExtension();
    }
    
    public function assertPreConditions()
    {
        $this->assertInstanceOf('Symfony\Component\HttpKernel\DependencyInjection\Extension', $this->ext);
    }
    
    public function testLoad()
    {
        $this->ext->load([], $this->createMock('Symfony\Component\DependencyInjection\ContainerBuilder'));
    }
}
