<?php

namespace JLM\InstallationBundle\Model;

interface PartFamilyInterface
{

    /**
     * @return string
     */
    public function getName();
}
