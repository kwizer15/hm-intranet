<?php

namespace JLM\TransmitterBundle\Entity;

use JLM\CommerceBundle\Entity\TextModel;
use JLM\ProductBundle\Model\ProductInterface;

class Model extends TextModel
{
    /**
     * @var Product
     */
    private $product;

    /**
     * Set product
     *
     * @param ProductInterface $product
     * @return Model
     */
    public function setProduct(ProductInterface $product = null)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return ProductInterface
     */
    public function getProduct()
    {
        return $this->product;
    }
}
