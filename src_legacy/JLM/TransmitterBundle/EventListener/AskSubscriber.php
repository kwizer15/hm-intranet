<?php

namespace JLM\TransmitterBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CommerceBundle\Event\QuoteVariantEvent;
use JLM\TransmitterBundle\Factory\AskFactory;
use JLM\TransmitterBundle\Builder\VariantAskBuilder;

class AskSubscriber implements EventSubscriberInterface
{
   
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * Constructor
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            JLMCommerceEvents::QUOTEVARIANT_GIVEN => 'createAskFromQuote',
        ];
    }
    
    /**
     * @param QuoteVariantEvent $event
     */
    public function createAskFromQuote(QuoteVariantEvent $event)
    {
        $entity = $event->getQuoteVariant();
        if ($entity->hasLineType('TRANSMITTER')) {
            $ask = AskFactory::create(new VariantAskBuilder($entity));
            $this->objectManager->persist($ask);
            $this->objectManager->flush();
        }
    }
}
