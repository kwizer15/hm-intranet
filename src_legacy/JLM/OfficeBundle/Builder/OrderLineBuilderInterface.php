<?php

namespace JLM\OfficeBundle\Builder;

interface OrderLineBuilderInterface
{
    public function create();
    
    public function buildReference();
    
    public function buildQuantity();
    
    public function buildDesignation();
    
    public function getLine();
}
