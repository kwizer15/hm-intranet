<?php

namespace JLM\DailyBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Form\DataTransformer\ObjectToStringAutocreateTransformer;
use JLM\DailyBundle\Entity\PartFamily;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;

class PartFamilyType extends AbstractType
{
   
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $transformer = new ObjectToStringAutocreateTransformer($this->objectManager, PartFamily::class, 'name');
        $builder->addModelTransformer($transformer);
    }
    
    public function getParent()
    {
        return TextType::class;
    }
    
    public function getBlockPrefix(): string
    {
        return 'jlm_daily_partfamilytype';
    }
    
    /**
     * Ajoute l'option source
     *
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setOptional(['source']);
    }
    
    /**
     * Passe la source de données à la vue
     *
     * @param FormView $view
     * @param FormInterface $form
     * @param array $options
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $parts = $this->objectManager->getRepository(PartFamily::class)->findAll();
        $view->vars['source'] = $parts;
    }
}
