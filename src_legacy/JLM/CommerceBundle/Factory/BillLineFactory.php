<?php

namespace JLM\CommerceBundle\Factory;

use JLM\CommerceBundle\Builder\BillLineBuilderInterface;

class BillLineFactory
{
    
    public static function create(BillLineBuilderInterface $builder)
    {
        $builder->create();
        $builder->buildProduct();
        $builder->buildQuantity();
        $builder->buildPrice();
    
        return $builder->getLine();
    }
}
