<?php

namespace JLM\ModelBundle\Form\Type;

use JLM\ContactBundle\Form\Type\PersonSelectType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ModelBundle\Entity\SiteContact;

class SiteContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('site', SiteSelectType::class, ['label'=>'Affaire','attr'=>['class'=>'input-xxlarge']])
            ->add('person', PersonSelectType::class, ['label'=>'Contact'])
            ->add('role', null, ['label'=>'Rôle du contact'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => SiteContact::class
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_modelbundle_sitecontacttype';
    }
}
