<?php

namespace JLM\DailyBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\QuoteVariant;
use JLM\DailyBundle\Entity\Intervention;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class InterventionToIntTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * Transforms an object (quote) to an integer (id).
     *
     * @param  QuoteVariant|null $entity
     * @return int
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return '';
        }
        return $entity->getId();
    }
    
    /**
     * Transforms an integer (number) to an object (quote).
     *
     * @param  int $id
     * @return QuoteVariant|null
     * @throws TransformationFailedException if object (quote) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
        
        $entity = $this->objectManager
            ->getRepository(Intervention::class)
            ->find($id)
        ;

        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'An intervention with id "%s" does not exist!',
                $id
            ));
        }
    
        return $entity;
    }
}
