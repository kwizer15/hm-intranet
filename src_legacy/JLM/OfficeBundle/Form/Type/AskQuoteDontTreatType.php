<?php
namespace JLM\OfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\OfficeBundle\Entity\AskQuote;

class AskQuoteDontTreatType extends AskDontTreatType
{
   
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => AskQuote::class
        ]);
    }
    
    public function getBlockPrefix(): string
    {
        return 'askquotedonttreat';
    }
}
