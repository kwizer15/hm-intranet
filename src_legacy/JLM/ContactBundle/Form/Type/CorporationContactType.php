<?php

namespace JLM\ContactBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ContactBundle\Entity\CorporationContact;

class CorporationContactType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('person', PersonSelectType::class, ['label'=>'Contact'])
            ->add('corporation', CorporationSelectType::class, ['label'=>'Groupement'])
            ->add('position', null, ['label'=>'Rôle'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_contact_corporation_contact';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => CorporationContact::class,
                'label' => 'Contacts de groupement',
            ]);
    }
}
