<?php

namespace JLM\CondominiumBundle\Model;

interface PropertyInterface
{
    /**
     * Get the address
     * @return AddressInterface
     */
    public function getAddress();
    
    /**
     * Get the manager
     * @return AdministratorInterface
     */
    public function getAdministrator();
}
