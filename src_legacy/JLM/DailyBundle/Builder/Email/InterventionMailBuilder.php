<?php

namespace JLM\DailyBundle\Builder\Email;

use JLM\CoreBundle\Builder\MailBuilderAbstract;
use JLM\DailyBundle\Model\InterventionInterface;

abstract class InterventionMailBuilder extends MailBuilderAbstract
{
    private $intervention;

    public function __construct(InterventionInterface $intervention)
    {
        $this->intervention = $intervention;
    }

    public function getIntervention()
    {
        return $this->intervention;
    }

    public function buildFrom()
    {
        $this->addFrom('secretariat@jlm-entreprise.fr', 'Secretariat (JLM Entreprise)');
    }

    public function buildTo()
    {
        $managerContacts = $this->intervention->getManagerContacts();
        foreach ($managerContacts as $contact) {
            //$this->addTo($contact);
            $this->addTo($contact->getEmail(), $contact->getName());
        }
    }

    public function buildCc()
    {
        $administratorContacts = $this->intervention->getAdministratorContacts();
        foreach ($administratorContacts as $contact) {
            //$this->addCc($contact);
            $this->addCc($contact->getEmail(), $contact->getName());
        }
    }

    public function buildBcc()
    {
        $this->addBcc('secretariat@jlm-entreprise.fr', 'Secretariat (JLM Entreprise)');
    }


    public function buildAttachements()
    {
    }

    protected function getSignature()
    {
        $str = PHP_EOL.PHP_EOL;
        $date = new \DateTime;
        if ($date->format('m') == '01') {
            $str .= 'Toute l\'équipe de JLM Entreprise vous souhaite une bonne année '.$date->format('Y').'.'.PHP_EOL;
        }
        $str .= 'Cordialement'
                .parent::getSignature();
        return $str;
    }
}
