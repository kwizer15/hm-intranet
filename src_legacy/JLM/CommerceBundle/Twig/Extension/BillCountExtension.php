<?php

namespace JLM\CommerceBundle\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\Bill;
use JLM\DailyBundle\Entity\Intervention;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class BillCountExtension extends AbstractExtension implements GlobalsInterface
{
    private $objectManager;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public function getName()
    {
        return 'billcount_extension';
    }
    
    public function getGlobals()
    {
        $repo = $this->objectManager->getRepository(Bill::class);
        
        return ['billcount' => [
            'todo' => $this->objectManager->getRepository(Intervention::class)->getCountToBilled(),
            'all' => $repo->getTotal(),
            'input' => $repo->getCount(0),
            'send' => $repo->getCount(1),
            'payed' => $repo->getCount(2),
            'canceled' => $repo->getCount(-1),
        ]];
    }
}
