<?php

namespace JLM\DailyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use JLM\DailyBundle\Entity\Maintenance;
use JLM\DailyBundle\Entity\Ride;
use JLM\DailyBundle\Entity\ShiftTechnician;
use JLM\DailyBundle\Form\Type\AddTechnicianType;
use JLM\DailyBundle\Form\Type\MaintenanceCloseType;
use JLM\ModelBundle\Entity\Door;
use JLM\CoreBundle\Factory\MailFactory;
use JLM\ModelBundle\JLMModelEvents;
use JLM\ModelBundle\Event\DoorEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JLM\CoreBundle\Form\Type\MailType;
use JLM\CoreBundle\Builder\MailSwiftMailBuilder;
use JLM\DailyBundle\Builder\Email\MaintenancePlannedMailBuilder;
use JLM\DailyBundle\Builder\Email\MaintenanceOnSiteMailBuilder;
use JLM\DailyBundle\Builder\Email\MaintenanceEndMailBuilder;
use JLM\DailyBundle\Builder\Email\MaintenanceReportMailBuilder;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class MaintenanceController extends AbstractInterventionController
{
    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->repository = $entityManager->getRepository(Maintenance::class);
    }

    /**
     * Finds and displays a InterventionPlanned entity.
     */
    public function listAction(Paginator $paginator, Request $request)
    {
        return $request->isXmlHttpRequest()
            ? new JsonResponse([
                'entities' => $this->repository->getArray($request->get('q', ''), $request->get('page_limit', 10))
            ])
            : $this->render(
                '@JLMDaily/Maintenance/list.html.twig',
                $paginator->pagination($request, $this->repository, 'getCountOpened', 'getOpened', 'maintenance_list', [])
            );
    }

    /**
     * Finds and displays a Maintenance entity.
     */
    public function showAction(Maintenance $entity)
    {
        return $this->render('@JLMDaily/Maintenance/show.html.twig', $this->show($entity));
    }

    /**
     * Close an existing Fixing entity.
     */
    public function closeAction(Maintenance $entity)
    {
        $form = $this->createForm(MaintenanceCloseType::class, $entity);

        return $this->render('@JLMDaily/Maintenance/close.html.twig', [
                'entity'      => $entity,
                'form'   => $form->createView(),
        ]);
    }

    /**
     * Close an existing Maintenance entity.
     */
    public function closeupdateAction(Request $request, Maintenance $entity)
    {
        $form = $this->createForm(MaintenanceCloseType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setClose(new \DateTime);
            $entity->setMustBeBilled(false);
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            return ($this->redirectToRoute('maintenance_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMDaily/Maintenance/closeupdate.html.twig', [
            'entity'      => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a InterventionPlanned entity.
     */
    public function emailAction(
        \Swift_Mailer $mailer,
        EventDispatcherInterface $eventDispatcher,
        Request $request,
        Maintenance $entity,
        $step
    ) {
        $steps = [
                'planned' => MaintenancePlannedMailBuilder::class,
                'onsite' => MaintenanceOnSiteMailBuilder::class,
                'end' => MaintenanceEndMailBuilder::class,
                'report' => MaintenanceReportMailBuilder::class,
        ];
        $class = (array_key_exists($step, $steps)) ? $steps[$step] : null;
        if (null === $class) {
            throw new NotFoundHttpException('Page inexistante');
        }
        $mail = MailFactory::create(new $class($entity));
        $editForm = $this->createForm(MailType::class, $mail);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $mailer->send(MailFactory::create(new MailSwiftMailBuilder($editForm->getData())));
            $eventDispatcher->dispatch(
                JLMModelEvents::DOOR_SENDMAIL,
                new DoorEvent($entity->getDoor(), $request)
            );

            return $this->redirectToRoute('maintenance_show', ['id' => $entity->getId()]);
        }
        return $this->render('@JLMDaily/Maintenance/email.html.twig', [
                'entity' => $entity,
                'form' => $editForm->createView(),
                'step' => $step,
        ]);
    }

    /**
     * Creation des entretiens a faire
     */
    public function scanAction()
    {
        $date = new \DateTime;
        $date->sub(new \DateInterval('P6M'));
        $doors = $this->entityManager->getRepository(Door::class)->findAll();
        $count = 0;
        $removed = 0;
        foreach ($doors as $door) {
            $maint = $door->getNextMaintenance();
            $contract = $door->getActualContract();
            if ($contract !== null) {
                if ($door->getLastMaintenance() < $date
                        && $maint === null
                        && $door->getCountMaintenance() < 2) {
                    $main = new Maintenance;
                    $main->setCreation(new \DateTime);
                    $main->setPlace($door.'');
                    $main->setReason('Visite d\'entretien');
                    $main->setContract($door->getActualContract());
                    $main->setDoor($door);
                    $main->setPriority(5);
                    $main->setPublished(true);
                    $this->entityManager->persist($main);
                    $count++;
                } elseif ($door->getLastMaintenance() > $limi && $maint !== null) {
                    $limi = \DateTime::createFromFormat('Y-m-d H:i:s', '2017-01-01 00:00:00');
                    $this->entityManager->remove($maint);
                    $removed++;
                }
            } elseif ($contract === null && $maint !== null) {
                $shifts = $maint->getShiftTechnicians();
                if (count($shifts) > 0) {
                    $maint->setClosed();
                    $maint->setReport('Cloturé pour rupture de contrat');
                } else {
                    $this->entityManager->remove($maint);
                }
                $removed++;
            }
        }
        $this->entityManager->flush();

        return $this->render('@JLMDaily/Maintenance/scan.html.twig', ['count' => $count,'removed' => $removed]);
    }

    /**
     * Cherche les entretiens les plus proche d'une adresse
     */
    public function neighborAction(FormFactoryInterface $formFactory, Door $door)
    {
        // Choper les entretiens à faire
        $repo = $this->entityManager->getRepository(Maintenance::class);
        $maints = $repo->getOpened();
        $repo = $this->entityManager->getRepository(Ride::class);
        $baseUrl = 'http://maps.googleapis.com/maps/api/distancematrix/json?sensor=false&language=fr-FR&origins='
            .$door->getCoordinates().'&destinations=';
        foreach ($maints as $maint) {
            $dest = $maint->getDoor();
            if (!$repo->hasRide($door, $dest)) {
                $url = $baseUrl.$dest->getCoordinates();
                $string = file_get_contents($url);
                $json = json_decode($string);
                if (isset($json->rows[0]->elements[0]->duration->value, $json->rows[0]->elements[0]->duration->value)
                    && $json->status == 'OK'
                ) {
                    $ride = new Ride;
                    $ride->setDeparture($door);
                    $ride->setDestination($dest);
                    $ride->setDuration($json->rows[0]->elements[0]->duration->value);
                    $ride->setDistance($json->rows[0]->elements[0]->distance->value);
                    $this->entityManager->persist($ride);
                }
            }
        }
        $this->entityManager->flush();
        $entities = $repo->getMaintenanceNeighbor($door, 30);
        $forms = [];
        foreach ($entities as $entity) {
            $shift = new ShiftTechnician();
            $shift->setBegin(new \DateTime);
            $forms[] = $formFactory->createNamed(
                'shiftTechNew'.$entity->getDestination()->getNextMaintenance()->getId(),
                AddTechnicianType::class,
                $shift
            )->createView();
        }

        return $this->render('@JLMDaily/Maintenance/neighbor.html.twig', [
                'door'=>$door,
                'entities' => $entities,
                'forms_addTech' => $forms,
        ]);
    }
}
