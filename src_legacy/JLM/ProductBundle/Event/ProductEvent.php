<?php

namespace JLM\ProductBundle\Event;

use Symfony\Component\HttpFoundation\Request;
use JLM\ProductBundle\Model\ProductInterface;
use Symfony\Component\EventDispatcher\Event;

class ProductEvent extends Event
{
    /**
     * @var ProductInterface
     */
    private $product;

    /**
     * @param ProductInterface $product
     */
    public function __construct(ProductInterface $product)
    {
        $this->product = $product;
    }
    
    public function getProduct()
    {
        return $this->product;
    }
}
