<?php

namespace JLM\ModelBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use JLM\ModelBundle\Form\DataTransformer\SiteToStringTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SiteSelectType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $transformer = new SiteToStringTransformer($this->objectManager);
        $builder->addModelTransformer($transformer);
    }

    public function getParent()
    {
        return TextareaType::class;
    }
    
    public function getBlockPrefix(): string
    {
        return 'site_select';
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'invalid_message' => 'The selected site does not exist',
        ]);
    }
}
