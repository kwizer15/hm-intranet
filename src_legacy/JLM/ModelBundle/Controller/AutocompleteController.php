<?php

namespace JLM\ModelBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\City;
use JLM\ContractBundle\Entity\Contract;
use JLM\ModelBundle\Entity\Door;
use JLM\ModelBundle\Entity\Site;
use JLM\ModelBundle\Entity\Trustee;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AutocompleteController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     *
     */
    public function cityAction(Request $request)
    {
        $query = $request->request->get('term');
        $results = $this->entityManager->getRepository(City::class)->searchResult($query);
        $json = json_encode($results);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($json);

        return $response;
    }

    /**
     *
     */
    public function trusteeAction(Request $request)
    {
        $query = $request->request->get('term');
        $results = $this->entityManager->getRepository(Trustee::class)->searchResult($query);
        $json = json_encode($results);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($json);

        return $response;
    }

    /**
     *
     */
    public function siteAction(Request $request)
    {
        $query = $request->request->get('term');
        $results = $this->entityManager->getRepository(Site::class)->searchResult($query);
        $json = json_encode($results);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($json);

        return $response;
    }

    /**
     *
     */
    public function contractAction(Request $request)
    {
        $query = $request->request->get('term');
        $results = $this->entityManager->getRepository(Contract::class)->searchResult($query);
        $json = json_encode($results);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($json);

        return $response;
    }

    /**
     *
     */
    public function indexAction(Request $request)
    {
        $query = $request->request->get('term');
        $repository = $request->request->get('repository');
        $action = $request->request->get('action');
        $action = empty($action) ? 'Result' : $action;
        $action = 'search' . $action;
        $results = $this->entityManager->getRepository($repository)->$action($query);
        $json = json_encode($results);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($json);

        return $response;
    }

    /**
     * @todo Voir si cette action est utile car pas de "Action" dans le nom de la fonction quand j'ai réécrit le
     *     routage en yml
     */
    public function doorsiteAction(Request $request)
    {
        $id = $request->request->get('id_site');
        $site = $this->entityManager->getRepository(Site::class)->find($id);
        $results = $this->entityManager->getRepository(Door::class)->findBy(['site' => $site]);
        $doors = [];
        foreach ($results as $result) {
            $doors[] = [
                'id' => $result->getId(),
                'string' => $result->getType() . ' - ' . $result->getLocation() . ' / ' . $result->getStreet(),
            ];
        }
        $json = json_encode($doors);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($json);

        return $response;
    }
}
