<?php

namespace JLM\TransmitterBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\TransmitterBundle\Entity\UserGroupRepository;
use JLM\TransmitterBundle\Entity\Model;
use JLM\TransmitterBundle\Entity\UserGroup;
use JLM\TransmitterBundle\Entity\Series;

class SeriesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $id = $options['id'];
        $builder
            ->add('attribution', AttributionHiddenType::class)
            ->add(
                'userGroup',
                EntityType::class,
                [
                'class' => UserGroup::class,
                'label' => 'Groupe utilisateur',
                'query_builder' => function (UserGroupRepository $er) use ($id) {
                    return $er->getFromSite($id);
                },
                'placeholder' => 'Choisissez...',
                ]
            )
            ->add('model', EntityType::class, [
                'class' => Model::class,
                'label' => 'Type d\'émetteurs',
                'placeholder' => 'Choisissez...',
            ])
            ->add('quantity', null, [
                'label' => 'Nombre d\'émetteurs',
                'attr' => ['class' => 'input-mini', 'maxlength' => 3],
            ])
            ->add('first', null, ['label' => 'Premier numéro', 'attr' => ['class' => 'input-small', 'maxlength' => 6]])
            ->add('last', null, ['label' => 'Dernier numéro', 'attr' => ['class' => 'input-small', 'maxlength' => 6]])
            ->add('guarantee', null, [
                'label' => 'Garantie', 'attr' => [
                    'placeholder' => 'MMAA',
                    'class' => 'input-mini',
                    'maxlength' => 4,
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Series::class,
            'attr' => ['class' => 'transmitter_series'],
            'id' => null,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_transmitterbundle_seriestype';
    }
}
