<?php

declare(strict_types=1);

namespace HMFermeture\Domain\Devis\Repository\Exception;

use HMFermeture\Domain\Common\Repository\Exception\NoResultException;

class DemandeDeDevisNotFoundException extends NoResultException
{

}
