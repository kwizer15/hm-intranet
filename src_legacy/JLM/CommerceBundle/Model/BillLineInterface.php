<?php

namespace JLM\CommerceBundle\Model;

interface BillLineInterface
{
    public function getQuantity();
    
    public function getUnitPrice();
}
