<?php

namespace JLM\ContactBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\Contact;
use JLM\ContactBundle\Manager\ContactManager;
use JLM\CoreBundle\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class ContactController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var ContactManager
     */
    private $manager;

    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(
        ContactManager $manager,
        EntityManagerInterface $entityManager
    ) {
        $this->manager = $manager;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Contact::class);
    }

    /**
     * Edit or add a contact
     */
    public function editAction(Request $request, $id)
    {
        if (in_array($id, ['person','company','association'])) {
            $type = $id;
            $id = null;
            $formName = 'new';
            $entity = null;
        } else {
            $entity = $this->repository->find($id);
            $formName = 'edit';
            $type = $entity->getType();
        }
        $form = $this->manager->createForm($request, $formName, ['type' => $type, 'entity' => $entity]);
        $process = $this->manager->getHandler($form, $request, $entity)->process();

        if ($request->isXmlHttpRequest()) {
            return $process
                ? new JsonResponse(['ok'=>true])
                : $this->render(
                    '@JLMContact/Contact/modal_new.html.twig',
                    ['form' => $form->createView()]
                );
        }

        return $process
            ? $this->redirectToRoute('jlm_contact_contact_show', ['id' => $form->getData()->getId()])
            : $this->render('@JLMContact/Contact/new.html.twig', ['form' => $form->createView()])
        ;
    }

    /**
     * @param Request $request
     *
     * @return mixed
     */
    public function listAction(Paginator $paginator, Request $request)
    {
        $ajax = $request->isXmlHttpRequest();

        return $ajax || $request->get('format') == 'json'
            ? new JsonResponse([
                'contacts' => $this->repository->getArray($request->get('q', ''), $request->get('page_limit', 10))
            ])
            : $this->render(
                '@JLMContact/Contact/list.html.twig',
                $paginator->pagination($request, $this->repository, 'getCountAll', 'getAll', 'jlm_contact_contact', [])
            );
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return mixed
     */
    public function showAction(Request $request, $id)
    {
        $entity = $this->repository->find($id);

        return $request->isXmlHttpRequest()
            ? new JsonResponse($this->repository->getByIdToArray($id))
            : $this->render(
                '@JLMContact/Contact/show_' . $entity->getType() . '.html.twig',
                ['entity'=>$entity]
            );
    }

    /**
     * @param Request $request
     * @param $id
     *
     * @return mixed
     */
    public function unactiveAction(Request $request, $id)
    {
        $entity = $this->repository->find($id);
        $entity->setActive(false);

        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        $this->addFlash('notice', 'Contact ' . $entity->getName() . ' désactivé');

        return $this->redirect($request->headers->get('referer'));
    }
}
