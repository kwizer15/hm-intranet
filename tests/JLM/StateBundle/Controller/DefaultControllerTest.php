<?php

namespace Tests\JLM\StateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DomCrawler\Crawler;

class DefaultControllerTest extends WebTestCase
{
    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->client = static::createClient();
        $this->client->followRedirects();
    }
    
    public function getUrls()
    {
        return [
            ['GET', '/state/technicians', 200],
            ['GET', '/state/technicians/2013', 200],
            ['GET', '/state/maintenance', 200],
            ['GET', '/state/top', 200],
            ['GET', '/state/contracts', 200],
            ['GET', '/state/quotes/2015', 200],
            ['GET', '/state/transmitters', 200],
        ];
    }
    
    /**
     * @dataProvider getUrls
     * @param string $method
     * @param string $url
     */
    public function testUrlIsSuccessful($method, $url, $status)
    {
        $crawler = $this->client->request(
            $method,
            $url
        );
        $crawler = $this->login($crawler);
        $this->assertSame($status, $this->client->getResponse()->getStatusCode());
    }
    
    /**
     * Log the user
     * @param Crawler $crawler
     */
    private function login(Crawler $crawler)
    {
        $form = $crawler->selectButton('S\'identifier')->form();
    
        // définit certaines valeurs
        $form['username'] = 'kwizer';
        $form['password'] = 'sslover';
        return $this->client->submit($form);
    }
}
