<?php

namespace JLM\TransmitterBundle\Builder;

interface AskBuilderInterface
{
    /**
     * @return AskInterface
     */
    public function getAsk();

    public function create();
    
    public function buildCreation();
    
    public function buildTrustee();
    
    public function buildSite();
    
    public function buildMethod();
    
    public function buildMaturity();
    
    public function buildPerson();
    
    public function buildAsk();
}
