<?php

namespace JLM\OfficeBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use JLM\DailyBundle\Entity\Intervention;

/**
 * @Route("/tocontact")
 * @IsGranted("ROLE_OFFICE")
 */
class ContactController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="tocontact")
     */
    public function indexAction()
    {
        $list = $this->entityManager->getRepository(Intervention::class)->getToContact();

        return $this->render('@JLMOffice/Contact/index.html.twig', ['entities'=>$list]);
    }
    
    /**
     * @Route("/tocontact/contacted/{id}", name="tocontact_contacted")
     */
    public function contactedAction(Intervention $entity)
    {
        $entity->setContactCustomer(true);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('tocontact'));
    }
}
