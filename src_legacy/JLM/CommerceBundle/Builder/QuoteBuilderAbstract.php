<?php

namespace JLM\CommerceBundle\Builder;

use JLM\CommerceBundle\Entity\Quote;

abstract class QuoteBuilderAbstract implements QuoteBuilderInterface
{
    /**
     * @var Quote
     */
    protected $quote;
    
    /**
     * @var array
     */
    protected $options;
    
    /**
     * {@inheritdoc}
     */
    public function getQuote()
    {
        return $this->quote;
    }
    
    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->quote = new Quote;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildCreation()
    {
        $this->quote->setCreation(new \DateTime);
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildIntro()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildDetails()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildConditions()
    {
        foreach ($this->options as $key => $value) {
            switch ($key) {
                case 'vatTransmitter':
                    $this->quote->setVatTransmitter($value);
                    break;
                case 'vat':
                    $this->quote->setVat($value);
                    break;
            }
        }
    }
    
    public function __construct($options = [])
    {
        $this->options = $options;
    }
    
    protected function getOptions()
    {
        return $this->options;
    }
    
    protected function getOption($key)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }
        
        return null;
    }
}
