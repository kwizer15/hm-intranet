<?php

namespace JLM\OfficeBundle\Controller;

use JLM\DefaultBundle\Controller\PaginableController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Ask controller.
 */
abstract class AskController extends PaginableController
{

    public function indexAction($page = 1)
    {
        return $this->pagination($this->getRepositoryName(), 'All', $page, 10);
    }
    
    public function listtreatedAction($page = 1)
    {
        return $this->pagination($this->getRepositoryName(), 'Treated', $page, 10);
    }
    
    public function listuntreatedAction($page = 1)
    {
        return $this->pagination($this->getRepositoryName(), 'Untreated', $page, 10);
    }

    public function canceldonttreatAction(Request $request, $id)
    {
        $entity = $this->getEntity($id);
        $entity->setDontTreat();
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $this->redirect($request->headers->get('referer'));
    }
    
    /**
     * @return Repository
     */
    abstract protected function getRepositoryName();
    
    /**
     * @return Entity
     */
    protected function getEntity($id)
    {
        $entity = $this->entityManager->getRepository($this->getRepositoryName())->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find '.$this->getRepositoryName().' entity.');
        }

        return $entity;
    }
}
