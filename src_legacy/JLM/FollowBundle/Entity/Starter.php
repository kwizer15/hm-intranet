<?php

namespace JLM\FollowBundle\Entity;

use JLM\FollowBundle\Model\StarterInterface;

abstract class Starter implements StarterInterface
{
    private $id;
    
    private $work;
    
    public function getId()
    {
        return $this->id;
    }
    
    final public function getWork()
    {
        $this->work = $this->getMyWork();
        
        return $this->work;
    }

    abstract protected function getMyWork();
}
