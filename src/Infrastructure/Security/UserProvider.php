<?php

namespace HMFermeture\Infrastructure\Security;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use HMFermeture\Infrastructure\Entity\User;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use function get_class;

class UserProvider implements UserProviderInterface
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @inheritDoc
     */
    public function loadUserByUsername($username)
    {
        $statement = $this->connection->prepare(<<<SQL
SELECT username, password, salt, roles
FROM users
WHERE username = :username
SQL
        );

        if (false === $statement->execute(['username' => $username])) {
            throw new UsernameNotFoundException('Unable to execute user query from database.');
        }

        $user = $statement->fetch(FetchMode::ASSOCIATIVE);
        if (false === $user) {
            throw new UsernameNotFoundException('Unable to find user from database.');
        }

        return new User(
            $user['username'],
            $user['password'],
            $user['salt'],
            unserialize($user['roles'], ['allowed_class' => false])
        );
    }

    /**
     * @inheritDoc
     */
    public function refreshUser(UserInterface $user)
    {
        $userClass = get_class($user);
        if (false === $this->supportsClass($userClass)) {
            throw new UnsupportedUserException("{$userClass} not supported.");
        }

        return $user;
    }

    /**
     * @inheritDoc
     */
    public function supportsClass($class): bool
    {
        return User::class === $class;
    }
}
