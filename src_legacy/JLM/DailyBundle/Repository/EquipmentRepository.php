<?php

namespace JLM\DailyBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * EquipmentRepository
 * @author Emmanuel Bernaszuk <emmanuel.bernaszuk@kw12er.com>
 */
class EquipmentRepository extends EntityRepository
{
   
    public function getCountWithDate(\DateTime $date1, \DateTime $date2)
    {
        $queryBuilder = $this->createQueryBuilder('i')
        ->select('COUNT(i)')
        ->leftJoin('i.shiftTechnicians', 't')
        ->where('t.begin BETWEEN ?1 AND ?2')
        ->setParameter(1, $date1)
        ->setParameter(2, $date2);
        return $queryBuilder->getQuery()->getSingleScalarResult();
    }
    
    public function getWithDate(\DateTime $date1, \DateTime $date2)
    {
        $queryBuilder = $this->createQueryBuilder('i')
            ->leftJoin('i.shiftTechnicians', 't')
            ->where('t.begin BETWEEN ?1 AND ?2')
            ->addOrderBy('t.begin', 'asc')
            ->addOrderBy('t.creation', 'asc')
            ->addOrderBy('i.creation', 'asc')
            ->setParameter(1, $date1)
            ->setParameter(2, $date2);
        return $queryBuilder->getQuery()->getResult();
    }
    
    public function getCountToday()
    {
        $today = new \DateTime;
        $todaystring =  $today->format('Y-m-d');
        // Interventions en cours
        $queryBuilder = $this->createQueryBuilder('i')
            ->select('COUNT(i)')
            ->leftJoin('i.shiftTechnicians', 't')
            ->where('t.begin = ?1')
            ->setParameter(1, $todaystring)
            ;
        return $queryBuilder->getQuery()->getSingleScalarResult();
    }
    
    public function getToday()
    {
        $today = new \DateTime;
        $todaystring =  $today->format('Y-m-d');
        $queryBuilder = $this->createQueryBuilder('i')
            ->leftJoin('i.shiftTechnicians', 't')
            ->where('t.begin = ?1')
            ->orderBy('i.creation', 'asc')
            ->setParameter(1, $todaystring)
            ;
            
        return $queryBuilder->getQuery()->getResult();
    }
}
