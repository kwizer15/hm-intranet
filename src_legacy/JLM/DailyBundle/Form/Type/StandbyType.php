<?php

namespace JLM\DailyBundle\Form\Type;

use JLM\ModelBundle\Form\Type\DatepickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\DailyBundle\Entity\Standby;

class StandbyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('technician', null, ['label'=>'Technicien'])
            ->add('begin', DatepickerType::class, ['label'=>'Début'])
            ->add('end', DatepickerType::class, ['label'=>'Fin'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Standby::class
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'standbytype';
    }
}
