<?php

namespace JLM\DailyBundle\Builder;

interface WorkBuilderInterface
{
    /**
     * @return BillInterface
     */
    public function getWork();

    public function create();
    
    public function buildCreation();
    
    public function buildBusiness();
    
    public function buildReason();
    
    public function buildContact();

    public function buildPriority();
    
    public function buildOrder();
    
    public function buildLink();
}
