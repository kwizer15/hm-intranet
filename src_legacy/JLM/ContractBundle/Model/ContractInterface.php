<?php

namespace JLM\ContractBundle\Model;

use JLM\CondominiumBundle\Model\ManagerInterface;

interface ContractInterface
{
    /**
     * Get number
     *
     * @return string
     */
    public function getNumber();
    
    /**
     * Get door
     *
     * @return JLM\ModelBundle\Entity\Door
     */
    public function getDoor();
    
    /**
     * Get fee
     *
     * @return float
     */
    public function getFee();
    
    /**
     * Get manager
     *
     * @return ManagerInterface
     */
    public function getManager();
    
    /**
     * To String
     * @return string
     */
    public function __toString();
}
