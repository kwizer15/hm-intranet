<?php

namespace JLM\FrontBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\FrontBundle\Form\Type\ContactType;
use JLM\FrontBundle\Mailer\TwigSwiftMailer;
use JLM\ModelBundle\Entity\Door;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function contactAction(TwigSwiftMailer $mailer, Request $request)
    {
        $form = $this->createForm(ContactType::class, null, [
                'action' => $this->generateUrl('jlm_front_contact'),
                'method' => 'POST',
        ]);
        $form->add('submit', SubmitType::class);
        $form->handleRequest($request);
         
        if ($form->isSubmitted() && $form->isValid()) {
            $mailer->sendContactEmailMessage($form->getData());
            $mailer->sendConfirmContactEmailMessage($form->getData());
            
            return $this->render('@JLMFront/Default/contact_confirm.html.twig');
        }
        
        return $this->render('@JLMFront/Default/contact.html.twig', [
                'form' => $form->createView(),
        ]);
    }
    
    public function installationAction($code)
    {
        $entity = $this->entityManager->getRepository(Door::class)->getByCode($code);
        if ($entity === null) {
            throw $this->createNotFoundException('Cette installation n\'existe pas');
        }
    
        return $this->render('@JLMFront/Default/installation.html.twig', ['door'=>$entity]);
    }
}
