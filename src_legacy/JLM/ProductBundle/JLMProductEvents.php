<?php

namespace JLM\ProductBundle;

final class JLMProductEvents
{
    const PRODUCT_CREATE = "jlm_product.product_create";
}
