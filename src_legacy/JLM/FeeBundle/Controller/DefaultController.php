<?php

namespace JLM\FeeBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContractBundle\Entity\Contract;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\FeeBundle\Entity\Fee;

/**
 * Route("/feedefault")
 */
class DefaultController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * TODO: faire une commande
     *
     * Action temporaire
     *
     * Création à la volée des redevances
     *
     * Route("/autocreate")
     * Template()
     * @deprecated
     */
    public function autocreatefee()
    {
        $contracts = $this->entityManager->getRepository(Contract::class)->findAll();
        
        foreach ($contracts as $contract) {
            if ($contract->getInProgress()) {
                $fee = new Fee();
                $fee->addContract($contract);
                $fee->setTrustee($contract->getTrustee());
                $fee->setAddress($contract->getDoor()->getSite()->getAddress()->toString());
                $fee->setPrelabel($contract->getDoor()->getSite()->getBillingPrelabel());
                $fee->setVat($contract->getDoor()->getSite()->getVat());
                $this->entityManager->persist($fee);
            }
        }
        $this->entityManager->flush();
        return [];
    }
}
