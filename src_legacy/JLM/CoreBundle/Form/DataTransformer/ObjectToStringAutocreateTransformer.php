<?php

namespace JLM\CoreBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\DailyBundle\Entity\PartFamily;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ObjectToStringAutocreateTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * @var string
     */
    private $entityClass;
    
    /**
     * @var string
     */
    private $parameterName;

    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager, $entityClass, $parameterName = 'name')
    {
        $this->objectManager = $objectManager;
        $this->entityClass = $entityClass;
        $this->parameterName = $parameterName;
    }
    
    /**
     * Transforms an object (product) to a string (name).
     *
     * @param  PartFamily|null $entity
     * @return string
     */
    public function transform($entity)
    {
        $class = $this->entityClass;
        if ($entity instanceof $class) {
            $getter = $this->getUniqueGetFunction();
            
            if (method_exists($entity, $getter)) {
                return $entity->$getter();
            }
        }
        
        return '';
    }
    
    /**
     * Transforms a string (name) to an object (product).
     *
     * @param  string $name
     * @return PartFamily|null
     * @throws TransformationFailedException if object (trustee) is not found.
     */
    public function reverseTransform($name)
    {
        if (!$name) {
            return null;
        }
    
        $param = $this->parameterName;
        $class = $this->entityClass;
        $entity = $this->objectManager->getRepository($class)->findOneBy([$param => $name]);
        if (null === $entity) {
            $entity = new $class();
            $setter = $this->getUniqueSetFunction();
            $entity->$setter($name);
            $entity = $this->initEntity($entity);
            $this->objectManager->persist($entity);
        }

        return $entity;
    }
    
    /**
     * Get getter method name
     * @return string
     */
    private function getUniqueGetFunction()
    {
        return 'get'.ucwords($this->parameterName);
    }
    
    /**
     * Get setter method name
     * @return string
     */
    private function getUniqueSetFunction()
    {
        return 'set'.ucwords($this->parameterName);
    }
    
    protected function initEntity($entity)
    {
        return $entity;
    }
}
