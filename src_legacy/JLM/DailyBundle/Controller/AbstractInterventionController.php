<?php

namespace JLM\DailyBundle\Controller;

use JLM\CommerceBundle\Entity\Quote;
use JLM\DailyBundle\Entity\Intervention;
use JLM\DailyBundle\Form\Type\ExternalBillType;
use JLM\DailyBundle\Form\Type\InterventionCancelType;
use JLM\DefaultBundle\Controller\PaginableController;
use Symfony\Component\Form\FormFactoryInterface;

abstract class AbstractInterventionController extends PaginableController
{
    /**
     * @param Intervention $entity
     * @return array
     */
    public function show(FormFactoryInterface $formFactory, Intervention $entity)
    {
        $form_externalbill = $formFactory->createNamed(
            'externalBill'.$entity->getId(),
            ExternalBillType::class,
            $entity
        );
        $form_cancel = $this->createForm(InterventionCancelType::class, $entity);
        
        return [
            'entity' => $entity,
            'form_externalbill' => $form_externalbill->createView(),
            'form_cancel' => $form_cancel->createView(),
            'quotes' => $this->entityManager->getRepository(Quote::class)->getByDoor($entity->getDoor()),
        ];
    }
}
