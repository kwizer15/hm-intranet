import './style/bootstrap.css'
import './style/bootstrap-responsive.css'
import 'select2/select2.css'
import './style/datepicker.css'
import './style/style.css'

import 'jquery'
import './vendor/jquery-ui/jquery-ui'
import './vendor/bootstrap/bootstrap'
import number_format from './script/core/number_format'
import './vendor/jquery-ui/i18n/jquery.ui.datepicker-fr'
import './script/core/modalform'
import './vendor/highcharts/highstock'
import './script/core/highcharts_init'
import 'select2/select2'
import 'select2/select2_locale_fr'
import './script/contact/modal_contact'

import './script/commerce/bill'
import './script/commerce/quote'
import './script/model/fee'
import './script/office/ask'
import './script/office/askquote'
import './script/office/coding'
import './script/office/order'
import './script/product/product'
import './script/transmitter/transmitter'
import './script/transmitter/transmitter_series'

(function() {
    $(".input-money").on('change', function () {
        $(this).val(
            number_format(
                parseFloat(
                    $(this).val()
                        .replace(',', '.')
                        .replace(/[\s]{1,}/g, "")
                ), 2, ',', ' '
            )
        );
        return this;
    });
})();
