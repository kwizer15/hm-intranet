<?php

namespace JLM\ProductBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Manager\BaseManager as Manager;
use JLM\ProductBundle\Entity\Stock;
use JLM\ProductBundle\Form\Type\StockType;
use JLM\ProductBundle\Form\Type\InventoryType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\RouterInterface;

class StockManager extends Manager
{
    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        FormFactoryInterface $formFactory
    ) {
        parent::__construct(
            Stock::class,
            $entityManager,
            $router,
            $formFactory
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function getFormParam($name, $options = [])
    {
        switch ($name) {
            case 'edit':
                return [
                    'method' => 'POST',
                    'route' => 'jlm_product_stock_update',
                    'params' => ['id' => $options['entity']->getId()],
                    'label' => 'Modifier',
                    'type'  => StockType::class,
                    'entity' => $options['entity'],
                ];
            case 'inventory':
                return [
                    'method' => 'POST',
                    'route' => 'jlm_product_stock_inventory',
                    'params' => [],
                    'label' => 'Valider',
                    'type'  => InventoryType::class,
                    'entity' => $options['entity'],
                    ];
        }
    
        return parent::getFormParam($name, $options);
    }
}
