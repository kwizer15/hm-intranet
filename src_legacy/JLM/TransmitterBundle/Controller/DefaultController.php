<?php

namespace JLM\TransmitterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\TransmitterBundle\Entity\Ask;
use JLM\TransmitterBundle\Entity\Transmitter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use JLM\CoreBundle\Entity\Search;

class DefaultController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/search",name="transmitter_search", methods={"GET"})
     * @IsGranted("ROLE_OFFICE")
     */
    public function searchAction(Request $request)
    {
        $formData = $request->get('jlm_core_search');
         
        if (is_array($formData) && array_key_exists('query', $formData)) {
            $entity = new Search();
            $query = $formData['query'];
            $entity->setQuery($query);

            return $this->render('@JLMTransmitter/Default/search.html.twig', [
                    'transmitters' => $this->entityManager->getRepository(Transmitter::class)->search($entity),
                    'asks'          => $this->entityManager->getRepository(Ask::class)->search($entity),
            ]);
        }

        return $this->render('@JLMTransmitter/Default/search.html.twig', []);
    }
}
