<?php
namespace JLM\ModelBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ProductBundle\Entity\Product;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ProductToIntTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * Transforms an object (trustee) to a string (name).
     *
     * @param  Trustee|null $entity
     * @return string
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return "";
        }
        return $entity->getId();
    }
    
    /**
     * Transforms a string (number) to an object (trustee).
     *
     * @param  string $number
     * @return Trustee|null
     * @throws TransformationFailedException if object (trustee) is not found.
     */
    public function reverseTransform($string)
    {
        if (!$string) {
            return null;
        }
    
        
            $entity = $this->objectManager
                ->getRepository(Product::class)
                ->find($string)
            ;
        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'A trustee with id "%s" does not exist!',
                $string
            ));
        }
    
        return $entity;
    }
}
