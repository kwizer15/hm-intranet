<?php

namespace JLM\ProductBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\ProductBundle\Entity\ProductCategory;
use JLM\ProductBundle\Form\Type\ProductCategoryType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class ProductCategoryController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all ProductCategory entities.
     */
    public function indexAction()
    {
        $entities = $this->entityManager->getRepository(ProductCategory::class)->findAll();

        return $this->render('@JLMProduct/ProductCategory/index.html.twig', ['entities' => $entities]);
    }

    /**
     * Finds and displays a ProductCategory entity.
     */
    public function showAction($id)
    {


        $entity = $this->getEntity($id);

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@JLMProduct/ProductCategory/show.html.twig', [
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to create a new ProductCategory entity.
     */
    public function newAction()
    {


        $entity = new ProductCategory();
        $form   = $this->createNewForm($entity);

        return $this->render('@JLMProduct/ProductCategory/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Creates a new ProductCategory entity.
     */
    public function createAction(Request $request)
    {


        $entity  = new ProductCategory();
        $form    = $this->createNewForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('jlm_product_productcategory_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMProduct/ProductCategory/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing ProductCategory entity.
     */
    public function editAction($id)
    {


        $entity = $this->getEntity($id);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@JLMProduct/ProductCategory/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Edits an existing ProductCategory entity.
     */
    public function updateAction(Request $request, $id)
    {


        $entity = $this->getEntity($id);

        $editForm   = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('jlm_product_productcategory_edit', ['id' => $id]));
        }

        return $this->render('@JLMProduct/ProductCategory/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a ProductCategory entity.
     */
    public function deleteAction(Request $request, $id)
    {


        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity = $this->getEntity($id);
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }

        return ($this->redirectToRoute('jlm_product_productcategory'));
    }
    
    /**
     * Get the entity from id
     * @param int $id
     * @return ProductCategory
     */
    private function getEntity($id)
    {
        $entity = $this->entityManager->getRepository(ProductCategory::class)->find($id);
    
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductCategory entity.');
        }
    
        return $entity;
    }
    
    /**
     * Get the delete form
     * @param int $id
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
        ->add('id', HiddenType::class)
        ->getForm()
        ;
    }
    
    /**
     * Get the edit form
     * @param ProductCategory $entity
     * @return \Symfony\Component\Form\Form
     */
    private function createEditForm(ProductCategory $entity)
    {
        return $this->createForm(ProductCategoryType::class, $entity);
    }
    
    /**
     * Get the new form
     * @param ProductCategory $entity
     * @return \Symfony\Component\Form\Form
     */
    private function createNewForm(ProductCategory $entity)
    {
        return $this->createForm(ProductCategoryType::class, $entity);
    }
}
