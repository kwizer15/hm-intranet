<?php
namespace JLM\UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use JLM\ContactBundle\Model\ContactInterface;

class User extends BaseUser implements ContactInterface
{
    protected $id;

    private $contact;

    /**
     * Set contact
     *
     * @param ContactInterface $contact
     * @return self
     */
    public function setContact(ContactInterface $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return ContactInterface
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        if (null === $this->getContact()) {
            return parent::__toString();
        }

        return $this->getContact()->__toString();
    }

    /**
     * {@inheritdoc}
     */
    public function getFax()
    {
        return $this->contact->getFax();
    }

    /**
     * {@inheritdoc}
     */
    public function getAddress()
    {
        return $this->contact->getAddress();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->contact->getName();
    }
}
