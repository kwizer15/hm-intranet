<?php

declare(strict_types=1);

namespace HMFermeture\Domain\Devis\Repository;

use HMFermeture\Domain\Common\Repository\Repository;
use HMFermeture\Domain\Devis\Entity\DemandeDeDevis;

interface DemandeDeDevisRepository extends Repository
{
    /**
     * @param int $devisId
     *
     * @return DemandeDeDevis
     */
    public function get(int $devisId): DemandeDeDevis;
}
