<?php

namespace JLM\ContractBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContractBundle\Entity\Contract;
use JLM\ContractBundle\Event\ContractEvent;
use JLM\ContractBundle\JLMContractEvents;
use JLM\ContractBundle\Manager\ContractManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class ContractController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Contract::class);
    }

    /**
     * Lists all Contract entities.
     *
     * @deprecated not used
     */
    public function indexAction()
    {
        $entities = $this->repository->findAll();

        return $this->render('@JLMContract/Contract/index.html.twig', ['entities' => $entities]);
    }

    /**
     * Finds and displays a Contract entity.
     */
    public function showAction($id)
    {
        $entity = $this->repository->find($id);
        
        return $this->render('@JLMContract/Contract/show.html.twig', ['entity' => $entity]);
    }
    
    /**
     * Creates a new Contract entity.
     */
    public function newAction(ContractManager $manager, EventDispatcherInterface $eventDispatcher, Request $request)
    {
        $form   = $manager->createForm($request, 'new');
        $ajax = $request->isXmlHttpRequest();
        if ($manager->getHandler($form, $request)->process('POST')) {
            $event = new ContractEvent($form->getData());
            $eventDispatcher->dispatch(JLMContractEvents::AFTER_CONTRACT_CREATE, $event);

            return $ajax
                ? new JsonResponse()
                : $this->redirectToRoute('door_show', ['id' => $form->getData()->getDoor()->getId()])
            ;
        }

        $template = $ajax
            ? '@JLMContract/Contract/modal_new.html.twig'
            : '@JLMContract/Contract/new.html.twig'
        ;
                
        return $this->render($template, [
            'form'   => $form->createView()
        ]);
    }
    
    /**
     * Edits an existing Contract entity.
     */
    public function editAction(ContractManager $manager, Request $request, $id, $formName)
    {
        $entity = $this->repository->find($id);
        $ajax = $request->isXmlHttpRequest();
        $form = $manager->createForm($request, $formName, ['entity' => $entity]);
        if ($manager->getHandler($form, $request)->process()) {
            return ($ajax) ? new JsonResponse([])
                           : $this->redirectToRoute('door_show', ['id' => $entity->getDoor()->getId()])
            ;
        }
        $template = $ajax
            ? '@JLMContract/Contract/modal_edit.html.twig'
            : '@JLMContract/Contract/edit.html.twig'
        ;
        
        return $this->render($template, [
                    'form'   => $form->createView(),
            ]);
    }
}
