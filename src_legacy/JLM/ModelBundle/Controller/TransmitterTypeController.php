<?php

namespace JLM\ModelBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\ModelBundle\Entity\TransmitterType;
use JLM\ModelBundle\Form\Type\TransmitterTypeType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class TransmitterTypeController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all TransmitterType entities.
     */
    public function indexAction()
    {
        $entities = $this->entityManager->getRepository(TransmitterType::class)->findAll();

        return $this->render('@JLMModel/TransmitterType/index.html.twig', ['entities' => $entities]);
    }

    /**
     * Displays a form to create a new TransmitterType entity.
     */
    public function newAction()
    {
        $entity = new TransmitterType();
        $form   = $this->createForm(TransmitterTypeType::class, $entity);

        return $this->render('@JLMModel/TransmitterType/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Creates a new TransmitterType entity.
     */
    public function createAction(Request $request)
    {
        $entity  = new TransmitterType();
        $form    = $this->createForm(TransmitterTypeType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('transmittertype'));
        }

        return $this->render('@JLMModel/TransmitterType/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing TransmitterType entity.
     */
    public function editAction(TransmitterType $entity)
    {
        $editForm = $this->createForm(TransmitterTypeType::class, $entity);

        return $this->render('@JLMModel/TransmitterType/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Edits an existing TransmitterType entity.
     */
    public function updateAction(Request $request, TransmitterType $entity)
    {
        $editForm   = $this->createForm(TransmitterTypeType::class, $entity);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('transmittertype'));
        }

        return $this->render('@JLMModel/TransmitterType/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }
}
