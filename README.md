JLM Intranet
============

Pré-requis
----------

* Apache
* PHP
* MySQL
* Git
* Composer

Installation
------------

```
make install
make server
```
