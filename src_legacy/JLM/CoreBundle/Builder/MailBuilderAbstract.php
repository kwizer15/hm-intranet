<?php

namespace JLM\CoreBundle\Builder;

use JLM\CoreBundle\Entity\Email;

abstract class MailBuilderAbstract implements MailBuilderInterface
{
    protected $mail;
    
    public function getMail()
    {
        return $this->mail;
    }
    
    public function create()
    {
        $this->mail = new Email();
    }
    
    public function setSubject($subject)
    {
        return $this->mail->setSubject($subject);
    }
    
    public function addFrom($address, $name = null)
    {
        return $this->mail->addFrom($address, $name);
    }
    
    public function addTo($address, $name = null)
    {
        return $this->mail->addTo($address, $name);
    }
    
    public function addCc($address, $name = null)
    {
        return $this->mail->addCc($address, $name);
    }
    
    public function addBcc($address, $name = null)
    {
        return $this->mail->addBcc($address, $name);
    }
    
    public function setBody($body)
    {
        return $this->mail->setBody($body);
    }
    
    protected function getSignature()
    {
        return PHP_EOL.PHP_EOL
        .'--'.PHP_EOL
        .'JLM Entreprise'.PHP_EOL
        .'17 avenue de Montboulon'.PHP_EOL
        .'77165 SAINT-SOUPPLETS'.PHP_EOL
        .'Tel : 01 64 33 77 70'.PHP_EOL
        .'Fax : 01 64 33 78 45';
    }
    
    public function buildAttachements()
    {
    }
    
    public function buildPreAttachements()
    {
    }
}
