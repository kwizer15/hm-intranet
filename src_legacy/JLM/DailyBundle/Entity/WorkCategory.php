<?php

namespace JLM\DailyBundle\Entity;

use JLM\ModelBundle\Entity\StringModel;

class WorkCategory extends StringModel
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
