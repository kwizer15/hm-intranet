<?php

namespace JLM\CommerceBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Builder\Email\BillBoostMailBuilder;
use JLM\CommerceBundle\Builder\Email\BillBoostBusinessMailBuilder;
use JLM\CommerceBundle\Entity\Bill;
use JLM\CommerceBundle\Event\BillEvent;
use JLM\CommerceBundle\Excel\BillState;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CommerceBundle\Manager\BillManager;
use JLM\CommerceBundle\Pdf\Bill as BillPdf;
use JLM\CommerceBundle\Pdf\BillBoost;
use JLM\CommerceBundle\Pdf\BillList;
use JLM\CoreBundle\Factory\MailFactory;
use JLM\CoreBundle\Builder\MailSwiftMailBuilder;
use JLM\CoreBundle\Form\Type\MailType;
use JLM\CoreBundle\Response\PdfResponse;
use JLM\CoreBundle\Service\Paginator;
use JLM\DailyBundle\Entity\Intervention;
use JLM\DailyBundle\Form\Type\ExternalBillType;
use Liuggio\ExcelBundle\Factory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class BillController extends Controller
{
    /**
     * @var BillManager
     */
    private $billManager;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(BillManager $billManager, EntityManagerInterface $entityManager)
    {
        $this->billManager = $billManager;
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Bill::class);
    }

    /**
     * List bills
     */
    public function indexAction(Paginator $paginator, Request $request)
    {
        return $this->render(
            '@JLMCommerce/Bill/index.html.twig',
            $paginator->paginator($request, $this->repository, [
                'sort' => '!number',
                'state' => null,
                'year' => null
            ])
        );
    }
    
    /**
     * Finds and displays a Bill entity.
     */
    public function showAction($id)
    {
        return $this->render('@JLMCommerce/Bill/show.html.twig', ['entity'=> $this->repository->find($id)]);
    }
    
    /**
     * Displays a form to create a new Bill entity.
     */
    public function newAction(Request $request, EventDispatcherInterface $eventDispatcher)
    {
        $form = $this->billManager->createForm($request, 'new');
        if ($this->billManager->getHandler($form, $request)->process()) {
            $entity = $form->getData();
            $eventDispatcher->dispatch(JLMCommerceEvents::BILL_AFTER_PERSIST, new BillEvent($entity, $request));

            return $this->redirectToRoute('bill_show', ['id' => $entity->getId()]);
        }
        
        return $this->render('@JLMCommerce/Bill/new.html.twig', [
            'form'   => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Bill entity.
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->repository->find($id);
        $this->assertState($entity, [0]);
        $editForm = $this->billManager->createForm($request, 'edit', ['entity'=> $entity]);
        
        if ($this->billManager->getHandler($editForm, $request, $entity)->process()) {
            return $this->redirectToRoute('bill_show', ['id' => $entity->getId()]);
        }
        
        return $this->render('@JLMCommerce/Bill/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Imprimer la facture
     */
    public function printAction($id)
    {
        return $this->printer($id);
    }
    
    /**
     * Imprimer un duplicata de facture
     */
    public function printduplicateAction($id)
    {
        return $this->printer($id, true);
    }
    
    private function printer($id, $duplicate = false)
    {
        $entity = $this->repository->find($id);
        $filename = $entity->getNumber();
        if ($duplicate) {
            $filename .= '-duplicata';
        }
        $filename .= '.pdf';
        
        return new PdfResponse($filename, BillPdf::get([$entity], $duplicate));
    }
    
    /**
     * Imprimer la liste des factures à faire
     */
    public function printlistAction()
    {
        $entities = $this->entityManager->getRepository(Intervention::class)->getToBilled();

        return new PdfResponse('factures-a-faire', BillList::get($entities));
    }
    
    /**
     * Note Bill as ready to send.
     */
    public function readyAction($id)
    {
        return $this->stateChange($id, 1);
    }
    
    /**
     * Note Bill as been send.
     */
    public function sendAction(Request $request, $id)
    {
        $entity = $this->repository->find($id);
        // TODO: constantes pour les states
        if ($entity->getState() != 1) {
            $entity->setState(1);
        }
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        
        return $this->redirect($request->headers->get('referer'));
    }
    
    /**
     * Note Bill as been canceled.
     */
    public function cancelAction($id)
    {
        return $this->stateChange($id, -1);
    }
    
    /**
     * Note Bill retour à la saisie.
     */
    public function backAction($id)
    {
        return $this->stateChange($id, 0);
    }
    
    /**
     * Note Bill réglée.
     */
    public function payedAction($id)
    {
        return $this->stateChange($id, 2);
    }
    
    private function stateChange(Request $request, $id, $newState)
    {
        $entity = $this->repository->find($id);
        switch ($newState) {
            case 1:
                $redirect = ($entity->getState() < 0);
                $set = ($entity->getState() < $newState);
                break;
            case 2:
                $redirect = ($entity->getState() > 1);
                $set = ($entity->getState() == 1);
                break;
            case -1:
            case 0:
                $redirect = ($entity->getState() < 1);
                $set = ($entity->getState() > $newState);
                break;
        }
        if ($redirect) {
            return $this->redirectToRoute('bill_show', ['id' => $entity->getId()]);
        }
        if ($set) {
            $entity->setState($newState);
        }
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
         
        return $this->redirect($request->headers->get('referer'));
    }
    
    /**
     * Display bills to do
     */
    public function todoAction(FormFactoryInterface $formFactory)
    {
        $list = $this->entityManager->getRepository(Intervention::class)->getToBilled();
        $forms_externalBill = [];
        foreach ($list as $interv) {
            $forms_externalBill[] = $formFactory->createNamed(
                'externalBill'.$interv->getId(),
                ExternalBillType::class,
                $interv
            )->createView();
        }
        return $this->render('@JLMCommerce/Bill/todo.html.twig', [
                'entities'=>$list,
                'forms_externalbill' => $forms_externalBill,
        ]);
    }
    
    /**
     * Display bills to boost
     */
    public function toboostAction()
    {
        return $this->render('@JLMCommerce/Bill/toboost.html.twig', [
            'entities' => $this->repository->getToBoost()
        ]);
    }
    
    /**
     * Email de relance facture
     */
    public function boostemailAction(Request $request, $id, \Swift_Mailer $mailer, EventDispatcherInterface $eventDispatcher)
    {
        // @todo Passer par un service de formPopulate et créer un controller unique dans CoreBundle

        $entity = $this->repository->find($id);
        $site = $entity->getSiteObject();
        $builder = ($site === null) ? new BillBoostMailBuilder($entity) : new BillBoostBusinessMailBuilder($site);
        $mail = MailFactory::create($builder);
        $editForm = $this->createForm(MailType::class, $mail);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $mailer->send(MailFactory::create(new MailSwiftMailBuilder($editForm->getData())));
            $eventDispatcher->dispatch(
                JLMCommerceEvents::BILL_BOOST_SENDMAIL,
                new BillEvent($entity, $request)
            );
            
            return $this->redirect($request->headers->get('referer'));
        }
    
        return $this->render('@JLMCommerce/Bill/boostemail.html.twig', [
                'entity' => $entity,
                'form' => $editForm->createView(),
        ]);
    }
    
    /**
     * Imprimer le courrier de relance
     */
    public function printboostAction($id)
    {
        $entity = $this->repository->find($id);

        return new PdfResponse($entity->getNumber(), Billboost::get([$entity]));
    }
    
    /**
     * Noter relance effectuée
     */
    public function boostokAction($id)
    {
        $entity = $this->repository->find($id);
        $date = new \DateTime;
        if ($entity->getFirstBoost() === null) {
            $entity->setFirstBoost($date);
        } else {
            $entity->setSecondBoost($date);
        }
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        
        return $this->redirectToRoute('bill_toboost');
    }
    
    /**
     * Search
     */
    public function searchAction(Request $request)
    {
        $formData = $request->get('jlm_core_search');
        $params = [];
        if (is_array($formData) && array_key_exists('query', $formData)) {
            $params = ['results' => $this->repository->search($formData['query'])];
        }
         
        return $this->render('@JLMCommerce/Bill/search.html.twig', $params);
    }
    
    public function updateAction(Request $request)
    {
        $bills = $this->repository->findAll();
        foreach ($bills as $bill) {
            $bill->getTotalPrice();
            $this->entityManager->persist($bill);
        }
        $this->entityManager->flush();
        
        return $this->redirect($request->headers->get('referer'));
    }

    public function stateExcelAction(Request $request, Factory $excel)
    {
        $list = $this->repository->getStateBill($request->get('fee', 11));
    
        $excelBuilder = new BillState($excel);
         
        return $excelBuilder->createList($list)->getResponse();
    }

    private function assertState(Bill $bill, $states = [])
    {
        if (!in_array($bill->getState(), $states)) {
            // return $this->redirect('bill_show', ['id' => $bill->getId()]);
        }
    }
}
