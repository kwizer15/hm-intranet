<?php

namespace JLM\CommerceBundle\Builder;

interface QuoteBuilderInterface
{
    /**
     * @return BillInterface
     */
    public function getQuote();

    public function create();
    
    public function buildCreation();
    
    public function buildLines();
    
    public function buildCustomer();
    
    public function buildBusiness();
    
    public function buildIntro();
    
    public function buildConditions();
}
