<?php

namespace JLM\CommerceBundle\Builder;

use JLM\CommerceBundle\Entity\BillLine;

abstract class BillLineBuilderAbstract implements BillLineBuilderInterface
{
    /**
     *
     * @var BillLineInterface $line
     */
    private $line;
    
    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->line = new BillLine;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildQuantity()
    {
        $this->line->setQuantity(1);
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildPrice()
    {
        $this->line->setUnitPrice(0);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getLine()
    {
        return $this->line;
    }
}
