<?php
namespace JLM\DefaultBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class PaginableController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Pagination
     */
    protected function pagination($repository, $functiondata = 'All', $page = 1, $limit = 10, $route = null)
    {
        $repo = $this->entityManager->getRepository($repository);
        $functionCount = 'getCount'.$functiondata;
        $functionDatas = 'get'.$functiondata;
        if (!method_exists($repo, $functionCount)) {
            throw $this->createNotFoundException(
                'Page insexistante (La méthode '.$repository.'Repository#'.$functionCount.' n\'existe pas)'
            );
        }
        if (!method_exists($repo, $functionDatas)) {
            throw $this->createNotFoundException(
                'Page insexistante (La méthode '.$repository.'Repository#'.$functionDatas.' n\'existe pas)'
            );
        }
        $nb = $repo->$functionCount();
        $nbPages = ceil($nb/$limit);
        $nbPages = ($nbPages < 1) ? 1 : $nbPages;
        $offset = ($page-1) * $limit;
        if ($page < 1 || $page > $nbPages) {
            throw $this->createNotFoundException('Page insexistante (page '.$page.'/'.$nbPages.')');
        }

        return [
            'entities' => $repo->$functionDatas($limit, $offset),
            'pageTotal' => $nbPages,
            'page' => $page,
            'pageRoute' => $route
        ];
    }
}
