<?php

namespace JLM\CoreBundle\Twig\Extension;

use JLM\CoreBundle\Form\Type\SearchType;
use Symfony\Component\Form\FormFactoryInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class SearchExtension extends AbstractExtension implements GlobalsInterface
{
    private $formFactory;
    
    public function __construct(FormFactoryInterface $formFactory)
    {
        $this->formFactory = $formFactory;
    }
    
    public function getName()
    {
        return 'search_extension';
    }
    
    public function getGlobals()
    {
        $form = $this->formFactory->create(SearchType::class);

        return ['search' => [
            'form' => $form->createView()
            ],
        ];
    }
}
