<?php

namespace JLM\DailyBundle\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;
use JLM\DailyBundle\Entity\Fixing;
use JLM\DailyBundle\Entity\Intervention;
use JLM\DailyBundle\Entity\Maintenance;
use JLM\DailyBundle\Entity\Work;
use JLM\ModelBundle\Entity\Door;
use Twig\Extension\GlobalsInterface;

class InterventionCountExtension extends \Twig\Extension\AbstractExtension implements GlobalsInterface
{
    private $objectManager;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public function getName()
    {
        return 'interventioncount_extension';
    }
    
    public function getGlobals()
    {
        return [
            'interventioncount' => [
                'today' => $this->objectManager->getRepository(Intervention::class)->getCountToday(),
                'stopped' => $this->objectManager->getRepository(Door::class)->getCountStopped(),
                'fixing' => $this->objectManager->getRepository(Fixing::class)->getCountOpened(),
                'work'   => $this->objectManager->getRepository(Work::class)->getCountOpened(),
                'maintenance' => $this->objectManager->getRepository(Maintenance::class)->getCountOpened(),
            ],
        ];
    }
}
