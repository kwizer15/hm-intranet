<?php

namespace JLM\ProductBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ProductBundle\Entity\Product;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ProductToIntTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * Transforms an object (product) to a int (id).
     *
     * @param  Product|null $entity
     * @return string
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return "";
        }
        
        return $entity->getId();
    }
    
    /**
     * Transforms a int (id) to an object (product).
     *
     * @param  string $id
     * @return Trustee|null
     * @throws TransformationFailedException if object (trustee) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
    
        $entity = $this->objectManager->getRepository(Product::class)->find($id);
        if (null === $entity) {
            throw new TransformationFailedException(sprintf('A product with id "%s" does not exist!', $id));
        }
    
        return $entity;
    }
}
