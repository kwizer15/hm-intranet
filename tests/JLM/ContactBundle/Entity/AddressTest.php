<?php

namespace Tests\JLM\ContactBundle\Entity;

use JLM\ContactBundle\Entity\Address;
use JLM\ContactBundle\Model\AddressInterface;
use JLM\ContactBundle\Model\CityInterface;

class AddressTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Address
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new Address;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf(AddressInterface::class, $this->entity);
        $this->assertNull($this->entity->getId());
        $this->assertSame('', $this->entity->getStreet());
        $this->assertNull($this->entity->getCity());
        $this->assertSame('', (string) $this->entity);
    }
    
    public function getStreets()
    {
        return [
            ['1, rue Bidule Machin Truc', '1, rue Bidule Machin Truc'],
            [153, '153'],
        ];
    }
    
    /**
     * @dataProvider getStreets
     */
    public function testStreet($in, $out)
    {
        $this->assertSame($this->entity, $this->entity->setStreet($in));
        $this->assertSame($out, $this->entity->getStreet());
    }
    
    public function getCities()
    {
        return [
            [$this->createMock(CityInterface::class)],
        ];
    }
    
    /**
     * @dataProvider getCities
     */
    public function testCity($city)
    {
        $this->assertSame($this->entity, $this->entity->setCity($city));
        $this->assertSame($city, $this->entity->getCity());
    }
    
    public function getToStrings()
    {
        
        return [
            [
                '17 avenue de Montboulon',
                '77165',
                'Saint-Soupplets',
                '17 avenue de Montboulon'.PHP_EOL.'77165 - Saint-Soupplets'
            ],
            
        ];
    }
    
    /**
     * @dataProvider getToStrings
     */
    public function testtoString($street, $zip, $cityname, $out)
    {
        $city = $this->createMock('JLM\ContactBundle\Model\CityInterface');
        $city->expects($this->once())->method('__toString')->willReturn($zip . ' - ' . $cityname);

        $this->entity->setStreet($street);
        $this->entity->setCity($city);
        $this->assertSame($out, (string) $this->entity);
    }
}
