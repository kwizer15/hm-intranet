<?php

namespace HMFermeture\Domain\Devis\Repository;

use HMFermeture\Domain\Common\Repository\Repository;
use HMFermeture\Domain\Devis\Entity\Responsable;

interface ResponsableRepository extends Repository
{
    public function getByUsername(string $username): Responsable;
}
