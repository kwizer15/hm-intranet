<?php

namespace JLM\CoreBundle\Factory;

use JLM\CoreBundle\Builder\MailBuilderInterface;

class MailFactory
{
    /**
     *
     * @param MailBuilderInterface $builder
     * @return mixed
     */
    public static function create(MailBuilderInterface $builder)
    {
        $builder->create();
        $builder->buildSubject();
        $builder->buildFrom();
        $builder->buildTo();
        $builder->buildCc();
        $builder->buildBcc();
        $builder->buildBody();
        $builder->buildPreAttachements();
        $builder->buildAttachements();
        
        return $builder->getMail();
    }
}
