<?php

namespace JLM\ContactBundle\Form\Type;

use JLM\CoreBundle\Form\Type\AbstractSelectType;
use JLM\ContactBundle\Form\DataTransformer\PersonToIntTransformer;

class PersonSelectType extends AbstractSelectType
{

    protected function getTransformerClass()
    {
        return PersonToIntTransformer::class;
    }
    
    protected function getTypeName()
    {
        return 'jlm_contact_person';
    }
}
