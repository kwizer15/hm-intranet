<?php

namespace JLM\ModelBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Genemu\Bundle\FormBundle\Form\JQuery\Type\Select2HiddenType;
use Symfony\Component\Form\AbstractType;
use JLM\ModelBundle\Form\DataTransformer\TrusteeToIntTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrusteeSelectType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public function getParent()
    {
        return Select2HiddenType::class;
    }
    
    public function getBlockPrefix(): string
    {
        return 'trustee_select';
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'transformer' => new TrusteeToIntTransformer($this->objectManager),
        ]);
    }
}
