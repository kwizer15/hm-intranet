<?php

namespace JLM\TransmitterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\TransmitterBundle\Entity\Attribution;
use JLM\TransmitterBundle\Entity\Replacement;
use JLM\TransmitterBundle\Form\Type\ReplacementType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/replacement")
 * @IsGranted("ROLE_OFFICE")
 */
class ReplacementController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Displays a form to create a new replacement entity.
     *
     * @Route("/new/{id}", name="transmitter_replacement_new")
     */
    public function newAction(Attribution $attribution)
    {
        $entity = new Replacement();
        $entity->setAttribution($attribution);
        $form   = $this->createForm(ReplacementType::class, $entity, ['id' => $attribution->getSite()->getId()]);

        return $this->render('@JLMTransmitter/Replacement/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a new Series entity.
     *
     * @Route("/create/{id}", name="transmitter_replacement_create", methods={"POST"})
     */
    public function createAction(Request $request, Attribution $attribution)
    {
        $entity  = new Replacement();
        $form = $this->createForm(ReplacementType::class, $entity, ['id' => $attribution->getSite()->getId()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $new = $entity->getNew();
            $old = $entity->getOld();
            $this->entityManager->persist($new);
            $old->setReplacedTransmitter($new);
            $this->entityManager->persist($old);
            $this->entityManager->flush();
            
            // On met à jour la page de base
            return new JsonResponse([]);
        }

        return $this->render('@JLMTransmitter/Replacement/create.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }
}
