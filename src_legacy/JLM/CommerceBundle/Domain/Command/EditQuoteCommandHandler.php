<?php

namespace JLM\CommerceBundle\Domain\Command;

use Doctrine\ORM\EntityManagerInterface;
use DomainException;

class EditQuoteCommandHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function handle(EditQuoteCommand $command): iterable
    {
        $quote = $command->quote;

        if ($quote->getState() !== 0) {
            throw new DomainException('Le devis n\'est pas en édition.');
        }

        $this->entityManager->persist($quote);
        $this->entityManager->flush();

        return [$quote->getId()];
    }
}
