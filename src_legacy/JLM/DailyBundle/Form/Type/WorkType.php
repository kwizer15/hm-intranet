<?php

namespace JLM\DailyBundle\Form\Type;

use JLM\CommerceBundle\Form\Type\QuoteVariantHiddenType;
use JLM\ModelBundle\Form\Type\DoorHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\DailyBundle\Entity\Work;

class WorkType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('door', DoorHiddenType::class)
            ->add('place', null, ['label'=>'Lieu','attr'=>['class'=>'input-xlarge']])
            ->add('quote', QuoteVariantHiddenType::class, ['required'=>false])
            ->add('reason', null, ['label'=>'Raison de l\'intervention','attr'=>['class'=>'input-xlarge']])
            ->add('contactName', null, ['label'=>'Nom du contact','required'=>false])
            ->add('contactPhones', null, ['label'=>'Téléphones','required'=>false])
            ->add('contactEmail', EmailType::class, ['label'=>'e-mail','required'=>false,'attr'=>['class'=>'input-xlarge']])
            ->add('category', null, ['label'=>'Type de travaux'])
            ->add('objective', null, ['label'=>'Objectif'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Work::class,
            'attr' => ['class'=>  'interventionForm'],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_dailybundle_worktype';
    }
}
