<?php

namespace JLM\DailyBundle\Model;

use JLM\ModelBundle\Entity\Door;

interface InterventionInterface
{
    /**
     * @return \DateTime
     */
    public function getLastDate();
    
    /**
     * @return string
     */
    public function getPlace();
    
    /**
     * @return Door
     */
    public function getDoor();
    
    /**
     * @return string
     */
    public function getRest();
    
    /**
     * @return string
     */
    public function getContactName();
    
    /**
     * @return string
     */
    public function getContactPhones();
    
    /**
     * @return string
     */
    public function getContactEmail();
    
    /**
     * @return WorkInterface
     */
    public function getWork();
    
    public function getManagerContacts();
    
    public function getAdministratorContacts();
}
