<?php

namespace JLM\DailyBundle\Builder;

use JLM\ModelBundle\Builder\DoorBillBuilderAbstract;
use JLM\DailyBundle\Entity\Intervention;

class InterventionBillBuilder extends DoorBillBuilderAbstract
{
    private $intervention;
    
    public function __construct(Intervention $intervention, $options = [])
    {
        $this->intervention = $intervention;
        parent::__construct($this->intervention->getDoor(), $options);
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildReference()
    {
        $this->getBill()->setReference(
            'Selon notre intervention du '.$this->intervention->getLastDate()->format('d/m/Y')
        );
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildLines()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildIntro()
    {
        $this->getBill()->setIntro($this->intervention->getReason());
    }
}
