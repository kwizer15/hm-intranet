<?php

namespace JLM\ModelBundle\Form\Type;

use JLM\ContactBundle\Form\Type\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use JLM\ModelBundle\Entity\Door;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContractDoorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', null, ['label'=>'Type de porte'])
            ->add('address', AddressType::class, ['label'=>'Adresse'])
            ->add('location', TextType::class, ['label'=>'Localisation'])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_modelbundle_contractdoortype';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Door::class,
        ]);
    }
}
