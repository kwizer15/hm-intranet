<?php

namespace Tests\JLM\AskBundle\Entity;

use PHPUnit\Framework\TestCase;

class AskTest extends TestCase
{
    /**
     * @var Country
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = $this->getMockForAbstractClass('JLM\AskBundle\Entity\Ask');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf('JLM\AskBundle\Model\AskInterface', $this->entity);
    }
    
    public function getGetterSetter()
    {
        return [
            ['Creation', new \DateTime],
            ['Maturity', new \DateTime],
            ['Ask', 'Foo'],
            ['DontTreat', 'Foo'],
            ['Contact', $this->createMock('JLM\AskBundle\Model\ContactInterface')],
            ['Payer', $this->createMock('JLM\AskBundle\Model\PayerInterface')],
            ['Method', $this->createMock('JLM\AskBundle\Model\CommunicationMeansInterface')],
            ['Subject', $this->createMock('JLM\AskBundle\Model\SubjectInterface')],
            // Deprecateds
            ['Person', $this->createMock('JLM\ContactBundle\Entity\Person')],
            ['Trustee', $this->createMock('JLM\ModelBundle\Entity\Trustee')],
            ['Site', $this->createMock('JLM\ModelBundle\Entity\Site')],
        ];
    }
    
    /**
     * @dataProvider getGetterSetter
     */
    public function testGetterSetter($attribute, $value)
    {
        $getter = 'get'.$attribute;
        $setter = 'set'.$attribute;
        $this->assertSame($this->entity, $this->entity->$setter($value));
        $this->assertSame($value, $this->entity->$getter());
    }
    
    public function testIsCreationBeforeMaturity()
    {
        $this->entity->setCreation(new \DateTime);
        $this->entity->setMaturity(new \DateTime);
        $this->assertTrue($this->entity->isCreationBeforeMaturity());
    }
}
