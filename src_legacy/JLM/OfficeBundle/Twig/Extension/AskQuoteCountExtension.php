<?php

namespace JLM\OfficeBundle\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;
use JLM\OfficeBundle\Entity\AskQuote;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class AskQuoteCountExtension extends AbstractExtension implements GlobalsInterface
{
    private $objectManager;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public function getName()
    {
        return 'askquotecount_extension';
    }
    
    public function getGlobals()
    {
        $repo = $this->objectManager->getRepository(AskQuote::class);
        
        return ['askquotecount' => [
                'all' => $repo->getTotal(),
                'untreated' => $repo->getCountUntreated(),
                'treated' => $repo->getCountTreated(),
        ]];
    }
}
