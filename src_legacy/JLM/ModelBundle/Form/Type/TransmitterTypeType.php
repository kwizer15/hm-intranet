<?php

namespace JLM\ModelBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ModelBundle\Entity\TransmitterType;

class TransmitterTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, ['label'=>'Nom'])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_modelbundle_transmittertypetype';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => TransmitterType::class,
        ]);
    }
}
