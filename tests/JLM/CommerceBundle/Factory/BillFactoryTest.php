<?php

namespace Tests\JLM\CommerceBundle\Factory;

use JLM\CommerceBundle\Factory\BillFactory;
use JLM\CommerceBundle\Builder\BillBuilderInterface;
use JLM\CommerceBundle\Model\BillInterface;
use PHPUnit\Framework\TestCase;

class BillFactoryTest extends TestCase
{
    private $builder;
    
    private $bill;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->builder = $this->createMock(BillBuilderInterface::class);
        
        $this->bill = null;
    }

    /**
     * {@inheritdoc}
     */
    public function assertPreConditions()
    {
        $this->builder
            ->expects($this->once())
            ->method('getBill')
            ->willReturn($this->createMock(BillInterface::class))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function assertPostConditions()
    {
        $this->assertInstanceOf(BillInterface::class, $this->bill);
    }

    /**
     * @dataProvider builders
     */
    public function testBuilders($method)
    {
        $this->builder->expects($this->once())->method($method);
        $this->bill = BillFactory::create($this->builder);
    }
    
    public function builders()
    {
        return [
            ['create'],
            ['buildCreation'],
            ['buildCustomer'],
            ['buildBusiness'],
            ['buildReference'],
            ['buildIntro'],
            ['buildDetails'],
            ['buildConditions'],
            ['buildLines'],
            
        ];
    }
}
