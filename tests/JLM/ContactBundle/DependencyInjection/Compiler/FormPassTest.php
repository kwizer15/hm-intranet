<?php

namespace Tests\JLM\ContactBundle\DependencyInjection\Compiler;

use JLM\ContactBundle\DependencyInjection\Compiler\FormPass;

class FormPassTest extends \PHPUnit\Framework\TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        
        $this->container = $this->createMock('Symfony\Component\DependencyInjection\ContainerBuilder');
        $this->pass = new FormPass();
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf('Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface', $this->pass);
    }
    
    public function testProcess()
    {
        $this->container->expects($this->once())->method('getParameter');
        $this->container->expects($this->once())->method('setParameter');
        $this->pass->process($this->container);
    }
}
