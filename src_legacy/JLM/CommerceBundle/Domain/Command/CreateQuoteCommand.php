<?php

namespace JLM\CommerceBundle\Domain\Command;

use DateTime;
use HMFermeture\Domain\Devis\Entity\DemandeDeDevis;
use HMFermeture\Domain\Devis\Entity\Responsable;
use JLM\OfficeBundle\Entity\AskQuote;

class CreateQuoteCommand extends AbstractQuoteCommand
{
    public function __construct(DateTime $creation, Responsable $follower, float $vat)
    {
        $this->creation = $creation;
        $this->followerCp = $follower->getNom();
        $this->vat = $vat;
        $this->vatTransmitter = $vat;
    }

    public static function fromAskQuote(DemandeDeDevis $askQuote, Responsable $responsable): self
    {
        $door = $askQuote->getDoor();
        if ($door !== null) {
            $site = $door->getSite();
            $doorCp = $door->toString();
        } else {
            $site = $askQuote->getSite();
            $doorCp = $site->toString();
        }
        $vat = $site->getVat()->getRate();
        $trustee = $askQuote->getTrustee();
        $contact = $askQuote->getPerson();

        $command = new self(new DateTime, $responsable, $vat);
        $command->door = $door;
        $command->doorCp = $doorCp;
        $command->trustee = $trustee;
        $command->trusteeName = $trustee->getName();
        $command->trusteeAddress = (string) $trustee->getAddress();
        $command->contact = $contact;
        $command->contactCp = (string) $contact;
        $command->ask = $askQuote;

        return $command;
    }
}
