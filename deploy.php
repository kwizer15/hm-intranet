<?php

namespace Deployer;

use Symfony\Component\Dotenv\Dotenv;

require_once 'vendor/autoload.php';

$dotenv = new Dotenv();
$dotenv->loadEnv('.env');

require 'recipe/symfony.php';

desc('Prevent PHP 7.2.20 bug');
task('prevent:php:bug', function () {
    if (test('[ ! -z "$({{bin/php}} --version | grep 7.2.20-)" ]')) {
        $composerOptions = get('composer_options');
        $composerOptions = str_replace(' --no-dev', '', $composerOptions);
        set('composer_options', $composerOptions);
    }
});
before('deploy:update_code', 'prevent:php:bug');

// Project name
set('application', 'hm-intranet');

// Project repository
set('repository', getenv('GIT_REPOSITORY'));

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true);

set('clear_paths', []);

// Shared files/dirs between deploys
add('shared_files', [
    '{{release_path}}/.env.local',
]);

add('shared_dirs', [
    '{{release_path}}/web/uploads',
]);

// Writable dirs by web server
add('writable_dirs', [
    '{{release_path}}/web/uploads'
]);

set('phinx_config_path', '{{release_path}}/phinx.yml');
set('bin/phinx', '{{bin/php}} {{release_path}}/bin/phinx --no-interaction --configuration={{phinx_config_path}}');

// Hosts
host('production')
    ->hostname(getenv('DEPLOY_PRODUCTION_HOST'))
    ->set('branch', 'master')
    ->set('keep_releases', getenv('DEPLOY_PRODUCTION_KEEP_RELEASES', 5))
    ->set('deploy_path', getenv('DEPLOY_PRODUCTION_PATH'));

host('staging')
    ->hostname(getenv('DEPLOY_STAGING_HOST'))
    ->set('keep_releases', getenv('DEPLOY_STAGING_KEEP_RELEASES', 5))
    ->set('deploy_path', getenv('DEPLOY_STAGING_PATH'));

// Tasks

/**
 * Override migrate database
 */
task('database:migrate', static function () {
    run(sprintf('{{bin/phinx}} migrate'));
})->desc('Migrate database');

task('reload:php-fpm', static function () {
    run('sudo /usr/sbin/service php7-fpm reload');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');
//after('deploy', 'reload:php-fpm');
// Migrate database before symlink new release.
before('deploy:symlink', 'database:migrate');
