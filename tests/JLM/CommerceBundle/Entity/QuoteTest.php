<?php

namespace Tests\JLM\CommerceBundle\Entity;

use JLM\CommerceBundle\Entity\Quote;

class QuoteTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Country
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new Quote;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
    }
    
    public function testRecipient()
    {
        $person = $this->createMock('JLM\CommerceBundle\Model\QuoteRecipientInterface');
        $this->assertSame($this->entity, $this->entity->setRecipient($person));
        $this->assertSame($person, $this->entity->getRecipient());
    }
}
