<?php

namespace JLM\DailyBundle\Model;

interface PartFamilyInterface
{

    /**
     * @return string
     */
    public function getName();
}
