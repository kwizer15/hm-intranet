<?php

namespace JLM\ContactBundle\Form\DataTransformer;

use JLM\ContactBundle\Entity\Person;
use JLM\CoreBundle\Form\DataTransformer\ObjectToIntTransformer;

class PersonToIntTransformer extends ObjectToIntTransformer
{
    /**
     * {@inheritdoc}
     */
    protected function getClass()
    {
        return Person::class;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getErrorMessage()
    {
        return 'A person with id "%s" does not exist!';
    }
}
