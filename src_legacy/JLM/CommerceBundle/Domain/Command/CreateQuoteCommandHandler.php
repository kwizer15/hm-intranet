<?php

namespace JLM\CommerceBundle\Domain\Command;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\Quote;
use JLM\CommerceBundle\Repository\QuoteRepository;

class CreateQuoteCommandHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        EntityManagerInterface $entityManager
    ) {
        $this->entityManager = $entityManager;
    }

    public function handle(CreateQuoteCommand $command): iterable
    {
        /** @var QuoteRepository $quoteRepository */
        $quoteRepository = $this->entityManager->getRepository(Quote::class);
        $lastNumber = $quoteRepository->getLastNumber();

        $newNumber = $this->generateNumber($command->creation, $lastNumber);
        $quote = new Quote();
        $quote->setCreation($command->creation);
        $quote->setNumber($newNumber);
        $quote->setTrustee($command->trustee);
        $quote->setTrusteeName($command->trusteeName);
        $quote->setTrusteeAddress($command->trusteeAddress);
        $quote->setContact($command->contact);
        $quote->setContactCp($command->contactCp);
        $quote->setFollower($command->follower);
        $quote->setFollowerCp($command->followerCp);
        $quote->setDoor($command->door);
        $quote->setDoorCp($command->doorCp);
        $quote->setVat($command->vat);
        $quote->setDescription($command->description);
        $quote->setVatTransmitter($command->vatTransmitter);
        $quote->setAsk($command->ask);
        $quote->addEvent(Quote::EVENT_CREATION, []);

        $this->entityManager->persist($quote);
        $this->entityManager->flush();

        return [$quote->getId()];
    }

    /**
     * Generation du numéro de devis
     */
    private function generateNumber(\DateTime $creation, $lastnumber)
    {
        $n = $lastnumber + 1;
        $number = $creation->format('ym');
        for ($i = strlen($n); $i < 4; $i++) {
            $number.= '0';
        }
        $number.= $n;

        return $number;
    }
}
