<?php

namespace JLM\DefaultBundle\Entity;

use Doctrine\ORM\EntityRepository;
use JLM\CoreBundle\Entity\Search;
use JLM\CoreBundle\Repository\SearchRepositoryInterface;

class SearchRepository extends EntityRepository implements SearchRepositoryInterface
{
    /**
     *
     * @param Search $search
     * @return array|null
     */
    public function search($search)
    {
        if (!$search instanceof Search) {
            $s = new Search;
            $s->setQuery($search);
            $keywords = $s->getKeywords();
        }
        
        if (empty($keywords)) {
            return [];
        }
        $queryBuilder = $this->getSearchQb();
        if ($queryBuilder === null) {
            return null;
        }
        $params = $this->getSearchParams();
        $wheres = [];
        foreach ($keywords as $key => $keyword) {
            foreach ($params as $param) {
                $wheres[$param][] = $param . ' LIKE ?'.$key;
            }
            $queryBuilder->setParameter($key, '%'.$keyword.'%');
            //$queryBuilder->orWhere($param . ' LIKE ?'.$key);
        }
        
        foreach ($params as $param) {
            $queryBuilder->orWhere(implode(' OR ', $wheres[$param]));
        }
        
        $orderBys = $this->getSearchOrderBy();
        foreach ($orderBys as $champ => $order) {
            /* Vérifier $order = 'asc' ou 'desc' */
            $queryBuilder->addOrderBy($champ, $order);
        }
        
        return $queryBuilder->getQuery()->getResult();
    }
    
    /**
     * @return QueryBuilder|null
     */
    protected function getSearchQb()
    {
        return null;
    }
    
    /**
     * @return array
     */
    protected function getSearchParams()
    {
        return [];
    }
    
    /**
     * @return array
     */
    protected function getSearchOrderBy()
    {
        return [];
    }
}
