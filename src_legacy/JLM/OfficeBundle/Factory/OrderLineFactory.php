<?php

namespace JLM\OfficeBundle\Factory;

use JLM\OfficeBundle\Builder\OrderLineBuilderInterface;

class OrderLineFactory
{
    
    public static function create(OrderLineBuilderInterface $builder)
    {
        $builder->create();
        $builder->buildReference();
        $builder->buildQuantity();
        $builder->buildDesignation();
    
        return $builder->getLine();
    }
}
