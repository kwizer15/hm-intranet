<?php

namespace JLM\ModelBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use JLM\ModelBundle\Form\DataTransformer\TrusteeToIntTransformer;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TrusteeHiddenType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $transformer = new TrusteeToIntTransformer($this->objectManager);
        $builder->addModelTransformer($transformer);
    }

    public function getParent()
    {
        return HiddenType::class;
    }
    
    public function getBlockPrefix(): string
    {
        return 'trustee_hidden';
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'invalid_message' => 'The selected trustee does not exist',
        ]);
    }
}
