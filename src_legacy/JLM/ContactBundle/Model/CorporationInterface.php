<?php

namespace JLM\ContactBundle\Model;

interface CorporationInterface extends ContactInterface
{
    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone();
}
