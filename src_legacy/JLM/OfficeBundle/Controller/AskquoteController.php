<?php

namespace JLM\OfficeBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Service\Paginator;
use JLM\DefaultBundle\Controller\PaginableController;
use JLM\OfficeBundle\Pdf\AskQuoteList;
use JLM\OfficeBundle\Repository\AskQuoteRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JLM\OfficeBundle\Entity\AskQuote;
use JLM\OfficeBundle\Form\Type\AskQuoteType;
use JLM\OfficeBundle\Form\Type\AskQuoteDontTreatType;
use JLM\DefaultBundle\Entity\Search;
use JLM\DefaultBundle\Form\Type\SearchType;

/**
 * AskQuote controller.
 *
 * @Route("/quote/ask")
 * @IsGranted("ROLE_OFFICE")
 */
class AskquoteController extends PaginableController
{
    /**
     * @var AskQuoteRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);

        $this->repository = $entityManager->getRepository(AskQuote::class);
    }

    /**
     * @Route("/", name="askquote")
     */
    public function indexAction(Paginator $paginator, Request $request)
    {
        $states = [
                'all' => 'All',
                'treated' => 'Treated',
                'untreated' => 'Untreated',
        ];
        $state = $request->get('state');
        $state = (!array_key_exists($state, $states)) ? 'all' : $state;
        $method = $states[$state];
        $functionCount = 'getCount'.$method;
        $functionDatas = 'get'.$method;
    
        return $this->render(
            '@JLMOffice/Askquote/index.html.twig',
            $paginator->pagination($request, $this->repository, $functionCount, $functionDatas, 'askquote', ['state' => $state])
        );
    }
    
    /**
     * @Route("/{id}/show", name="askquote_show")
     */
    public function showAction(AskQuote $entity)
    {
        $form = $this->createForm(AskQuoteDontTreatType::class, $entity);

        return $this->render('@JLMOffice/Askquote/show.html.twig', [
            'entity'=>$entity,
            'form_donttreat'=>$form->createView()
        ]);
    }
    
    /**
     * @Route("/new", name="askquote_new")
     */
    public function newAction()
    {
        $askquote = new AskQuote;
        $askquote->setCreation(new \DateTime);
        $form = $this->createForm(AskQuoteType::class, $askquote);

        return $this->render('@JLMOffice/Askquote/new.html.twig', [
            'form' => $form->createView(),
            'entity'=>$askquote
        ]);
    }
    
    /**
     * @Route("/create", name="askquote_create")
     */
    public function createAction(Request $request)
    {
        $entity = new AskQuote;
        $form = $this->createForm(AskQuoteType::class, $entity);
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                if ($entity->getMaturity() === null) {
                    $matu = clone $entity->getCreation();
                    $entity->setMaturity($matu->add(new \DateInterval('P15D')));
                }
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
                return ($this->redirectToRoute('askquote_show', ['id' => $entity->getId()]));
            }
        }
        
        return $this->render('@JLMOffice/Askquote/new.html.twig', ['form' => $form->createView()]);
    }
    
    /**
     * @Route("/{id}/donttreat", name="askquote_donttreat")
     */
    public function donttreatAction(Request $request, AskQuote $entity)
    {
        $form = $this->createForm(AskQuoteDontTreatType::class, $entity);
        
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
            if ($form->isSubmitted() && $form->isValid()) {
                $this->entityManager->persist($entity);
                $this->entityManager->flush();
            }
        }
        return ($this->redirectToRoute('askquote_show', ['id' => $entity->getId()]));
    }
    
    /**
     * @Route("/{id}/canceldonttreat", name="askquote_canceldonttreat")
     */
    public function canceldonttreatAction(AskQuote $entity)
    {
        $entity->setDontTreat();
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('askquote_show', ['id' => $entity->getId()]));
    }
    
    /**
     * Imprimer la liste des demande de devis non-traités
     *
     * @Route("/printlist", name="askquote_printlist")
     */
    public function printlistAction()
    {
        $entities = $this->repository->getUntreated(1000);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename=devis-a-faire.pdf');
        $response->setContent(AskQuoteList::get($entities));

        return $response;
    }
    
    /**
     * Resultats de la barre de recherche.
     *
     * @Route("/search", name="askquote_search")
     */
    public function searchAction(Request $request)
    {
        $formData = $request->get('jlm_core_search');
        $params = [];
        if (is_array($formData) && array_key_exists('query', $formData)) {
            $params = ['entities' => $this->repository->search($formData['query'])];
        }
        
        return $this->render('@JLMOffice/Askquote/index.html.twig', $params);
    }
}
