<?php
namespace JLM\FeeBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\FeeBundle\Entity\Fee;
use JLM\FeeBundle\Form\Type\FeeType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/")
 *
 * @IsGranted("ROLE_OFFICE")
 */
class FeeController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Contract entities.
     *
     * @Route("/", name="fee")
     */
    public function indexAction()
    {
        $entities = $this->entityManager->getRepository(Fee::class)->findAll();

        return $this->render('@JLMFee/Fee/index.html.twig', ['entities' => $entities]);
    }
    
    /**
     * Finds and displays a Fee entity.
     *
     * @Route("/{id}/show", name="fee_show")
     */
    public function showAction(Fee $entity)
    {
        return $this->render('@JLMFee/Fee/show.html.twig', [
            'entity' => $entity,
        ]);
    }
    
    /**
     * Displays a form to create a new Fee entity.
     *
     * @Route("/new", name="fee_new")
     */
    public function newAction()
    {
        $entity = new Fee();

        $form = $this->createForm(FeeType::class, $entity);

        return $this->render(
            '@JLMFee/Fee/new.html.twig',
            [
                'entity' => $entity,
                'form' => $form->createView()
            ]
        );
    }
    
    /**
     * Creates a new Fee entity.
     *
     * @Route("/create", name="fee_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $entity  = new Fee();
        $form    = $this->createForm(FeeType::class, $entity);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
    
            return ($this->redirectToRoute('fee_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMFee/Fee/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }
    
    /**
     * Displays a form to edit an existing Fee entity.
     *
     * @Route("/{id}/edit", name="fee_edit")
     */
    public function editAction(Fee $entity)
    {
        $editForm = $this->createForm(FeeType::class, $entity);
    
        return $this->render('@JLMFee/Fee/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }
    
    /**
     * Edits an existing Fee entity.
     *
     * @Route("/{id}/update", name="fee_update", methods={"POST"})
     */
    public function updateAction(Request $request, Fee $entity)
    {
        $editForm   = $this->createForm(FeeType::class, $entity);
        $editForm->handleRequest($request);
    
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
    
            return ($this->redirectToRoute('fee_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMFee/Fee/edit.html.twig', [
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
        ]);
    }
}
