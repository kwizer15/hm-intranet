<?php

namespace JLM\DailyBundle\Builder\Email;

class FixingDistributedMailBuilder extends FixingMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Intervention distribuée');
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'Le technicien est en route'
        .$this->getSignature());
    }
    
    public function buildAttachements()
    {
    }
}
