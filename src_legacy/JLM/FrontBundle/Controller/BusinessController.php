<?php

namespace JLM\FrontBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\Quote;
use JLM\DailyBundle\Entity\Intervention;
use JLM\FrontBundle\Form\Type\AskQuoteType;
use JLM\FrontBundle\Mailer\TwigSwiftMailer;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use JLM\DailyBundle\Entity\Maintenance;
use JLM\ModelBundle\Entity\Trustee;
use JLM\ModelBundle\Entity\Site;
use Symfony\Component\HttpFoundation\Request;

class BusinessController extends Controller
{
    public function tabAction()
    {
    }
    
    public function listAction(Request $request)
    {
        $manager = $this->getConnectedManager($request);
        $repoSite = $this->entityManager->getRepository(Site::class);
        if ($manager instanceof Trustee) {
            $sites = $repoSite->getByManager($manager);
            $activeBusinessId = sizeof($sites) ? $request->get('business', reset($sites)->getId()) : null;
            try {
                $activeBusiness = $repoSite->find($activeBusinessId);
                if (!$activeBusiness->hasContractWith($manager)) {
                    $activeBusinessId = sizeof($sites) ? reset($sites)->getId() : null;
                    $activeBusiness = $repoSite->find($activeBusinessId);
                }
                // Vérifier si l'affaire fait partie du syndic
            } catch (\Doctrine\ORM\NoResultException $e) {
                throw $this->createNotFoundException('Cette affaire n\'existe pas');
            }
        }
        if ($manager instanceof Site) {
            $activeBusiness = $manager;
            $sites = [$activeBusiness];
            $manager = $activeBusiness->getTrustee();
        }

        // Filtre pour les contrats actuels
        $doors = $activeBusiness->getDoors();
        $businessDoors    = [];
        $lastsMaintenance = [];
        $lastsFixing      = [];
        $askQuoteForms    = [];
        $qs = [];
        foreach ($doors as $key => $door) {
            $contract = $door->getActualContract();
            if ($contract !== null && $contract->getTrustee() == $manager) {
                $businessDoors[$key] = $door;
                $lastsMaintenance[$key] = $this->entityManager->getRepository(Maintenance::class)->getLastsByDoor($door, 2);
                $lastsFixing[$key] = $this->entityManager->getRepository(Intervention::class)->getLastsByDoor($door, 5, true);
                usort(
                    $lastsFixing[$key],
                    function ($a, $b) {
                            return ($a->getLastDate() < $b->getLastDate())
                                ? 1
                                : (($a->getLastDate() == $b->getLastDate()) ? 0 : -1)
                            ;
                    }
                );
                $lastsFixing[$key] = array_filter($lastsFixing[$key], function ($item) {
                    return !$item instanceof Maintenance;
                });
                $qs[$key] = $this->entityManager->getRepository(Quote::class)->getSendedByDoor($door, 12);
                $askQuoteForms[$key] = [];
                foreach ($qs[$key] as $quote) {
                    $form = $this->createAskQuoteForm();
                    $form->get('quoteNumber')->setData($quote->getNumber());
                    $askQuoteForms[$key][] = $form->createView();
                }
            }
        }
        
        return $this->render('@JLMFront/Business/list.html.twig', [
                'manager' => $manager,
                'businesses' => $sites,
                'activeBusiness' => $activeBusiness,
                'doors' => $businessDoors,
                'lastsMaintenance' => $lastsMaintenance,
                'lastsFixing' => $lastsFixing,
                'quotes' => $qs,
                'askQuoteForms' => $askQuoteForms,
        ]);
    }
    
    public function askquoteAction(TwigSwiftMailer $mailer, Request $request)
    {
        $form = $this->createAskQuoteForm();
        $form->handleRequest($request);
    
        if ($success = ($form->isSubmitted() && $form->isValid())) {
            $mailer->sendAskQuoteEmailMessage($form->getData());
            $mailer->sendConfirmAskQuoteEmailMessage($form->getData());
        }
         
        return new JsonResponse(['success' => $success]);
    }
    
    private function createAskQuoteForm()
    {
        $form = $this->createForm(AskQuoteType::class, null, [
            'action' => $this->generateUrl('jlm_front_business_askquote'),
            'method' => 'POST',
        ]);
        $form->add('submit', SubmitType::class);
        
        return $form;
    }
}
