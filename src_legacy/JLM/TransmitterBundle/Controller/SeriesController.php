<?php

namespace JLM\TransmitterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\TransmitterBundle\Entity\Series;
use JLM\TransmitterBundle\Entity\Attribution;
use JLM\TransmitterBundle\Form\Type\SeriesType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/series")
 * @IsGranted("ROLE_OFFICE")
 */
class SeriesController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Displays a form to create a new Series entity.
     *
     * @Route("/new/{id}", name="transmitter_series_new")
     */
    public function newAction(Attribution $attribution)
    {

        $entity = new Series();
        $entity->setAttribution($attribution);
        $form   = $this->createForm(SeriesType::class, $entity, ['id' => $attribution->getSite()->getId()]);
        $form->add('submit', SubmitType::class, ['label'=>'Enregistrer']);
        
        return $this->render('@JLMTransmitter/Series/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a new Series entity.
     *
     * @Route("/create/{id}", name="transmitter_series_create", methods={"POST"})
     */
    public function createAction(Request $request, Attribution $attribution)
    {

        $entity  = new Series();
        $form = $this->createForm(SeriesType::class, $entity, ['id' => $attribution->getSite()->getId()]);
        $form->add('submit', SubmitType::class, ['label'=>'Enregistrer']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transmitters = $entity->getTransmitters();
            foreach ($transmitters as $transmitter) {
                $this->entityManager->persist($transmitter);
            }
            $this->entityManager->flush();
            
            // On met à jour la page de base
            return new JsonResponse([]);
        }

        return $this->render('@JLMTransmitter/Series/create.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }
}
