<?php

namespace JLM\ProductBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ProductBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\ProductBundle\Entity\Supplier;
use JLM\ProductBundle\Form\Type\SupplierType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class SupplierController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Supplier entities.
     */
    public function indexAction($page = 1)
    {
        $limit = 15;
        $repository = $this->entityManager->getRepository(Supplier::class);
        $nb = $repository->getTotal();
        $nbPages = ceil($nb/$limit);
        $nbPages = ($nbPages < 1) ? 1 : $nbPages;
        $offset = ($page-1) * $limit;
        if ($page < 1 || $page > $nbPages) {
            throw $this->createNotFoundException('Page insexistante (page '.$page.'/'.$nbPages.')');
        }

        $entities = $repository->getAll($limit, $offset);

        return $this->render('@JLMProduct/Supplier/index.html.twig', [
            'entities' => $entities,
            'page'     => $page,
            'nbPages'  => $nbPages,
        ]);
    }

    /**
     * Finds and displays a Supplier entity.
     */
    public function showAction(Supplier $entity)
    {
        $products = $this->entityManager->getRepository(Product::class)->findBy(
            ['supplier' => $entity],
            ['designation'=>'asc']
        );
        
        return $this->render('@JLMProduct/Supplier/show.html.twig', [
            'entity'      => $entity,
            'products'    => $products,
        ]);
    }

    /**
     * Displays a form to create a new Supplier entity.
     */
    public function newAction()
    {
        $entity = new Supplier();
        $form   = $this->createNewForm($entity);

        return $this->render('@JLMProduct/Supplier/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Creates a new Supplier entity.
     */
    public function createAction(Request $request)
    {
        $entity  = new Supplier();

        $form = $this->createNewForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('supplier_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMProduct/Supplier/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Supplier entity.
     */
    public function editAction($id)
    {
        $entity = $entity = $this->getEntity($id);

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@JLMProduct/Supplier/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Edits an existing Supplier entity.
     * @Secure(roles="ROLE_OFFICE")
     */
    public function updateAction(Request $request, $id)
    {
        $entity = $this->getEntity($id);

        $editForm   = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity->getAddress());
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('supplier_show', ['id' => $id]));
        }

        return $this->render('@JLMProduct/Supplier/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a Supplier entity.
     *
     * @Secure(roles="ROLE_OFFICE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity = $this->getEntity($id);

            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }

        return ($this->redirectToRoute('supplier'));
    }

    /**
     * Create a delete form
     * @param integer $id
     * @return \Symfony\Component\Form\Form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
    
    /**
     * Get the entity from id
     * @param integer $id
     * @return Supplier
     */
    private function getEntity($id)
    {
        $entity = $this->entityManager->getRepository(Supplier::class)->find($id);
        
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Supplier entity.');
        }
        
        return $entity;
    }
    
    /**
     * Create a new form
     * @param Supplier $entity
     * @return \Symfony\Component\Form\Form
     */
    private function createNewForm(Supplier $entity)
    {
        return $this->createForm(SupplierType::class, $entity);
    }
    
    /**
     * Create an edit form
     * @param Supplier $entity
     * @return \Symfony\Component\Form\Form
     */
    private function createEditForm(Supplier $entity)
    {
        return $this->createForm(SupplierType::class, $entity);
    }
}
