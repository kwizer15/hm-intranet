<?php

/*
 * This file is part of the GenemuFormBundle package.
 *
 * (c) Olivier Chauvel <olivier@generation-multiple.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Genemu\Bundle\FormBundle\Controller;

use Genemu\Bundle\FormBundle\Gd\Type\Captcha;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class Base64Controller
 *
 * @author Olivier Chauvel <olivier@generation-multiple.com>
 */
class Base64Controller extends AbstractController
{
    public function refreshCaptchaAction(Captcha $captcha, SessionInterface $session)
    {
        $options = $session->get('genemu_form.captcha.options', []);
        $captcha->setOptions($options);
        $datas = preg_split('([;,]{1})', substr($captcha->getBase64(), 5));

        return new Response(base64_decode($datas[2]), 200, ['Content-Type' => $datas[0]]);
    }

    public function base64Action(Request $request)
    {
        $query = $request->server->get('QUERY_STRING');
        $datas = preg_split('([;,]{1})', $query);

        return new Response(base64_decode($datas[2]), 200, ['Content-Type' => $datas[0]]);
    }
}
