<?php
namespace JLM\ModelBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

abstract class ObjectToIntTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * Transforms an object to an int.
     *
     * @param  Object|null $entity
     * @return int
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return "";
        }
        return $entity->getId();
    }
    
    /**
     * Transforms an int to an object.
     *
     * @param  int $id
     * @return Object|null
     * @throws TransformationFailedException if object is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }
    
        
            $entity = $this->objectManager
                ->getRepository($this->getClass())
                ->find($id)
            ;
        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                $this->getErrorMessage(),
                $id
            ));
        }
    
        return $entity;
    }
    
    abstract public function getClass();
    
    protected function getErrorMessage()
    {
        return 'A '.$this->getClass.' object with id "%s" does not exist!';
    }
}
