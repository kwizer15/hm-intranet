<?php
namespace JLM\TransmitterBundle\Form\DataTransformer;

use JLM\ModelBundle\Form\DataTransformer\ObjectToIntTransformer;
use JLM\TransmitterBundle\Entity\Attribution;

class AttributionToIntTransformer extends ObjectToIntTransformer
{
    public function getClass()
    {
        return Attribution::class;
    }
    
    protected function getErrorMessage()
    {
        return 'A transmitter attribution with id "%s" does not exist!';
    }
}
