<?php

namespace HMFermeture\Infrastructure\Repository\Devis;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\FetchMode;
use HMFermeture\Domain\Devis\Entity\Responsable;
use HMFermeture\Domain\Devis\Repository\Exception\ResponsableNotFoundException;
use HMFermeture\Domain\Devis\Repository\ResponsableRepository;

class DoctrineConnectionResponsableRepository implements ResponsableRepository
{
    /**
     * @var Connection
     */
    private $connection;

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getByUsername(string $username): Responsable
    {
        $statement = $this->connection->prepare(<<<SQL
SELECT name
FROM jlm_contact_contact 
INNER JOIN users ON jlm_contact_contact.id = users.contact_id
WHERE users.username = :username
SQL
        );

        if (false === $statement->execute(['username' => $username])) {
            throw new ResponsableNotFoundException("Aucun responsable n'a été trouvé pour le nom d'utilisateur \"{$username}\".");
        }

        $result = $statement->fetch(FetchMode::ASSOCIATIVE);
        if (false === $result) {
            throw new ResponsableNotFoundException("Aucun responsable n'a été trouvé pour le nom d'utilisateur \"{$username}\".");
        }

        return new Responsable($result['name']);
    }
}
