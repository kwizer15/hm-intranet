<?php
namespace JLM\ModelBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ModelBundle\Entity\Door;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CommerceBundle\Factory\BillFactory;
use JLM\ModelBundle\Builder\DoorBillBuilder;
use JLM\CoreBundle\Event\FormPopulatingEvent;

class BillSubscriber implements EventSubscriberInterface
{
   
    private $objectManager;
    private $form;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public static function getSubscribedEvents()
    {
        return [
            JLMCommerceEvents::BILL_FORM_POPULATE => 'populateFromDoor',
        ];
    }
    
    public function populateFromDoor(FormPopulatingEvent $event)
    {
        if (null !== $id = $event->getParam('installation')) {
            $install = $this->objectManager->getRepository(Door::class)->find($id);
            $entity = BillFactory::create(new DoorBillBuilder($install));
            $event->getForm()->setData($entity);
        }
    }
}
