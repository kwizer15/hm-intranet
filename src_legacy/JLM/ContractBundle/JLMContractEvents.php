<?php

namespace JLM\ContractBundle;

final class JLMContractEvents
{
    const AFTER_CONTRACT_CREATE = "jlm_contract.after_contract_create";
}
