<?php

namespace JLM\CoreBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Form\Handler\DoctrineHandler;
use Symfony\Component\DependencyInjection\Exception\LogicException;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpFoundation\Request;

class BaseManager implements ManagerInterface
{
    protected $class;

    /**
     * @var EntityManagerInterface
     */
    protected $objectManager;

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;

    public function __construct(
        string $class,
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        FormFactoryInterface $formFactory
    ) {
        $this->class = $class;
        $this->objectManager = $entityManager;
        $this->router = $router;
        $this->formFactory = $formFactory;
    }
    
    protected function getFormParam($name, $options = [])
    {
        return null;
    }

    protected function setterFromRequest($param, $repoName, Request $request)
    {
        if ($id = $request->get($param)) {
            return $this->objectManager->getRepository($repoName)->find($id);
        }

        return null;
    }

    public function createForm(Request $request, $name, $options = [])
    {
        $param = $this->getFormParam($name, $options);
        if ($param !== null) {
            // @todo Temporaire, mauvaise gestion des methodes en prod
            if ($param['method'] != 'GET') {
                $param['method'] = 'POST';
            }
            $form = $this->formFactory->create(
                $param['type'],
                $param['entity'],
                [
                    'action' => $this->router->generate($param['route'], $param['params']),
                    'method' => $param['method'],
                ]
            );
            $form->add('submit', SubmitType::class, ['label' => $param['label']]);

            return $this->populateForm($form, $request);
        }
        
        throw new LogicException('HTTP request method must be POST, PUT or DELETE only');
    }
    
    public function populateForm($form, Request $request)
    {
        return $form;
    }

    public function getHandler($form, Request $request, $entity = null)
    {
        return new DoctrineHandler($form, $request, $this->objectManager, $entity);
    }
}
