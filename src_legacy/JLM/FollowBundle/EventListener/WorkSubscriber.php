<?php

namespace JLM\FollowBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JLM\FollowBundle\Entity\Thread;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\DailyBundle\JLMDailyEvents;
use JLM\CoreBundle\Event\DoctrineEvent;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

class WorkSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            JLMDailyEvents::WORK_PREREMOVE => 'removeThread',
            JLMDailyEvents::WORK_POSTUPDATE => 'updateThread',
        ];
    }
    
    /**
     * Update thread since work update
     * @param InterventionEvent $event
     */
    public function updateThread(DoctrineEvent $event)
    {
        if ($thread = $this->getThread($event)) {
            $thread->getState();
            $this->objectManager->persist($thread);
            $this->objectManager->flush();
        }
    }
    
    /**
     * Update thread since work update
     * @param InterventionEvent $event
     */
    public function removeThread(DoctrineEvent $event)
    {
        $thread = $this->getThread($event);
        if ($thread) {
            if ($thread->getStarter() !== null) {
                $this->objectManager->remove($thread->getStarter());
            }
            $this->objectManager->remove($thread);
            $this->objectManager->flush();
        }
    }
    
    /**
     *
     * @param DoctrineEvent $event
     * @return mixed|NULL
     */
    private function getThread(DoctrineEvent $event)
    {
        try {
            return $this->objectManager->getRepository(Thread::class)->getByWork($event->getEntity());
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
