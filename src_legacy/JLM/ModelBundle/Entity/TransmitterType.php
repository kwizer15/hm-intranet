<?php

namespace JLM\ModelBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class TransmitterType
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotNull
     * @Assert\Type(type="string")
     * @Assert\NotBlank
     */
    private $name = '';
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set text
     *
     * @param string $text
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }
    
    /**
     * Get text
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    /**
     * To String
     */
    public function __toString()
    {
        return $this->getName();
    }
}
