<?php

namespace JLM\ContractBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContractBundle\Entity\Contract;
use JLM\CoreBundle\Manager\BaseManager as Manager;
use JLM\ContractBundle\Form\Type\ContractType;
use JLM\ContractBundle\Form\Type\ContractStopType;
use JLM\ModelBundle\Entity\Door;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class ContractManager extends Manager
{
    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        FormFactoryInterface $formFactory
    ) {
        parent::__construct(
            Contract::class,
            $entityManager,
            $router,
            $formFactory
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function getFormParam($name, $options = [])
    {
        switch ($name) {
            case 'new':
                return [
                    'method' => 'POST',
                    'route' => 'jlm_contract_contract_create',
                    'params' => [],
                    'label' => 'Créer',
                    'type'  => ContractType::class,
                    'entity' => null,
                ];
            case 'edit':
                return [
                    'method' => 'POST',
                    'route' => 'jlm_contract_contract_update',
                    'params' => ['id' => $options['entity']->getId(), 'formName'=>'edit'],
                    'label' => 'Modifier',
                    'type'  => ContractType::class,
                    'entity' => $options['entity'],
                ];
            case 'delete':
                return [
                    'method' => 'DELETE',
                    'route' => 'jlm_contract_contract_delete',
                    'params' => ['id' => $options['entity']->getId()],
                    'label' => 'Supprimer',
                    'type'  => 'form',
                    'entity' => $options['entity'],
                ];
            case 'stop':
                return [
                        'method' => 'PUT',
                        'route' => 'jlm_contract_contract_update',
                        'params' => ['id' => $options['entity']->getId(), 'formName'=>'stop'],
                        'label' => 'Arrêter',
                        'type'  => ContractStopType::class,
                        'entity' => $options['entity'],
                ];
        }
    
        return parent::getFormParam($name, $options);
    }
    
    /**
     * {@inheritdoc}
     */
    public function populateForm($form, Request $request)
    {
        if ($form->getName() == 'jlm_contract_contract') {
            $door = $this->setterFromRequest('door', Door::class, $request);
            if ($door) {
                $form->get('door')->setData($door);
                $form->get('trustee')->setData($door->getSite()->getTrustee());
            }
            $begin = $form->get('begin');
            if (!$begin->getData()) {
                $begin->setData(new \DateTime);
            }
        }
        
        return parent::populateForm($form, $request);
    }
    
    public function getEditUrl($id)
    {
        return $this->router->generate('jlm_contract_contract_edit', ['id' => $id]);
    }
}
