<?php

namespace Tests\JLM\FeeBundle\DependencyInjection;

use JLM\FeeBundle\DependencyInjection\JLMFeeExtension;

class JLMFeeExtensionTest extends \PHPUnit\Framework\TestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->ext = new JLMFeeExtension();
    }
    
    public function assertPreConditions()
    {
        $this->assertInstanceOf('Symfony\Component\HttpKernel\DependencyInjection\Extension', $this->ext);
    }
    
    public function testLoad()
    {
        $this->ext->load([], $this->createMock('Symfony\Component\DependencyInjection\ContainerBuilder'));
    }
}
