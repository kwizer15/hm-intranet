<?php

namespace JLM\OfficeBundle\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;
use JLM\DailyBundle\Entity\Work;
use JLM\OfficeBundle\Entity\Order;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class OrderCountExtension extends AbstractExtension implements GlobalsInterface
{
    private $objectManager;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public function getName()
    {
        return 'ordercount_extension';
    }
    
    public function getGlobals()
    {
        $repo = $this->objectManager->getRepository(Order::class);
        
        return ['ordercount' => [
                'todo' => $this->objectManager->getRepository(Work::class)->getCountOrderTodo(),
                'all' => $repo->getTotal(),
                'input' => $repo->getCount(0),
                'ordered' => $repo->getCount(1),
                'ready' => $repo->getCount(2),
        ]];
    }
}
