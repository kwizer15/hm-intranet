<?php

namespace JLM\ProductBundle\Repository;

use Doctrine\ORM\EntityRepository;
use JLM\ProductBundle\Model\ProductInterface;

class StockRepository extends EntityRepository
{
   
    public function getCount()
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('COUNT(a)')
        ;

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }
    
    public function getAll($limit = 10, $offset = 0)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a,b')
            ->leftJoin('a.product', 'b')
            ->orderBy('b.designation', 'ASC')
            ->setFirstResult($offset)
            ->setMaxResults($limit)
        ;
        
        return $queryBuilder->getQuery()->getResult();
    }
    
    public function getByProduct(ProductInterface $product)
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a')
            ->where('a.product = ?1')
            ->setParameter(1, $product)
        ;
        
        return $queryBuilder->getQuery()->getSingleResult();
    }
}
