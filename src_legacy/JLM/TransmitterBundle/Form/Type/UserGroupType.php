<?php

namespace JLM\TransmitterBundle\Form\Type;

use JLM\ModelBundle\Form\Type\SiteHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\TransmitterBundle\Entity\UserGroup;

class UserGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('site', SiteHiddenType::class)
            ->add('name', null, ['label'=>'Nom du groupe'])
            ->add('model', null, ['label'=>'Type d\'émetteurs'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => UserGroup::class,
            'attr'=>['class'=>'usergroup'],
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_transmitterbundle_usergrouptype';
    }
}
