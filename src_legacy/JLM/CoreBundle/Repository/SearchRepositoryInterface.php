<?php

namespace JLM\CoreBundle\Repository;

interface SearchRepositoryInterface
{
    public function search($query);
}
