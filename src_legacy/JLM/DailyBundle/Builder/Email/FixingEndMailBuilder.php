<?php

namespace JLM\DailyBundle\Builder\Email;

class FixingEndMailBuilder extends FixingMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Intervention #'.$this->getFixing()->getId().' Intervention terminée');
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'Nous vous informons que le technicien à terminé son intervention.'.PHP_EOL
        .'L\'installation est '.$this->getStringState().$this->getStringTodo().PHP_EOL
        .PHP_EOL
        .'Cordialement'
        .$this->getSignature());
    }
    
    public function buildAttachements()
    {
    }
    
    protected function getStringState()
    {
        return ($this->getFixing()->getDoor()->isStopped()) ? 'à l\'arrêt' : 'en service';
    }

    protected function getStringTodo()
    {
        $hasWork = $this->getFixing()->hasWork();
        $hasAskQuote = $this->getFixing()->hasAskQuote();
        $out = ($hasWork || $hasAskQuote) ? ' et nécessite ' : '';
        if ($hasWork) {
            $out .= 'une intervention ultérieure';
        }
        if ($hasAskQuote) {
            if ($hasWork) {
                $out .= ' et ';
            }
            $out .= 'un devis qui vous sera envoyé dans les plus brefs délais';
        }
        $out .= '.';
        
        return $out;
    }
}
