<?php

namespace JLM\CondominiumBundle\Model;

interface AdministratorMemberInterface
{
    /**
     * Get the administrator
     * @return AdministratorInterface
     */
    public function getAdministrator();
}
