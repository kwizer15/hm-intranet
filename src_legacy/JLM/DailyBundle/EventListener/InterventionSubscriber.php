<?php

namespace JLM\DailyBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JLM\DailyBundle\Entity\WorkCategory;
use JLM\DailyBundle\Entity\WorkObjective;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\DailyBundle\JLMDailyEvents;
use JLM\DailyBundle\Builder\InterventionWorkBuilder;
use JLM\DailyBundle\Event\InterventionEvent;
use JLM\DailyBundle\Factory\WorkFactory;
use JLM\CommerceBundle\Event\QuoteVariantEvent;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\DailyBundle\Builder\VariantWorkBuilder;

class InterventionSubscriber implements EventSubscriberInterface
{
   
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * Constructor
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            JLMDailyEvents::INTERVENTION_SCHEDULEWORK => 'createWorkFromIntervention',
            JLMDailyEvents::INTERVENTION_UNSCHEDULEWORK => 'deleteWorkFromIntervention',
            JLMCommerceEvents::QUOTEVARIANT_GIVEN => 'createWorkFromQuote',
        ];
    }
    
    /**
     * Create work since intervention
     * @param InterventionEvent $event
     */
    public function createWorkFromIntervention(InterventionEvent $event)
    {
        $interv = $event->getIntervention();
        $options = [
            'category' => $this->objectManager->getRepository(WorkCategory::class)->find(1),
            'objective' => $this->objectManager->getRepository(WorkObjective::class)->find(1),
        ];
        $work = WorkFactory::create(new InterventionWorkBuilder($interv, $options));
        $this->objectManager->persist($work);
        $interv->setWork($work);
        $this->objectManager->persist($interv);
        $this->objectManager->flush();
    }
    
    /**
     * Create work since intervention
     * @param InterventionEvent $event
     */
    public function deleteWorkFromIntervention(InterventionEvent $event)
    {
        $interv = $event->getIntervention();
        $work = $interv->getWork();
        $interv->setWork();
        if ($work !== null) {
            $this->objectManager->remove($work);
            $this->objectManager->persist($interv);
            $this->objectManager->flush();
        }
    }
    
    /**
     * @param QuoteVariantEvent $event
     */
    public function createWorkFromQuote(QuoteVariantEvent $event)
    {
        $entity = $event->getQuoteVariant();
        if ($entity->getWork() === null && $entity->getQuote()->getDoor() !== null) {
            // Création de la ligne travaux pré-remplie
            $work = WorkFactory::create(new VariantWorkBuilder($entity, [
                    'category' => $this->objectManager->getRepository(WorkCategory::class)->find(1),
                    'objective' => $this->objectManager->getRepository(WorkObjective::class)->find(1),
            ]));
            $order = $work->getOrder();
            $this->objectManager->persist($order);
            $olines = $order->getLines();
            foreach ($olines as $oline) {
                $oline->setOrder($order);
                $this->objectManager->persist($oline);
            }
            $this->objectManager->persist($work);
            $entity->setWork($work);
            $this->objectManager->flush();
        }
    }
}
