<?php

namespace JLM\ContactBundle\Repository;

use JLM\DefaultBundle\Entity\SearchRepository;

class PersonRepository extends SearchRepository
{
    /**
     * {@inheritdoc}
     */
    protected function getSearchQb()
    {
        return $this->createQueryBuilder('a');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getSearchParams()
    {
        return ['a.firstName','a.lastName'];
    }

    /**
     * @deprecated
     */
    public function match($query)
    {
        return $this->search($query);
    }
    
    /**
     * @deprecated
     * @param unknown $query
     * @param number $limit
     * @return multitype:multitype:string
     */
    public function searchResult($query, $limit = 8)
    {
        $res = $this->search($query);
        $r2 = [];
        foreach ($res as $r) {
            $r2[] = [
                    'id'=>''.$r->getId(),
                    'label'=>''.$r,
            ];
        }
        return $r2;
    }
    
    /**
     *
     * @param string $query
     * @param int $limit
     * @return array
     */
    public function getArray($query, $limit = 8)
    {
        $queryBuilder = $this->createQueryBuilder('c')
        ->where('c.name LIKE :query')
        ->setParameter('query', '%'.$query.'%')
        ;
        $res = $queryBuilder->getQuery()->getArrayResult();
    
        return $res;
    }
    
    /**
     *
     * @param int $id
     * @return array|null
     */
    public function getByIdToArray($id)
    {
        $queryBuilder = $this->createQueryBuilder('c')
        ->where('c.id = :id')
        ->setParameter('id', $id)
        ;
        $res = $queryBuilder->getQuery()->getArrayResult();
    
        if (isset($res[0])) {
            return $res[0];
        }
    
        return [];
    }
}
