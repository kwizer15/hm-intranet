<?php

namespace JLM\DailyBundle\Builder\Email;

class MaintenanceEndMailBuilder extends MaintenanceMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Visite d\'entretien terminée');
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'Le technicien à terminé la visite d\'entretien'.PHP_EOL
        .$this->getSignature());
    }
}
