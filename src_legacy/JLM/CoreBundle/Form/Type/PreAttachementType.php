<?php

namespace JLM\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class PreAttachementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'file';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_core_preattachement';
    }
}
