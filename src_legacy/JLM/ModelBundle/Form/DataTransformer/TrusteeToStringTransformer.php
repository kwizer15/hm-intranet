<?php
namespace JLM\ModelBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ModelBundle\Entity\Trustee;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class TrusteeToStringTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * Transforms an object (trustee) to a string (name).
     *
     * @param  Trustee|null $entity
     * @return string
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return '';
        }
        return $entity.'';
    }
    
    /**
     * Transforms a string (number) to an object (trustee).
     *
     * @param  string $number
     * @return Trustee|null
     * @throws TransformationFailedException if object (trustee) is not found.
     */
    public function reverseTransform($string)
    {
        if (!$string) {
            return null;
        }
            
        
        $entity = $this->objectManager
            ->getRepository(Trustee::class)
            ->findOneBy(['name' => trim($string)])
        ;

        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'A trustee with name "%s" does not exist!',
                $string
            ));
        }
        
        return $entity;
    }
}
