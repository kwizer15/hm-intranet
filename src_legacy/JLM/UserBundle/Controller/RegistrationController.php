<?php
namespace JLM\UserBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Controller\RegistrationController as BaseController;

class RegistrationController extends BaseController
{
    /**
     * @IsGranted("ROLE_OFFICE")
     */
    public function registerAction(Request $request)
    {
        return parent::registerAction($request);
    }
}
