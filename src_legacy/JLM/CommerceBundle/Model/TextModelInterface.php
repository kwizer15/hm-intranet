<?php

namespace JLM\CommerceBundle\Model;

interface TextModelInterface
{
    /**
     * Get text
     *
     * @return string
     */
    public function getText();
    
    /**
     * To string
     *
     * @return string
     */
    public function __toString();
}
