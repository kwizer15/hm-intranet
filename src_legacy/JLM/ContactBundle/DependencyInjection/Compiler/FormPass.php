<?php

namespace JLM\ContactBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Add a new twig.form.resources
 */
class FormPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $resources = $container->getParameter('twig.form.resources');

        foreach (['fields','javascript_layout'] as $template) {
            $resources[] = '@JLMContact/Form/' . $template . '.html.twig';
        }

        $container->setParameter('twig.form.resources', $resources);
    }
}
