<?php

namespace JLM\FollowBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JLM\FollowBundle\Entity\Thread;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\CoreBundle\Event\DoctrineEvent;
use JLM\OfficeBundle\JLMOfficeEvents;
use JLM\FollowBundle\Factory\ThreadFactory;

class OrderSubscriber implements EventSubscriberInterface
{
   
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * Constructor
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            // JLMOfficeEvents::ORDER_POSTPERSIST => 'createThread',
            JLMOfficeEvents::ORDER_POSTUPDATE => 'updateThread',
        ];
    }
    
    /**
     * Update thread since order update
     * @param InterventionEvent $event
     */
    public function createThread(DoctrineEvent $event)
    {
        $entity = $event->getEntity();
        $thread = ThreadFactory::create($entity->getWork());
        $thread->getState();
        $this->objectManager->persist($thread);
        $this->objectManager->flush();
    }
    
    /**
     * Update thread since order update
     * @param InterventionEvent $event
     */
    public function updateThread(DoctrineEvent $event)
    {
        $entity = $event->getEntity();
        $thread = $this->objectManager->getRepository(Thread::class)->getByOrder($entity);
        $this->objectManager->persist($thread);
        $this->objectManager->flush();
    }
}
