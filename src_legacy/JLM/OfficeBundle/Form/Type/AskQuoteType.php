<?php

namespace JLM\OfficeBundle\Form\Type;

use JLM\ModelBundle\Form\Type\DatepickerType;
use JLM\ModelBundle\Form\Type\SiteSelectType;
use JLM\ModelBundle\Form\Type\TrusteeSelectType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\OfficeBundle\Entity\AskQuote;
use JLM\ModelBundle\Entity\Door;

class AskQuoteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('creation', DatepickerType::class, ['label' => 'Date de la demande'])
            ->add('trustee', TrusteeSelectType::class, ['label' => 'Syndic', 'attr' => ['class' => 'input-xlarge']])
            ->add('site', SiteSelectType::class, ['label' => 'Affaire', 'attr' => ['class' => 'input-xxlarge', 'rows' => 5]])
            ->add('door', EntityType::class, [
                'class' => Door::class,
                'placeholder' => 'Affaire complète',
                'label' => 'Installation',
                'attr' => ['class' => 'input-xxlarge'],
                'required' => false,
            ])
            ->add('method', null, ['label' => 'Arrivée par', 'attr' => ['class' => 'input-medium']])
            ->add('maturity', DatepickerType::class, ['label' => 'Date d\'échéance', 'required' => false])
            ->add('ask', null, ['label' => 'Demande', 'attr' => ['class' => 'input-xxlarge', 'rows' => 5]])
            ->add('file', null, ['label' => 'Fichier joint']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AskQuote::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'askquote';
    }
}
