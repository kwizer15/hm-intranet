<?php

namespace JLM\CommerceBundle\Domain\Command;

use JLM\CommerceBundle\Entity\Quote;

class EditQuoteCommand
{
    /**
     * @var Quote
     */
    public $quote;

    public function __construct(Quote $quote)
    {
        $this->quote = $quote;
    }
}
