<?php

namespace JLM\CommerceBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Manager\QuoteVariantManager;
use JLM\CommerceBundle\Pdf\Coding;
use JLM\CommerceBundle\Pdf\Quote;
use JLM\CoreBundle\Form\Type\MailType as CoreMailType;
use JLM\CoreBundle\Response\PdfResponse;
use JLM\ModelBundle\Entity\Mail;
use JLM\ModelBundle\Form\Type\MailType;
use JLM\CommerceBundle\Entity\QuoteVariant;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CommerceBundle\Event\QuoteVariantEvent;
use JLM\CoreBundle\Factory\MailFactory;
use JLM\CommerceBundle\Builder\Email\QuoteVariantConfirmGivenMailBuilder;
use JLM\CoreBundle\Builder\MailSwiftMailBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class QuoteVariantController extends Controller
{
    /**
     * @var QuoteVariantManager
     */
    private $manager;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var EngineInterface
     */
    private $templating;

    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(QuoteVariantManager $manager, EntityManagerInterface $entityManager, Environment $templating)
    {
        $this->manager = $manager;
        $this->entityManager = $entityManager;
        $this->templating = $templating;
        $this->repository = $entityManager->getRepository(QuoteVariant::class);
    }

    /**
     * Displays a form to create a new Variant entity.
     */
    public function newAction(Request $request)
    {
        $form   = $this->manager->createForm($request, 'new');
        if ($this->manager->getHandler($form, $request)->process()) {
            return $this->redirectToRoute('quote_show', ['id' => $form->get('quote')->getData()->getId()]);
        }
        
        return $this->render('@JLMCommerce/QuoteVariant/new.html.twig', [
                'quote' => $form->get('quote')->getData(),
                'entity' => $form->getData(),
                'form'   => $form->createView()
        ]);
    }
    
    /**
     * Displays a form to edit an existing QuoteVariant entity.
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->repository->find($id);
        $this->manager->assertState($entity, [QuoteVariant::STATE_INSEIZURE]);
        $form = $this->manager->createForm($request, 'edit', ['entity' => $entity]);
        if ($this->manager->getHandler($form, $request)->process()) {
            return $this->redirectToRoute('quote_show', ['id' => $entity->getQuote()->getId()]);
        }
        
        return $this->render('@JLMCommerce/QuoteVariant/edit.html.twig', [
            'quote' => $form->get('quote')->getData(),
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * Change entity state and return the redirect show quote response
     * Can send a QuoteVariantEvent if the event name is defined
     * @param int $id
     * @param int $state
     * @param string $event
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    private function changeState(Request $request, EventDispatcherInterface $eventDispatcher, $id, $state, $event = null)
    {
        $entity = $this->repository->find($id);
        if ($entity->setState($state)) {
            if ($event !== null) {
                $eventDispatcher->dispatch($event, new QuoteVariantEvent($entity, $request));
            }
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
        
        return $this->redirectToRoute('quote_show', ['id' => $entity->getQuote()->getId()]);
    }
    
    /**
     * Note QuoteVariant as ready to send.
     */
    public function readyAction($id)
    {
        return $this->changeState($id, QuoteVariant::STATE_READY, JLMCommerceEvents::QUOTEVARIANT_READY);
    }

    /**
     * Note QuoteVariant as not ready.
     */
    public function unvalidAction($id)
    {
        return $this->changeState($id, QuoteVariant::STATE_INSEIZURE, JLMCommerceEvents::QUOTEVARIANT_INSEIZURE);
    }
    
    /**
     * Note QuoteVariant as faxed.
     */
    public function faxAction($id)
    {
        return $this->changeState($id, QuoteVariant::STATE_SENDED, JLMCommerceEvents::QUOTEVARIANT_SENDED);
    }
    
    /**
     * Note QuoteVariant as canceled.
     */
    public function cancelAction($id)
    {
        return $this->changeState($id, QuoteVariant::STATE_CANCELED, JLMCommerceEvents::QUOTEVARIANT_CANCELED);
    }
    
    /**
     * Note QuoteVariant as receipt.
     */
    public function receiptAction($id)
    {
        return $this->changeState($id, QuoteVariant::STATE_RECEIPT, JLMCommerceEvents::QUOTEVARIANT_RECEIPT);
    }
    
    /**
     * Accord du devis / Création de l'intervention
     */
    public function givenAction($id)
    {
        $this->changeState($id, QuoteVariant::STATE_GIVEN, JLMCommerceEvents::QUOTEVARIANT_GIVEN);
        
        return $this->redirectToRoute('variant_email', ['id' => $id]);
    }
    
    /**
     * Email de confirmation d'accord de devis
     */
    public function emailAction(Request $request, \Swift_Mailer $mailer, $id)
    {
        // @todo Passer par un service de formPopulate et créer un controller unique dans CoreBundle
        $entity = $this->repository->find($id);
        $mail = MailFactory::create(new QuoteVariantConfirmGivenMailBuilder($entity));
        $editForm = $this->createForm(CoreMailType::class, $mail);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $mailer->send(MailFactory::create(new MailSwiftMailBuilder($editForm->getData())));
            return $this->redirectToRoute('quote_show', ['id' => $entity->getQuote()->getId()]);
        }
        
        return $this->render('@JLMCommerce/QuoteVariant/email.html.twig', [
                'entity' => $entity,
                'form' => $editForm->createView(),
        ]);
    }
    
    /**
     * Mail
     */
    public function mailAction($id)
    {
        $entity = $this->repository->find($id);
        $this->manager->assertState($entity, [
                QuoteVariant::STATE_READY,
                QuoteVariant::STATE_PRINTED,
                QuoteVariant::STATE_SENDED,
                QuoteVariant::STATE_RECEIPT,
                QuoteVariant::STATE_GIVEN
        ]);
        $mail = new Mail();
        $mail->setSubject('Devis n°'.$entity->getNumber());
        $mail->setFrom('commerce@jlm-entreprise.fr');
        $mail->setBody($this->templating->render('@JLMCommerce/QuoteVariant/email.txt.twig', [
            'intro' => $entity->getIntro(),
            'door' => $entity->getQuote()->getDoorCp()
        ]));
        $mail->setSignature($this->templating->render('@JLMCommerce/QuoteVariant/emailsignature.txt.twig', [
            'name' => $entity->getQuote()->getFollowerCp()
        ]));
        if ($entity->getQuote()->getContact() &&
            $entity->getQuote()->getContact()->getPerson() &&
            $entity->getQuote()->getContact()->getPerson()->getEmail()) {
            $mail->setTo($entity->getQuote()->getContact()->getPerson()->getEmail());
        }
        $form = $this->createForm(MailType::class, $mail);
    
        return $this->render('@JLMCommerce/QuoteVariant/mail.html.twig', [
                'entity' => $entity,
                'form'   => $form->createView()
        ]);
    }
    
    /**
     * Send by mail a QuoteVariant entity.
     */
    public function sendmailAction(Request $request, \Swift_Mailer $mailer, EventDispatcherInterface $eventDispatcher, $id)
    {
        $entity = $this->repository->find($id);
        if ($entity->getState() < QuoteVariant::STATE_READY) {
            return $this->redirectToRoute('quote_show', ['id' => $entity->getQuote()->getId()]);
        }
        
        // Message
        $mail = new Mail;
        $form = $this->createForm(MailType::class, $mail);
        $form->handleRequest($request);
         
        if ($form->isSubmitted() && $form->isValid()) {
            $mail->setBcc('commerce@jlm-entreprise.fr');
            
            $message = $mail->getSwift();
            $message->setReadReceiptTo('commerce@jlm-entreprise.fr');
            $message->attach(\Swift_Attachment::newInstance(
                Quote::get([$entity]),
                $entity->getNumber().'.pdf',
                'application/pdf'
            ))
            ;

            if ($entity->getQuote()->getVat() == $entity->getQuote()->getVatTransmitter()) {
                $message->attach(\Swift_Attachment::fromPath(
                    $this->get('kernel')->getRootDir().'/../web/bundles/jlmcommerce/pdf/attestation.pdf'
                ))
                ;
            }
            $mailer->send($message);
            $entity->setState(QuoteVariant::STATE_SENDED);
            $eventDispatcher->dispatch(JLMCommerceEvents::QUOTEVARIANT_SENDED, new QuoteVariantEvent($entity, $request));
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
        
        return $this->redirectToRoute('quote_show', ['id' => $entity->getQuote()->getId()]);
    }
    
    /**
     * Print a Quote
     */
    public function printAction($id)
    {
        $entity = $this->repository->find($id);

        return new PdfResponse($entity->getNumber(), Quote::get([$entity]));
    }
    
    /**
     * Print a coding
     */
    public function printcodingAction($id)
    {
        $entity = $this->repository->find($id);
        if ($entity->getState() == QuoteVariant::STATE_CANCELED) {
            return $this->redirectToRoute('quote_show', ['id' => $entity->getQuote()->getId()]);
        }

        return new PdfResponse('chiffrage-'.$entity->getNumber(), Coding::get($entity));
    }
    
    public function boostAction()
    {
        $quotes = $this->repository->getToBoost();
        usort($quotes, function ($a, $b) {
            if ($a->getTotalPrice() > $b->getTotalPrice()) {
                return -1;
            } elseif ($a->getTotalPrice() == $b->getTotalPrice()) {
                return 0;
            } else {
                return 1;
            }
        });
        
        return $this->render('@JLMCommerce/QuoteVariant/boost.html.twig', [
                'quotes' => $quotes,
        ]);
    }
}
