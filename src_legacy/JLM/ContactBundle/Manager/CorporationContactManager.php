<?php

namespace JLM\ContactBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\Corporation;
use JLM\ContactBundle\Entity\CorporationContact;
use JLM\ContactBundle\Entity\Person;
use JLM\CoreBundle\Manager\BaseManager as Manager;
use JLM\ContactBundle\Form\Type\CorporationContactType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class CorporationContactManager extends Manager
{
    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        FormFactoryInterface $formFactory
    ) {
        parent::__construct(
            CorporationContact::class,
            $entityManager,
            $router,
            $formFactory
        );
    }

    /**
     * {@inheritdoc}
     */
    protected function getFormParam($name, $options = [])
    {
        switch ($name) {
            case 'new':
                return [
                    'method' => 'POST',
                    'route' => 'jlm_contact_corporationcontact_create',
                    'params' => [],
                    'label' => 'Créer',
                    'type'  => CorporationContactType::class,
                    'entity' => null,
                ];
            case 'edit':
                return [
                    'method' => 'POST',
                    'route' => 'jlm_contact_corporationcontact_update',
                    'params' => ['id' => $options['entity']->getId()],
                    'label' => 'Modifier',
                    'type'  => CorporationContactType::class,
                    'entity' => $options['entity'],
                ];
            case 'delete':
                return [
                    'method' => 'DELETE',
                    'route' => 'jlm_contact_corporationcontact_delete',
                    'params' => ['id' => $options['entity']->getId()],
                    'label' => 'Supprimer',
                    'type'  => 'form',
                    'entity' => $options['entity'],
                ];
        }

        return parent::getFormParam($name, $options);
    }
    
    /**
     * {@inheritdoc}
     */
    public function populateForm($form, Request $request)
    {
        $params = [
            'corporation' => Corporation::class,
            'person' => Person::class,
        ];
        foreach ($params as $param => $repo) {
            if ($data = $this->setterFromRequest($param, $repo, $request)) {
                $form->get($param)->setData($data);
            }
        }
    
        return $form;
    }
    
    public function getEditUrl($id)
    {
        return $this->router->generate('jlm_contact_corporationcontact_edit', ['id' => $id]);
    }
    
    public function getDeleteUrl($id)
    {
        return $this->router->generate('jlm_contact_corporationcontact_confirmdelete', ['id' => $id]);
    }
}
