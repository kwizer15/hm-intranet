<?php

namespace JLM\CommerceBundle\Builder\Email;

class QuoteVariantConfirmGivenMailBuilder extends QuoteVariantMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Confirmation de réception de votre accord sur devis n°'
            .$this->getQuoteVariant()->getNumber());
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
            .'Nous accusons bonne réception de votre accord sur le devis n°'
            .$this->getQuoteVariant()->getNumber().'.' .PHP_EOL
            .'Nous vous tiendrons informé de la date d\'exécution des travaux.'.PHP_EOL.PHP_EOL
            .'Cordialement'
            .$this->getSignature());
    }
}
