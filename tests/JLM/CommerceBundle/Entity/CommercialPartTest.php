<?php

namespace Tests\JLM\CommerceBundle\Entity;

class CommercialPartTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Mock CommercialPart
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = $this->getMockForAbstractClass('JLM\CommerceBundle\Entity\CommercialPart');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
         $this->assertInstanceOf('JLM\CommerceBundle\Model\CommercialPartInterface', $this->entity);
    }
    
    public function getAttributes()
    {
        return [
            ['Creation', $this->createMock('DateTime')],
            ['Number', '123456'],
            ['Customer', $this->createMock('JLM\CommerceBundle\Model\CustomerInterface')],
            ['CustomerName', 'Foo'],
            ['CustomerAddress', 'Bar'],
            ['Vat', 19.6],
            
            // Deprecated
            ['Trustee',  $this->createMock('JLM\CommerceBundle\Model\CustomerInterface')],
            ['TrusteeName', 'Foo'],
            ['TrusteeAddress', 'Bar'],
            ['VatTransmitter', 19.6],
        ];
    }
    
    /**
     * Test getters and setters
     * @param string $attribute
     * @param mixed $value
     * @dataProvider getAttributes
     */
    public function testGettersSetters($attribute, $value)
    {
        $getter = 'get'.$attribute;
        $setter = 'set'.$attribute;
        $this->assertSame($this->entity, $this->entity->$setter($value));
        $this->assertSame($value, $this->entity->$getter());
    }
}
