<?php

namespace Tests\JLM\ContactBundle\Entity;

use JLM\ContactBundle\Entity\Association;

class AssociationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Person
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new Association;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf('JLM\ContactBundle\Model\AssociationInterface', $this->entity);
        $this->assertNull($this->entity->getId());
        $this->assertCount(0, $this->entity->getContacts());
    }

    public function getAttributes()
    {
        return [
            ['Name', 'Foo'],
            ['Email', 'commerce@jlm-entreprise.fr'],
            ['Address', $this->createMock('JLM\ContactBundle\Model\AddressInterface')],
        ];
    }
    
    /**
     * Test getters and setters
     * @param string $attribute
     * @param mixed $value
     * @dataProvider getAttributes
     */
    public function testGettersSetters($attribute, $value)
    {
        $getter = 'get'.$attribute;
        $setter = 'set'.$attribute;
        $this->assertSame($this->entity, $this->entity->$setter($value));
        $this->assertSame($value, $this->entity->$getter());
    }
    
    public function getAdderRemover()
    {
        return [
            ['Phone', 'Phones', $this->createMock('JLM\ContactBundle\Model\ContactPhoneInterface')],
            ['Contact', 'Contacts', $this->createMock('JLM\ContactBundle\Model\CorporationContactInterface')],
        ];
    }
    
    /**
     * @dataProvider getAdderRemover
     */
    public function testAdderRemover($attribute, $attributes, $value)
    {
        $getters = 'get'.$attributes;
        $adder = 'add'.$attribute;
        $remover = 'remove'.$attribute;
        $this->assertCount(0, $this->entity->$getters());
        $this->assertFalse($this->entity->$remover($value));
        $this->assertTrue($this->entity->$adder($value));
        $this->assertCount(1, $this->entity->$getters());
        $this->assertTrue($this->entity->$remover($value));
        $this->assertCount(0, $this->entity->$getters());
    }
}
