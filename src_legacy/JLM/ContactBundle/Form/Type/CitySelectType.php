<?php

namespace JLM\ContactBundle\Form\Type;

use JLM\CoreBundle\Form\Type\AbstractSelectType;
use JLM\ContactBundle\Form\DataTransformer\CityToIntTransformer;

class CitySelectType extends AbstractSelectType
{
    /**
     * {@inheritdoc}
     */
    protected function getTransformerClass(): string
    {
        return CityToIntTransformer::class;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getTypeName(): string
    {
        return 'jlm_contact_city';
    }
}
