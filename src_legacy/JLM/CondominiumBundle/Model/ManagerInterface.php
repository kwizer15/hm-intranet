<?php

namespace JLM\CondominiumBundle\Model;

interface ManagerInterface
{
    /**
     * Get the name
     * @return string
     */
    public function getName();
}
