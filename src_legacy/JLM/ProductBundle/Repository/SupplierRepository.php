<?php

namespace JLM\ProductBundle\Repository;

use JLM\DefaultBundle\Entity\SearchRepository;

class SupplierRepository extends SearchRepository
{
    /**
     * {@inheritdoc}
     */
    protected function getSearchQb()
    {
        return $this->createQueryBuilder('a')
            ->select('a,b,c,d,h,i')
            ->leftJoin('a.contact', 'b')
                ->leftJoin('b.phones', 'c')
                    ->leftJoin('c.phone', 'd')
                ->leftJoin('b.address', 'h')
                    ->leftJoin('h.city', 'i')
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getSearchParams()
    {
        return ['b.name'];
    }
    
    /**
     * Search a query
     * @param string $query
     * @param integer $limit
     * @return array
     */
    public function searchResult($query, $limit = 8)
    {
        $res = $this->search($query);
        $r2 = [];
        foreach ($res as $r) {
            $r2[] = ''.$r;
        }
        return $r2;
    }
    
    /**
     * Count total suppliers
     * @return integer
     */
    public function getTotal()
    {
        $queryBuilder = $this->createQueryBuilder('s')->select('COUNT(s)');
        
        return (int) $queryBuilder->getQuery()->getSingleScalarResult();
    }
    
    public function getAll($limit, $offset)
    {
        $query = $this->createQueryBuilder('a')
            ->leftJoin('a.contact', 'b')
            ->orderBy('b.name', 'ASC')
            ->setMaxResults($limit)
            ->setFirstResult($offset)
        ;
            
        return $query->getQuery()->getResult();
    }
}
