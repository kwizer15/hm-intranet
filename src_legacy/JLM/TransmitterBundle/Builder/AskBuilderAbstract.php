<?php

namespace JLM\TransmitterBundle\Builder;

use JLM\TransmitterBundle\Entity\Ask;

abstract class AskBuilderAbstract implements AskBuilderInterface
{
    /**
     * @var Quote
     */
    protected $ask;
    
    /**
     * @var array
     */
    protected $options;
    
    /**
     * {@inheritdoc}
     */
    public function getAsk()
    {
        return $this->ask;
    }
    
    /**
     * {@inheritdoc}
     */
    public function create()
    {
        $this->ask = new Ask();
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildCreation()
    {
        $this->ask->setCreation(new \DateTime);
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildTrustee()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildSite()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildMethod()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildMaturity()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildPerson()
    {
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildAsk()
    {
    }
    
    public function __construct($options = [])
    {
        $this->options = $options;
    }
    
    protected function getOptions()
    {
        return $this->options;
    }
    
    protected function getOption($key)
    {
        if (isset($this->options[$key])) {
            return $this->options[$key];
        }
        
        return null;
    }
}
