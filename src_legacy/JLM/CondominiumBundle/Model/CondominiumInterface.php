<?php

namespace JLM\CondominiumBundle\Model;

interface CondominiumInterface
{

    /**
     * Get the name
     * ex : Résidence de la Dhuys
     * @return string
     */
    public function getName();
    
    /**
     * Get the administrator
     * ex : SDC Résidence de la Dhuys
     * @return AdministratorInterface
     */
    public function getAdministrator();
}
