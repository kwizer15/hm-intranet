<?php

namespace JLM\CommerceBundle\Pdf;

class Coding extends \FPDF
{
    private $entity;

    public static function get($entity)
    {
        $pdf = new self();
        $pdf->setEntity($entity);
        $pdf->init();

        return $pdf->Output('', 'S');
    }

    private function setEntity($entity)
    {
        $this->entity = $entity;

        return $this;
    }

    private function init()
    {
        $this->aliasNbPages();
        $this->addPage('L');

        $content = array_fill(0, 4, array_fill(0, 13, ''));
        $fill = [
            [255, 204, 153],
            [255, 204, 153],
            [252, 213, 180],
            [252, 213, 180],
        ];
        $content[0][1] = utf8_decode(str_replace(PHP_EOL, ' / ', $this->entity->getQuote()->getDoorCp()));
        $content[1][1] = $this->entity->getQuote()->getCreation()->format('d/m/Y');
        $content[2] = [
            'Q',
            'fourniture',
            'PA',
            'taux',
            'remise',
            'PU',
            'frais',
            'port',
            'PAHT',
            'coef',
            'marge',
            'PU HT',
            'PVHT',
        ];

        $euro = ' ' . chr(128);

        $totalpurchase = $totalpurchaseFourniture = 0;
        $totalsell = 0;
        foreach ($this->entity->getLines() as $line) {
            if (!$line->isService() && $line->getReference() !== 'TITLE' && $line->getReference() !== 'ST') {
                $purchasePrice = $line->getPurchasePrice();
                $discountSupplier = $line->getDiscountSupplier();
                $unitPurchasePrice = $purchasePrice * (1 - $discountSupplier);
                $quantity = $line->getQuantity();
                $expenseRatio = $line->getExpenseRatio() + 1;
                $shipping = $line->getShipping();
                $unitPrice = $line->getUnitPrice();

                $denom = ($purchasePrice * (1 - $discountSupplier) * $expenseRatio);
                $p = ($denom != 0) ? (($unitPrice - $shipping) / $denom - 1) * 100 : 0;
                $content[] = [
                    number_format($quantity, 0, ',', ' '),
                    utf8_decode($line->getDesignation()),
                    number_format($purchasePrice, 2, ',', ' ') . $euro,
                    number_format($discountSupplier * 100, 0, ',', ' ') . ' %',
                    number_format($purchasePrice * $discountSupplier, 2, ',', ' ') . $euro,
                    number_format($unitPurchasePrice, 2, ',', ' ') . $euro,
                    number_format($expenseRatio, 1, ',', ' '),
                    number_format($shipping, 2, ',', ' ') . $euro,
                    number_format($quantity * $unitPurchasePrice * $expenseRatio + $shipping, 2, ',', ' ') . $euro,
                    number_format($p, 0, ',', ' ') . ' %',
                    number_format($quantity * ($unitPrice - ($denom + $shipping)), 2, ',', ' ') . $euro,
                    number_format($unitPrice, 2, ',', ' ') . $euro,
                    number_format($quantity * $unitPrice, 2, ',', ' ') . $euro,
                ];
                $totalpurchase += $quantity * $unitPurchasePrice * $expenseRatio + $shipping;
                $totalsell += $quantity * $unitPrice;
                $totalpurchaseFourniture += $quantity * $unitPurchasePrice;
                $fill[] = [255, 255, 255];
            }
        }
        $subtotal = array_fill(0, 13, '');
        $subtotal[1] = 'achat';
        $subtotal[8] = number_format($totalpurchase, 2, ',', ' ') . $euro;
        $subtotal[12] = number_format($totalsell, 2, ',', ' ') . $euro;
        $content[] = $subtotal;
        $fill[] = [192, 192, 192];
        foreach ($this->entity->getLines() as $line) {
            if ($line->isService()) {
                $purchasePrice = $line->getPurchasePrice();
                $discountSupplier = $line->getDiscountSupplier();
                $expenseRatio = $line->getExpenseRatio() + 1;
                $shipping = $line->getShipping();
                $quantity = $line->getQuantity();
                $unitPrice = $line->getUnitPrice();
                $unitPurchasePrice = $purchasePrice * (1 - $discountSupplier);
                $marge = (($unitPrice - $shipping) / ($unitPurchasePrice * $expenseRatio) - 1) * 100;
                $margeTotale = $quantity * ($unitPrice - ($unitPurchasePrice * $expenseRatio + $shipping));
                $content[] = [
                    number_format($quantity, 0, ',', ' '),
                    utf8_decode($line->getDesignation()),
                    number_format($purchasePrice, 2, ',', ' ') . $euro,
                    number_format($discountSupplier * 100, 0, ',', ' ') . ' %',
                    number_format($purchasePrice * $discountSupplier, 2, ',', ' ') . $euro,
                    number_format($unitPurchasePrice, 2, ',', ' ') . $euro,
                    number_format($expenseRatio, 1, ',', ' '),
                    number_format($shipping, 2, ',', ' ') . $euro,
                    number_format($quantity * $unitPurchasePrice * $expenseRatio + $shipping, 2, ',', ' ') . $euro,
                    number_format($marge, 0, ',', ' ') . ' %',
                    number_format($margeTotale, 2, ',', ' ') . $euro,
                    number_format($unitPrice, 2, ',', ' ') . $euro,
                    number_format($quantity * $unitPrice, 2, ',', ' ') . $euro,
                ];
                $totalpurchase += $quantity * $unitPurchasePrice * $expenseRatio + $shipping;
                $totalsell += $quantity * $unitPrice;
                $fill[] = [255, 255, 255];
            }
        }
        $total = array_fill(0, 13, '');
        $total[1] = 'total';
        $total[8] = number_format($totalpurchase, 2, ',', ' ') . $euro;
        $total[12] = number_format($totalsell, 2, ',', ' ') . $euro;
        $content[] = $total;
        $fill[] = [255, 255, 0];
        $content[] = [
            '',
            utf8_decode('PV'),
            utf8_decode('remise %'),
            number_format($this->entity->getDiscount(), 0, ',', ' ') . ' %',
            number_format($totalsell * $this->entity->getDiscount(), 2, ',', ' ') . $euro,
            '',
            '',
            '',
            '',
            utf8_decode('reste'),
            number_format(($totalsell * (1 - $this->entity->getDiscount())) - $totalpurchase, 2, ',', ' ') . $euro,
            'PV',
            number_format($totalsell * (1 - $this->entity->getDiscount()), 2, ',', ' ') . $euro,
        ];
        $fill[] = [184, 204, 228];

        $this->setFont('Arial', '', 10);
        $this->cell(0, 6, 'Texte mail :', 0, 1);
        $this->multicell(0, 6, utf8_decode($this->entity->getIntro()));
        $this->ln(10);
        foreach ($content as $key => $c) {
            $col = ($key < 4 ? 'L' : 'R');
            $this->setFont('Arial', '', 10);
            $this->setFillColor($fill[$key][0], $fill[$key][1], $fill[$key][2]);
            $this->cell(10, 6, $c[0], 1, 0, 'R', 1);
            $this->cell(80, 6, $c[1], 1, 0, 'L', 1);
            $this->cell(20, 6, $c[2], 1, 0, $col, 1);
            $this->cell(10, 6, $c[3], 1, 0, $col, 1);
            $this->cell(20, 6, $c[4], 1, 0, $col, 1);
            $this->cell(20, 6, $c[5], 1, 0, $col, 1);
            $this->cell(10, 6, $c[6], 1, 0, $col, 1);
            $this->cell(15, 6, $c[7], 1, 0, $col, 1);
            $this->cell(20, 6, $c[8], 1, 0, $col, 1);
            $this->cell(15, 6, $c[9], 1, 0, $col, 1);
            $this->cell(20, 6, $c[10], 1, 0, $col, 1);
            $this->cell(20, 6, $c[11], 1, 0, $col, 1);
            $this->cell(20, 6, $c[12], 1, 1, $col, 1);
        }
        $this->ln(10);
        $this->cell(0, 6, 'Taux de marque : ' . number_format($totalsell / $totalpurchaseFourniture, 2, ',', ' '));
    }
}
