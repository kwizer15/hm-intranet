<?php

namespace JLM\OfficeBundle\Builder;

interface OrderBuilderInterface
{
    /**
     * @return BillInterface
     */
    public function getOrder();

    public function create();
    
    public function buildCreation();
    
    public function buildTime();
    
    public function buildLines();
}
