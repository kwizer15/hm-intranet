<?php

namespace JLM\CoreBundle\Form\Type;

use Symfony\Component\Form\AbstractType;

class EmailType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return 'email';
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_core_email';
    }
}
