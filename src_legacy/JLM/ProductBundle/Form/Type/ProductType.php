<?php

namespace JLM\ProductBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ProductBundle\Entity\Product;

class ProductType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('reference', null, ['label' => 'Référence', 'attr' => ['class' => 'input-small']])
            ->add('category', null, ['label' => 'Famille de produit'])
            ->add('designation', null, ['label' => 'Designation', 'attr' => ['class' => 'input-xxlarge']])
            ->add('description', null, [
                'label' => 'Description longue',
                'required' => false,
                'attr' => ['class' => 'input-xxlarge'],
            ])
            ->add('supplier', null, ['label' => 'Fournisseur'])// Typeahead
            ->add('unity', null, ['label' => 'Unité', 'attr' => ['class' => 'input-small']])
            ->add('purchase', MoneyType::class, [
                'label' => 'Prix d\'achat HT',
                'grouping' => true,
                'attr' => ['class' => 'input-small'],
            ])
            ->add('discountSupplier', PercentType::class, [
                'type' => 'integer',
                'label' => 'Remise fournisseur',
                'attr' => ['class' => 'input-mini'],
            ])
            ->add('expenseRatio', PercentType::class, [
                'type' => 'integer',
                'label' => 'Frais',
                'attr' => ['class' => 'input-mini'],
            ])
            ->add('shipping', MoneyType::class, ['label' => 'Port', 'grouping' => true, 'attr' => ['class' => 'input-mini']])
            ->add('unitPrice', MoneyType::class, ['label' => 'PVHT', 'grouping' => true, 'attr' => ['class' => 'input-mini']])
            ->add('active', CheckboxType::class, ['required' => false]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_product_product';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setDefaults([
                'data_class' => Product::class,
            ]);
    }
}
