<?php

namespace JLM\DailyBundle\Builder\Email;

class MaintenanceOnSiteMailBuilder extends MaintenanceMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Visite d\'entretien en cours');
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'Technicien sur site pour la visite d\'entretien'.PHP_EOL
        .'Cordialement'
        .$this->getSignature());
    }
}
