<?php

namespace JLM\ProductBundle\Model;

interface PriceInterface
{
    /**
     * Get quantity
     *
     * @return integer
     */
    public function getMinimumQuantity();
    
    /**
     * Get unitPrice
     *
     * @return float
    */
    public function getUnitPrice();
}
