<?php

namespace JLM\ModelBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\Contact;
use JLM\ContractBundle\Entity\Contract;
use JLM\ModelBundle\Repository\TrusteeRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use JLM\ModelBundle\Entity\Trustee;
use JLM\ModelBundle\Form\Type\TrusteeType;
use JLM\ContactBundle\Form\Type\PersonType;
use JLM\ContactBundle\Manager\ContactManager;
use JLM\ContractBundle\Excel\ListManager;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class TrusteeController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Trustee entities.
     */
    public function indexAction($page = 1)
    {
        $limit = 15;
        $repo = $this->entityManager->getRepository(Trustee::class);
        $nb = $repo->getTotal();
        $nbPages = ceil($nb/$limit);
        $nbPages = ($nbPages < 1) ? 1 : $nbPages;
        $offset = ($page-1) * $limit;
        if ($page < 1 || $page > $nbPages) {
            throw $this->createNotFoundException('Page insexistante (page '.$page.'/'.$nbPages.')');
        }

        $entities = $repo->getList($limit, $offset);

        return $this->render('@JLMModel/Trustee/index.html.twig', [
            'entities' => $entities,
            'page'     => $page,
            'nbPages'  => $nbPages,
        ]);
    }

    /**
     * Finds and displays a Trustee entity.
     */
    public function showAction(Trustee $entity)
    {
        return $this->render('@JLMModel/Trustee/show.html.twig', [
            'entity'      => $entity,
        ]);
    }

    /**
     * Displays a form to create a new Trustee entity.
     */
    public function newAction(Request $request)
    {
        $contact = $request->get('contact');
        $entity = new Trustee();
        $form   = $this->createForm(TrusteeType::class, $entity);
        if ($contact) {
            $c = $this->entityManager->getRepository(Contact::class)->find($contact);
            if ($c) {
                $form->get('contact')->setData($c);
            }
        }

        return $this->render('@JLMModel/Trustee/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Creates a new Trustee entity.
     */
    public function createAction(Request $request)
    {
        $entity  = new Trustee();
        $form    = $this->createForm(TrusteeType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($entity->getBillingAddress() !== null) {
                $this->entityManager->persist($entity->getBillingAddress());
            }
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('trustee_show', ['id' => $entity->getId()]));
        }
        return $this->render('@JLMModel/Trustee/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Trustee entity.
     */
    public function editAction(Trustee $entity)
    {
        $editForm = $this->createForm(TrusteeType::class, $entity);
        $deleteForm = $this->createDeleteForm($entity);

        return $this->render('@JLMModel/Trustee/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Edits an existing Trustee entity.
     */
    public function updateAction(Request $request, Trustee $entity)
    {
        $editForm   = $this->createForm(TrusteeType::class, $entity);
        $deleteForm = $this->createDeleteForm($entity);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity->getAddress());
            if ($entity->getBillingAddress() !== null) {
                $this->entityManager->persist($entity->getBillingAddress());
            }
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('trustee_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMModel/Trustee/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a Trustee entity.
     */
    public function deleteAction(Request $request, Trustee $entity)
    {
        $form = $this->createDeleteForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }

        return ($this->redirectToRoute('trustee'));
    }

    private function createDeleteForm(Trustee $entity)
    {
        return $this->createFormBuilder(['id' => $entity->getId()])
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
    
    /**
     * Formulaire d'ajout d'un contact au syndic.
     */
    public function contactnewAction(Trustee $trustee)
    {
        $entity = ContactManager::create('Person');
        $form   = $this->createForm(PersonType::class, $entity);
        
        return $this->render('@JLMModel/Trustee/contactnew.html.twig', [
                'trustee' => $trustee,
                'entity' => $entity,
                'form'   => $form->createView()
        ]);
    }
    
    /**
     * Creates a new Trustee entity.
     */
    public function contactcreateAction(Request $request, Trustee $trustee)
    {
        $entity  = ContactManager::create('Person');
        $form    = $this->createForm(PersonType::class, $entity);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $trustee->addContact($entity);
            $this->entityManager->persist($entity);
            $this->entityManager->persist($trustee);
            $this->entityManager->flush();
    
            return ($this->redirectToRoute('trustee_show', ['id' => $trustee->getId()]));
        }
    
        return $this->render('@JLMModel/Trustee/contactnew.html.twig', [
                'trustee' => $trustee,
                'entity' => $entity,
                'form'   => $form->createView()
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchAction(Request $request): JsonResponse
    {
        $term = $request->get('q');
        $pageLimit = $request->get('page_limit');

        /** @var TrusteeRepository $repository */
        $repository = $this->entityManager->getRepository(Trustee::class);
        $entities = $repository->getArray($term, $pageLimit);
        
        return new JsonResponse(['entities' => $entities]);
    }
    
    /**
     * City json
     */
    public function jsonAction(Request $request)
    {
        $id = $request->get('id');
        $entity = $this->entityManager->getRepository(Trustee::class)->getByIdToArray($id);
    
        return new JsonResponse($entity);
    }
    

    /**
     *
     * @param Request $request
     * @return multitype:unknown
     */
    public function listExcelAction(Request $request)
    {
        $list = $this->entityManager->getRepository(Contract::class)->getForRDV();
         
        $excelBuilder = new ListManager($this->get('phpexcel'));
        
        return $excelBuilder->createList($list)->getResponse();
    }
}
