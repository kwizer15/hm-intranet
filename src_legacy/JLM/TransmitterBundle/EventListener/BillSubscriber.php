<?php

namespace JLM\TransmitterBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\VAT;
use JLM\ProductBundle\Entity\Product;
use JLM\TransmitterBundle\Entity\Attribution;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\CommerceBundle\Event\BillEvent;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CommerceBundle\Factory\BillFactory;
use JLM\TransmitterBundle\Builder\AttributionBillBuilder;
use JLM\CoreBundle\Event\FormPopulatingEvent;
use JLM\CoreBundle\Event\RequestEvent;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class BillSubscriber implements EventSubscriberInterface
{

    private $objectManager;

    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            JLMCommerceEvents::BILL_FORM_POPULATE => 'populateFromAttribution',
            JLMCommerceEvents::BILL_AFTER_PERSIST => 'setBillToAttribution',
        ];
    }

    public function populateFromAttribution(FormPopulatingEvent $event)
    {
        if (null !== $attribution = $this->getAttribution($event)) {
            $options = [
                'port' => $this->objectManager->getRepository(Product::class)->find(134),
            ];
            $entity = BillFactory::create(
                new AttributionBillBuilder(
                    $attribution,
                    $this->objectManager->getRepository(VAT::class)->find(1)->getRate(),
                    $options
                )
            );
            $event->getForm()->setData($entity);
            $event->getForm()->add('attribution', HiddenType::class, [
                'data' => $attribution->getId(),
                'mapped' => false
            ]);
        }
    }

    public function setBillToAttribution(BillEvent $event)
    {
        if (null !== $entity = $this->getAttribution($event)) {
            $entity->setBill($event->getBill());
            $this->objectManager->persist($entity);
            $this->objectManager->flush();
        }
    }

    private function getAttribution(RequestEvent $event)
    {
        $id = $event->getParam('jlm_commerce_bill', ['attribution' => $event->getParam('attribution')]);

        return (isset($id['attribution']) && $id['attribution'] !== null)
            ? $this->objectManager->getRepository(Attribution::class)->find($id['attribution'])
            : null
        ;
    }
}
