<?php

namespace JLM\ContactBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\CorporationContact;
use JLM\ContactBundle\Manager\CorporationContactManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class CorporationContactController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(CorporationContact::class);
    }

    /**
     * Edit or add a contact
     *
     * @param int|string $id The entity identifier
     *
     * @return Response
     */
    public function editAction(CorporationContactManager $manager, Request $request, $id = 0)
    {
        $entity = $this->repository->find($id);
        $formName = ($id) ? 'edit' : 'new';
        $form = $manager->createForm($request, $formName, ['entity' => $entity]);
        $ajax = $request->isXmlHttpRequest();
        if ($manager->getHandler($form, $request)->process()) {
            $entity = $form->getData();
            if ($ajax) {
                $contact = $this->repository->getByIdToArray($entity->getId());
                $contact['contact']['contact']['show_link'] = $this->generateUrl(
                    'jlm_contact_contact_show',
                    ['id' => $contact['contact']['id']]
                );
                $contact['edit_link'] = $manager->getEditUrl($contact['id']);
                $contact['delete_link'] = $manager->getDeleteUrl($contact['id']);

                $response = new JsonResponse($contact);
            } else {
                $response = $this->redirectToRoute(
                    'jlm_contact_contact_show',
                    ['id' => $entity->getCorporation()->getId()]
                );
            }
        } else {
            $template = ($ajax) ? 'modal_new.html.twig' : 'new.html.twig';
            $response = $this->render(
                '@JLMContact/CorporationContact/' . $template,
                ['form' => $form->createView()]
            );
        }

        return $response;
    }

    /**
     * Remove a CorporationContact
     *
     */
    public function deleteAction(CorporationContactManager $manager, Request $request, $id)
    {
        $entity = $this->repository->find($id);
        $form = $manager->createForm($request, 'delete', ['entity' => $entity]);
        $process = $manager->getHandler($form, $request, $entity)->process('DELETE');
        if ($process) {
            return new JsonResponse(['delete' => true]);
        }
        $ajax = $request->isXmlHttpRequest();
        $template = ($ajax) ? 'modal_delete.html.twig' : 'delete.html.twig';

        return $this->render('@JLMContact/CorporationContact/' . $template, [
            'form' => $form->createView()
        ]);
    }
}
