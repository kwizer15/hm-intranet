<?php

namespace Tests\JLM\ContactBundle\DependencyInjection;

use JLM\ContactBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class ConfigurationTest extends \PHPUnit\Framework\TestCase
{
    public function setUp()
    {
        $this->conf = new Configuration();
    }
    
    public function assertPreCondition()
    {
        $this->assertInstanceOf(ConfigurationInterface::class, $this->conf);
    }
    
    public function testGetConfigTreeBuilder()
    {
        $this->assertInstanceOf(TreeBuilder::class, $this->conf->getConfigTreeBuilder());
    }
}
