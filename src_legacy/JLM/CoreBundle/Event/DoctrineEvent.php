<?php

namespace JLM\CoreBundle\Event;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\Event;

class DoctrineEvent extends Event
{
    /**
     * @var mixed
     */
    private $entity;
    
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * Constructor
     *
     * @param $entity
     * @param EntityManagerInterface $objectManager
     */
    public function __construct($entity, EntityManagerInterface $objectManager)
    {
        $this->entity = $entity;
        $this->objectManager = $objectManager;
    }
    
    /**
     * Get the entity
     * @return mixed
     */
    public function getEntity()
    {
        return $this->entity;
    }
    
    /**
     * Get thje object manager
     * @return EntityManagerInterface
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }
}
