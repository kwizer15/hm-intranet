<?php

namespace JLM\OfficeBundle\Form\Type;

use JLM\ModelBundle\Form\Type\DoorHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Probablement pas utilisée
 */
class TaskType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('type', null, ['label' => 'Type'])
            ->add('door', DoorHiddenType::class, ['required' => false])
            ->add('place', null, ['label' => 'Lieu concerné'])
            ->add('todo', null, ['label' => 'À faire']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => 'JLM\OfficeBundle\Entity\Task',
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'task';
    }
}
