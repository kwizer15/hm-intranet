<?php

namespace Tests\JLM\ModelBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SiteContactControllerTest extends WebTestCase
{
    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->client = static::createClient();
    }
    
    public function testShow()
    {
        $this->client->followRedirects();
        
        $crawler = $this->client->request('GET', '/model/sitecontact/1/show');
        // Page d'identification (a supprimer plus tard)
        $crawler = $this->login($crawler);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testEdit()
    {
        $this->client->followRedirects();
    
        $crawler = $this->client->request('GET', '/model/sitecontact/1/edit');
        // Page d'identification (a supprimer plus tard)
        $crawler = $this->login($crawler);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    /**
     * Log the user
     * @param Crawler $crawler
     */
    private function login($crawler)
    {
        $form = $crawler->selectButton('S\'identifier')->form();
        
        // définit certaines valeurs
        $form['username'] = 'kwizer';
        $form['password'] = 'sslover';
        return $this->client->submit($form);
    }
}
