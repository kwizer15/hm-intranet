<?php
namespace JLM\OfficeBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\OfficeBundle\Entity\AskQuote;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class AskQuoteToIntTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * Transforms an object (askquote) to an integer (id).
     *
     * @param  AskQuote|null $entity
     * @return int
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return "";
        }
        return $entity->getAggregateId();
    }
    
    /**
     * Transforms an integer (number) to an object (askquote).
     *
     * @param  int $number
     * @return AskQuote|null
     * @throws TransformationFailedException if object (askquote) is not found.
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $entity = $this->objectManager
            ->getRepository(AskQuote::class)
            ->find($id)
        ;
        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'An askquote with id "%s" does not exist!',
                $id
            ));
        }
    
        return $entity;
    }
}
