<?php

namespace JLM\ContactBundle\Model;

interface ContactInterface
{

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax();
    
    /**
     * Get email
     *
     * @return string
     */
    public function getEmail();
    
    /**
     * Get address
     *
     * @return AddressInterface
     */
    public function getAddress();
    
    /**
     * Get first name
     *
     * @return string
     */
    public function getName();
    
    /**
     * To String
     *
     * @return string
    */
    public function __toString();
}
