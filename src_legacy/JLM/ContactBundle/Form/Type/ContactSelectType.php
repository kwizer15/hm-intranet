<?php

namespace JLM\ContactBundle\Form\Type;

use JLM\CoreBundle\Form\Type\AbstractSelectType;
use JLM\ContactBundle\Form\DataTransformer\ContactToIntTransformer;

class ContactSelectType extends AbstractSelectType
{
    protected function getTransformerClass()
    {
        return ContactToIntTransformer::class;
    }
    
    protected function getTypeName()
    {
        return 'jlm_contact_contact';
    }
}
