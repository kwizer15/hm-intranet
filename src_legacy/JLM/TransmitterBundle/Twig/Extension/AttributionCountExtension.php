<?php

namespace JLM\TransmitterBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class AttributionCountExtension extends AbstractExtension implements GlobalsInterface
{
    public function getName()
    {
        return 'transmitterattributioncount_extension';
    }
    
    public function getGlobals()
    {
        return ['transmitterattributioncount' => [
                'all' => 0,
        ]];
    }
}
