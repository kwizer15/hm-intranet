<?php

namespace JLM\ModelBundle\Builder;

use JLM\ModelBundle\Entity\Door;
use JLM\ContractBundle\Model\ContractInterface;

abstract class DoorBillBuilderAbstract extends SiteBillBuilderAbstract
{
    protected $door;
    
    public function __construct(Door $door, $options = [])
    {
        $this->door = $door;
        parent::__construct($this->door->getAdministrator(), $options);
        $contract = $this->door->getActualContract();
        $this->trustee = ($contract instanceof ContractInterface) ? $contract->getManager() : $this->trustee ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildDetails()
    {
        $this->getBill()->setDetails($this->door->getType().' - '.$this->door->getLocation());
    }
}
