<?php

namespace JLM\CoreBundle\Model\Repository;

use Doctrine\ORM\Tools\Pagination\Paginator;

interface PaginableInterface
{
    /**
     * Get entity list paginated
     * @param int $page
     * @param int $resultsByPage
     * @param array $filters
     * @return Paginator
     */
    public function getPaginable($page, $resultsByPage, array $filters);
}
