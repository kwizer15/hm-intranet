<?php

namespace JLM\FrontBundle\Mailer;

use JLM\FrontBundle\Model\ContactInterface;

interface MailerInterface
{
    /**
     * Send a contact email to admin
     *
     * @param ContactInterface $contact
     *
     * @return void
     */
    public function sendContactEmailMessage(ContactInterface $contact);
    
    /**
     * Send a confirmation to contact
     *
     * @param ContactInterface $contact
     *
     * @return void
     */
    public function sendConfirmContactEmailMessage(ContactInterface $contact);
}
