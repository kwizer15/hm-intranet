<?php
namespace JLM\DailyBundle\Pdf;

use \JLM\DefaultBundle\Pdf\FPDFext;

class Day extends FPDFext
{
    private $entities;
    private $date;
    
    public static function get(\DateTime $date, $entities, $standby)
    {
        $pdf = new self();
        $pdf->init();
        $pdf->printheader($date);
        foreach ($entities as $entity) {
            $pdf->show($entity, $date);
        }
        return $pdf->Output('', 'S');
    }
    
    private function init()
    {
        $this->aliasNbPages();
        $this->setFillColor(200);
        $this->addPage('L');
    }
    
    private function printheader(\DateTime $date)
    {
        $this->setFont('Arial', 'B', 18);
        $this->cell(0, 12, 'Interventions du '.$date->format('d/m/Y'), 1, 1, 'C', true);
        $this->ln(5);
        $this->setFont('Arial', 'B', 11);
        $this->setWidths([24,77,8,69,70,29]);
        $this->row(['Type','Affaire','Ctr','Raison','Rapport','Technicien'], 6, 1, true);
        $this->setFont('Arial', '', 10);
    }
    
    private function show($entity, $date)
    {
        $types = [
                'fixing' => 'Dépannage',
                'equipment' => 'Matériel',
                'maintenance' => 'Entretien',
                'work' => 'Travaux',
        ];
        $datas[0] = $types[$entity->getType()];
        if ($entity->getType() == 'equipment') {
            $datas[1] = $entity->getPlace();
            $datas[2] = '';
            $datas[4] = '';
        } else {
            if ($entity->getDoor()) {
                $datas[1] = $entity->getDoor()->getType()
                .' - '
                        .$entity->getDoor()->getLocation()
                        .PHP_EOL
                        .$entity->getDoor()->getStreet()
                        .PHP_EOL
                        .$entity->getDoor()->getSite()->getAddress()->getCity()
                        ;
            } else {
                $datas[1] = $entity->getPlace();
            }
            $datas[2] = ($entity->getContract() == 'Hors contrat') ? 'HC' : $entity->getContract();
            $datas[4] = $entity->getReport();
            if ($entity->getRest()) {
                $datas[4] .= PHP_EOL.PHP_EOL.'Reste à faire :'.PHP_EOL.$entity->getRest();
            }
        }
        $datas[3] = $entity->getReason();
        $datas[5] = '';
        foreach ($entity->getShiftTechnicians() as $tech) {
            if ($tech->getBegin()->format('Ymd') == $date->format('Ymd')) {
                $datas[5] .= ($datas[5] != '') ? PHP_EOL : '';
                $datas[5] .= $tech->getTechnician();
                if ($tech->getEnd()) {
                    $datas[5] .= PHP_EOL.$tech->getBegin()->format('H\hi').' - '.$tech->getEnd()->format('H\hi');
                }
            }
        }
        $this->row($datas, 5, 1, false);
    }
}
