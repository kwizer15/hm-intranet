<?php

namespace JLM\AskBundle\Entity;

use JLM\AskBundle\Model\CommunicationMeansInterface;

class CommunicationMeans implements CommunicationMeansInterface
{
    /**
     * @var integer $id
     */
    private $id;

    /**
     * @var string
     */
    private $name = '';
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Set text
     *
     * @param string $text
     */
    public function setName($name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    /**
     * Get text
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    /**
     * To String
     */
    public function __toString()
    {
        return $this->getName();
    }
}
