<?php

namespace JLM\DailyBundle\Controller;

use DateInterval;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use JLM\DailyBundle\Entity\Equipment;
use JLM\DailyBundle\Entity\Fixing;
use JLM\DailyBundle\Entity\Maintenance;
use JLM\DailyBundle\Entity\Standby;
use JLM\DailyBundle\Entity\Work;
use JLM\DailyBundle\Pdf\Day;
use JLM\DailyBundle\Pdf\Door as DoorPdf;
use JLM\DailyBundle\Pdf\Tomorrow;
use JLM\ModelBundle\Entity\Door;
use Liuggio\ExcelBundle\Factory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use JLM\DailyBundle\Entity\Intervention;
use JLM\DailyBundle\Entity\Shifting;
use JLM\DailyBundle\Form\Type\ExternalBillType;
use JLM\DailyBundle\Form\Type\InterventionCancelType;
use JLM\DailyBundle\JLMDailyEvents;
use JLM\DailyBundle\Event\InterventionEvent;
use JLM\OfficeBundle\Entity\AskQuote;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class InterventionController extends Controller
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var ObjectRepository
     */
    private $repository;
    private $doorRepository;

    public function __construct(EventDispatcherInterface $eventDispatcher, EntityManagerInterface $entityManager)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Intervention::class);
        $this->doorRepository = $entityManager->getRepository(Door::class);
    }

    /**
     * Finds and displays a Intervention entity.
     */
    public function indexAction()
    {
        $entities = $this->repository->getPrioritary();

        return $this->render('@JLMDaily/Intervention/index.html.twig', [
                'entities'      => $entities,
        ]);
    }

    /**
     * Bill intervention
     */
    public function tobillAction(Intervention $entity)
    {
        $entity->setMustBeBilled(true);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Don't Bill intervention
     */
    public function dontbillAction(Intervention $entity)
    {
        $entity->setMustBeBilled(false);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Cancel Bill action
     */
    public function cancelbillAction(Intervention $entity)
    {
        if ($entity->getMustBeBilled()) {
            if ($entity->getBill() !== null) {
                //  annuler la facture existante
                $bill = $entity->getBill();
                $bill->setIntervention();
                $bill->setState(-1);
                $entity->setBill();
                $this->entityManager->persist($bill);
            } elseif ($entity->getExternalBill() !== null) {
                $entity->setExternalBill();
            }
        }
        $entity->setMustBeBilled(null);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Crée une demande de devis
     */
    public function toquoteAction(Intervention $entity)
    {
        $ask = new AskQuote;
        $ask->populateFromIntervention($entity);
        $this->entityManager->persist($ask);
        $entity->setAskQuote($ask);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Supprime une demande de devis
     */
    public function cancelquoteAction(Intervention $entity)
    {
        if (($ask = $entity->getAskQuote()) !== null) {
            $ask->setIntervention();
            $entity->setAskQuote();
            $this->entityManager->remove($ask);
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Active contacter client
     */
    public function tocontactAction(Intervention $entity)
    {
        $entity->setContactCustomer(false);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Supprime une demande de devis
     */
    public function cancelcontactAction(Intervention $entity)
    {
        $entity->setContactCustomer(null);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Créer un ligne travaux
     */
    public function toworkAction(Intervention $entity)
    {
        $this->eventDispatcher->dispatch(
            JLMDailyEvents::INTERVENTION_SCHEDULEWORK,
            new InterventionEvent($entity)
        );

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Supprime une ligne travaux
     */
    public function cancelworkAction(Intervention $entity)
    {
        $this->eventDispatcher->dispatch(
            JLMDailyEvents::INTERVENTION_UNSCHEDULEWORK,
            new InterventionEvent($entity)
        );

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Annule l'intervention
     */
    public function cancelAction(Request $request, Intervention $entity)
    {
        $form = $this->createForm(InterventionCancelType::class, $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entity->cancel();
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Désannule l'intervention
     */
    public function uncancelAction(Request $request, Intervention $entity)
    {
        $entity->uncancel();
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('intervention_redirect', ['id'=>$entity->getId(),'act'=>'show']));
    }

    /**
     * Numéro de facture
     */
    public function externalbillAction(FormFactoryInterface $formFactory, Request $request, Intervention $entity)
    {
        $form = $formFactory->createNamed(
            'externalBill'.$entity->getId(),
            ExternalBillType::class,
            $entity
        );
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * Liste des interventions par date(s)
     */
    public function todayAction()
    {
        $today = new DateTime;
        $todaystring =  $today->format('Y-m-d');
        $intervs = array_merge(
            $this->entityManager->getRepository(Fixing::class)->getToday(),
            $this->entityManager->getRepository(Work::class)->getToday(),
            $this->entityManager->getRepository(Maintenance::class)->getToday()
        );
        $inprogress = $notclosed = $closed = $ago = [];
        foreach ($intervs as $interv) {
            $flag = false;
            if ($interv->getState() == 3) {
                $closed[] = $interv;
                $flag = true;
            } else {
                foreach ($interv->getShiftTechnicians() as $tech) {
                    if ($tech->getBegin()->format('Y-m-d') == $todaystring && !$flag) {
                        $inprogress[] = $interv;
                        $flag = true;
                    } elseif ($tech->getBegin()->getTimestamp() >= $today->getTimestamp() && !$flag) {
                        $ago[] = $interv;
                        $flag = true;
                    }
                }
            }
            if (!$flag) {
                $notclosed[] = $interv;
            }
        }

        return $this->render('@JLMDaily/Intervention/today.html.twig', [
                'inprogress' => $inprogress,
                'fixing' => $this->entityManager->getRepository(Fixing::class)->getToGive(),
                'equipment' => $this->entityManager->getRepository(Equipment::class)->getToday(),
                'notclosed' => $notclosed,
                'closed' => $closed,
                'ago' => $ago,
        ]);
    }

    /**
     * Liste des interventions par date(s)
     */
    public function reportAction($date1 = null, $date2 = null)
    {
        $now = new DateTime;
        $today = DateTime::createFromFormat('YmdHis', $now->format('Ymd').'000000');
        $d1 = ($date1 === null) ? $today : DateTime::createFromFormat('YmdHis', $date1.'000000');
        $d2 = ($date2 === null)
            ? DateTime::createFromFormat('YmdHis', $d1->format('Ymd').'235959')
            : DateTime::createFromFormat('YmdHis', $date2.'235959')
        ;

        $intervs = $this->repository->getWithDate($d1, $d2);
        $equipment = $this->entityManager->getRepository(Equipment::class)->getWithDate($d1, $d2);
        $now->sub(new DateInterval('P4D'));
        $days = [
            DateTime::createFromFormat('YmdHis', $now->add(new DateInterval('P1D'))->format('Ymd').'000000'),
            DateTime::createFromFormat('YmdHis', $now->add(new DateInterval('P1D'))->format('Ymd').'000000'),
            DateTime::createFromFormat('YmdHis', $now->add(new DateInterval('P1D'))->format('Ymd').'000000'),
            DateTime::createFromFormat('YmdHis', $now->add(new DateInterval('P1D'))->format('Ymd').'000000'),
            DateTime::createFromFormat('YmdHis', $now->add(new DateInterval('P1D'))->format('Ymd').'000000'),
        ];

        return $this->render('@JLMDaily/Intervention/report.html.twig', [
                'standby' => $this->entityManager->getRepository(Standby::class)->getByDate($date1),
                'd1' => $d1,
                'd2' => ($date2 === null) ? null : $d2,
                'entities' => array_merge($equipment, $intervs),
                'days' => $days,
                'layout' => ['form_searchByDate_date' => $d1]
        ]);
    }

    /**
     * Liste des interventions par date(s)
     */
    public function reportdateAction(Request $request)
    {
        $date = DateTime::createFromFormat('d/m/Y', $request->get('datepicker'));

        return ($this->redirectToRoute('intervention_listdate1', ['date1'=>$date->format('Ymd')]));
    }


    /**
     * Supprimer une intervention
     */
    public function deleteAction(Shifting $entity)
    {
        foreach ($entity->getShiftTechnicians() as $tech) {
            $this->entityManager->remove($tech);
        }
        $this->entityManager->remove($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('intervention_today'));
    }

    /**
     * Finds and displays a Intervention entity.
     */
    public function redirectAction(Intervention $entity, $act)
    {
        if (!in_array($act, ['show','edit','close'])) {
            throw $this->createNotFoundException('Page inexistante');
        }

        return ($this->redirectToRoute($entity->getType() . '_' . $act, ['id'=>$entity->getId()]));
    }

    /**
     * Imprime les intervs de la prochaine journée
     */
    public function printdayAction($date1)
    {
        $d1 = DateTime::createFromFormat('YmdHis', $date1.'000000');
        $d2 =  DateTime::createFromFormat('YmdHis', $d1->format('Ymd').'235959');

        $intervs = $this->repository->getWithDate($d1, $d2);
        $equipment = $this->entityManager->getRepository(Equipment::class)->getWithDate($d1, $d2);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename='.$d1->format('Y-m-d').'.pdf');
        $response->setContent(Day::get(
            $d1,
            array_merge($equipment, $intervs),
            $this->entityManager->getRepository(Standby::class)->getByDate($date1)
        ));

        return $response;
    }

    /**
     * Imprime les intervs de la prochaine journée
     */
    public function printtomorrowAction()
    {
        $now = new DateTime;

        do {
            $tomorrow = DateTime::createFromFormat(
                'YmdHis',
                $now->add(new DateInterval('P1D'))->format('Ymd').'000000'
            );
            $results = $this->entityManager->getRepository(Standby::class)->getCountByDate($tomorrow);
        } while ($results);

        $intervs = $this->entityManager->getRepository(Intervention::class)->getWithDate($tomorrow, $tomorrow);
        $equipment = $this->entityManager->getRepository(Equipment::class)->getWithDate($tomorrow, $tomorrow);
        $fixing = $this->entityManager->getRepository(Fixing::class)->getToGive();
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename='.$tomorrow->format('Y-m-d').'.pdf');
        $response->setContent(Tomorrow::get($tomorrow, array_merge($equipment, $intervs, $fixing)));

        return $response;
    }

    /**
     * Imprime les intervs d'une intallation
     */
    public function printdoorAction($id)
    {
        $door = $this->doorRepository->find($id);
        $shifts = [];
        foreach ($door->getInterventions() as $interv) {
            foreach ($interv->getShiftTechnicians() as $shift) {
                $shifts[(string)$shift->getBegin()->getTimestamp()] = $shift;
            }
        }
        krsort($shifts);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename='.$door->getId().'.pdf');
        $response->setContent(DoorPdf::get($door, $shifts));

        return $response;
    }

    /**
     * Export CSV intervs porte
     */
    public function doorcsvAction($id)
    {


        $door = $this->doorRepository->find($id);
        $shifts = [];
        foreach ($door->getInterventions() as $interv) {
            foreach ($interv->getShiftTechnicians() as $shift) {
                $shifts[(string)$shift->getBegin()->getTimestamp()] = $shift;
            }
        }
        krsort($shifts);
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', 'inline; filename='.$door->getId().'.csv');
        $response->setContent($this->render('@JLMDaily/Intervention/door.csv.twig', [
                        'entity' => $door,
                ]));

        return $response;
    }

    /**
     * Export CSV intervs porte
     */
    public function doorxlsAction(TranslatorInterface $translator, Factory $excel, $id)
    {


        $door = $this->doorRepository->find($id);

        $phpExcelObject = $excel->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("JLM Entreprise")
            ->setLastModifiedBy("JLM Entreprise")
            ->setTitle("Rapport d'intrevention");
//          ->setSubject("Office 2005 XLSX Test Document")
//          ->setDescription("")
        $as = $phpExcelObject->setActiveSheetIndex(0);
        $titles = ['A'=>'Type','B'=>'Date','C'=>'Raison','D'=>'Constat','E'=>'Action menée','F'=>'Techniciens'];
        foreach ($titles as $col => $value) {
            $as->setCellValue($col.'1', $value);
        }
        $intervs = $door->getInterventions();
        $row = 2;
        foreach ($intervs as $interv) {
            if (!$interv->isCanceled() && $interv->getFirstDate()) {
                $date = ($interv->getFirstDate() != $interv->getLastDate())
                    ? 'du '.$interv->getFirstDate()->format('d/m/Y').PHP_EOL
                        .' au '.$interv->getLastDate()->format('d/m/Y')
                    : $interv->getFirstDate()->format('d/m/Y');
                $reason = '';
                if ($interv->getType() == 'work') {
                    if ($interv->getQuote()) {
                        $reason = 'Selon devis n°'.$interv->getQuote()->getNumber().PHP_EOL;
                    }
                }
                $reason .= $interv->getReason();
                $constat = ($interv->getType() == 'fixing') ? $interv->getObservation() : '';
                $report = $interv->getReport();
                $techs = [];
                foreach ($interv->getShiftTechnicians() as $shift) {
                    $tech = $shift->getTechnician().' ('.$shift->getBegin()->format('d/m/Y');
                    if ($shift->getEnd()) {
                        $tech .= ' - '.$shift->getTime()->format('%hh%I');
                    }
                    $tech .= ')';
                    $techs[] = $tech;
                }

                $as->setCellValue('A'.$row, $translator->trans($interv->getType()))
                   ->setCellValue('B'.$row, $date)
                   ->setCellValue('C'.$row, $reason)
                   ->setCellValue('D'.$row, $constat)
                   ->setCellValue('E'.$row, $report)
                   ->setCellValue('F'.$row, implode(PHP_EOL, $techs));
                $row++;
            }
        }

        $phpExcelObject->getActiveSheet()->setTitle('Rapport');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $excel->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $excel->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename='.$door->getId().'.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }

    /**
     * Export CSV intervs porte
     */
    public function doorsxlsAction(TranslatorInterface $translator, Request $request, Factory $excel)
    {


        $page = $request->get('page', 1);
        $limit = $request->get('limit', 500);
        $intervs = $this->entityManager->getRepository(Intervention::class)
            ->findBy([], ['id'=>'ASC'], $limit, ($page - 1) * $limit);

        $phpExcelObject = $excel->createPHPExcelObject();

        $phpExcelObject->getProperties()->setCreator("JLM Entreprise")
            ->setLastModifiedBy("JLM Entreprise")
            ->setTitle("Rapport d'intrevention");
    //          ->setSubject("Office 2005 XLSX Test Document")
    //          ->setDescription("")
        $as = $phpExcelObject->setActiveSheetIndex(0);

        $titles = [
            'A' => 'date',
            'B' => 'code installation',
            'C' => 'rue',
            'D' => 'cp',
            'E' => 'ville',
            'F' => 'type contrat',
            'G' => 'interlocuteur',
            'H' => 'tel interlocuteur',
            'I' => 'demande',
            'J' => 'type',
            'K' => 'raison',
            'L' => 'action menée',
            'M' => 'type de\'install',
            'N' => 'constat',
            'O' => 'action menée',
            'P' => 'reste a faire',
            'Q' => 'n° bon d\'intervention',
            'R' => 'devis',
            'S' => 'facture',
            'T' => 'technicien(s)'
        ];
        foreach ($titles as $col => $value) {
            $as->setCellValue($col.'1', $value);
        }
        $row = 2;
        foreach ($intervs as $interv) {
            if (!$interv->isCanceled() && $interv->getFirstDate() && $interv->getClosed()) {
                // A
                $date = ($interv->getFirstDate() != $interv->getLastDate())
                    ? 'du '.$interv->getFirstDate()->format('d/m/Y').PHP_EOL
                        .' au '.$interv->getLastDate()->format('d/m/Y')
                    : $interv->getFirstDate()->format('d/m/Y');
                $as->setCellValue('A'.$row, $date);

                // B
                $door = $interv->getDoor();
                $code = ($door) ? $door->getCode() : '';
                $as->setCellValue('B'.$row, $code);

                // C
                $street = $door ? $door->getAddress()->getStreet() : $interv->getPlace();
                    //  faire un regexp sur place pour sortir zip et ville
                $as->setCellValue('C'.$row, $street);

                // D
                $cp = $door ? $door->getAddress()->getCity()->getZip() : '';
                $as->setCellValue('D'.$row, $cp);

                // E
                $city = $door ? $door->getAddress()->getCity()->getName() : '';
                $as->setCellValue('E'.$row, $city);

                // F
                $contrat = $interv->getDynCOntract();
                $as->setCellValue('F'.$row, $contrat);

                // G
                $contact = $interv->getContactName();
                $as->setCellValue('G'.$row, $contact);

                // H
                $contactTel = $interv->getContactPhones();
                $as->setCellValue('H'.$row, $contactTel);

                // I
                $reason = '';
                if ($interv->getType() == 'work') {
                    if ($interv->getQuote()) {
                        $reason = 'Selon devis n°'.$interv->getQuote()->getNumber().PHP_EOL;
                    }
                }
                $reason .= $interv->getReason();
                $as->setCellValue('I'.$row, $reason);

                // J
                $as->setCellValue('J'.$row, $translator->trans($interv->getType()));

                // K
                $due = ($interv->getType() === 'fixing') ? $interv->getDue() : '';
                $as->setCellValue('K'.$row, $due);

                // L
                $done = ($interv->getType() === 'fixing') ? $interv->getDone() : '';
                $as->setCellValue('L'.$row, $done);

                // M
                $installtype = $door->getType();
                $as->setCellValue('M'.$row, $installtype);

                // N
                $constat = ($interv->getType() === 'fixing') ? $interv->getObservation() : '';
                $as->setCellValue('N'.$row, $constat);

                // O
                $report = $interv->getReport();
                $as->setCellValue('O'.$row, $report);

                // P
                $rest = $interv->getRest();
                $as->setCellValue('P'.$row, $rest);

                // Q
                $voucher = $interv->getVoucher();
                $as->setCellValue('Q'.$row, $voucher);

                // R
                // S
                $bill = $interv->getBill();
                $bill = $bill === null ? $interv->getExternalBill() : $bill->getNumber();
                $as->setCellValue('S'.$row, $bill);

                // T
                $techs = [];
                foreach ($interv->getShiftTechnicians() as $shift) {
                    $tech = $shift->getTechnician().' ('.$shift->getBegin()->format('d/m/Y');
                    if ($shift->getEnd()) {
                        $tech .= ' - '.$shift->getTime()->format('%hh%I');
                    }
                    $tech .= ')';
                    $techs[] = $tech;
                }
                $as->setCellValue('T'.$row, implode(PHP_EOL, $techs));

                $row++;
            }
        }

        $phpExcelObject->getActiveSheet()->setTitle('Rapport');
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $phpExcelObject->setActiveSheetIndex(0);

        // create the writer
        $writer = $excel->createWriter($phpExcelObject, 'Excel5');
        // create the response
        $response = $excel->createStreamedResponse($writer);
        // adding headers
        $response->headers->set('Content-Type', 'text/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Content-Disposition', 'attachment;filename=export_interventions.xls');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');

        return $response;
    }


    public function publishAction(Request $request, Intervention $entity)
    {
        $entity->setPublished(true);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $this->redirect($request->headers->get('referer'));
    }

    public function unpublishAction(Request $request, Intervention $entity)
    {
        $entity->setPublished(false);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return $this->redirect($request->headers->get('referer'));
    }
}
