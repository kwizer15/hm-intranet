<?php

namespace JLM\ContactBundle\Repository;

use Doctrine\ORM\EntityRepository;

class ContactRepository extends EntityRepository
{
    private function getQueryBuilder()
    {
        return $this->createQueryBuilder('a')
            ->select('a')
            //->leftJoin('a.phones','b')
            //->leftJoin('b.phone','c')
        ;
    }
    
    /**
     *
     * @param string $query
     * @param int $limit
     * @return array
     */
    public function getArray($query, $limit = 10)
    {
        return $this->getByQuery($query)->getQuery()->getArrayResult();
    }
    
    public function search($query)
    {
        return $this->getByQuery($query)->getQuery()->getResult();
    }
    
    public function getByQuery($query)
    {
        return $this->getQueryBuilder()
            ->where('a.name LIKE :query')
            ->orderBy('a.name', 'ASC')
            ->setParameter('query', '%'.$query.'%')
        ;
    }
    
    public function getAll($limit = 10, $offset = 0)
    {
        $queryBuilder = $this->getQueryBuilder()
            ->orderBy('a.name')
            ->setFirstResult($offset)
            ->setMaxResults($limit);
        $res = $queryBuilder->getQuery()->getResult();
        
        return $res;
    }
    
    public function getCountAll()
    {
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('COUNT(a)');
        
        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     *
     * @param int $id
     * @return array|null
     */
    public function getByIdToArray($id)
    {
        $queryBuilder = $this->getQueryBuilder()
            ->where('a.id = :id')
            ->setParameter('id', $id)
        ;
        $res = $queryBuilder->getQuery()->getArrayResult();

        return $res[0];
    }
}
