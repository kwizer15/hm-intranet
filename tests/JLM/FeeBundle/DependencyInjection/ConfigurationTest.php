<?php

namespace Tests\JLM\FeeBundle\DependencyInjection;

use JLM\FeeBundle\DependencyInjection\Configuration;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class ConfigurationTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Configuration
     */
    private $conf;

    public function setUp()
    {
        $this->conf = new Configuration();
    }
    
    public function assertPreCondition(): void
    {
        $this->assertInstanceOf(ConfigurationInterface::class, $this->conf);
    }
    
    public function testGetConfigTreeBuilder(): void
    {
        $this->assertInstanceOf(TreeBuilder::class, $this->conf->getConfigTreeBuilder());
    }
}
