<?php

namespace JLM\CommerceBundle\Form\Type;

use JLM\DailyBundle\Form\Type\InterventionHiddenType;
use JLM\ModelBundle\Form\Type\DatepickerType;
use JLM\ModelBundle\Form\Type\SiteHiddenType;
use JLM\ModelBundle\Form\Type\TrusteeHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\CommerceBundle\Entity\Bill;

class BillType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('intervention', InterventionHiddenType::class, [
                'required' => false,
            ])
            ->add('siteObject', SiteHiddenType::class, [
                'required' => false,
            ])
            ->add('creation', DatepickerType::class, [
                'label' => 'Date de création'
            ])
            ->add('trustee', TrusteeHiddenType::class, [
                'required' => false,
            ])
            ->add('prelabel', null, [
                'label' => 'Libellé de facturation',
                'required' => false,
            ])
            ->add('trusteeName', null, [
                'label' => 'Syndic',
            ])
            ->add('trusteeAddress', null, [
                'label' => 'Adresse de facturation',
                'attr' => [
                    'class' => 'input-xlarge',
                ]
            ])
            ->add('accountNumber', null, [
                'label' => 'Numéro de compte',
            ])
            ->add('reference', null, [
                'label' => 'Références',
                'attr' => [
                    'class' => 'input-xlarge',
                    'rows' => '3'
                ]
            ])
            ->add('site', null, [
                'label' => 'Affaire', 'attr' => [
                    'class' => 'input-xlarge',
                    'rows' => '3'
                ]
            ])
            ->add('details', null, [
                'label' => 'Détails',
                'attr' => [
                    'class' => 'input-xlarge',
                    'rows' => '3'
                ]
            ])
            ->add('discount', PercentType::class, [
                'label' => 'Remise',
                'attr' => [
                    'class' => 'input-mini'
                ]
            ])
            ->add('maturity', null, [
                'label' => 'Echéance',
                'attr' => [
                    'class' => 'input-mini'
                ]
            ])
            ->add('property', null, [
                'label' => 'Clause de propriété',
                'required' => false,
                'attr' => [
                    'class' => 'input-xxlarge'
                ],
            ])
            ->add('earlyPayment', null, [
                'label' => 'Escompte',
                'attr' => [
                    'class' => 'input-xxlarge'
                ]
            ])
            ->add('penalty', null, [
                'label' => 'Penalités',
                'attr' => [
                    'class' => 'input-xxlarge'
                ]
            ])
            ->add('intro', null, [
                'label' => 'Introduction',
                'required' => false,
                'attr' => [
                    'class' => 'span12',
                    'placeholder' => 'Suite à ...'
                ],
            ])
            ->add('lines', CollectionType::class, [
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => BillLineType::class,
            ])
            ->add('vat', PercentType::class, [
                'scale' => 1,
                'label' => 'TVA applicable',
                'attr' => ['class' => 'input-mini'],
            ])
            ->add('vatTransmitter', HiddenType::class);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Bill::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_commerce_bill';
    }
}
