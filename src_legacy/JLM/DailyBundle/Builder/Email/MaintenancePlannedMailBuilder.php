<?php

namespace JLM\DailyBundle\Builder\Email;

class MaintenancePlannedMailBuilder extends MaintenanceMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Visite d\'entretien planifiée');
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'La visite d\'entretien est plannifié pour la date du'.PHP_EOL
        .'Cordialement'
        .$this->getSignature());
    }
}
