<?php

namespace JLM\ModelBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

abstract class AbstractHiddenType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $cl = $this->getTransformerClass();
        $transformer = new $cl($this->objectManager);
        $builder->addModelTransformer($transformer);
    }

    public function getParent()
    {
        return HiddenType::class;
    }
    
    public function getBlockPrefix(): string
    {
        return $this->getTypeName().'_hidden';
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'invalid_message' => 'The selected '.$this->getTypeName().' does not exist',
        ]);
    }
    
    abstract protected function getTransformerClass();
    
    abstract protected function getTypeName();
}
