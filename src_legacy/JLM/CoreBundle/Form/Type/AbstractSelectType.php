<?php

namespace JLM\CoreBundle\Form\Type;

use Doctrine\ORM\EntityManagerInterface;
use Genemu\Bundle\FormBundle\Form\JQuery\Type\Select2HiddenType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;

abstract class AbstractSelectType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    protected $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return Select2HiddenType::class;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $transformer = $this->getTransformerClass();
        $resolver->setDefaults([
                'transformer' => new $transformer($this->objectManager),
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return $this->getTypeName().'_select';
    }
    
    /**
     * Get the transformer class
     *
     * @return string
     */
    abstract protected function getTransformerClass();
    
    /**
     * Get the type name
     *
     * @return string
    */
    abstract protected function getTypeName();
}
