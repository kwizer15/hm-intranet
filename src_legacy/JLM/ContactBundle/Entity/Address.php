<?php

namespace JLM\ContactBundle\Entity;

use JLM\ContactBundle\Model\AddressInterface;
use JLM\ContactBundle\Model\CityInterface;

class Address implements AddressInterface
{
    /**
     * @var integer $id
     */
    protected $id;
    
    /**
     * @var string $street
     */
    protected $street = '';
    
    /**
     * @var string $city
     */
    protected $city;
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return self
     */
    public function setStreet($street)
    {
        $this->street = (string)$street;
        
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set city
     *
     * @param CityInterface $city
     * @return self
     */
    public function setCity(CityInterface $city = null)
    {
        $this->city = $city;
        
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCity()
    {
        return $this->city;
    }
    
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return ($this->getStreet() == '') ? (string)$this->getCity() : $this->getStreet().PHP_EOL.$this->getCity();
    }
    
    /**
     * To String
     *
     * @deprecated
     * @return string
     */
    public function toString()
    {
        return $this->getStreet().PHP_EOL.$this->getCity()->toString();
    }
}
