<?php

namespace JLM\TransmitterBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\TransmitterBundle\Entity\UserGroupRepository;
use JLM\TransmitterBundle\Entity\UserGroup;
use JLM\TransmitterBundle\Entity\Transmitter;

class TransmitterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $id = $options['id'];
        $builder
            ->add('attribution', AttributionHiddenType::class)
            ->add(
                'userGroup',
                EntityType::class,
                [
                    'class'=> UserGroup::class,
                    'label'=>'Groupe utilisateur',
                    'query_builder'=> function (UserGroupRepository $er) use ($id) {
                        return $er->getFromSite($id);
                    },
                    'placeholder' => 'Choisissez...',
                ]
            )
            ->add('model', null, ['label'=>'Type d\'émetteur','placeholder' => 'Choisissez...',])
            ->add('number', null, ['label'=>'Numéro', 'attr'=>['class'=>'input-small']])
            ->add('guarantee', null, ['label'=>'Garantie', 'attr'=>['class'=>'input-mini','placeholder'=>'MMAA']])
            ->add('userName', null, ['label'=>'Utilisateur','required'=>false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Transmitter::class,
            'attr'=>['class'=>'transmitter'],
            'id' => null,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_transmitterbundle_transmittertype';
    }
}
