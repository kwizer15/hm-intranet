<?php

namespace JLM\DailyBundle\Builder\Email;

class MaintenanceReportMailBuilder extends MaintenanceMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Visite d\'entretien de l\'installation '.$this->getMaintenance()->getInstallationCode());
    }
    
    public function buildBody()
    {
        $maintenance = $this->getMaintenance();
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'La visite d\'entretien de l\'installation : '.PHP_EOL
        .$maintenance->getInstallationCode().PHP_EOL
        .$maintenance->getPlace().PHP_EOL
        .'a été effectuée le '.$maintenance->getLastDate()->format('d/m/Y')
        .$this->getSignature());
    }
}
