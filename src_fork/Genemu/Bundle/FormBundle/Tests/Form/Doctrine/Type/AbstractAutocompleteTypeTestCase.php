<?php

/*
 * This file is part of the GenemuFormBundle package.
 *
 * (c) Olivier Chauvel <olivier@generation-multiple.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Genemu\Bundle\FormBundle\Tests\Form\Doctrine\Type;

use Genemu\Bundle\FormBundle\Tests\Form\Type\TypeTestCase;
use Doctrine\Persistence\ManagerRegistry;

abstract class AbstractAutocompleteTypeTestCase extends TypeTestCase
{
    protected $entityManager;

    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager = null;
    }

    protected function persist(array $entities)
    {
        foreach ($entities as $entity) {
            $this->entityManager->persist($entity);
        }

        $this->entityManager->flush();
    }

    public function testDefaultValue()
    {
        $class = static::SINGLE_IDENT_CLASS;
        $entity1 = new $class(1, 'Foo');
        $entity2 = new $class(2, 'Bar');

        $this->persist([$entity1, $entity2]);

        $form = $this->factory->createNamed('name', $this->getTypeName(), null, [
            'em' => 'default',
            'class' => static::SINGLE_IDENT_CLASS,
            'property' => 'name',
        ]);
        $form->setData(null);

        $view = $form->createView();

        $this->assertEquals(['Foo', 'Bar'], $view->vars['suggestions']);

        $this->assertNull($form->getData());
        $this->assertEquals('', $form->getViewData());

        $this->assertNull($view->vars['route_name']);
    }

    public function testValueData()
    {
        $class = static::SINGLE_IDENT_CLASS;
        $entity1 = new $class(1, 'Foo');
        $entity2 = new $class(2, 'Bar');

        $this->persist([$entity1, $entity2]);

        $form = $this->factory->createNamed('name', $this->getTypeName(), null, [
            'em' => 'default',
            'class' => static::SINGLE_IDENT_CLASS,
        ]);
        $form->setData('Foo');
        $view = $form->createView();
        $form->bind('Bar');

        $this->assertEquals(['Foo', 'Bar'], $view->vars['suggestions']);

        $this->assertEquals('Bar', $form->getViewData());
        $this->assertSame('Bar', $form->getData());

        $this->assertNull($view->vars['route_name']);
    }

    public function testValueAjaxData()
    {
        $class = static::SINGLE_IDENT_CLASS;
        $entity1 = new $class(1, 'Foo');
        $entity2 = new $class(2, 'Bar');

        $this->persist([$entity1, $entity2]);

        $form = $this->factory->createNamed('name', $this->getTypeName(), null, [
            'em' => 'default',
            'class' => static::SINGLE_IDENT_CLASS,
            'property' => 'name',
            'route_name' => 'genemu_ajax'
        ]);

        $form->setData('Foo');
        $view = $form->createView();

        $form->bind('Bar');

        $this->assertEquals('genemu_ajax', $view->vars['route_name']);

        $this->assertEquals([], $view->vars['suggestions']);
        $this->assertEquals('Bar', $form->getViewData());
        $this->assertSame('Bar', $form->getData());
    }

    protected function createRegistryMock($name, $entityManager)
    {
        $registry = $this->createMock(ManagerRegistry::class);
        $registry->expects($this->any())
            ->method('getManager')
            ->with($this->equalTo($name))
            ->will($this->returnValue($entityManager));

        return $registry;
    }
}
