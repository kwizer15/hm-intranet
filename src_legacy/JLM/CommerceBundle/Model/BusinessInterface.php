<?php

namespace JLM\CommerceBundle\Model;

interface BusinessInterface
{
    public function getUnpayedBills();
}
