<?php

namespace JLM\CondominiumBundle\Entity;

use JLM\ContactBundle\Entity\ContactDecorator;
use JLM\CondominiumBundle\Model\ManagerInterface;

class Manager extends ContactDecorator implements ManagerInterface
{

}
