<?php

namespace JLM\ContactBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\City;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class CityController extends AbstractController
{
    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(City::class);
    }

    /**
     * City json
     */
    public function searchAction(Request $request)
    {
        $term = $request->get('q');
        $page_limit = $request->get('page_limit');

        $cities = $this->repository->getArray($term, $page_limit);
        
        return new JsonResponse(['cities' => $cities]);
    }
    
    /**
     * City json
     */
    public function jsonAction(Request $request)
    {
        $id = $request->get('id');
        $city = $this->repository->getByIdToArray($id);
        
        return new JsonResponse($city);
    }
}
