<?php

namespace Tests\JLM\CommerceBundle\Factory;

use JLM\CommerceBundle\Factory\BillLineFactory;
use JLM\CommerceBundle\Builder\BillLineBuilderInterface;
use JLM\CommerceBundle\Model\BillLineInterface;
use PHPUnit\Framework\TestCase;

class BillLineFactoryTest extends TestCase
{
    private $builder;
    
    private $line;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->builder = $this->createMock(BillLineBuilderInterface::class);
        
        $this->line = null;
    }

    /**
     * {@inheritdoc}
     */
    public function assertPreConditions()
    {
        $this->builder
            ->expects($this->once())
            ->method('getLine')
            ->willReturn($this->createMock(BillLineInterface::class))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function assertPostConditions()
    {
        $this->assertInstanceOf('JLM\CommerceBundle\Model\BillLineInterface', $this->line);
    }

    /**
     * @dataProvider builders
     */
    public function testBuilders($method)
    {
        $this->builder->expects($this->once())->method($method);
        $this->line = BillLineFactory::create($this->builder);
    }
    
    public function builders()
    {
        return [
            ['create'],
            ['buildProduct'],
            ['buildQuantity'],
            ['buildPrice'],
        ];
    }
}
