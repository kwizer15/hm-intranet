<?php

namespace JLM\FeeBundle\Form\Type;

use JLM\ModelBundle\Form\Type\DatepickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\FeeBundle\Entity\FeesFollower;

class FeesFollowerType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('activation', DatepickerType::class, ['label'=>'Date d\'activation'])
            ->add('frequence1', null, ['label'=>'Annuelle'])
            ->add('frequence2', null, ['label'=>'Semestriellle'])
            ->add('frequence4', null, ['label'=>'Trimestrielle'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'feesfollower';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => FeesFollower::class,
        ]);
    }
}
