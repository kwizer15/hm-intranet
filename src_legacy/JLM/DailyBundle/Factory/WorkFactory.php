<?php

namespace JLM\DailyBundle\Factory;

use JLM\DailyBundle\Builder\WorkBuilderInterface;

class WorkFactory
{
    /**
     * @param WorkBuilderInterface $bill
     *
     * @return WorkInterface
     */
    public static function create(WorkBuilderInterface $builder)
    {
        $builder->create();
        $builder->buildCreation();
        $builder->buildBusiness();
        $builder->buildReason();
        $builder->buildContact();
        $builder->buildPriority();
        $builder->buildOrder();
        $builder->buildLink();
        
        return $builder->getWork();
    }
}
