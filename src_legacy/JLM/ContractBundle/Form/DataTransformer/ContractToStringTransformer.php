<?php

namespace JLM\ContractBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContractBundle\Entity\Contract;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class ContractToStringTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * Transforms an object (contract) to a string (name).
     *
     * @param  Contract|null $entity
     * @return string
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return "";
        }
        return $entity->getNumber().' / '.$entity->getDoor()->getLocation().' / '.$entity->getDoor()->getType();
    }
    
    /**
     * Transforms a string (number) to an object (contract).
     *
     * @param  string $number
     * @return Contract|null
     * @throws TransformationFailedException if object (contract) is not found.
     */
    public function reverseTransform($string)
    {
        if (!$string) {
            return null;
        }
        $matches = [];
        if (preg_match('#(.+) / (.+) / (.+)#', $string, $matches)) {
            $entity = $this->objectManager
                ->getRepository(Contract::class)
                ->createQueryBuilder('c')
                ->leftJoin('c.door', 'd')
                ->leftJoin('d.type', 't')
                ->where('c.number = :number')
                ->andWhere('d.location = :location')
                ->andWhere('t.name = :type')
                ->setParameter('number', trim($matches[1]))
                ->setParameter('location', trim($matches[2]))
                ->setParameter('type', trim($matches[3]))
                ->getQuery()->getResult();
        }
            
        if (!isset($entity)) {
            throw new TransformationFailedException(sprintf(
                'A contract with name "%s" does not exist!',
                $string
            ));
        }
        
        return $entity[0];
    }
}
