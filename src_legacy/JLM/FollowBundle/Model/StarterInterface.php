<?php

namespace JLM\FollowBundle\Model;

use JLM\DailyBundle\Model\WorkInterface;

interface StarterInterface
{
    /**
     * @return string
     */
    public function getName();
    
    /**
     * @return BayInterface
     */
    public function getBusiness();
    
    /**
     * @return string
     */
    public function getAmount();
    
    /**
     * @return string
     */
    public function getType();
    
    /**
     * @return WorkInterface
     */
    public function getWork();
}
