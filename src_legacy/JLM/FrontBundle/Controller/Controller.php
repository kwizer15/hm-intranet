<?php

namespace JLM\FrontBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ModelBundle\Entity\Site;
use JLM\ModelBundle\Entity\Trustee;
use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\ORM\NoResultException;

class Controller extends BaseController
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    public function __construct(
        EntityManagerInterface $entityManager,
        AuthorizationCheckerInterface $authorizationChecker,
        TokenStorageInterface $tokenStorage
    ) {
        $this->entityManager = $entityManager;
        $this->authorizationChecker = $authorizationChecker;
        $this->tokenStorage = $tokenStorage;
    }

    protected function getConnectedManager(Request $request)
    {
        if (false === $this->authorizationChecker->isGranted('ROLE_MANAGER') &&
            false === $this->authorizationChecker->isGranted('ROLE_BUSINESS')) {
            throw new AccessDeniedException();
        }

        $repoManager = $this->authorizationChecker->isGranted('ROLE_MANAGER')
            ? $this->entityManager->getRepository(Trustee::class)
            : $this->entityManager->getRepository(Site::class)
        ;
         
        try {
            $manager = $repoManager->getByUser($this->tokenStorage->getToken()->getUser());
        } catch (NoResultException $e) {
            if (false === $this->authorizationChecker->isGranted('ROLE_ADMIN')) {
                throw new AccessDeniedException('Pas de contact lié à ce compte');
            }
            $session = $request->getSession();
            if ($managerId = $request->get('managerId')) {
                $session->set('managerId', $managerId);
            }
            $manager = $repoManager->find($session->get('managerId', 316)); // Pour tests
        }
        
        return $manager;
    }
}
