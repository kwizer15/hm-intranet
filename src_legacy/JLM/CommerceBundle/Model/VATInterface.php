<?php

namespace JLM\CommerceBundle\Model;

interface VATInterface
{
    /**
     * Get rate
     *
     * @return int
     */
    public function getRate();
}
