<?php

namespace JLM\FeeBundle\Form\Type;

use JLM\ContractBundle\Form\Type\ContractSelectType;
use JLM\ModelBundle\Form\Type\TrusteeSelectType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\CommerceBundle\Entity\VAT;
use JLM\FeeBundle\Entity\Fee;

class FeeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('trustee', TrusteeSelectType::class, [
                'label' => 'Syndic',
                'attr' => [
                    'class' => 'input-xlarge',
                ],
            ])
            ->add('address', null, [
                'label' => 'Adresse',
                'attr' => [
                    'class' => 'input-xlarge',
                ],
            ])
            ->add('prelabel', null, [
                'label' => 'Libélé',
                'attr' => [
                    'class' => 'input-xlarge',
                ],
            ])
            ->add('frequence', ChoiceType::class, [
                'label' => 'Fréquence',
                'choices' => [
                    1 => 'Annuelle',
                    2 => 'Semestrielle',
                    4 => 'Trimestrielle',
                ], 'attr' => [
                    'class' => 'input-normal',
                ],
            ])
            ->add('vat', EntityType::class, [
                'label' => 'TVA',
                'class' => VAT::class,
                'attr' => [
                    'class' => 'input-small',
                ],
            ])
            ->add('contracts', CollectionType::class, [
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => ContractSelectType::class,
                'label' => 'Contrats',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'fee';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Fee::class,
        ]);
    }
}
