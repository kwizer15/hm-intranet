<?php

namespace HMFermeture\Domain\Devis\Repository\Exception;

use HMFermeture\Domain\Common\Repository\Exception\NoResultException;

class ResponsableNotFoundException extends NoResultException
{

}
