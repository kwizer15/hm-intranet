<?php

namespace JLM\FollowBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JLM\FollowBundle\Entity\Thread;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\DailyBundle\JLMDailyEvents;
use JLM\CoreBundle\Event\DoctrineEvent;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\NonUniqueResultException;

class ShiftTechnicianSubscriber implements EventSubscriberInterface
{
   
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * Constructor
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            JLMDailyEvents::SHIFTTECHNICIAN_POSTPERSIST => 'updateThread',
            JLMDailyEvents::SHIFTTECHNICIAN_POSTREMOVE => 'updateThread',
        ];
    }
    
    /**
     * Update thread since work update
     * @param InterventionEvent $event
     */
    public function updateThread(DoctrineEvent $event)
    {
        if ($thread = $this->getThread($event)) {
            $thread->getState();
            $this->objectManager->persist($thread);
            $this->objectManager->flush();
        }
    }
    
    /**
     *
     * @param DoctrineEvent $event
     * @return mixed|NULL
     */
    private function getThread(DoctrineEvent $event)
    {
        try {
            return $this->objectManager->getRepository(Thread::class)->getByShiftTechnician($event->getEntity());
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
