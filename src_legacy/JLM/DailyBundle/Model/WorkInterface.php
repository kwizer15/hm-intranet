<?php

namespace JLM\DailyBundle\Model;

interface WorkInterface extends InterventionInterface
{
    public function getOrder();
}
