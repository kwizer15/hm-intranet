<?php

namespace JLM\ModelBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\Quote;
use JLM\ContactBundle\Entity\Contact;
use JLM\ModelBundle\Entity\Door;
use JLM\ModelBundle\Entity\Site;
use JLM\ModelBundle\Entity\SiteContact;
use JLM\ModelBundle\Entity\Trustee;
use JLM\ProductBundle\Entity\Product;
use JLM\ProductBundle\Entity\Supplier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class DefaultController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Resultats de la barre de recherche.
     */
    public function searchAction(Request $request)
    {

        $formData = $request->get('jlm_core_search');

        if (is_array($formData) && array_key_exists('query', $formData)) {
            $query = $formData['query'];
            return $this->render('@JLMModel/Default/index.html.twig', [
                    'query' => $query,
                    'contacts' => $this->entityManager->getRepository(Contact::class)->search($query),
                    'doors'   => $this->entityManager->getRepository(Door::class)->search($query),
                    'sites'   => $this->entityManager->getRepository(Site::class)->search($query),
                    'trustees'=> $this->entityManager->getRepository(Trustee::class)->search($query),
                    'suppliers'=> $this->entityManager->getRepository(Supplier::class)->search($query),
                    'products' => $this->entityManager->getRepository(Product::class)->search($query),
            ]);
        }

        return $this->render('@JLMModel/Default/index.html.twig', []);
    }
    
    /**
     * Upgrade contacts
     */
    public function upgradeAction()
    {
        $repo = $this->entityManager->getRepository(SiteContact::class);
        $contacts = $repo->findAll();
        foreach ($contacts as $contact) {
            $role = $contact->getOldRole();
            $person = $contact->getPerson();
            $person->setRole($role);
            $this->entityManager->persist($person);
        }
        $quotes = $this->entityManager->getRepository(Quote::class)->findAll();
        foreach ($quotes as $quote) {
            $contact = $quote->getContact();
            if ($contact !== null) {
                $quote->setContactPerson($contact->getPerson());
            }
            $this->entityManager->persist($quote);
        }
        $this->entityManager->flush();
        return $this->render('@JLMModel/Default/upgrade.html.twig', []);
    }
}
