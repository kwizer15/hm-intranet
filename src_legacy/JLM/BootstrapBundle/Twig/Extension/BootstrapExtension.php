<?php

namespace JLM\BootstrapBundle\Twig\Extension;

class BootstrapExtension extends \Twig\Extension\AbstractExtension
{
    public function getName()
    {
        return 'bootstrap_extension';
    }
    
    public function getFilters()
    {
        return [
                new \Twig_SimpleFilter('icon', [$this, 'iconFilter'], ['is_safe' => ['all']]),
                new \Twig_SimpleFilter('badge', [$this, 'badgeFilter'], ['is_safe' => ['all']]),
        ];
    }
    
    public function iconFilter($iconName, $white = false, $version = '3.3.5')
    {
        return '<span class="glyphicon glyphicon-'.$iconName.'"></span>';
    }
    
    public function badgeFilter($content, $class = null)
    {
        $class = ($class === null) ? 'badge' : 'badge badge-'.$class;
        
        return '<span class="'.$class.'">'.$content.'</span>';
    }
        
    public function getGlobals()
    {
        return [
                'twbs' => ['version' => 3],
        ];
    }
}
