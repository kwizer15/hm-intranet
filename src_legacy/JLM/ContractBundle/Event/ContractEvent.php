<?php

namespace JLM\ContractBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use JLM\ContractBundle\Entity\Contract;

class ContractEvent extends Event
{
    /** @var string */
    protected $contract = null;

    public function __construct(Contract $contract)
    {
        $this->contract = $contract;
    }

    public function getContract()
    {
        return $this->contract;
    }
}
