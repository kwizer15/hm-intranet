<?php

namespace Tests\JLM\StateBundle\Entity;

use JLM\StateBundle\Entity\Calendar;

class StateTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Country
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new Calendar();
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
//        $this->assertInstanceOf('JLM\AskBundle\Model\AskInterface', $this->entity);
    }
    
    public function getGetterSetter()
    {
        return [
            ['Dt', new \DateTime],
            ['Date', new \DateTime],
        ];
    }
    
    /**
     * @dataProvider getGetterSetter
     */
    public function testGetterSetter($attribute, $value)
    {
        $getter = 'get'.$attribute;
        $setter = 'set'.$attribute;
        $this->assertSame($this->entity, $this->entity->$setter($value));
        $this->assertSame($value, $this->entity->$getter());
    }
}
