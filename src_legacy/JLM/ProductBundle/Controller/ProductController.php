<?php

namespace JLM\ProductBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ProductBundle\Entity\Stock;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use JLM\ProductBundle\Entity\Product;
use JLM\ProductBundle\Form\Type\ProductType;
use JLM\ProductBundle\JLMProductEvents;
use JLM\ProductBundle\Event\ProductEvent;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class ProductController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Product::class);
    }

    /**
     * Lists all Product entities.
     */
    public function indexAction(Request $request)
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 15);
        $all = $request->get('all', 0);

        $nb = $this->repository->getTotal(!$all);
        $nbPages = ceil($nb/$limit);
        $nbPages = ($nbPages < 1) ? 1 : $nbPages;
        $offset = ($page-1) * $limit;
        if ($page < 1 || $page > $nbPages) {
            throw $this->createNotFoundException('Page inexistante (page '.$page.'/'.$nbPages.')');
        }

        $entities = $this->repository->getAll($limit, $offset, !$all);

        return $this->render('@JLMProduct/Product/index.html.twig', [
            'entities' => $entities,
            'page'     => $page,
            'nbPages'  => $nbPages,
        ]);
    }

    /**
     * Finds and displays a Product entity.
     */
    public function showAction($id)
    {
        $entity = $this->getEntity($id);
        $stock = $this->entityManager->getRepository(Stock::class)->getByProduct($entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@JLMProduct/Product/show.html.twig', [
            'entity'      => $entity,
            'stock'=>$stock,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to create a new Product entity.
     */
    public function newAction()
    {

        $entity = new Product();
        $entity->setUnity('pièce');
        $entity->setDiscountSupplier(0);
        $entity->setExpenseRatio(10);
        $entity->setShipping(0);
        $entity->setUnitPrice(0);
        
        $form   = $this->createNewForm($entity);
            
        return $this->render('@JLMProduct/Product/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Creates a new Product entity.
     */
    public function createAction(EventDispatcherInterface $eventDispatcher, Request $request)
    {

        $entity = new Product();

        $form = $this->createNewForm($entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            $eventDispatcher->dispatch(JLMProductEvents::PRODUCT_CREATE, new ProductEvent($entity));
            
            return ($this->redirectToRoute('jlm_product_product_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMProduct/Product/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Product entity.
     */
    public function editAction($id)
    {

        $entity = $this->getEntity($id);
        $editForm = $this->createEditForm($entity);

        return $this->render('@JLMProduct/Product/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Edits an existing Product entity.
     */
    public function updateAction(Request $request, $id)
    {

        $entity = $this->getEntity($id);
        $editForm = $this->createEditForm($entity);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('jlm_product_product_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMProduct/Product/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Deletes a Product entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity = $this->getEntity($id);

            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }

        return ($this->redirectToRoute('jlm_product_product'));
    }
    
    /**
     * Get the entity from id
     * @param int $id
     * @return Product
     */
    private function getEntity($id)
    {
        $entity = $this->repository->find($id);
    
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }
    
        return $entity;
    }
    
    /**
     * Get the delete form
     * @param int $id
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
    
    /**
     * Get the edit form
     * @param ProductCategory $entity
     */
    private function createEditForm(Product $entity)
    {
        return $this->createForm(ProductType::class, $entity);
    }
    
    /**
     * Get the new form
     * @param ProductCategory $entity
     */
    private function createNewForm(Product $entity)
    {
        return $this->createForm(ProductType::class, $entity);
    }
}
