<?php

namespace JLM\ProductBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Response\PdfResponse;
use JLM\CoreBundle\Service\Paginator;
use JLM\ProductBundle\Manager\StockManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use JLM\ProductBundle\Pdf\Stock;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class StockController extends AbstractController
{
    /**
     * @var StockManager
     */
    private $manager;
    
    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager, StockManager $manager)
    {
        $this->manager = $manager;
        $this->repository = $entityManager->getRepository(\JLM\ProductBundle\Entity\Stock::class);
    }

    /**
     * Lists all Stock entities.
     *
     */
    public function indexAction(Paginator $paginator, Request $request)
    {
        return $this->render(
            '@JLMProduct/Stock/index.html.twig',
            $paginator->pagination($request, $this->repository, 'getCount', 'getAll', 'jlm_product_stock')
        );
    }

    /**
     * Displays a form to edit an existing Stock entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $entity = $this->repository->find($id);
        $form = $this->manager->createForm($request, 'edit', ['entity' => $entity]);
        if ($this->manager->getHandler($form, $request)->process()) {
            return $request->isXmlHttpRequest()
                ? new JsonResponse([])
                : $this->redirectToRoute('jlm_product_stock_edit', ['id' => $id])
            ;
        }
        $template = $request->isXmlHttpRequest() ? 'modal_edit.html.twig' : 'edit.html.twig';
        
        return  $this->render('@JLMProduct/Stock/'.$template, [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }
    
    public function inventoryAction(Request $request)
    {
        $entity = $this->repository->getAll(500);
        $form = $this->manager->createForm($request, 'inventory', ['entity' => $entity]);
        
        if ($this->manager->getHandler($form, $request)->process()) {
            return $this->redirectToRoute('jlm_product_stock_inventory');
        }
        
        return $this->render('@JLMProduct/Stock/inventory.html.twig', [
            'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Imprime la liste d'attribution
     */
    public function printAction()
    {
        $stocks = $this->repository->getAll(500);
    
        return new PdfResponse('stock.pdf', Stock::get($stocks));
    }
}
