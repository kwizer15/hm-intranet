<?php

namespace JLM\CommerceBundle\Builder\Email;

use JLM\CommerceBundle\Pdf\Bill;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BillBoostBusinessMailBuilder extends BusinessMailBuilder
{

    /**
     * {@inheritdoc}
     */
    public function buildSubject()
    {
        $s = ($this->getBusiness()->getUnpayedBills() > 1) ? 's' : '';
        $this->setSubject('Relance facture'.$s.' non payée'.$s);
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildBody()
    {
        $s = '';
        $la = 'la';
        if ($this->getBusiness()->getUnpayedBills() > 1) {
            $s .= 's';
            $la = 'les';
        }
        $tot = 0;
        $numbers = '';
        foreach ($this->getBusiness()->getUnpayedBills() as $bill) {
            $p = $bill->getTotalPriceAti();
            $tot += $p;
            $numbers .= 'n°'.$bill->getNumber().' - '.$p.' €'.PHP_EOL;
        }
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'Suite à nos relances restées sans réponse de votre part concernant '.$la.' facture'.$s.' ci-jointe'.$s
        .' dont la totalité des réglements ne nous est pas encore parvenue à ce jour et dont l\'échéance est '
        .'dépassée, le montant global restant à payer s\'élevant à : '.$tot.' €'.PHP_EOL.PHP_EOL
        .$numbers.PHP_EOL
        .'Nous vous prions de nous faire parvenir ce règlement sous 72 heures, en cas de retard de paiement,'
        .' une indémnité légale forfaitaire pour frais de recouvrement de 40 € sera appliquée.'.PHP_EOL.PHP_EOL
        .'Nous vous prions d\'agréer, Madame, Monsieur, l\'expression de nos salutations distinguées.'
        .$this->getSignature());
    }
    
    public function buildPreAttachements()
    {
        foreach ($this->getBusiness()->getUnpayedBills() as $bill) {
            $name = 'uploads/FAC'.$bill->getNumber().'.pdf';
            Bill::save([$bill], true, $name);
            $file = new UploadedFile($name, 'FAC'.$bill->getNumber().'.pdf', 'application/pdf');
            $this->getMail()->addPreAttachement($file);
        }
    }
}
