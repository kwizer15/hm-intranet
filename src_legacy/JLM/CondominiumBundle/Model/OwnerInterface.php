<?php

namespace JLM\CondominiumBundle\Model;

interface OwnerInterface
{
    /**
     * Get the name
     * @return string
     */
    public function getName();
    
    /**
     * Get the property
     * @return PropertyInterface
     */
    public function getProperty();
}
