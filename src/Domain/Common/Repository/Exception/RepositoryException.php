<?php

namespace HMFermeture\Domain\Common\Repository\Exception;

use HMFermeture\Domain\Common\Exception\DomainException;

class RepositoryException extends DomainException
{

}
