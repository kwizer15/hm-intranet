<?php

namespace JLM\CommerceBundle\Entity;

use JLM\CommerceBundle\Model\BillLineInterface;
use JLM\CommerceBundle\Model\BillInterface;

class BillLine extends CommercialPartLineProduct implements BillLineInterface
{
    /**
     * @var integer $id
     */
    private $id;
    
    /**
     * @var BillInterface $bill
     */
    private $bill;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set bill
     *
     * @param BillInterface $bill
     * @return self
     */
    public function setBill(BillInterface $bill = null)
    {
        $this->bill = $bill;
    
        return $this;
    }

    /**
     * Get bill
     *
     * @return BillInterface
     */
    public function getBill()
    {
        return $this->bill;
    }
}
