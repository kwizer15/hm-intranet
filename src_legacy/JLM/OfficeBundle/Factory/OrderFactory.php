<?php

namespace JLM\OfficeBundle\Factory;

use JLM\OfficeBundle\Builder\OrderBuilderInterface;

class OrderFactory
{
    /**
     *
     * @param BillBuilderInterface $bill
     *
     * @return BillInterface
     */
    public static function create(OrderBuilderInterface $builder)
    {
        $builder->create();
        $builder->buildCreation();
        $builder->buildTime();
        $builder->buildLines();
     
        return $builder->getOrder();
    }
}
