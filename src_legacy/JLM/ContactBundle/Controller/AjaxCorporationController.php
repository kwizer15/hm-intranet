<?php

namespace JLM\ContactBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\Corporation;
use JLM\ContactBundle\Repository\CorporationRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AjaxCorporationController extends Controller
{
    /**
     * @var CorporationRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->repository = $entityManager->getRepository(Corporation::class);
    }

    /**
     * Person json
     */
    public function searchAction(Request $request)
    {
        $term = $request->get('q');
        $page_limit = $request->get('page_limit');
        $contacts = $this->repository->getArray($term, $page_limit);
        
        return new JsonResponse(['corporations' => $contacts]);
    }
    
    /**
     * Person json
     */
    public function jsonAction(Request $request)
    {
        $id = $request->get('id');
        $contact = $this->repository->getByIdToArray($id);
    
        return new JsonResponse($contact);
    }
}
