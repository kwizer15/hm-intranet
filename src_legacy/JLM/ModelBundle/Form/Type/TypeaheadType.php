<?php
namespace JLM\ModelBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TypeaheadType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'widget'=> TextType::class,
        ]);
    }
    
    public function getParent()
    {
        return EntityType::class;
    }
    
    public function getBlockPrefix(): string
    {
        return 'typeahead';
    }
}
