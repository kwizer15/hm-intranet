<?php

namespace JLM\DailyBundle\Builder\Email;

class WorkEndMailBuilder extends WorkMailBuilder
{

    public function buildSubject()
    {
        $source = $this->getSource();
        $this->setSubject('Installation '.$this->getWork()->getInstallationCode().' : Travaux'.$source.' terminés');
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
                .'Installation : '.PHP_EOL
                .$this->getWork()->getInstallationCode().PHP_EOL
                .$this->getWork()->getPlace().PHP_EOL
                .PHP_EOL
        .'Les travaux'.$this->getSource().' ont bien été réalisés le '
            .$this->getWork()->getLastDate()->format('d/m/Y').'.'.PHP_EOL
        .'L\'installation est en fonction.'.PHP_EOL
        .$this->getSignature());
    }
}
