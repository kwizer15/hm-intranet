<?php

namespace Tests\JLM\DailyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class InterventionControllerTest extends WebTestCase
{
    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->client = static::createClient();
    }
    
    public function testNew()
    {
        $this->client->enableProfiler();
        $this->client->followRedirects();
        
        $crawler = $this->client->request('GET', '/daily/intervention/today');
        // Page d'identification (a supprimer plus tard)
        $crawler = $this->login($crawler);
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testEntitiesAreValid()
    {
        $this->client->enableProfiler();
        $this->client->followRedirects();
        
        $crawler = $this->client->request('GET', '/daily/intervention/today');
        // Page d'identification (a supprimer plus tard)
        $crawler = $this->login($crawler);
        if ($profile = $this->client->getProfile()) {
            $this->assertEquals(0, $profile->getCollector('db')->getInvalidEntityCount());
        } else {
            $this->markTestSkipped('Profiler is not activated');
        }
    }
    
    /**
     * Log the user
     * @param Crawler $crawler
     */
    private function login($crawler)
    {
        $form = $crawler->selectButton('S\'identifier')->form();
        
        // définit certaines valeurs
        $form['username'] = 'kwizer';
        $form['password'] = 'sslover';
        return $this->client->submit($form);
    }
}
