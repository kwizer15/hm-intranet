<?php

namespace JLM\TransmitterBundle\Twig\Extension;

use Doctrine\ORM\EntityManagerInterface;
use JLM\TransmitterBundle\Entity\Ask;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class AskCountExtension extends AbstractExtension implements GlobalsInterface
{
    private $objectManager;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public function getName()
    {
        return 'transmitteraskcount_extension';
    }
    
    public function getGlobals()
    {
        $repo = $this->objectManager->getRepository(Ask::class);
        
        return ['transmitteraskcount' => [
                'all' => $repo->getTotal(),
                'untreated' => $repo->getCountUntreated(),
                'treated' => $repo->getCountTreated(),
        ]];
    }
}
