<?php

namespace JLM\CommerceBundle\Form\Type;

use JLM\CommerceBundle\Entity\QuoteLine;
use JLM\ModelBundle\Form\Type\DatepickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\CommerceBundle\Entity\QuoteVariant;

class QuoteVariantType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('quote', QuoteHiddenType::class)
            ->add('creation', DatepickerType::class, [
                'label' => 'Date de création'
            ])
            ->add('discount', PercentType::class, [
                'label' => 'Remise',
                'attr' => [
                    'class' => 'input-mini'
                ]
            ])
            ->add('paymentRules', null, [
                'label' => 'Réglement',
                'attr' => [
                    'class' => 'input-xxlarge'
                ]
            ])
            ->add('deliveryRules', null, [
                'label' => 'Délai',
                'attr' => [
                    'class' => 'input-xxlarge'
                ]
            ])
            ->add('intro', null, [
                'label' => 'Introduction',
                'attr' => [
                    'class' => 'span12',
                    'placeholder' => 'Suite à ...'
                ],
            ])
            ->add('lines', CollectionType::class, [
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'entry_type' => QuoteLineType::class,
            ])
            ->add('vat', HiddenType::class, ['mapped' => false])
            ->add('vatTransmitter', HiddenType::class, ['mapped' => false]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => QuoteVariant::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'quote_variant';
    }
}
