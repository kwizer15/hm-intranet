<?php
namespace JLM\ModelBundle\Form\DataTransformer;

use JLM\ModelBundle\Entity\Site;

class SiteToIntTransformer extends ObjectToIntTransformer
{
    public function getClass()
    {
        return Site::class;
    }
    
    protected function getErrorMessage()
    {
        return 'A site with id "%s" does not exist!';
    }
}
