<?php

namespace JLM\ProductBundle\Model;

interface ProductInterface
{
    /**
     * Get unitPrice
     *
     * @return float
     */
    public function getUnitPrice($quantity = null);
    
    /**
     * Get reference
     *
     * @return string
     */
    public function getReference();
    
    /**
     * Get category
     *
     * @return ProductCategoryInterface
     */
    public function getCategory();
    
    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation();
    
    /**
     * Get description
     *
     * @return text
     */
    public function getDescription();
    
    /**
     * Get is small supplies
     * @return boolean
     */
    public function isSmallSupply();
    
    /**
     * Get if is service
     * @return boolean
    */
    public function isService();
}
