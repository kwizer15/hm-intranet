<?php

namespace JLM\ContactBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\Contact;
use JLM\CoreBundle\Manager\BaseManager as Manager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\RouterInterface;
use JLM\ContactBundle\Form\Type\PersonType;
use JLM\ContactBundle\Form\Type\CompanyType;
use JLM\ContactBundle\Form\Type\AssociationType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ContactManager extends Manager
{
    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        TokenStorageInterface $tokenStorage,
        FormFactoryInterface $formFactory
    ) {
        parent::__construct(
            Contact::class,
            $entityManager,
            $router,
            $tokenStorage,
            $formFactory
        );
    }

    protected function getFormType($type = null)
    {
        $objects = [
                'person' => PersonType::class,
                'company' => CompanyType::class,
                'association' => AssociationType::class,
        ];
        if (array_key_exists($type, $objects)) {
            return new $objects[$type];
        }
    
        return null;
    }
    
    protected function getFormParam($name, $options = [])
    {
        switch ($name) {
            case 'new':
                return [
                    'method' => 'POST',
                    'route' => 'jlm_contact_contact_create',
                    'params' => ['id' => $options['type']],
                    'label' => 'Créer',
                    'type'  => $this->getFormType($options['type']),
                    'entity' => null,
                ];
            case 'edit':
                return [
                    'method' => 'POST',
                    'route' => 'jlm_contact_contact_update',
                    'params' => ['id' => $options['entity']->getId()],
                    'label' => 'Modifier',
                    'type'  => $this->getFormType($options['type']),
                    'entity' => $options['entity']
                ];
        }
        
        return parent::getFormParam($name, $options);
    }
}
