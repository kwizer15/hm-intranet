<?php

namespace JLM\ModelBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\TextType;

abstract class AbstractSelectType extends AbstractHiddenType
{
    public function getBlockPrefix(): string
    {
        return $this->getTypeName().'_select';
    }
    
    public function getParent(): string
    {
        return TextType::class;
    }
}
