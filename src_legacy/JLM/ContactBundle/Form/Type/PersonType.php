<?php

namespace JLM\ContactBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ContactBundle\Entity\Person;

class PersonType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', ChoiceType::class, [
                'label'=>'Titre',
                'choices'=>[
                    'M.'=>'M.',
                    'Mme'=>'Mme',
                    'Mlle'=>'Mlle'
                ]
            ])
            ->add('lastName', null, ['label'=>'Nom'])
            ->add('firstName', null, ['label'=>'Prénom', 'required'=>false])
            ->add('contact', ContactType::class, ['data_class' => Person::class])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_contact_person';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
        ->setDefaults([
            'data_class' => Person::class,
            'label' => 'Personne',
        ]);
    }
}
