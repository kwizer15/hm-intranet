<?php

namespace JLM\DailyBundle\Form\Type;

use JLM\ModelBundle\Form\Type\DatepickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\DailyBundle\Entity\ShiftTechnician;

class AddTechnicianType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('technician', null, ['label'=>'Technicien'])
            ->add('begin', DatepickerType::class, ['label'=>'Date'])
            ->add('comment', TextareaType::class, ['label'=>'Commentaire','required'=>false,'attr'=>['class'=>'input-xlarge']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ShiftTechnician::class
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_dailybundle_addtechniciantype';
    }
}
