<?php

namespace JLM\ModelBundle\Form\Type;

use JLM\ContactBundle\Form\Type\AddressType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ModelBundle\Entity\Site;

class SiteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('trustee', TrusteeSelectType::class, ['label' => 'Syndic', 'attr' => ['class' => 'input-large']])
            ->add('address', AddressType::class, ['label' => 'Adresse'])
            ->add('observations', null, ['label' => 'Observations', 'attr' => ['class' => 'input-xlarge']])
            ->add('groupNumber', null, [
                'label' => 'Groupe (RIVP)',
                'required' => false,
                'attr' => ['class' => 'input-mini'],
            ])
            ->add('accession', ChoiceType::class, [
                'label' => 'Accession/Social',
                'choices' => ['1' => 'Accession', '0' => 'Social'],
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('vat', null, ['label' => 'TVA', 'attr' => ['class' => 'input-small']])
            ->add('lodge', AddressType::class, ['label' => 'Loge gardien', 'required' => false]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Site::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_modelbundle_sitetype';
    }
}
