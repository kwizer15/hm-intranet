<?php

namespace JLM\ContactBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CityRepository extends EntityRepository
{
    /**
     *
     * @param string $query
     * @param int $limit
     * @return array
     */
    public function searchResult($query, $limit = 8)
    {
        $queryBuilder = $this->createQueryBuilder('c')
               ->where('c.zip LIKE :query')
               ->orWhere('c.name LIKE :query')
               ->setParameter('query', $query.'%')
        ;
        $res = $queryBuilder->getQuery()->getResult();
        $r2 = [];
        foreach ($res as $r) {
            $r2[] = ''.$r;
        }
        
        return $r2;
    }
    
    /**
     *
     * @param string $query
     * @param int $limit
     * @return array
     */
    public function getArray($query, $limit = 8)
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->where('c.zip LIKE :query')
            ->orWhere('c.name LIKE :query')
            ->setParameter('query', $query.'%')
        ;
        $res = $queryBuilder->getQuery()->getArrayResult();
        
        return $res;
    }
    
    /**
     *
     * @param int $id
     * @return array|null
     */
    public function getByIdToArray($id)
    {
        $queryBuilder = $this->createQueryBuilder('c')
            ->where('c.id = :id')
            ->setParameter('id', $id)
        ;
        $res = $queryBuilder->getQuery()->getArrayResult();
        
        if (isset($res[0])) {
            return $res[0];
        }
        
        return null;
    }
}
