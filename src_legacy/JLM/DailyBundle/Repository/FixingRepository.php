<?php

namespace JLM\DailyBundle\Repository;

class FixingRepository extends InterventionRepository
{
    public function getToGive()
    {
        $today = new \DateTime;
        $todaystring =  $today->format('Y-m-d');
        // Interventions en cours
        $queryBuilder = $this->createQueryBuilder('a')
            ->select('a,b,k,l,c,e,f,g,h,i,j,m,n')
            ->leftJoin('a.shiftTechnicians', 'b')
            ->leftJoin('a.askQuote', 'k')
            ->leftJoin('a.work', 'l')
            ->leftJoin('a.door', 'c')
            //->leftJoin('c.interventions','d')
            ->leftJoin('c.site', 'e')
            ->leftJoin('e.address', 'f')
            ->leftJoin('f.city', 'g')
            ->leftJoin('e.bills', 'h')
            ->leftJoin('c.stops', 'i')
            ->leftJoin('c.contracts', 'j')
            ->leftJoin('j.trustee', 'm')
            ->leftJoin('c.type', 'n')
            //->leftJoin('d.shiftTechnicians','o')
            ->where('b.id is null')
            ->orderBy('a.creation', 'asc')
            ;
        return $queryBuilder->getQuery()->getResult();
    }

    public function getToday()
    {
        $today = new \DateTime;
        $todaystring =  $today->format('Y-m-d');
        // Interventions en cours
        $queryBuilder = $this->createQueryBuilder('a')
        ->select('a,b,k,l,c,e,f,g,h,i,j,m,n')
        ->leftJoin('a.shiftTechnicians', 'b')
        ->leftJoin('a.askQuote', 'k')
        ->leftJoin('a.work', 'l')
        ->leftJoin('a.door', 'c')
        //->leftJoin('c.interventions','d')
        ->leftJoin('c.site', 'e')
        ->leftJoin('e.address', 'f')
        ->leftJoin('f.city', 'g')
        ->leftJoin('e.bills', 'h')
        ->leftJoin('c.stops', 'i')
        ->leftJoin('c.contracts', 'j')
        ->leftJoin('j.trustee', 'm')
        ->leftJoin('c.type', 'n')
        //->leftJoin('d.shiftTechnicians','o')
        ->where('b.begin = ?1')
        //          ->orWhere('b is null')
        //          ->orWhere('a.close is null')
        //          ->orWhere('a.report is null')
        ->orWhere('a.mustBeBilled is null and b.id is not null')
        ->orWhere('l.id is null and k.id is null and a.contactCustomer is null '
            .'and a.rest is not null and b.id is not null')
        ->orderBy('a.creation', 'asc')
        ->setParameter(1, $todaystring)
        ;
        return $queryBuilder->getQuery()->getResult();
    }
}
