<?php

namespace JLM\ContactBundle\Model;

interface CountryInterface
{
    /**
     * Get code
     *
     * @return string
     */
    public function getCode();
    
    /**
     * Get name
     *
     * @return string
     */
    public function getName();
    
    /**
     * To string
     *
     * @return string
     */
    public function __toString();
}
