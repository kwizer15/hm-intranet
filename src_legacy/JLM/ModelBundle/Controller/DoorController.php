<?php

namespace JLM\ModelBundle\Controller;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JLM\ModelBundle\Form\Type\DoorTagType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use JLM\ModelBundle\Entity\Site;
use JLM\ModelBundle\Entity\Door;
use JLM\ContractBundle\Entity\Contract;
use JLM\ModelBundle\Form\Type\DoorType;
use JLM\ContractBundle\Form\Type\ContractType;
use JLM\ContractBundle\Form\Type\ContractStopType;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class DoorController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Door::class);
    }

    /**
     * Lists all Door entities.
     */
    public function indexAction()
    {
        $entities = $this->repository->findAll();

        return $this->render('@JLMModel/Door/index.html.twig', ['entities' => $entities]);
    }

    /**
     * Finds and displays a Door entity.
     */
    public function showAction(FormFactoryInterface $formFactory, Door $entity)
    {
        $contracts = $this->entityManager->getRepository(Contract::class)->findByDoor($entity, ['begin' => 'DESC']);

        // Modal nouveau contrat
        $contractNew = new Contract();
        $contractNew->setDoor($entity);
        $contractNew->setTrustee($entity->getAdministrator()->getTrustee());
        $contractNew->setBegin(new DateTime());
        $formContractNew = $this->createForm(ContractType::class, $contractNew);

        // Formulaires d'edition des contrat
        $formContractEdits = $formContractStops = [];

        foreach ($contracts as $contract) {
            $formContractEdits[] = $formFactory->createNamed(
                'contractEdit' . $contract->getId(),
                ContractType::class,
                $contract
            )->createView();
            $formContractStops[] = $formFactory->createNamed(
                'contractStop' . $contract->getId(),
                ContractStopType::class,
                $contract
            )->createView();
        }

        return $this->render('@JLMModel/Door/show.html.twig', [
            'entity' => $entity,
            'contracts' => $contracts,
            'formContractNew' => $formContractNew->createView(),
            'formContractEdits' => $formContractEdits,
            'formContractStops' => $formContractStops,
        ]);
    }

    /**
     * Displays a form to create a new Door entity.
     */
    public function newAction(Site $site = null)
    {
        $entity = new Door();
        if ($site) {
            $entity->setAdministrator($site);
            $entity->setStreet($site->getAddress()->getStreet());
        }
        $form = $this->createForm(DoorType::class, $entity);

        return $this->render('@JLMModel/Door/new.html.twig', [
            'site' => $site,
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a new Door entity.
     */
    public function createAction(Request $request)
    {
        $entity = new Door();
        $form = $this->createForm(DoorType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('door_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMModel/Door/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Door entity.
     */
    public function editAction(Door $entity)
    {
        $editForm = $this->createForm(DoorType::class, $entity);
        $deleteForm = $this->createDeleteForm($entity->getId());

        return $this->render('@JLMModel/Door/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }


    /**
     * Edits an existing Door entity.
     */
    public function updateAction(Request $request, Door $entity)
    {
        $editForm = $this->createForm(DoorType::class, $entity);
        $deleteForm = $this->createDeleteForm($entity->getId());
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('door_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMModel/Door/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Edits an existing Door entity.
     */
    public function updateCodeAction(Request $request, Door $entity)
    {
        $codeForm = $this->createCodeForm($entity);
        $codeForm->handleRequest($request);

        if ($codeForm->isSubmitted() && $codeForm->isValid()) {
            $code = $entity->getCode();
            $doublon = $this->repository->findByCode($code);
            if (sizeof($doublon) > 0) {
                return $this->redirect($request->headers->get('referer'));
            }
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }

        return $this->redirect($request->headers->get('referer'));
    }

    private function createCodeForm(Door $door)
    {
        $form = $this->createForm(
            DoorTagType::class,
            $door,
            [
                'action' => $this->generateUrl('model_door_update_code', ['id' => $door->getId()]),
                'method' => 'POST',
            ]
        );

        return $form;
    }

    /**
     * Deletes a Door entity.
     */
    public function deleteAction(Request $request, Door $entity)
    {
        $form = $this->createDeleteForm($entity->getId());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }

        return ($this->redirectToRoute('door'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', HiddenType::class)
            ->getForm();
    }

    /**
     * Lists all Door entities.
     */
    public function geocodeAction()
    {
        $entities = $this->repository->findBy(['latitude' => null]);
        $count = 0;
        $logs = [];
        foreach ($entities as $entity) {
            if ($entity->getLatitude() === null) {
                $this->entityManager->persist($entity);
//              $address = str_replace(array(' - ',' ',PHP_EOL),'+',$entity->getAddress()->toString());
//              $url = 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address='.$address;
//              $string = file_get_contents($url);
//              $json = json_decode($string);
//          //  var_dump($json); exit;
//
//              if ($json->status == "OK")
//              {
//                  if (sizeof($json->results) > 1)
//                  {
//                      $logs[] = 'multi : '.$address.'<br>';
//                  }
//                  else
//                  {
                $count++;
//                      foreach ($json->results as $result)
//                      {
//                          $lat = $result->geometry->location->lat;
//                          $lng = $result->geometry->location->lng;
//                          $entity->setLatitude($lat);
//                          $entity->setLongitude($lng);
//                          $entityManager->persist($entity);
//                      }
//                  }
//              }
//              else
//              {
//                  $logs[] = $json->status.' : '.$address.'<br>';
//              }
            }
        }
        $this->entityManager->flush();

        return $this->render('@JLMModel/Door/geocode.html.twig', ['count' => $count, 'logs' => $logs]);
    }

    /**
     * Maps Door entities.
     */
    public function mapAction()
    {
        $latMin = $lonMin = 40000;
        $latMax = $lonMax = -40000;
        $entities = $this->repository->findAll();
        foreach ($entities as $key => $entity) {
            if ($entity->getNextMaintenance() !== null &&
                $entity->getActualContract() != null &&
                $entity->getLatitude() !== null &&
                $entity->getLongitude() !== null
            ) {
                $latMin = min($latMin, $entity->getLatitude());
                $latMax = max($latMax, $entity->getLatitude());
                $lonMin = min($lonMin, $entity->getLongitude());
                $lonMax = max($lonMax, $entity->getLongitude());
            } else {
                unset($entities[$key]);
            }
        }
        $latCentre = ($latMin + $latMax) / 2;
        $lonCentre = ($lonMin + $lonMax) / 2;

        return $this->render('@JLMModel/Door/map.html.twig', [
            'entities' => $entities,
            'latCenter' => $latCentre,
            'lngCenter' => $lonCentre,
        ]);
    }
}
