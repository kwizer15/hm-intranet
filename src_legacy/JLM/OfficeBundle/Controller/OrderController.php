<?php

namespace JLM\OfficeBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Entity\Search;
use JLM\CoreBundle\Form\Type\SearchType;
use JLM\OfficeBundle\Pdf\Order as OrderPdf;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JLM\OfficeBundle\Entity\Order;
use JLM\OfficeBundle\Form\Type\OrderType;
use JLM\OfficeBundle\Entity\Task;
use JLM\DailyBundle\Entity\Work;

/**
 * @Route("/order")
 * @IsGranted("ROLE_OFFICE")
 */
class OrderController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Order entities.
     *
     * @Route("/", name="order")
     * @Route("/page/{page}", name="order_page")
     * @Route("/page/{page}/state/{state}", name="order_state")
     *
     * @param int $page
     * @param null $state
     *
     * @return Response
     */
    public function indexAction($page = 1, $state = null): Response
    {
        $limit = 10;

        $nb = $this->entityManager->getRepository(Order::class)->getCount($state);
        $nbPages = ceil($nb/$limit);
        $nbPages = ($nbPages < 1) ? 1 : $nbPages;
        $offset = ($page-1) * $limit;
        if ($page < 1 || $page > $nbPages) {
            throw $this->createNotFoundException('Page inexistante (page '.$page.'/'.$nbPages.')');
        }
        
        $entities = $this->entityManager->getRepository(Order::class)->getByState(
            $state,
            $limit,
            $offset
        );
        
        return $this->render('@JLMOffice/Order/index.html.twig', [
            'entities' => $entities,
            'page'     => $page,
            'nbPages'  => $nbPages,
            'state' => $state,
        ]);
    }

    /**
     * Finds and displays an Order entity.
     *
     * @Route("/{id}/show", name="order_show")
     *
     * @param Order $entity
     *
     * @return Response
     */
    public function showAction(Order $entity): Response
    {
        return $this->render('@JLMOffice/Order/show.html.twig', ['entity'=> $entity]);
    }

    /**
     * Displays a form to create a new Bill entity.
     *
     * @Route("/new/{id}", name="order_new")
     *
     * @param Work $work
     *
     * @return Response
     */
    public function newAction(Work $work): Response
    {
        $entity = new Order();
        $entity->setWork($work);
        $form  = $this->createForm(OrderType::class, $entity);
        return $this->render('@JLMOffice/Order/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Creates a new Bill entity.
     *
     * @Route("/create", name="order_create", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createAction(Request $request): Response
    {
        $entity  = new Order();
        $form    = $this->createForm(OrderType::class, $entity);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($entity->getLines() as $line) {
                $line->setOrder($entity);
                $this->entityManager->persist($line);
            }
            $this->entityManager->persist($entity);
            $work = $entity->getWork();
            $work->setOrder($entity);
            $this->entityManager->persist($work);
            $this->entityManager->flush();
            return ($this->redirectToRoute('order_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMOffice/Order/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Displays a form to edit an existing Order entity.
     *
     * @Route("/{id}/edit", name="order_edit")
     *
     * @param Order $entity
     *
     * @return Response
     */
    public function editAction(Order $entity): Response
    {
        if ($entity->getState() > 2) {
            return ($this->redirectToRoute('order_show', ['id' => $entity->getId()]));
        }
        $editForm = $this->createForm(OrderType::class, $entity);

        return $this->render('@JLMOffice/Order/edit.html.twig', [
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Edits an existing Order entity.
     *
     * @Route("/{id}/update", name="order_update", methods={"POST"})
     *
     * @param Request $request
     * @param Order $entity
     *
     * @return Response
     */
    public function updateAction(Request $request, Order $entity): Response
    {
        // Si la commande est déjà validé, on empèche quelconque modification
        if ($entity->getState() > 2) {
            return ($this->redirectToRoute('order_show', ['id' => $entity->getId()]));
        }
    
        $editForm = $this->createForm(OrderType::class, $entity);
        $editForm->handleRequest($request);
    
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            foreach ($entity->getLines() as $line) {
                $line->setOrder($entity);
                $this->entityManager->persist($line);
            }
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('order_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMOffice/Order/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Imprimer la fiche travaux
     *
     * @Route("/{id}/print", name="order_print")
     * @param Order $entity
     *
     * @return Response
     */
    public function printAction(Order $entity): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename='.$entity->getId().'.pdf');
        $response->setContent(OrderPdf::get($entity));

        return $response;
    }

    /**
     * En préparation
     *
     * @Route("/{id}/ordered", name="order_ordered")
     * @param Order $entity
     *
     * @return RedirectResponse
     */
    public function orderedAction(Order $entity): RedirectResponse
    {
        if ($entity->getState() > 0) {
            return ($this->redirectToRoute('order_show', ['id' => $entity->getId()]));
        }
        
        if ($entity->getState() < 1) {
            $entity->setState(1);
        }
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('order_show', ['id' => $entity->getId()]));
    }

    /**
     * En préparation
     *
     * @Route("/{id}/ready", name="order_ready")
     *
     * @param Order $entity
     *
     * @return RedirectResponse
     */
    public function readyAction(Order $entity): RedirectResponse
    {
        if ($entity->getState() != 1) {
            return ($this->redirectToRoute('order_show', ['id' => $entity->getId()]));
        }
    
        if ($entity->getState() < 2) {
            $entity->setState(2);
            $entity->setClose();
        }
        $this->entityManager->persist($entity);
        $this->entityManager->flush();

        return ($this->redirectToRoute('order_show', ['id' => $entity->getId()]));
    }
    
    /**
     * @Route("/todo", name="order_todo")
     */
    public function todoAction(): Response
    {
        $list = $this->entityManager->getRepository(Work::class)->getOrderTodo();

        return $this->render('@JLMOffice/Order/todo.html.twig', ['entities'=>$list]);
    }

    /**
     * Resultats de la barre de recherche.
     *
     * @Route("/search", name="order_search", methods={"POST"})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function searchAction(Request $request): Response
    {
        $entity = new Search();
        $form = $this->createForm(SearchType::class, $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $this->render('@JLMOffice/Order/search.html.twig', [
                    'layout'=> ['form_search_query'=>$entity],
                    'entities' => $this->entityManager->getRepository(Order::class)->search($entity),
                    'query' => $entity->getQuery(),
            ]);
        }
        return $this->render('@JLMOffice/Order/search.html.twig', ['layout'=>['form_search_query'=>$entity],'query' => $entity->getQuery(),]);
    }
}
