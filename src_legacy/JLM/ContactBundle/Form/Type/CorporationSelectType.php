<?php

namespace JLM\ContactBundle\Form\Type;

use JLM\CoreBundle\Form\Type\AbstractSelectType;
use JLM\ContactBundle\Form\DataTransformer\CorporationToIntTransformer;

class CorporationSelectType extends AbstractSelectType
{

    protected function getTransformerClass()
    {
        return CorporationToIntTransformer::class;
    }
    
    protected function getTypeName()
    {
        return 'jlm_contact_corporation';
    }
}
