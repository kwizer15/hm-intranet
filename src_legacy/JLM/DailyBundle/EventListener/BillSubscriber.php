<?php

namespace JLM\DailyBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JLM\DailyBundle\Entity\Intervention;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CommerceBundle\Event\BillEvent;
use JLM\CommerceBundle\Factory\BillFactory;
use JLM\CoreBundle\Event\FormPopulatingEvent;
use JLM\CoreBundle\Event\RequestEvent;
use JLM\DailyBundle\Builder\WorkBillBuilder;
use JLM\DailyBundle\Builder\InterventionBillBuilder;
use JLM\DailyBundle\Entity\Work;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;    // @todo Change to WorkInterface

class BillSubscriber implements EventSubscriberInterface
{
   
    private $objectManager;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public static function getSubscribedEvents()
    {
        return [
            JLMCommerceEvents::BILL_FORM_POPULATE => 'populateFromIntervention',
            JLMCommerceEvents::BILL_AFTER_PERSIST => 'setBillToIntervention',
        ];
    }
    
    public function populateFromIntervention(FormPopulatingEvent $event)
    {
        if (null !== $interv = $this->getIntervention($event)) {
            $builder = null;
            if ($interv->getQuote() !== null) {
                $builder = ($interv instanceof Work)
                    ? new WorkBillBuilder($interv)
                    : null;
            }
            $builder = $builder ?? new InterventionBillBuilder($interv);
            $entity = BillFactory::create($builder);
            $event->getForm()->setData($entity);
            $event->getForm()->add('intervention', HiddenType::class, ['data' => $interv->getId(), 'mapped' => false]);
        }
    }
    
    public function setBillToIntervention(BillEvent $event)
    {
        if (null !== $entity = $this->getIntervention($event)) {
            $entity->setBill($event->getBill());
            $this->objectManager->persist($entity);
            $this->objectManager->flush();
        }
    }
    
    private function getIntervention(RequestEvent $event)
    {
        $id = $event->getParam('jlm_commerce_bill', ['intervention'=>$event->getParam('intervention')]);
    
        return (isset($id['intervention']) && $id['intervention'] !== null)
            ? $this->objectManager->getRepository(Intervention::class)->find($id['intervention'])
            : null
        ;
    }
}
