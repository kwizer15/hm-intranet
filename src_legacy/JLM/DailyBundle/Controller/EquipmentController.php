<?php

namespace JLM\DailyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\DailyBundle\Entity\Equipment;
use JLM\DailyBundle\Entity\ShiftTechnician;
use JLM\DailyBundle\Form\Type\EquipmentType;
use JLM\DailyBundle\Form\Type\RecuperationEquipmentType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class EquipmentController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Displays a form to create a new InterventionPlanned entity.
     */
    public function newAction()
    {
        $entity = new ShiftTechnician();
        $entity->setBegin(new \DateTime);
        $shifting = new Equipment();
        $shifting->setPlace('Saint-Soupplets (Bureau)');
        $shifting->setReason('Récupération matériel');
        $entity->setShifting($shifting);
        $form   = $this->createForm(RecuperationEquipmentType::class, $entity);
        return $this->render('@JLMDaily/Equipment/new.html.twig', [
                'entity' => $entity,
                'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a new ShiftTechnician entity.
     */
    public function createAction(Request $request)
    {
        $entity  = new ShiftTechnician();
        $entity->setCreation(new \DateTime);
        $form = $this->createForm(RecuperationEquipmentType::class, $entity);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity->getShifting()->setCreation(new \DateTime));
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([]);
            }
            
            return $this->redirect($request->headers->get('referer'));
        }
        return $this->render('@JLMDaily/Equipment/create.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
            'previous'=> $request->headers->get('referer'),
        ]);
    }
    
    /**
     * Show
     */
    public function showAction(Request $request, Equipment $entity)
    {
        return $this->render('@JLMDaily/Equipment/show.html.twig', [
            'previous'=> $request->headers->get('referer'),
            'entity' => $entity,
        ]);
    }
    
    /**
     * Edit a form to edit an existing Equipment entity.
     */
    public function editAction(Equipment $entity)
    {
        $editForm = $this->createForm(EquipmentType::class, $entity);
    
        return $this->render('@JLMDaily/Equipment/edit.html.twig', [
            'entity'      => $entity,
            'form'   => $editForm->createView(),
        ]);
    }
    
    /**
     * Edits an existing Equipment entity.
     */
    public function updateAction(Request $request, Equipment $entity)
    {
        $editForm = $this->createForm(EquipmentType::class, $entity);
        $editForm->handleRequest($request);
    
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            return $this->redirect($request->headers->get('referer'));
        }
    
        return $this->render('@JLMDaily/Equipment/update.html.twig', [
            'entity' => $entity,
            'form' => $editForm->createView(),
            'previous' => $request->headers->get('referer'),
        ]);
    }
}
