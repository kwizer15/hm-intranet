<?php

namespace JLM\CommerceBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\QuoteVariant;
use JLM\CoreBundle\Manager\BaseManager as Manager;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CoreBundle\Event\FormPopulatingEvent;
use JLM\CommerceBundle\Form\Type\QuoteVariantType;
use JLM\CommerceBundle\Entity\QuoteLine;
use JLM\CommerceBundle\Entity\Quote;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\Exception\LogicException;
use JLM\CommerceBundle\Model\QuoteVariantInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

class QuoteVariantManager extends Manager
{
    private $eventDispatcher;

    public function __construct(
        EntityManagerInterface $entityManager,
        RouterInterface $router,
        EventDispatcherInterface $eventDispatcher,
        FormFactoryInterface $formFactory
    ) {
        parent::__construct(
            QuoteVariant::class,
            $entityManager,
            $router,
            $formFactory
        );
        $this->eventDispatcher = $eventDispatcher;
    }

    protected function getFormParam($name, $options = [])
    {
        switch ($name) {
            case 'new':
                return [
                    'method' => 'POST',
                    'route' => 'variant_create',
                    'params' => [],
                    'label' => 'Créer',
                    'type'  => QuoteVariantType::class,
                    'entity' => null,
                ];
            case 'edit':
                return [
                    'method' => 'POST',
                    'route' => 'variant_update',
                    'params' => ['id' => $options['entity']->getId()],
                    'label' => 'Modifier',
                    'type'  => QuoteVariantType::class,
                    'entity' => $options['entity']
                ];
        }
        
        return parent::getFormParam($name, $options);
    }
    
    /**
     * {@inheritdoc}
     */
    public function populateForm($form, Request $request)
    {
        // Appel des évenements de remplissage du formulaire
        $this->eventDispatcher->dispatch(
            JLMCommerceEvents::QUOTEVARIANT_FORM_POPULATE,
            new FormPopulatingEvent($form, $request)
        );
        $quote = $form->get('quote')->getData();
        if (!$quote instanceof Quote) {
            throw new LogicException('$quote is not a Quote object');
        }
        
        $form->get('vat')->setData(number_format($quote->getVat() * 100, 1, ',', ' '));
        $form->get('vatTransmitter')->setData(number_format($quote->getVatTransmitter() * 100, 1, ',', ' '));
        
        // On complète avec ce qui reste vide
        $params = [
                'creation' => new \DateTime,
                'discount' => 0,
        ];
        foreach ($params as $key => $value) {
            $param = $form->get($key)->getData();
            if (empty($param)) {
                $form->get($key)->setData($value);
            }
        }
        
        $lines = $form->get('lines')->getData();
        if (empty($lines)) {
            $l = new QuoteLine();
            $l->setVat($form->get('quote')->getData()->getVat());
            $form->get('lines')->setData([$l]);
        }

        return parent::populateForm($form, $request);
    }
    
    public function assertState(QuoteVariantInterface $variant, $states = [])
    {
        if (!in_array($variant->getState(), $states)) {
            throw new Exception('This action is impossible with the actual quote state');
        }
    }
}
