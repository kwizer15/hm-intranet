<?php

namespace JLM\TransmitterBundle\Form\Type;

use JLM\ModelBundle\Form\Type\DatepickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\TransmitterBundle\Entity\Attribution;

class AttributionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('ask', AskHiddenType::class)
            ->add('creation', DatepickerType::class, ['label'=>'Date'])
            ->add('contact')
            ->add('individual', null, ['label'=>'Particulier','required'=>false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Attribution::class
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_transmitterbundle_attributiontype';
    }
}
