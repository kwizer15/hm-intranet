<?php

namespace JLM\ModelBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ModelBundle\Entity\DoorStop;

class DoorStopEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('reason', TextareaType::class, ['label'=>'Raison'])
            ->add('state', TextareaType::class, ['label'=>'État'])
        ;
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_modelbundle_doorstopedittype';
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => DoorStop::class,
        ]);
    }
}
