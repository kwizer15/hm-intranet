<?php

namespace JLM\DailyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ContactBundle\Entity\Person;
use JLM\CoreBundle\Service\Paginator;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use JLM\DailyBundle\Entity\Work;
use JLM\DailyBundle\Form\Type\WorkType;
use JLM\DailyBundle\Form\Type\WorkEditType;
use JLM\DailyBundle\Form\Type\WorkCloseType;
use JLM\ModelBundle\Entity\Door;
use JLM\CommerceBundle\Entity\QuoteVariant;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JLM\CoreBundle\Factory\MailFactory;
use JLM\CoreBundle\Form\Type\MailType;
use JLM\CoreBundle\Builder\MailSwiftMailBuilder;
use JLM\ModelBundle\JLMModelEvents;
use JLM\ModelBundle\Event\DoorEvent;
use JLM\DailyBundle\Builder\Email\WorkPlannedMailBuilder;
use JLM\DailyBundle\Builder\Email\WorkOnSiteMailBuilder;
use JLM\DailyBundle\Builder\Email\WorkEndMailBuilder;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class WorkController extends AbstractInterventionController
{
    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct($entityManager);
        $this->repository = $this->entityManager->getRepository(Work::class);
    }

    /**
     * List the works
     */
    public function listAction(Paginator $paginator, Request $request)
    {
        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'entities' => $this->repository->getArray($request->get('q', ''), $request->get('page_limit', 10))
            ]);
        }
        return $this->render(
            '@JLMDaily/Work/list.html.twig',
            $paginator->pagination($request, $this->repository, 'getCountOpened', 'getOpened', 'work_list', [])
        );
    }
    
    /**
     * Finds and displays a Work entity.
     */
    public function showAction(Work $entity)
    {
        return $this->render('@JLMDaily/Work/show.html.twig', $this->show($entity));
    }
    
    /**
     * Displays a form to create a new Work entity.
     */
    public function newdoorAction(Door $door)
    {
        $entity = new Work();
        $entity->setDoor($door);
        $entity->setPlace($door.'');
        $form   = $this->createForm(WorkType::class, $entity);
    
        return $this->render('@JLMDaily/Work/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Displays a form to create a new Work entity.
     */
    public function newAction()
    {
        $entity = new Work();
        $form   = $this->createForm(WorkType::class, $entity);
    
        return $this->render('@JLMDaily/Work/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Displays a form to create a new Work entity.
     */
    public function newquoteAction(QuoteVariant $quote)
    {
        $entity = new Work();
        $entity->setQuote($quote);
        $door = $quote->getQuote()->getDoor();
        $entity->setDoor($door);
        $entity->setPlace($quote->getQuote()->getDoorCp());
        $entity->setReason($quote->getIntro());
        $contact = $quote->getQuote()->getContact();
        if ($contact === null) {
            $entity->setContactName($quote->getQuote()->getContactCp());
        } else {
            /** @var Person $person */
            $person = $contact->getPerson();
            $entity->setContactName($person->getName().' ('.$contact->getRole().')');
            $mobilePhone = $person->getMobilePhone();
            $fixedPhone = $person->getFixedPhone();
            $email = $person->getEmail();
            $phones = '';
            if ($mobilePhone != null) {
                $phones .= $mobilePhone;
            }
            if ($fixedPhone != null) {
                if ($phones != '') {
                    $phones .= PHP_EOL;
                }
                $phones .= $fixedPhone;
            }
            if ($email != null) {
                $entity->setContactEmail($email);
            }
            $entity->setContactPhones($phones);
        }
        $form = $this->createForm(WorkType::class, $entity);
    
        return $this->render('@JLMDaily/Work/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * Creates a new Work entity.
     */
    public function createAction(Request $request)
    {
        $entity  = new Work();
        
        $form = $this->createForm(WorkType::class, $entity);
        $entity->setCreation(new \DateTime);
        $entity->setPriority(4);
        $form->handleRequest($request);
        $entity->setContract($entity->getDoor()->getActualContract());

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
    
            return ($this->redirectToRoute('work_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMDaily/Work/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }
    
    /**
     * Displays a form to edit an existing Work entity.
     */
    public function editAction(Work $entity)
    {
        $editForm = $this->createForm(WorkEditType::class, $entity);
    
        return $this->render('@JLMDaily/Work/edit.html.twig', [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ]);
    }
    
    /**
     * Edits an existing Work entity.
     */
    public function updateAction(Request $request, Work $entity)
    {
        $editForm = $this->createForm(WorkEditType::class, $entity);
        $editForm->handleRequest($request);
    
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('work_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMDaily/Work/edit.html.twig', [
            'entity' => $entity,
            'form' => $editForm->createView(),
        ]);
    }
    
    /**
     * Close an existing Work entity.
     */
    public function closeAction(Work $entity)
    {
        $form = $this->createForm(WorkCloseType::class, $entity);
    
        return $this->render('@JLMDaily/Work/close.html.twig', [
                'entity'      => $entity,
                'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Close an existing Work entity.
     */
    public function closeupdateAction(Request $request, Work $entity)
    {
        $form = $this->createForm(WorkCloseType::class, $entity);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            if ($entity->getObjective()->getId() == 1) {  // Mise en service
                $stop = $entity->getDoor()->getLastStop();
                if ($stop !== null) {
                    $stop->setEnd(new \DateTime);
                    $this->entityManager->persist($stop);
                }
                $this->entityManager->persist($entity->getDoor());
            }
            $entity->setClose(new \DateTime);
            $entity->setMustBeBilled($entity->getQuote() !== null);
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            
            return ($this->redirectToRoute('work_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMDaily/Work/closeupdate.html.twig', [
                'entity'      => $entity,
                'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Finds and displays a InterventionPlanned entity.
     */
    public function emailAction(Request $request, Work $entity, $step, \Swift_Mailer $mailer, EventDispatcherInterface $eventDispatcher)
    {
        $steps = [
            'planned' => WorkPlannedMailBuilder::class,
            'onsite' => WorkOnSiteMailBuilder::class,
            'end' => WorkEndMailBuilder::class,
        ];
        $class = (array_key_exists($step, $steps)) ? $steps[$step] : null;
        if (null === $class) {
            throw new NotFoundHttpException('Page inexistante');
        }
        $mail = MailFactory::create(new $class($entity));
        $editForm = $this->createForm(MailType::class, $mail);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $mailer->send(MailFactory::create(new MailSwiftMailBuilder($editForm->getData())));
            $eventDispatcher->dispatch(
                JLMModelEvents::DOOR_SENDMAIL,
                new DoorEvent($entity->getDoor(), $request)
            );
            
            return ($this->redirectToRoute('work_show', ['id' => $entity->getId()]));
        }
        
        return $this->render('@JLMDaily/Work/email.html.twig', [
            'entity' => $entity,
            'form' => $editForm->createView(),
            'step' => $step,
        ]);
    }
}
