<?php

namespace Tests\JLM\CommerceBundle\Entity;

class CommercialPartLineProductTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Mock CommercialPartLineProduct
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = $this->getMockForAbstractClass('JLM\CommerceBundle\Entity\CommercialPartLineProduct');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
         $this->assertInstanceOf('JLM\CommerceBundle\Model\CommercialPartLineInterface', $this->entity);
    }
    
    public function getAttributes()
    {
        return [
            ['Position', 1],
            ['Product', $this->createMock('JLM\ProductBundle\Model\ProductInterface')],
            ['Reference', 'Foo'],
            ['Designation', 'Foo'],
            ['Description', 'Bar'],
            ['ShowDescription', true],
            ['IsTransmitter', false],
            ['Quantity', 2],
            ['UnitPrice', 25.50],
            ['Discount', 0.1],
            ['Vat', 20.0],
        ];
    }
    
    /**
     * Test getters and setters
     * @param string $attribute
     * @param mixed $value
     * @dataProvider getAttributes
     */
    public function testGettersSetters($attribute, $value)
    {
        $getter = 'get'.$attribute;
        $setter = 'set'.$attribute;
        $this->assertSame($this->entity, $this->entity->$setter($value));
        $this->assertSame($value, $this->entity->$getter());
    }
}
