<?php

namespace JLM\FrontBundle\Model;

interface ContactInterface
{
    public function getEmail();
}
