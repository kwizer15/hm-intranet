<?php

namespace Tests\JLM\ContactBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CityControllerTest extends WebTestCase
{
    /**
     * @var Symfony\Bundle\FrameworkBundle\Client
     */
    private $client;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->client = static::createClient();
    }
    
    public function testGet()
    {
        $this->client->request(
            'GET',
            '/contact/cityjson',
            ['id' => 1],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X-Requested-With' => 'XMLHttpRequest',
            ]
        );
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testGetBadId()
    {
        $this->client->request(
            'GET',
            '/contact/cityjson',
            ['id' => 'foo'],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X-Requested-With' => 'XMLHttpRequest',
            ]
        );
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
    
    public function testSearch()
    {
        $this->client->request(
            'GET',
            '/contact/citiesjson',
            ['q' => 'Othis'],
            [],
            [
                'CONTENT_TYPE' => 'application/json',
                'HTTP_X-Requested-With' => 'XMLHttpRequest',
            ]
        );
        $this->assertTrue($this->client->getResponse()->isSuccessful());
    }
}
