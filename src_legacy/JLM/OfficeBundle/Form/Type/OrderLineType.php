<?php

namespace JLM\OfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\OfficeBundle\Entity\OrderLine;

class OrderLineType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('position', HiddenType::class)
            ->add('reference', null, ['required' => false, 'attr' => ['class' => 'input-small']])
            ->add('designation', null, ['attr' => ['class' => 'input-xxlarge']])
            ->add('quantity', null, ['attr' => ['class' => 'input-mini']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => OrderLine::class,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'order_line';
    }
}
