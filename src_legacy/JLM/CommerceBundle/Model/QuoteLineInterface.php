<?php

namespace JLM\CommerceBundle\Model;

interface QuoteLineInterface
{
    /**
     *
     * @param QuoteVariantInterface|null $variant
     * @return bool
     */
    public function setVariant(QuoteVariantInterface $variant = null);
    
    /**
     * @return float
     */
    public function getPrice();
    
    /**
     * @return float
     */
    public function getPriceAti();
    
    /**
     * @return float
     */
    public function getTotalPurchasePrice();
    
    /**
     * @return float
     */
    public function getVatValue();
    
    /**
     * Get line type
     * @return string
     */
    public function getType();
}
