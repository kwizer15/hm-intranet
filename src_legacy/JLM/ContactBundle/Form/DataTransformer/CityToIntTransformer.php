<?php

namespace JLM\ContactBundle\Form\DataTransformer;

use JLM\ContactBundle\Entity\City;
use JLM\CoreBundle\Form\DataTransformer\ObjectToIntTransformer;

class CityToIntTransformer extends ObjectToIntTransformer
{
    /**
     * {@inheritdoc}
     */
    protected function getClass()
    {
        return City::class;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getErrorMessage()
    {
        return 'A city with id "%s" does not exist!';
    }
}
