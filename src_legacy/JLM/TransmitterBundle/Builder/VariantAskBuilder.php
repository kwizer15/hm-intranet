<?php

namespace JLM\TransmitterBundle\Builder;

use JLM\CommerceBundle\Model\QuoteVariantInterface;

class VariantAskBuilder extends AskBuilderAbstract
{
    private $variant;
    
    /**
     *
     * @param QuoteVariantInterface $variant
     */
    public function __construct(QuoteVariantInterface $variant, $options = [])
    {
        parent::__construct($options);
        $this->variant = $variant;
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildTrustee()
    {
        $this->getAsk()->setTrustee($this->variant->getQuote()->getTrustee());
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildSite()
    {
        $this->getAsk()->setSite($this->variant->getQuote()->getAsk()->getSite());
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildMethod()
    {
        $this->getAsk()->setMethod($this->variant->getQuote()->getAsk()->getMethod());
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildMaturity()
    {
        $maturity = new \DateTime();
        $maturity->add(new \DateInterval('P7D'));
        $this->getAsk()->setMaturity($maturity);
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildPerson()
    {
        $this->getAsk()->setPerson($this->variant->getQuote()->getAsk()->getPerson());
    }
    
    /**
     * {@inheritdoc}
     */
    public function buildAsk()
    {
        $ask = 'Selon accord sur devis n°'. $this->variant->getNumber().PHP_EOL;
        $lines = $this->variant->getLinesByType('TRANSMITTER');
        foreach ($lines as $line) {
            $ask .= $line->getQuantity(). ' '.$line->getDesignation().PHP_EOL;
        }
        $this->getAsk()->setAsk($ask);
    }
}
