<?php

namespace JLM\ContactBundle\Model;

interface CorporationContactInterface extends PersonInterface
{
    /**
     * Get company
     *
     * @return CompanyInterface
     */
    public function getCorporation();
    
    /**
     * Get position
     * Poste dans la société
     *
     * @return string
     */
    public function getPosition();
}
