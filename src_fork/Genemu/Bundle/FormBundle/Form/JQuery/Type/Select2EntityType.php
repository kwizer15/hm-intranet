<?php

declare(strict_types=1);

namespace Genemu\Bundle\FormBundle\Form\JQuery\Type;

class Select2EntityType extends Select2Type
{
    public function __construct()
    {
        parent::__construct('entity');
    }
}
