<?php

namespace Tests\JLM\UserBundle\Entity;

use JLM\UserBundle\Entity\User;

class UserTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Country
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new User;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf('FOS\UserBundle\Model\User', $this->entity);
    }
    
    public function testGetPerson()
    {
        $person = $this->createMock('JLM\ContactBundle\Model\ContactInterface');
        $this->assertSame($this->entity, $this->entity->setContact($person));
        $this->assertSame($person, $this->entity->getContact());
    }
}
