<?php

declare(strict_types=1);

namespace JLM\CoreBundle\Service;

use Doctrine\Persistence\ObjectRepository;
use JLM\CoreBundle\Model\Repository\PaginableInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Paginator
{
    /**
     * Pagination
     */
    public function pagination(Request $request, ObjectRepository $repository, $functionCount = 'getCountAll', $functionDatas = 'getAll', $route = null, $params = [])
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 10);
        $params = ($limit != 10) ? array_merge($params, ['limit' => $limit]) : $params;

        if (!method_exists($repository, $functionCount)) {
            throw $this->createNotFoundException(
                'Page inexistante (La méthode '.get_class($repository).'#'.$functionCount.' n\'existe pas)'
            );
        }
        if (!method_exists($repository, $functionDatas)) {
            throw $this->createNotFoundException(
                'Page inexistante (La méthode '.get_class($repository).'#'.$functionDatas.' n\'existe pas)'
            );
        }
        $nb = $repository->$functionCount();
        $nbPages = ceil($nb/$limit);
        $nbPages = ($nbPages < 1) ? 1 : $nbPages;
        $offset = ($page-1) * $limit;
        if ($page < 1 || $page > $nbPages) {
            throw $this->createNotFoundException('Page inexistante (page '.$page.'/'.$nbPages.')');
        }

        return [
            'entities' => $repository->$functionDatas($limit, $offset),
            'pagination' => [
                'total' => $nbPages,
                'current' => $page,
                'limit' => $limit,
                'route' => $route,
                'params' => $params,
            ]
        ];
    }

    public function paginator(Request $request, PaginableInterface $repository, array $defaultParams = [])
    {
        $route_params = [];
        $db_params = array_merge([
            'page' => 1,
            'resultsByPage' => 10,
        ], $defaultParams);
        foreach ($db_params as $param => $defaultValue) {
            $db_params[$param] = $request->get($param, $defaultValue);
            if ($db_params[$param] != $defaultValue) {
                $route_params[$param] = $db_params[$param];
            }
        }

        $entities = $repository->getPaginable($db_params['page'], $db_params['resultsByPage'], $db_params);

        return [
            'entities' => $entities,
            'pagination' => [
                'page' => $db_params['page'],
                'route' => $request->attributes->get('_route'),
                'pages_count' => ceil(count($entities) / $db_params['resultsByPage']),
                'route_params' => $route_params,
            ]
        ];
    }

    private function createNotFoundException($message = 'Not Found', \Exception $previous = null)
    {
        return new NotFoundHttpException($message, $previous);
    }
}
