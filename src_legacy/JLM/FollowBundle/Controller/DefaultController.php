<?php

namespace JLM\FollowBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Service\Paginator;
use JLM\FollowBundle\Entity\Thread;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class DefaultController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function indexAction(Paginator $paginator, Request $request)
    {
        return $this->render('@JLMFollow/Default/index.html.twig', $paginator->paginator($request, $this->entityManager->getRepository(Thread::class), [
            'type' => null,
            'sort' => '!date',
            'state' => null
        ]));
    }
    
    public function updateAction()
    {
        $threads = $this->entityManager->getRepository(Thread::class)->findAll();
        foreach ($threads as $thread) {
            $thread->getState();
            $thread->getAmount();
            $this->entityManager->persist($thread);
        }
        
        $this->entityManager->flush();
        
        return 'Mise à jour OK';
    }
}
