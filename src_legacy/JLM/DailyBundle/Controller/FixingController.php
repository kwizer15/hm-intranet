<?php

namespace JLM\DailyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CoreBundle\Response\PdfResponse;
use JLM\DailyBundle\Builder\Email\FixingTakenMailBuilder;
use JLM\DailyBundle\Pdf\FixingReport;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JLM\DailyBundle\Entity\Fixing;
use JLM\DailyBundle\Form\Type\FixingType;
use JLM\DailyBundle\Form\Type\FixingEditType;
use JLM\DailyBundle\Form\Type\FixingCloseType;
use JLM\ModelBundle\Entity\Door;
use JLM\ModelBundle\Entity\DoorStop;
use Symfony\Component\HttpFoundation\JsonResponse;
use JLM\CoreBundle\Form\Type\MailType;
use JLM\CoreBundle\Factory\MailFactory;
use JLM\CoreBundle\Builder\MailSwiftMailBuilder;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use JLM\ModelBundle\JLMModelEvents;
use JLM\ModelBundle\Event\DoorEvent;
use JLM\DailyBundle\Builder\Email\FixingDistributedMailBuilder;
use JLM\DailyBundle\Builder\Email\FixingOnSiteMailBuilder;
use JLM\DailyBundle\Builder\Email\FixingEndMailBuilder;
use JLM\DailyBundle\Builder\Email\FixingReportMailBuilder;
use Symfony\Component\Mailer\MailerInterface;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class FixingController extends AbstractInterventionController
{
    /**
     * Finds and displays a InterventionPlanned entity.
     */
    public function listAction()
    {
        $entities = $this->entityManager->getRepository(Fixing::class)->getPrioritary();

        return $this->render('@JLMDaily/Fixing/list.html.twig', [
            'entities' => $entities,
        ]);
    }
    
    /**
     * Finds and displays a InterventionPlanned entity.
     */
    public function emailAction(\Swift_Mailer $mailer, EventDispatcherInterface $eventDispatcher, Request $request, Fixing $entity, $step)
    {
        $steps = [
            'taken' => FixingTakenMailBuilder::class,
            'distributed' => FixingDistributedMailBuilder::class,
            'onsite' => FixingOnSiteMailBuilder::class,
            'end' => FixingEndMailBuilder::class,
            'report' => FixingReportMailBuilder::class,
        ];
        $class = (array_key_exists($step, $steps)) ? $steps[$step] : null;
        if (null === $class) {
            throw new NotFoundHttpException('Page inexistante');
        }
        $mail = MailFactory::create(new $class($entity));
        $editForm = $this->createForm(MailType::class, $mail);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $mailer->send(MailFactory::create(new MailSwiftMailBuilder($editForm->getData())));
            $eventDispatcher->dispatch(
                JLMModelEvents::DOOR_SENDMAIL,
                new DoorEvent($entity->getDoor(), $request)
            );
            return ($this->redirectToRoute('fixing_show', ['id' => $entity->getId()]));
        }
        return $this->render('@JLMDaily/Fixing/email.html.twig', [
            'entity' => $entity,
            'form' => $editForm->createView(),
            'step' => $step,
        ]);
    }
    
    /**
     * Finds and displays a InterventionPlanned entity.
     */
    public function showAction(Fixing $entity)
    {
        return $this->render('@JLMDaily/Fixing/show.html.twig', $this->show($entity));
    }
    
    /**
     * Displays a form to create a new InterventionPlanned entity.
     */
    public function newAction(FormFactoryInterface $formFactory, Door $door)
    {
        /*
         * Voir aussi
        *   DoorController:stoppedAction
        *   DefaultController:searchAction
        * @todo A factoriser
        */
        $entity = new Fixing();
        $entity->setDoor($door);
        $entity->setAskDate(new \DateTime);
        $form = $formFactory->createNamed('fixingNew'.$door->getId(), FixingType::class, $entity);
        return $this->render('@JLMDaily/Fixing/new.html.twig', [
                'door' => $door,
                'entity' => $entity,
                'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Creates a new InterventionPlanned entity.
     */
    public function createAction(FormFactoryInterface $formFactory, Request $request, Door $door)
    {
        $entity  = new Fixing();
        $entity->setCreation(new \DateTime);
        $entity->setDoor($door);
        $entity->setContract($door->getActualContract());
        $entity->setPlace($door.'');
        $entity->setPriority(2);
        $form = $formFactory->createNamed('fixingNew'.$door->getId(), FixingType::class, $entity);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['id' => $entity->getId()]);
            }
            return ($this->redirectToRoute('fixing_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMDaily/Fixing/create.html.twig', [
                'door' => $door,
                'entity' => $entity,
                'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Displays a form to edit an existing Fixing entity.
     */
    public function editAction(Fixing $entity)
    {
        $editForm = $this->createForm(FixingEditType::class, $entity);
    
        return $this->render('@JLMDaily/Fixing/edit.html.twig', [
                'entity'      => $entity,
                'form'   => $editForm->createView(),
        ]);
    }
    
    /**
     * Edits an existing Fixing entity.
     */
    public function updateAction(Request $request, Fixing $entity)
    {
        $editForm = $this->createForm(FixingEditType::class, $entity);
        $editForm->handleRequest($request);
    
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            return ($this->redirectToRoute('fixing_show', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMDaily/Fixing/edit.html.twig', [
                'entity'      => $entity,
                'form'   => $editForm->createView(),
        ]);
    }
    
    /**
     * Close an existing Fixing entity.
     */
    public function closeAction(Fixing $entity)
    {
        $form = $this->createForm(FixingCloseType::class, $entity);
    
        return $this->render('@JLMDaily/Fixing/close.html.twig', [
                'entity'      => $entity,
                'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Close an existing Fixing entity.
     */
    public function closeupdateAction(Request $request, Fixing $entity)
    {
        $form = $this->createForm(FixingCloseType::class, $entity);
        $form->handleRequest($request);
    
        if ($form->isSubmitted() && $form->isValid()) {
            // Mise à l'arrêt
            if ($entity->getDone()->getId() == 3) {
                $stop = $entity->getDoor()->getLastStop();
                if ($stop === null) {
                    $stop = new DoorStop;
                    $stop->setBegin(new \DateTime);
                    $stop->setState('Non traitée');
                }
                $stop->setReason($entity->getReport());
                $entity->getDoor()->addStop($stop);
                $this->entityManager->persist($stop);
            }
            
            $entity->setClose(new \DateTime);
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $this->redirectToRoute('fixing_show', ['id' => $entity->getId()]);
        }
    
        return $this->render('@JLMDaily/Fixing/closeupdate.html.twig', [
            'entity'      => $entity,
            'form'   => $form->createView(),
        ]);
    }
    
    /**
     * Imprime le rapport d'intervention
     */
    public function printdayAction(Fixing $entity)
    {
        return new PdfResponse('report-'.$entity->getId().'.pdf', FixingReport::get($entity));
    }
}
