<?php
namespace JLM\FeeBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\Bill;
use JLM\CommerceBundle\Entity\EarlyPaymentModel;
use JLM\CommerceBundle\Entity\PenaltyModel;
use JLM\CommerceBundle\Entity\VAT;
use JLM\CommerceBundle\Factory\BillFactory;
use JLM\CommerceBundle\Pdf\Bill as BillPdf;
use JLM\FeeBundle\Builder\FeeBillBuilder;
use JLM\FeeBundle\Entity\Fee;
use JLM\FeeBundle\Entity\FeesFollower;
use JLM\FeeBundle\Form\Type\FeesFollowerType;
use JLM\ProductBundle\Entity\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Fees controller.
 *
 * @Route("/fees")
 * @IsGranted("ROLE_OFFICE")
 */
class FeesFollowerController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Fees entities.
     *
     * @Route("/", name="fees")
     */
    public function indexAction()
    {
        $entities = $this->entityManager->getRepository(FeesFollower::class)->findBy(
            [],
            ['activation'=>'desc']
        );
    
        return $this->render('@JLMFee/FeesFollower/index.html.twig', [
            'entities' => $entities,
        ]);
    }
    
    /**
     * Edit a FeesFollower entities.
     *
     * @Route("/{id}/edit", name="fees_edit")
     */
    public function editAction(FeesFollower $entity)
    {
        $editForm = $this->createForm(FeesFollowerType::class, $entity);
        
        return $this->render('@JLMFee/FeesFollower/edit.html.twig', [
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
        ]);
    }
    
    /**
     * Edits an existing FeesFollower entity.
     *
     * @Route("/{id}/update", name="fees_update", methods={"POST"})
     */
    public function updateAction(Request $request, FeesFollower $entity)
    {
        $editForm = $this->createForm(FeesFollowerType::class, $entity);
        $editForm->handleRequest($request);
    
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            
            return ($this->redirectToRoute('fees', ['id' => $entity->getId()]));
        }
    
        return $this->render('@JLMFee/FeesFollower/edit.html.twig', [
                'entity'      => $entity,
                'edit_form'   => $editForm->createView(),
        ]);
    }
    
    /**
     * Edits an existing FeesFollower entity.
     *
     * @Route("/{id}/generate", name="fees_generate")
     */
    public function generateAction(FeesFollower $entity)
    {
        $fees = $this->entityManager->getRepository(Fee::class)
            ->createQueryBuilder('a')
            ->select('a')
            ->leftJoin('a.contracts', 'b')
                ->leftJoin('b.door', 'c')
                    ->leftJoin('c.site', 'd')
                        ->leftJoin('d.address', 'e')
                            ->leftJoin('e.city', 'f')
            ->orderBy('f.name', 'asc')
            ->getQuery()
            ->getResult();

        $number = null;
        // @todo Ajouter pas de facture si sous garantie
        foreach ($fees as $fee) {
            $contracts = $fee->getActiveContracts($entity->getActivation());
            if (count($contracts)) {
                $gf = 'getFrequence'.$fee->getFrequence();
                if ($entity->$gf() !== null) {
                    // On fait l'augmentation dans le contrat
                    $majoration = $entity->$gf();
                    if ($majoration > 0) {
                        foreach ($contracts as $contract) {
                            $amount = $contract->getFee();
                            $amount *= (1 + $majoration);
                            $contract->setFee($amount);
                            $this->entityManager->persist($contract);
                        }
                    }
                    $builder = new FeeBillBuilder($fee, $entity, [
                        'number' => $number,
                        'product' => $this->entityManager->getRepository(Product::class)->find(284),
                        'penalty' => (string)$this->entityManager->getRepository(PenaltyModel::class)->find(1),
                        'earlyPayment' => (string)$this->entityManager->getRepository(EarlyPaymentModel::class)->find(1),
                        'vatTransmitter' => $this->entityManager->getRepository(VAT::class)->find(1)->getRate(),
                    ]);
                    $bill = BillFactory::create($builder);
                    if ($bill->getTotalPrice() > 0) {
                        $this->entityManager->persist($bill);
                        $number = $bill->getNumber() + 1;
                    }
                }
            }
        }

        $entity->setGeneration(new \DateTime);
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        
        return ($this->redirectToRoute('fees', ['id' => $entity->getId()]));
    }
    
    /**
     * Print bills
     * @Route("/{id}/print", name="fees_print")
     */
    public function printAction(FeesFollower $follower)
    {
        $entities = $this->entityManager->getRepository(Bill::class)->getFees($follower);
        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set(
            'Content-Disposition',
            'inline; filename=redevances-'.$follower->getActivation()->format('m-Y').'.pdf'
        );
        $response->setContent(BillPdf::get($entities, false));
        
        return $response;
    }
}
