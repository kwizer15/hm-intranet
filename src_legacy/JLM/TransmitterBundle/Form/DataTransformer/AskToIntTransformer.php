<?php
namespace JLM\TransmitterBundle\Form\DataTransformer;

use JLM\AskBundle\Entity\Ask;
use JLM\ModelBundle\Form\DataTransformer\ObjectToIntTransformer;

class AskToIntTransformer extends ObjectToIntTransformer
{
    public function getClass()
    {
        return Ask::class;
    }
    
    protected function getErrorMessage()
    {
        return 'A transmitter ask with id "%s" does not exist!';
    }
}
