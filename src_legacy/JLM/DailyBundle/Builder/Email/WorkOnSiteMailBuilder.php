<?php

namespace JLM\DailyBundle\Builder\Email;

class WorkOnSiteMailBuilder extends WorkMailBuilder
{

    public function buildSubject()
    {
        $this->setSubject('Technicien sur place');
    }
    
    public function buildBody()
    {
        $this->setBody('Bonjour,'.PHP_EOL.PHP_EOL
        .'Le technicien est sur site'.PHP_EOL
        .'Cordialement'
        .$this->getSignature());
    }
}
