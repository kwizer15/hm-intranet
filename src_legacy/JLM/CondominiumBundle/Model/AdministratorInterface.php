<?php

namespace JLM\CondominiumBundle\Model;

interface AdministratorInterface
{
    /**
     * Get the name
     * @return string
     */
    public function getName();
    
    /**
     * Get manager
     * @return ManagerInterface
     */
    public function getManager();
    
    /**
     * Get the members
     * @return AdministratorMembre[]
     */
    public function getMembers();
}
