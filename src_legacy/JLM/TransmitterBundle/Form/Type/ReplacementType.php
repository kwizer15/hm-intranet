<?php

namespace JLM\TransmitterBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\TransmitterBundle\Entity\TransmitterRepository;
use JLM\TransmitterBundle\Entity\Transmitter;
use JLM\TransmitterBundle\Entity\Replacement;

class ReplacementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $id = $options['id'];
        $builder
            ->add('attribution', AttributionHiddenType::class)
            ->add('old', EntityType::class, [
                    'class'=> Transmitter::class,
                    'label'=>'Ancien émetteur',
                    'query_builder'=> function (TransmitterRepository $er) use ($id) {
                        return $er->getFromSite($id);
                    },
            ])
            ->add('newNumber', null, ['label'=>'Numéro du nouvel émetteur', 'attr'=>['class'=>'input-small']])
            ->add('guarantee', null, [
                'label'=>'Garantie du nouvel émetteur',
                'attr'=>['class'=>'input-mini','placeholder'=>'MMAA']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Replacement::class,
            'attr'=>['class'=>'transmitter_replacement'],
            'id' => null,
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_transmitterbundle_replacementtype';
    }
}
