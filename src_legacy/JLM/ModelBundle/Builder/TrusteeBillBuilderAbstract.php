<?php

namespace JLM\ModelBundle\Builder;

use JLM\CommerceBundle\Builder\BillBuilderAbstract;
use JLM\ModelBundle\Entity\Trustee;

abstract class TrusteeBillBuilderAbstract extends BillBuilderAbstract
{
    /**
     * @var Trustee
     */
    protected $trustee;

    public function __construct(Trustee $trustee, $options = [])
    {
        $this->trustee = $trustee;
        parent::__construct($options);
    }

    /**
     * {@inheritdoc}
     */
    public function buildCustomer()
    {
        $this->bill->setTrustee($this->trustee);
        $this->bill->setTrusteeName($this->trustee->getBillLabel());
        $this->bill->setTrusteeAddress($this->trustee->getBillAddress()->toString());
        $accountNumber = ($this->trustee->getAccountNumber() == null) ? '411000' : $this->trustee->getAccountNumber();
        $this->bill->setAccountNumber($accountNumber);
    }
}
