<?php

namespace JLM\ContactBundle\Model;

interface AddressInterface
{
    /**
     * Get street
     *
     * @return string
     */
    public function getStreet();
    
    /**
     * Get city
     *
     * @return CityInterface
     */
    public function getCity();
    
    /**
     * To String
     *
     * @return string
     */
    public function __toString();
    
    /**
     * Alternative to String
     *
     * @return string
     */
    public function toString();
}
