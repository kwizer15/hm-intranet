<?php

namespace JLM\DailyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\DailyBundle\Entity\Standby;
use JLM\DailyBundle\Form\Type\StandbyType;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class StandbyController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Standby entities.
     */
    public function indexAction()
    {
        $entities = $this->entityManager->getRepository(Standby::class)->findBy([], ['begin'=>'DESC']);

        return $this->render('@JLMDaily/Standby/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Displays a form to create a new Standby entity.
     */
    public function newAction()
    {
        $entity = new Standby();
        $form   = $this->createForm(StandbyType::class, $entity, [
            'method'=>'POST',
            'action'=>$this->generateUrl('standby_create'),
        ]);
        $form->add('submit', SubmitType::class, ['label'=>'Enregistrer']);

        return $this->render('@JLMDaily/Standby/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a new Standby entity.
     */
    public function createAction(Request $request)
    {
        $entity  = new Standby();
        $form = $this->createForm(StandbyType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('@JLMDaily/Standby/create.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Standby entity.
     */
    public function editAction(FormFactoryInterface $formFactory, Standby $entity)
    {
        $form = $formFactory->createNamed('shiftTechNew'.$entity->getId(), StandbyType::class, $entity);

        return $this->render('@JLMDaily/Standby/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Edits an existing Standby entity.
     */
    public function updateAction(FormFactoryInterface $formFactory, Request $request, Standby $entity)
    {
        $form = $formFactory->createNamed('shiftTechNew'.$entity->getId(), StandbyType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $this->redirect($request->headers->get('referer'));
        }

        return $this->render('@JLMDaily/Standby/update.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Standby entity.
     */
    public function deleteAction(Request $request, Standby $entity)
    {
        $id = $entity->getId();
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity = $this->entityManager->getRepository(Standby::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Standby entity.');
            }

            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }

        return $this->redirectToRoute('standby');
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
}
