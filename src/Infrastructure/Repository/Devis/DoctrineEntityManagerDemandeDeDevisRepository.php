<?php

declare(strict_types=1);

namespace HMFermeture\Infrastructure\Repository\Devis;

use Doctrine\ORM\EntityManagerInterface;
use HMFermeture\Domain\Devis\Entity\DemandeDeDevis;
use HMFermeture\Domain\Devis\Repository\DemandeDeDevisRepository;
use HMFermeture\Domain\Devis\Repository\Exception\DemandeDeDevisNotFoundException;
use JLM\OfficeBundle\Entity\AskQuote;

class DoctrineEntityManagerDemandeDeDevisRepository implements DemandeDeDevisRepository
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function get(int $devisId): DemandeDeDevis
    {
        /** @var AskQuote $askQuote */
        $askQuote = $this->entityManager->getRepository(AskQuote::class)->find($devisId);

        if (null === $askQuote) {
            throw new DemandeDeDevisNotFoundException();
        }

        return new DemandeDeDevis(
            (string) $askQuote->getId(),
            $askQuote->getDoor(),
            $askQuote->getSite(),
            $askQuote->getTrustee(),
            $askQuote->getPerson()
        );
    }
}
