<?php

namespace JLM\OfficeBundle;

final class JLMOfficeEvents
{
    const ORDER_POSTPERSIST = 'jlm_office.order_postpersist';
    const ORDER_POSTUPDATE = 'jlm_office.order_postupdate';
}
