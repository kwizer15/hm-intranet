<?php

namespace Tests\JLM\StateBundle\DependencyInjection;

use JLM\StateBundle\DependencyInjection\JLMStateExtension;
use PHPUnit\Framework\TestCase;

class JLMStateExtensionTest extends TestCase
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->ext = new JLMStateExtension();
    }
    
    public function assertPreConditions()
    {
        $this->assertInstanceOf('Symfony\Component\HttpKernel\DependencyInjection\Extension', $this->ext);
    }
    
    public function testLoad()
    {
        $this->ext->load([], $this->createMock('Symfony\Component\DependencyInjection\ContainerBuilder'));
    }
}
