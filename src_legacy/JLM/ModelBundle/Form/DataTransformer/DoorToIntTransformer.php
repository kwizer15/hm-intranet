<?php
namespace JLM\ModelBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ModelBundle\Entity\Door;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class DoorToIntTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * Transforms an object (door) to a string (name).
     *
     * @param  Door|null $entity
     * @return string
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return "";
        }
        return $entity->getId();
    }
    
    /**
     * Transforms an int (number) to an object (door).
     *
     * @param  int $number
     * @return Door|null
     * @throws TransformationFailedException if object (door) is not found.
     */
    public function reverseTransform($number)
    {
        if (!$number) {
            return null;
        }
        
        $entity = $this->objectManager
            ->getRepository(Door::class)
            ->find($number)
        ;

        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'A door with id "%s" does not exist!',
                $number
            ));
        }
    
        return $entity;
    }
}
