<?php

namespace JLM\ProductBundle\Form\Type;

use JLM\ContactBundle\Form\Type\CorporationSelectType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ContactBundle\Form\Type\AddressType;
use JLM\ProductBundle\Entity\Supplier;

class SupplierType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('contact', CorporationSelectType::class, ['label'=>'Société'])
            ->add('website', UrlType::class, ['label'=>'Site internet','required'=>false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_product_supplier';
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(
            [
                'data_class' => Supplier::class,
            ]
        );
    }
}
