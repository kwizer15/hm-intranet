<?php

namespace JLM\CommerceBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class FormPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $resources = $container->getParameter('twig.form.resources');

        foreach (['bill','quote'] as $template) {
            $resources[] = '@JLMCommerce/Form/' . $template . '.html.twig';
        }

        $container->setParameter('twig.form.resources', $resources);
    }
}
