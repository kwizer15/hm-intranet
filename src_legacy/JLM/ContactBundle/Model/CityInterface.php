<?php

namespace JLM\ContactBundle\Model;

interface CityInterface
{
    /**
     * @return string
     */
    public function getName();
    
    /**
     * @return string
     */
    public function getZip();
    
    /**
     * @return CountryInterface
     */
    public function getCountry();
    
    /**
     * @return string
     */
    public function __toString();
}
