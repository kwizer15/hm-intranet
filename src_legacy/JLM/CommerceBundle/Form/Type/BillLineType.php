<?php

namespace JLM\CommerceBundle\Form\Type;

use JLM\CommerceBundle\Entity\BillLine;
use JLM\ProductBundle\Form\Type\ProductHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BillLineType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('position', HiddenType::class)
            ->add('product', ProductHiddenType::class, [
                'required' => false
            ])
            ->add('reference', null, [
                'required' => false,
                'attr' => [
                    'class' => 'input-mini'
                ]
            ])
            ->add('designation', null, [
                'attr' => [
                    'class' => 'input-xlarge'
                ]
            ])
            ->add('description', null, [
                'required' => false,
                'attr' => [
                    'class' => 'input-xlarge'
                ]
            ])
            ->add('showDescription', HiddenType::class)
            ->add('quantity', null, [
                'attr' => [
                    'class' => 'input-mini'
                ]
            ])
            ->add('unitPrice', MoneyType::class, [
                'grouping' => true,
                'attr' => [
                    'class' => 'input-mini'
                ]
            ])
            ->add('discount', PercentType::class, [
                'scale' => 0,
                'attr' => [
                    'class' => 'input-mini'
                ]
            ])
            ->add('vat', PercentType::class, [
                'scale' => 1,
                'attr' => [
                    'class' => 'input-mini'
                ]
            ])
            ->add('isTransmitter', HiddenType::class)
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BillLine::class,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_commerce_bill_line';
    }
}
