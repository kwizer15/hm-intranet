<?php

namespace JLM\ProductBundle\Model;

interface ProductCategoryInterface
{
    /**
     * Get is small supplies
     * @return boolean
     */
    public function isSmallSupply();
    
    /**
     * Get if is service
     * @return boolean
     */
    public function isService();
}
