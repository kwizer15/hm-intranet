<?php

namespace JLM\DailyBundle\Repository;

use Doctrine\ORM\EntityRepository;
use JLM\ModelBundle\Entity\Door;

/**
 * RideRepository
 * @author Emmanuel Bernaszuk <emmanuel.bernaszuk@kw12er.com>
 */
class RideRepository extends EntityRepository
{
    public function getCountRides(Door $door)
    {
        $queryBuilder = $this->createQueryBuilder('a')
        ->select('COUNT(a)')
        ->where('a.departure = ?1')
        ->setParameter(1, $door);
        return $queryBuilder->getQuery()->getSingleScalarResult();
    }
    
    public function getMaintenanceNeighbor(Door $door, $limit)
    {
        $queryBuilder = $this->createQueryBuilder('a')
        ->select('a,b,c')
        ->leftJoin('a.destination', 'b')
        ->leftJoin('b.interventions', 'c')
        ->where('a.departure = ?1')
        ->orderBy('a.duration', 'ASC')
        ->setParameter(1, $door);
        
        $entities = $queryBuilder->getQuery()->getResult();
        
        $j = 0;
        $countEntities = sizeof($entities);
        $out = [];
        while (sizeof($out) < $limit && $j < $countEntities) {
            if ($entities[$j]->getDestination()->getNextMaintenance()) {
                $out[] = $entities[$j];
            }
            $j++;
        }
        return $out;
    }
    
    public function hasRide(Door $door, Door $dest)
    {
        if (!isset($this->dests)) {
            $this->dests = [];
        }
        if (!isset($this->dests[$door->getId()])) {
            $this->dests[$door->getId()] = [];
            $queryBuilder = $this->createQueryBuilder('a')
            ->select('b.id')
            ->leftJoin('a.destination', 'b')
            ->where('a.departure = ?1')
            ->setParameter(1, $door)
            ;
            $dests = $queryBuilder->getQuery()->getArrayResult();
            foreach ($dests as $destid) {
                $this->dests[$door->getId()][] = $destid['id'];
            }
        }
        return in_array($dest->getId(), $this->dests[$door->getId()]);
    }
}
