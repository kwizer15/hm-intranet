<?php

namespace JLM\ContactBundle\Form\DataTransformer;

use JLM\ContactBundle\Entity\Contact;
use JLM\CoreBundle\Form\DataTransformer\ObjectToIntTransformer;

class ContactToIntTransformer extends ObjectToIntTransformer
{
    /**
     * {@inheritdoc}
     */
    protected function getClass()
    {
        return Contact::class;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getErrorMessage()
    {
        return 'A contact with id "%s" does not exist!';
    }
}
