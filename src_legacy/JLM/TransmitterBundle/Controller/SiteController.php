<?php

namespace JLM\TransmitterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ModelBundle\Entity\Site;
use JLM\TransmitterBundle\Entity\Transmitter;
use JLM\TransmitterBundle\Pdf\SiteList;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/site")
 * @IsGranted("ROLE_OFFICE")
 */
class SiteController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/{id}/show",name="transmitter_site_show")
     */
    public function showAction(Site $entity)
    {
        return $this->render('@JLMTransmitter/Site/show.html.twig', ['entity' => $entity]);
    }

    /**
     * @Route("/{id}/printlist",name="transmitter_site_printlist")
     */
    public function printlistAction(Site $entity)
    {
        // Retrier les bips par Groupe puis par numéro
        $transmitters = $this->entityManager->getRepository(Transmitter::class)
            ->getFromSite($entity->getId())
            ->getQuery()
            ->getResult()
        ;

        $resort = [];
        foreach ($transmitters as $transmitter) {
            $index = $transmitter->getUserGroup()->getName();
            if (!isset($resort[$index])) {
                $resort[$index] = [];
            }
            $resort[$index][] = $transmitter;
        }

        $final = array_merge([], ...$resort);

        $response = new Response();
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Content-Disposition', 'inline; filename=liste-' . $entity->getId() . '.pdf');
        $response->setContent(SiteList::get($entity, $final, true));

        return $response;
    }
}
