<?php

namespace JLM\CommerceBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use JLM\CommerceBundle\Entity\QuoteVariant;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class QuoteVariantToIntTransformer implements DataTransformerInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;

    /**
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    /**
     * {@inheritdoc}
     */
    public function transform($entity)
    {
        if (null === $entity) {
            return "";
        }

        return $entity->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function reverseTransform($id)
    {
        if (!$id) {
            return null;
        }

        $entity = $this->objectManager->getRepository(QuoteVariant::class)->find($id);
        if (null === $entity) {
            throw new TransformationFailedException(sprintf(
                'A quote variant with id "%s" does not exist!',
                $id
            ));
        }

        return $entity;
    }
}
