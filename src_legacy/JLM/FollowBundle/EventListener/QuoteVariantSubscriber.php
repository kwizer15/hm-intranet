<?php

namespace JLM\FollowBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\CommerceBundle\JLMCommerceEvents;
use JLM\CommerceBundle\Event\QuoteVariantEvent;
use JLM\FollowBundle\Factory\ThreadFactory;

class QuoteVariantSubscriber implements EventSubscriberInterface
{
   
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * Constructor
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            JLMCommerceEvents::QUOTEVARIANT_GIVEN => 'createThread',
        ];
    }
    
    public function createThread(QuoteVariantEvent $event)
    {
        $entity = $event->getQuoteVariant();
        if ($entity->getQuote()->getDoor() !== null) {
            $thread = ThreadFactory::create($entity);
            $this->objectManager->persist($thread);
            $this->objectManager->flush();
        }
    }
}
