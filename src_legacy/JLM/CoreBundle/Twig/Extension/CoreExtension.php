<?php

namespace JLM\CoreBundle\Twig\Extension;

use Twig\Extension\GlobalsInterface;

class CoreExtension extends \Twig\Extension\AbstractExtension implements GlobalsInterface
{
    public function getName()
    {
        return 'core_extension';
    }
    
    public function getGlobals()
    {
        return [
            'today' => new \DateTime()
        ];
    }
}
