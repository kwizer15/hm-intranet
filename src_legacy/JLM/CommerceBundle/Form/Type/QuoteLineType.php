<?php

namespace JLM\CommerceBundle\Form\Type;

use JLM\ProductBundle\Form\Type\ProductHiddenType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\CommerceBundle\Entity\QuoteLine;

class QuoteLineType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('position', HiddenType::class)
            ->add('product', ProductHiddenType::class, [
                'required'=>false
            ])
            ->add('reference', null, [
                'required'=>false,
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('designation', null, [
                'attr'=>[
                    'class'=>'input-xlarge'
                ]
            ])
            ->add('description', null, [
                'required'=>false,
                'attr'=>[
                    'class'=>'input-xlarge'
                ]
            ])
            ->add('showDescription', HiddenType::class)
            ->add('quantity', null, [
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('purchasePrice', MoneyType::class, [
                'grouping'=>true,
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('discountSupplier', PercentType::class, [
                'scale'=>0,
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('expenseRatio', PercentType::class, [
                'scale'=>0,
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('shipping', MoneyType::class, [
                'grouping'=>true,
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('unitPrice', MoneyType::class, [
                'grouping'=>true,
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('discount', PercentType::class, [
                'scale'=>0,
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('vat', PercentType::class, [
                'scale'=>1,
                'attr'=>[
                    'class'=>'input-mini'
                ]
            ])
            ->add('isTransmitter', HiddenType::class)

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => QuoteLine::class
        ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix(): string
    {
        return 'quote_line';
    }
}
