<?php

namespace Tests\JLM\CommerceBundle\Builder;

use PHPUnit\Framework\TestCase;
use JLM\CommerceBundle\Model\BillInterface;

class BillBuilderAbstractTest extends TestCase
{
    private $builder;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->builder = $this->getMockForAbstractClass('JLM\CommerceBundle\Builder\BillBuilderAbstract');
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
        $this->assertInstanceOf('JLM\CommerceBundle\Builder\BillBuilderInterface', $this->builder);
        $this->builder->create();
    }
    
    /**
     * {@inheritdoc}
     */
    public function assertPostConditions()
    {
        $this->assertInstanceOf(BillInterface::class, $this->builder->getBill());
    }
    
    public function testBuildCreation()
    {
        $this->builder->buildCreation();
        $this->assertInstanceOf('DateTime', $this->builder->getBill()->getCreation());
    }
}
