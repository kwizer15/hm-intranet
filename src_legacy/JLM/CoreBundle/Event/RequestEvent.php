<?php

namespace JLM\CoreBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;

class RequestEvent extends Event
{
    /**
     * @var Request
     */
    private $request;

    /**
     * Constructor
     * @param FormInterface $form
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    /**
     * Get request parameter
     * @return string
     */
    public function getParam($param, $default = null, $deep = false)
    {
        return $this->request->get($param, $default, $deep);
    }
}
