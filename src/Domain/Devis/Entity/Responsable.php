<?php

namespace HMFermeture\Domain\Devis\Entity;

class Responsable
{
    /**
     * @var string
     */
    private $nom;

    public function __construct(string $nom)
    {
        $this->nom = $nom;
    }

    public function getNom(): string
    {
        return $this->nom;
    }
}
