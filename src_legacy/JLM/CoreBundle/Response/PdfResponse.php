<?php

declare(strict_types=1);

namespace JLM\CoreBundle\Response;

use Symfony\Component\HttpFoundation\Response;

class PdfResponse extends Response
{
    public function __construct($filename, $content = '', $status = 200, $headers = [])
    {
        $headers['Content-Type'] = 'application/pdf';
        $headers['Content-Disposition'] = 'inline; filename='.$filename.'.pdf';

        parent::__construct($content, $status, $headers);
    }
}
