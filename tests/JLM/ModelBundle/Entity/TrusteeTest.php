<?php

namespace Tests\JLM\ModelBundle\Entity;

use JLM\ModelBundle\Entity\Trustee;

class TrusteeTest extends \PHPUnit\Framework\TestCase
{
    /**
     * @var Country
     */
    protected $entity;
    
    /**
     * {@inheritdoc}
     */
    protected function setUp()
    {
        $this->entity = new Trustee;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function assertPreConditions()
    {
    }
    
    public function testBillingAddressAsNull()
    {
        $this->assertTrue(true);
    }
}
