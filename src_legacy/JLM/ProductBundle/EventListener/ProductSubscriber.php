<?php

namespace JLM\ProductBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\ProductBundle\JLMProductEvents;
use JLM\ProductBundle\Event\ProductEvent;
use JLM\ProductBundle\Entity\Stock;

class ProductSubscriber implements EventSubscriberInterface
{
   
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    public static function getSubscribedEvents()
    {
        return [
            JLMProductEvents::PRODUCT_CREATE => 'createStock',
        ];
    }
    
    public function createStock(ProductEvent $event)
    {
        $stock = new Stock($event->getProduct());
        $this->objectManager->persist($stock);
        $this->objectManager->flush();
    }
}
