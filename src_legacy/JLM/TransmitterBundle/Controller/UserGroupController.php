<?php

namespace JLM\TransmitterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\TransmitterBundle\Entity\Transmitter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use JLM\TransmitterBundle\Entity\UserGroup;
use JLM\TransmitterBundle\Form\Type\UserGroupType;
use JLM\ModelBundle\Entity\Site;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * UserGroup controller.
 *
 * @Route("/usergroup")
 * @IsGranted("ROLE_OFFICE")
 */
class UserGroupController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Displays a form to create a new UserGroup entity.
     *
     * @Route("/new/{id}", name="transmitter_usergroup_new")
     */
    public function newAction(Site $site)
    {
        $entity = new UserGroup();
        $entity->setSite($site);
        $form = $this->createForm(UserGroupType::class, $entity);

        return $this->render('@JLMTransmitter/UserGroup/new.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a new UserGroup entity.
     *
     * @Route("/create", name="transmitter_usergroup_create", methods={"POST"})
     */
    public function createAction(Request $request)
    {
        $entity = new UserGroup();
        $form = $this->createForm(UserGroupType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return new JsonResponse([]);
        }

        return $this->render('@JLMTransmitter/UserGroup/create.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing UserGroup entity.
     *
     * @Route("/{id}/edit", name="transmitter_usergroup_edit")
     */
    public function editAction(FormFactoryInterface $formFactory, $id)
    {
        $entity = $this->entityManager->getRepository(UserGroup::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserGroup entity.');
        }
        $hasTransmitter = $this->entityManager->getRepository(Transmitter::class)->getCountByUserGroup($entity) > 0;
        $editForm = $formFactory->createNamed('userGroupEdit' . $id, UserGroupType::class, $entity);

        return $this->render('@JLMTransmitter/UserGroup/edit.html.twig', [
            'entity' => $entity,
            'form' => $editForm->createView(),
            'hasTransmitter' => $hasTransmitter,
        ]);
    }

    /**
     * Edits an existing UserGroup entity.
     *
     * @Route("/{id}/update", name="transmitter_usergroup_update", methods={"POST"})
     */
    public function updateAction(FormFactoryInterface $formFactory, Request $request, $id)
    {
        $entity = $this->entityManager->getRepository(UserGroup::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserGroup entity.');
        }
        $hasTransmitter = $this->entityManager->getRepository(Transmitter::class)->getCountByUserGroup($entity) > 0;
        $editForm = $formFactory->createNamed('userGroupEdit' . $id, UserGroupType::class, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return new JsonResponse([]);
        }

        return $this->render('@JLMTransmitter/UserGroup/edit.html.twig', [
            'entity' => $entity,
            'form' => $editForm->createView(),
            'hasTransmitter' => $hasTransmitter,
        ]);
    }

    /**
     * Deletes a UserGroup entity.
     *
     * @Route("/{id}/delete", name="transmitter_usergroup_delete")
     */
    public function deleteAction(Request $request, $id)
    {
        $entity = $this->entityManager->getRepository(UserGroup::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserGroup entity.');
        }
        $hasTransmitter = $this->entityManager->getRepository(Transmitter::class)->getCountByUserGroup($entity) > 0;
        if (!$hasTransmitter) {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }

        return $this->redirect($request->headers->get('referer'));
    }

    /**
     * Get Model ID UserGroup entity.
     *
     * @Route("/{id}/defaultmodelid", name="transmitter_usergroup_defaultmodelid", methods={"POST"})
     */
    public function defaultmodelidAction($id)
    {
        $entity = $this->entityManager->getRepository(UserGroup::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find UserGroup entity.');
        }

        return new Response($entity->getModel()->getId());
    }
}
