<?php

namespace JLM\ModelBundle\Builder;

use JLM\CommerceBundle\Builder\BillLineBuilderAbstract;
use JLM\ProductBundle\Model\ProductInterface;

class ProductBillLineBuilder extends BillLineBuilderAbstract
{
    private $product;

    private $quantity;

    private $options = [];

    public function __construct(ProductInterface $product, $vat, $quantity = 1, $options = [])
    {
        $this->product = $product;
        $this->quantity = $quantity;
        $this->options = $options;
        $this->vat = $vat;
    }

    /**
     * {@inheritdoc}
     */
    public function buildPrice()
    {
        $price = $this->options['price'] ?? $this->product->getUnitPrice($this->quantity);
        $this->getLine()->setUnitPrice($price);
        $this->getLine()->setVat($this->vat);
    }

    public function buildProduct()
    {
        $this->getLine()->setProduct($this->product);
        $this->getLine()->setReference($this->product->getReference());
        $this->getLine()->setIsTransmitter($this->product->isSmallSupply());
        $desig = $this->options['designation'] ?? $this->product->getDesignation();
        $this->getLine()->setDesignation($desig);
        $descr = $this->options['description'] ?? $this->product->getDescription();
        $this->getLine()->setDescription($descr);
        $this->getLine()->setShowDescription(!empty($descr));
    }

    public function buildQuantity()
    {
        $this->getLine()->setQuantity($this->quantity);
    }
}
