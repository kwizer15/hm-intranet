<?php

namespace JLM\ModelBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\ModelBundle\Entity\Trustee;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\ModelBundle\Entity\Site;
use JLM\ModelBundle\Form\Type\SiteType;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class SiteController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Lists all Site entities.
     */
    public function indexAction()
    {
        $entities = $this->entityManager->getRepository(Site::class)->findAll();

        return $this->render('@JLMModel/Site/index.html.twig', [
            'entities' => $entities,
        ]);
    }

    /**
     * Finds and displays a Site entity.
     */
    public function showAction($id)
    {
        $entity = $this->entityManager->getRepository(Site::class)->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Site entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('@JLMModel/Site/show.html.twig', [
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to create a new Site entity.
     */
    public function newAction(Request $request)
    {
        $trustee = $request->get('trustee', null);
        $entity = new Site();
        $form   = $this->createForm(SiteType::class, $entity);
        $form->get('trustee')->setData($this->entityManager->getRepository(Trustee::class)->find($trustee));

        return $this->render('@JLMModel/Site/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Creates a new Site entity.
     */
    public function createAction(Request $request)
    {
        $entity  = new Site();
        $form = $this->createForm(SiteType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($entity->getAddress());
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return ($this->redirectToRoute('site_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMModel/Site/new.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Site entity.
     */
    public function editAction(Site $entity)
    {
        $editForm = $this->createForm(SiteType::class, $entity);

        return $this->render('@JLMModel/Site/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Edits an existing Site entity.
     */
    public function updateAction(Request $request, Site $entity)
    {
        $editForm = $this->createForm(SiteType::class, $entity);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->entityManager->persist($entity->getAddress());
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
            return ($this->redirectToRoute('site_show', ['id' => $entity->getId()]));
        }

        return $this->render('@JLMModel/Site/edit.html.twig', [
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ]);
    }

    /**
     * Deletes a Site entity.
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity = $this->entityManager->getRepository(Site::class)->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Site entity.');
            }

            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        }

        return ($this->redirectToRoute('site'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(['id' => $id])
            ->add('id', HiddenType::class)
            ->getForm()
        ;
    }
}
