<?php

namespace JLM\DailyBundle\Entity;

/**
 * Plannification d'intervention
 * JLM\DailyBundle\Entity\Equipment
 * @author Emmanuel Bernaszuk <emmanuel.bernaszuk@kw12er.com>
 */
class Equipment extends Shifting
{
    /**
     * Get Type
     * @see Shifting
     * @return string
     */
    public function getType()
    {
        return 'equipment';
    }
}
