<?php

declare(strict_types=1);

namespace HMFermeture\Domain\Devis\Entity;

use JLM\ContactBundle\Entity\Person;
use JLM\ModelBundle\Entity\Door;
use JLM\ModelBundle\Entity\Site;
use JLM\ModelBundle\Entity\Trustee;

class DemandeDeDevis
{
    /**
     * @var int
     */
    private $devisId;

    /**
     * @var Door
     */
    private $door;

    /**
     * @var Site
     */
    private $site;

    /**
     * @var Trustee
     */
    private $trustee;

    /**
     * @var Person
     */
    private $person;

    public function __construct(string $devisId, ?Door $door, ?Site $site, ?Trustee $trustee, ?Person $person)
    {
        $this->devisId = $devisId;
        $this->door = $door;
        $this->site = $site;
        $this->trustee = $trustee;
        $this->person = $person;
    }

    public function getAggregateId(): string
    {
        return $this->devisId;
    }

    public function getDoor(): ?Door
    {
        return $this->door;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function getTrustee(): ?Trustee
    {
        return $this->trustee;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }
}
