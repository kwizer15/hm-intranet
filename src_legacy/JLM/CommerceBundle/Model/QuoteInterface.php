<?php

namespace JLM\CommerceBundle\Model;

interface QuoteInterface
{
    /**
     * @return int
     */
    public function getNumber();
    
    /**
     * @return Door
     */
    public function getDoor();

    public function getAsk();
}
