<?php

namespace JLM\FrontBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\FrontBundle\Entity\AskQuote;

class AskQuoteType extends AbstractType
{
    /**
     *
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('firstName', TextType::class, ['required' => false]);
        $builder->add('lastName', TextType::class);
        $builder->add('phone', TextType::class);
        $builder->add('email', TextType::class);
        $builder->add('quoteNumber', HiddenType::class);
    }

    /**
     *
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'jlm_front_askquotetype';
    }
    
    /**
     *
     * @param OptionsResolverInterface $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                'data_class' => AskQuote::class,
        ]);
    }
}
