<?php

namespace JLM\FollowBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use JLM\FollowBundle\Entity\StarterIntervention;
use JLM\FollowBundle\Entity\Thread;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\DailyBundle\JLMDailyEvents;
use JLM\DailyBundle\Event\InterventionEvent;
use JLM\FollowBundle\Factory\ThreadFactory;

class InterventionSubscriber implements EventSubscriberInterface
{
   
    /**
     * @var EntityManagerInterface
     */
    private $objectManager;
    
    /**
     * Constructor
     * @param EntityManagerInterface $objectManager
     */
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }
    
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            JLMDailyEvents::INTERVENTION_SCHEDULEWORK => 'createThread',
            JLMDailyEvents::INTERVENTION_UNSCHEDULEWORK => 'deleteThread',
        ];
    }
    
    /**
     * Create thread since intervention
     * @param InterventionEvent $event
     */
    public function createThread(InterventionEvent $event)
    {
        $entity = $event->getIntervention();
        $thread = ThreadFactory::create($entity);
        $this->objectManager->persist($thread);
        $this->objectManager->flush();
    }
    
    /**
     * Create thread since intervention
     * @param InterventionEvent $event
     */
    public function deleteThread(InterventionEvent $event)
    {
        $entity = $event->getIntervention();
        try {
            $starter= $this->objectManager->getRepository(StarterIntervention::class)
                ->findOneBy(['intervention' => $entity]);
            $thread = $this->objectManager->getRepository(Thread::class)
                ->findOneBy(['starter' => $starter]);

            $this->objectManager->remove($thread);
            $this->objectManager->flush();
        } catch (\Exception $e) {
        }
    }
}
