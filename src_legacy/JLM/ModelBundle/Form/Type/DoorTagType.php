<?php

namespace JLM\ModelBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\ModelBundle\Entity\Door;

class DoorTagType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('code', null, ['label'=>'Code étiquette','attr'=>['class'=>'input-small']])

        ;
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_modelbundle_doortagtype';
    }
    
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Door::class,
        ]);
    }
}
