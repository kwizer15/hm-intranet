<?php

namespace JLM\TransmitterBundle\Model;

interface TransmitterInterface
{
    public function getModel();
   
    public function getNumber();
}
