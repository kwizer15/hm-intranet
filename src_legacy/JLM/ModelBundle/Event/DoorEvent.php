<?php

namespace JLM\ModelBundle\Event;

use Symfony\Component\HttpFoundation\Request;
use JLM\CommerceBundle\Model\BillInterface;
use JLM\CoreBundle\Event\RequestEvent;
use JLM\ModelBundle\Entity\Door;

class DoorEvent extends RequestEvent
{
    /**
     * @var BillInterface
     */
    private $door;

    /**
     * @param FormInterface $form
     * @param Request $request
     */
    public function __construct(Door $door, Request $request)
    {
        $this->door = $door;
        parent::__construct($request);
    }
    
    public function getDoor()
    {
        return $this->door;
    }
}
