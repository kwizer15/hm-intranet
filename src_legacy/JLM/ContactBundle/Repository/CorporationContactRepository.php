<?php

namespace JLM\ContactBundle\Repository;

use Doctrine\ORM\EntityRepository;

class CorporationContactRepository extends EntityRepository
{
   
    /**
     *
     * @param int $id
     * @return array|null
     */
    public function getByIdToArray($id)
    {
        $queryBuilder = $this->createQueryBuilder('a')
        ->select('a,b,c,d')
        ->leftJoin('a.contact', 'b')
        ->leftJoin('b.phones', 'c')
        ->leftJoin('c.phone', 'd')
        ->where('a.id = :id')
        ->setParameter('id', $id)
        ;
        $res = $queryBuilder->getQuery()->getArrayResult();
    
        if (isset($res[0])) {
            return $res[0];
        }
    
        return [];
    }
}
