<?php

namespace JLM\CommerceBundle\Model;

interface OrderInterface
{
    /**
     * @return int
     */
    public function getState();
    
    public function getCreation();
    
    public function getClose();
}
