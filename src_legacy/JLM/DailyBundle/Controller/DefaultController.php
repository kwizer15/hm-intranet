<?php

namespace JLM\DailyBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JLM\DailyBundle\Entity\Intervention;
use JLM\DailyBundle\Entity\Maintenance;
use JLM\DailyBundle\Entity\Work;
use JLM\ModelBundle\Entity\Door;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use JLM\ModelBundle\Form\Type\DatepickerType;
use JLM\DailyBundle\Entity\Fixing;
use JLM\DailyBundle\Form\Type\FixingType;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class DefaultController extends Controller
{
    /**
     * Search
     */
    public function searchAction(EntityManagerInterface $entityManager, FormFactoryInterface $formFactory, Request $request)
    {
        $formData = $request->get('jlm_core_search');
         
        if (is_array($formData) && array_key_exists('query', $formData)) {
            $doors = $entityManager->getRepository(Door::class)->search($formData['query']);

            /*
             * Voir aussi
            *   DoorController:stoppedAction
            *   FixingController:newAction -> utiliser formModal
            * @todo A factoriser de là ...
            */
            $fixingForms = [];
            foreach ($doors as $door) {
                $form = new Fixing();
                $form->setDoor($door);
                $form->setAskDate(new \DateTime);
                $fixingForms[] = $formFactory
                    ->createNamed('fixingNew'.$door->getId(), FixingType::class, $form)
                    ->createView()
                ;
            }
            /* à la */
            return $this->render('@JLMDaily/Default/search.html.twig', [
                    'query'   => $formData['query'],
                    'doors'   => $doors,
                    'fixing_forms' => $fixingForms,
            ]);
        }
        return $this->render('@JLMDaily/Default/search.html.twig', []);
    }
    
    /**
     * Search
     *
     * @deprecated
     */
    public function searchgetAction()
    {
        return $this->redirectToRoute('intervention_today');
    }
    
    /**
     * Sidebar
     *
     * @deprecated Use the TwigExtension
     */
    public function sidebarAction(EntityManagerInterface $entityManager)
    {
        return $this->render('@JLMDaily/Default/sidebar.html.twig', [
            'today' => $entityManager->getRepository(Intervention::class)->getCountToday(),
            'stopped' => $entityManager->getRepository(Door::class)->getCountStopped(),
            'fixing' => $entityManager->getRepository(Fixing::class)->getCountOpened(),
            'work'   => $entityManager->getRepository(Work::class)->getCountOpened(),
            'maintenance' => $entityManager->getRepository(Maintenance::class)->getCountOpened(),
        ]);
    }
    
    /**
     * Search by date form
     */
    public function datesearchAction()
    {
        $entity = new \DateTime();
        $form   = $this->createForm(DatepickerType::class, $entity);

        return $this->render('@JLMDaily/Default/datesearch.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
