<?php

namespace JLM\TransmitterBundle\Factory;

use JLM\TransmitterBundle\Builder\AskBuilderInterface;

class AskFactory
{
    /**
     *
     * @param AskBuilderInterface $bill
     *
     * @return AskInterface
     */
    public static function create(AskBuilderInterface $builder)
    {
        $builder->create();
        $builder->buildCreation();
        $builder->buildTrustee();
        $builder->buildMethod();
        $builder->buildMaturity();
        $builder->buildPerson();
        $builder->buildAsk();
        
        return $builder->getAsk();
    }
}
