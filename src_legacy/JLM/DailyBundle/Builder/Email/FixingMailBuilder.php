<?php

namespace JLM\DailyBundle\Builder\Email;

use JLM\DailyBundle\Model\FixingInterface;

abstract class FixingMailBuilder extends InterventionMailBuilder
{
    public function __construct(FixingInterface $fixing)
    {
        parent::__construct($fixing);
    }
    
    public function getFixing()
    {
        return $this->getIntervention();
    }
}
