<?php

namespace JLM\ContactBundle\Entity;

use JLM\ContactBundle\Model\ContactInterface;
use JLM\ContactBundle\Model\AddressInterface;

abstract class ContactDecorator implements ContactInterface
{
    /**
     * Identifier
     * @var int $id
     */
    private $id;
    
    /**
     * @var ContactInterface $person
     */
    protected $contact;
    
    /**
     * Get id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * Get contact
     */
    public function getContact()
    {
        return $this->contact;
    }
    
    /**
     * Get contact
     */
    public function setContact(ContactInterface $contact)
    {
        $this->contact = $contact;
        
        return $this;
    }
    
    /**
     * {@inheritdoc}
     */
    public function getAddress()
    {
        return $this->getContact()->getAddress();
    }
    
    /**
     * {@inheritdoc}
     */
    public function setAddress(AddressInterface $address)
    {
        return $this->getContact()->setAddress($address);
    }
    
    /**
     * {@inheritdoc}
     */
    public function getEmail()
    {
        return $this->getContact()->getEmail();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getFax()
    {
        return $this->getContact()->getFax();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getPhones()
    {
        return $this->getContact()->getPhones();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getContact()->getName();
    }
    
    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getContact()->__toString();
    }
    
    /**
     *
     * @param string $method
     * @param mixed $default
     * @return mixed
     */
    private function decoratedGetMethod($method, $default)
    {
        if ($this->getContact() === null) {
            return $default;
        }
        
        return $this->getContact()->$method();
    }
}
