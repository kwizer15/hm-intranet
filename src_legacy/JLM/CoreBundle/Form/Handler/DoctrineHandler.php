<?php

namespace JLM\CoreBundle\Form\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class DoctrineHandler extends FormHandler
{
    /**
     * @var Request
     */
    protected $objectManager;
    public $entity;

    /**
     * Constructor
     *
     * @param Form $form
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param null $entity
     */
    public function __construct(Form $form, Request $request, EntityManagerInterface $entityManager, $entity = null)
    {
        parent::__construct($form, $request);
        $this->objectManager = $entityManager;
        $this->entity = $entity;
    }
    
    /**
     * {@inheritdoc}
     */
    public function onSuccess()
    {
        switch ($this->request->getMethod()) {
            case 'GET':
                return false;
            case 'DELETE':
                $this->objectManager->remove($this->entity);
                break;
            default:
                $this->objectManager->persist($this->form->getData());
        }
        $this->objectManager->flush();
        
        return true;
    }
}
