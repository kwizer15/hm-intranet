<?php

namespace JLM\FeeBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use JLM\ContractBundle\JLMContractEvents;
use JLM\ContractBundle\Event\ContractEvent;
use JLM\FeeBundle\Entity\Fee;

class ContractSubscriber implements EventSubscriberInterface
{
    public function __construct(EntityManagerInterface $objectManager)
    {
        $this->objectManager = $objectManager;
    }

    public static function getSubscribedEvents()
    {
        return [
            JLMContractEvents::AFTER_CONTRACT_CREATE => 'feeCreate'
        ];
    }

    public function feeCreate(ContractEvent $event)
    {
        $entity = $event->getContract();
        if ($entity->getInProgress()) {
            $fee = new Fee();
            $fee->addContract($entity);
            $fee->setTrustee($entity->getTrustee());
            $fee->setAddress($entity->getDoor()->getSite()->getAddress()->toString());
            $fee->setPrelabel($entity->getDoor()->getSite()->getBillingPrelabel());
            $fee->setVat($entity->getDoor()->getSite()->getVat());
            $this->objectManager->persist($fee);
            $this->objectManager->flush();
        }
    }
}
