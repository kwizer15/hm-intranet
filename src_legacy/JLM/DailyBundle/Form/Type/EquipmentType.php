<?php

namespace JLM\DailyBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use JLM\DailyBundle\Entity\Equipment;

class EquipmentType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('place', null, ['label'=>'Lieu'])
            ->add('reason', null, ['label'=>'Raison'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Equipment::class
        ]);
    }

    public function getBlockPrefix(): string
    {
        return 'jlm_dailybundle_equipmenttype';
    }
}
