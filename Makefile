.PHONY: help server assets cache-clear cache-delete database-init database-data install phpcs fixcs phpmetrics phpmd phpstan
.DEFAULT_GOAL=help

include .env
-include .env.local

PHP ?= php
DATABASE_HOST ?= localhost
DATABASE_PORT ?= 3306
DATABASE_USERNAME ?= hmintranet
DATABASE_PASSWORD ?= hmintranet
DATABASE_NAME ?= hmintranet

php := $(shell which $(PHP)) -d memory_limit=-1
mysql := $(shell which mysql)
root_mysql := sudo $(mysql)
console := $(php) bin/console
composer := $(php) $(shell which composer)

bin_path := vendor/bin
public_path := public
cache_path := var/cache

database_migration := $(php) $(bin_path)/phinx migrate --no-interaction --environment=database
deploy := $(php) $(bin_path)/dep deploy
test := APP_ENV=test $(php) $(bin_path)/phpunit
phpcs := $(php) $(bin_path)/phpcs -p
fixcs := $(php) $(bin_path)/phpcbf
phpmetrics := $(php) $(bin_path)/phpmetrics
phpmd := $(php) $(bin_path)/phpmd
phpstan := $(php) $(bin_path)/phpstan analyse --level 0

npm := $(shell which npm)

docker := $(shell which docker)

HOST ?= localhost
PORT ?= 8000

phpmetrics_prerequisites = var/phpmetrics src src_legacy
phpmetrics_args = --report-html=$(phpmetrics_prerequisites)
phpmd_prerequisites = src
phpmd_args = $(phpmd_prerequisites) text codesize,unusedcode,naming
phpstan_prerequisites = src
phpstan_args = $(phpstan_prerequisites)
composer_args := --optimize-autoloader --prefer-dist
console_args := --env=$(APP_ENV)
ifneq ($(APP_ENV),dev)
  composer_args += --no-dev --no-suggest --no-interaction --classmap-authoritative
  console_args += --no-debug
endif

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

server: $(public_path) vendor ## Lance le serveur PHP
	$(php) -S $(HOST):$(PORT) -t $<

build: vendor node_modules $(public_path)/assets cache-clear
install: build database-init database-migrate ## Installe le projet

docker-build: docker/php/Dockerfile
	$(docker) login registry.gitlab.com
	$(docker) build -t registry.gitlab.com/kwizer15/hm-intranet/php docker/php
	$(docker) push registry.gitlab.com/kwizer15/hm-intranet/php

$(public_path)/bundles: $(shell find src_legacy/*/*/Resources) $(shell find src_fork/*/*/*/Resources)
	$(console) assets:install $(console_args)

$(public_path)/assets: node_modules $(shell find assets) webpack.config.js
	$(npm) run build

cache-clear: vendor
	$(console) cache:clear --no-warmup $(console_args)
	$(console) cache:warmup  $(console_args)

cache-delete:
	rm -rf $(cache_path)/*

database-init: ## Initialise la base de donnée et l'utilisateur lié (nécessite les droit super utilisateur)
	$(root_mysql) --execute="DROP DATABASE IF EXISTS $(DATABASE_NAME);"
	$(root_mysql) --execute="CREATE DATABASE $(DATABASE_NAME);"
	$(root_mysql) --execute="CREATE USER IF NOT EXISTS '$(DATABASE_USERNAME)'@'$(DATABASE_HOST)' IDENTIFIED BY '$(DATABASE_PASSWORD)';"
	$(root_mysql) --execute="GRANT ALL ON $(DATABASE_NAME).* TO '$(DATABASE_USERNAME)'@'$(DATABASE_HOST)' IDENTIFIED BY '$(DATABASE_PASSWORD)';"

%.sql.gz: %.sql
	gzip -9 $<

database-migrate:
	$(database_migration)

database-data: db_backup.sql.gz vendor
	gunzip < $< | $(mysql) \
	    --user=$(DATABASE_USERNAME) \
	    --password=$(DATABASE_PASSWORD) \
	    --host=$(DATABASE_HOST) \
	    --port=$(DATABASE_PORT) \
	    $(DATABASE_NAME)
	$(console) doctrine:schema:update --force $(console_args)

test phpcs fixcs phpmetrics phpmd phpstan: $(bin_path) $($@_prerequisites)
	$($@) $($@_args)

deploy-production:
	$(deploy) production

deploy-staging:
	$(deploy) staging

composer.lock: composer.json
	$(composer) update --lock $(composer_args)

vendor $(bin_path): composer.lock
	$(composer) install $(composer_args)

var/%:
	mkdir -p $@

node_modules: package-lock.json
	$(npm) install
