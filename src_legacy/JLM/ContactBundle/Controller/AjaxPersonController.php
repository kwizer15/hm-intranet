<?php

namespace JLM\ContactBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use JLM\ContactBundle\Manager\ContactManager;
use JLM\ContactBundle\Form\Type\PersonType;
use JLM\ContactBundle\Entity\Person;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class AjaxPersonController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Finds and displays a Person entity.
     */
    public function showajaxAction(Person $entity)
    {
        return $this->render('@JLMContact/AjaxPerson/showajax.html.twig', [
            'entity'      => $entity,
        ]);
    }

    /**
     * Displays a form to create a new Person entity.
     */
    public function newajaxAction(ContactManager $manager, Request $request)
    {
        $entity = new Person();
        $form = $manager->createForm($request, 'POST', $entity);

        return $this->render('@JLMContact/AjaxPerson/newajax.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }

    /**
     * Creates a new Person entity.
     */
    public function createajaxAction(Request $request)
    {
        $entity  = new Person();
        $form    = $this->createForm(PersonType::class, $entity);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($entity->getAddress() !== null) {
                $this->entityManager->persist($entity->getAddress());
            }
            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            return $this->redirectToRoute('jlm_contact_ajax_person_show', ['id' => $entity->getId()]);
        }
        
        return $this->render('@JLMContact/AjaxPerson/createajax.html.twig', [
            'entity' => $entity,
            'form'   => $form->createView()
        ]);
    }
    
    /**
     * Return JSON list of Person entity.
     */
    public function autocompleteAction(Request $request)
    {
        $query = $request->request->get('term');

        $results = $this->entityManager->getRepository(Person::class)->searchResult($query);

        return new JsonResponse($results);
    }
    
    /**
     * Person json
     */
    public function searchAction(Request $request)
    {
        $term = $request->get('q');
        $page_limit = $request->get('page_limit');
        $persons = $this->entityManager->getRepository(Person::class)->getArray($term, $page_limit);
        
        return new JsonResponse(['persons' => $persons]);
    }
    
    /**
     * Person json
     */
    public function jsonAction(Request $request)
    {
        $id = $request->get('id');
        $person = $this->entityManager->getRepository(Person::class)->getByIdToArray($id);
    
        return new JsonResponse($person);
    }
}
