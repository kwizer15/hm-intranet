<?php

namespace JLM\CommerceBundle\Entity;

use JLM\ProductBundle\Model\ProductInterface;
use JLM\CommerceBundle\Model\CommercialPartLineInterface;

abstract class CommercialPartLine implements CommercialPartLineInterface
{
    /**
     * Position de la ligne dans le devis
     * @var int
     */
    private $position = 0;
    
    /**
     * @var string $designation
     */
    private $designation;
    
    /**
     * Set position
     *
     * @param int $position
     * @return self
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }
    
    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }
    
    /**
     * Set designation
     *
     * @param string $designation
     * @return self
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
    
        return $this;
    }
    
    /**
     * Get designation
     *
     * @return string
     */
    public function getDesignation()
    {
        return $this->designation;
    }
}
