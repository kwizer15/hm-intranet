<?php

namespace JLM\ModelBundle\Builder;

use JLM\ModelBundle\Entity\Site;

abstract class SiteBillBuilderAbstract extends TrusteeBillBuilderAbstract
{
    protected $site;
    
    public function __construct(Site $site, $options = [])
    {
        $this->site = $site;
        parent::__construct($this->site->getManager(), $options);
    }
   
    /**
     * {@inheritdoc}
     */
    public function buildBusiness()
    {
        $site = $this->site;
        if ($site instanceof Site) {
            $this->getBill()->setSite($site->toString());
            $this->getBill()->setSiteObject($site);
            $this->getBill()->setPrelabel($site->getBillingPrelabel());
            $this->getBill()->setVat($site->getVat()->getRate());
        } else {
            $this->getBill()->setSite('');
        }
    }
}
