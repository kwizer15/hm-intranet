<?php

namespace JLM\CommerceBundle\Controller;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use DomainException;
use Exception;
use HMFermeture\Domain\Devis\Repository\DemandeDeDevisRepository;
use HMFermeture\Domain\Devis\Repository\ResponsableRepository;
use JLM\CommerceBundle\Domain\Command\CreateQuoteCommand;
use JLM\CommerceBundle\Domain\Command\CreateQuoteCommandHandler;
use JLM\CommerceBundle\Domain\Command\EditQuoteCommand;
use JLM\CommerceBundle\Domain\Command\EditQuoteCommandHandler;
use JLM\CommerceBundle\Entity\Quote;
use JLM\CommerceBundle\Entity\VAT;
use JLM\CommerceBundle\Form\Type\CreateQuoteType;
use JLM\CommerceBundle\Form\Type\EditQuoteType;
use JLM\CommerceBundle\Pdf\Jacket;
use JLM\CommerceBundle\Pdf\Quote as QuotePdf;
use JLM\CoreBundle\Response\PdfResponse;
use JLM\CoreBundle\Service\Paginator;
use JLM\CoreBundle\Service\SearchRenderer;
use JLM\OfficeBundle\Entity\AskQuote;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use JLM\ModelBundle\Entity\Mail;
use JLM\ModelBundle\Form\Type\MailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

/**
 * @IsGranted("ROLE_OFFICE")
 */
class QuoteController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var \Doctrine\Persistence\ObjectRepository
     */
    private $repository;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->repository = $entityManager->getRepository(Quote::class);
    }

    /**
     * Lists all Quote entities.
     */
    public function indexAction(Paginator $paginator, Request $request)
    {
        $states = [
                'all' => 'All',
                'in_seizure' => 'InSeizure',
                'waiting' => 'Waiting',
                'sended' => 'Sended',
                'given' => 'Given',
                'canceled' => 'Canceled'
        ];
        $state = $request->get('state');
        $state = (!array_key_exists($state, $states)) ? 'all' : $state;
        $views = ['index'=>'Liste','follow'=>'Suivi'];
        $view = $request->get('view');
        $view = (!array_key_exists($view, $views)) ? 'index' : $view;
        
        $method = $states[$state];
        
        return $this->render(
            '@JLMCommerce/Quote/'.$view.'.html.twig',
            $paginator->pagination($request, $this->repository, 'getCount'.$method, 'get'.$method, 'quote', ['state' => $state, 'view' => $view])
        );
    }

    /**
     * Finds and displays a Quote entity.
     */
    public function showAction($id)
    {

        return $this->render(
            '@JLMCommerce/Quote/show.html.twig',
            ['entity'=> $this->repository->find($id)]
        );
    }

    /**
     * Nouveau devis
     */
    public function newAction(
        Request $request,
        ResponsableRepository $responsableRepository,
        DemandeDeDevisRepository $demandeDeDevisRepository,
        CreateQuoteCommandHandler $handler
    ) {
        $askQuote = null;
        $askQuoteId = $request->query->get('ask');
        $responsable = $responsableRepository->getByUsername($this->getUser()->getUsername());
        $command = null;
        if (null !== $askQuoteId) {
            try {
                $askQuote = $demandeDeDevisRepository->get($askQuoteId);
                $command = CreateQuoteCommand::fromAskQuote($askQuote, $responsable);
            } catch (Exception $exception) {
            }
            $askQuoteRepository = $this->entityManager->getRepository(AskQuote::class);
            $askQuote = $askQuoteRepository->find($askQuoteId);
        }

        if (null === $command) {
            $vat = $this->entityManager->getRepository(VAT::class)->find(1)->getRate();
            $command = new CreateQuoteCommand(new DateTime(), $responsable, $vat);
        }

        $form = $this->createForm(CreateQuoteType::class, $command, [
            'action' => $this->generateUrl('quote_create'),
        ]);

        // TODO: add button in template
        $form->add('submit', SubmitType::class, ['label' => 'Créer']);
        $form->handleRequest($request);

        if (!$form->isSubmitted() || !$form->isValid()) {
            return $this->render('@JLMCommerce/Quote/new.html.twig', [
                'form' => $form->createView()
            ]);
        }

        [$id] = $handler->handle($command);

        return $this->redirectToRoute('quote_show', ['id' => $id]);
    }

    /**
     * Displays a form to edit an existing Quote entity.
     */
    public function editAction(Request $request, EditQuoteCommandHandler $handler, int $id)
    {
        $entity = $this->repository->find($id);
        $this->assertState($entity, [0]);

        $form = $this->createForm(EditQuoteType::class, $entity, [
            'action' => $this->generateUrl('quote_update', ['id' => $id]),
        ]);
        // TODO: add button in template
        $form->add('submit', SubmitType::class, ['label' => 'Modifier']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $handler->handle(new EditQuoteCommand($entity));

                return $this->redirectToRoute('quote_show', ['id' => $form->getData()->getId()]);
            } catch (DomainException $exception) {
                $this->addFlash('warning', $exception->getMessage());
            } catch (Exception $exception) {
                $this->addFlash('danger', 'Une erreur s\'est produite.');
            }
        }

        return $this->render('@JLMCommerce/Quote/edit.html.twig', [
            'entity' => $entity,
            'edit_form' => $form->createView()
        ]);
    }
     
    /**
     * Resultats de la barre de recherche.
     */
    public function searchAction(SearchRenderer $renderer, Request $request)
    {
        return $renderer->renderSearch($this->repository, $request, '@JLMCommerce/Quote/search.html.twig');
    }
    
    /**
     * Imprimer toute les variantes
     */
    public function printAction($id)
    {
        $entity = $this->repository->find($id);
        $filename = $entity->getNumber().'.pdf';

        return new PdfResponse($filename, QuotePdf::get($entity->getVariants()));
    }
    
    /**
     * Imprimer la chemise
     */
    public function jacketAction($id)
    {
        $entity = $this->repository->find($id);
        $filename = $entity->getNumber().'-jacket.pdf';

        return new PdfResponse($filename, Jacket::get($entity));
    }
    
    /**
     * Mail
     */
    public function mailAction($id)
    {
        $entity = $this->repository->find($id);
        $this->assertState($entity, [1,2,3,4,5]);
        $mail = new Mail();
        $mail->setSubject('Devis n°'.$entity->getNumber());
        $mail->setFrom('commerce@jlm-entreprise.fr');
        $mail->setBody($this->render('@JLMCommerce/Quote/email.txt.twig', ['entity' => $entity]));
        $mail->setSignature($this->render('@JLMCommerce/QuoteVariant/emailsignature.txt.twig', [
            'name' => $entity->getFollowerCp()
        ]));
        if ($entity->getContact()) {
            if ($entity->getContact()->getPerson()) {
                if ($entity->getContact()->getPerson()->getEmail()) {
                    $mail->setTo($entity->getContact()->getPerson()->getEmail());
                }
            }
        }
        $form = $this->createForm(MailType::class, $mail);
        
        return $this->render('@JLMCommerce/Quote/mail.html.twig', [
                'entity' => $entity,
                'form'   => $form->createView()
        ]);
    }
    
    /**
     * Send by mail a QuoteVariant entity.
     */
    public function sendmailAction(Request $request, $id, Environment $templating, \Swift_Mailer $mailer)
    {
        $entity = $this->repository->find($id);
        $this->assertState($entity, [1,2,3,4,5]);
        // Message
        $mail = new Mail();
        $form = $this->createForm(MailType::class, $mail);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $mail->setBcc('commerce@jlm-entreprise.fr');
                
            $message = $mail->getSwift();
            $message->setReadReceiptTo(['commerce@jlm-entreprise.fr']);
//          $eventComment = 'Destinataire : '.$mail->getTo();
//          if ($mail->getCc())
//          {
//              $eventComment .= PHP_EOL.'Copie : '.$mail->getCc();
//          }
//          $eventComment .= PHP_EOL.'Pièces jointes :';
            foreach ($entity->getVariants() as $variant) {
                if ($variant->getState() > 0) {
                    $message->attach(\Swift_Attachment::newInstance(
                        QuotePdf::get([$variant]),
                        $variant->getNumber().'.pdf',
                        'application/pdf'
                    ))
                    ;
//                  $eventComment .= PHP_EOL.'- Devis n°'.$variant->getNumber();
                    $variant->setState(3);
                }
            }
            if ($entity->getVat() == $entity->getVatTransmitter()) {
                $message->attach(\Swift_Attachment::fromPath(
                    $this->container->get('kernel')->getRootDir().'/../web/bundles/jlmcommerce/pdf/attestation.pdf'
                ));
//              $eventComment .= PHP_EOL.'- Attestation TVA à 10%';
            }

            //$entity->addEvent(Quote::EVENT_SEND, $eventComment);
            $mailer->send($message);
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        }
        
        return $this->redirectToRoute('quote_show', ['id' => $entity->getId()]);
    }

    private function assertState($quote, $states = [])
    {
        if (!in_array($quote->getState(), $states)) {
            // return $this->redirectToRoute('quote_show', ['id' => $quote->getId()]);
        }
    }
}
