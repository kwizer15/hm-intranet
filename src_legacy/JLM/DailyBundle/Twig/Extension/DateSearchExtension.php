<?php

namespace JLM\DailyBundle\Twig\Extension;

use DateTime;
use JLM\ModelBundle\Form\Type\DatepickerType;
use Symfony\Component\Form\FormFactoryInterface;
use Twig\Extension\AbstractExtension;
use Twig\Extension\GlobalsInterface;

class DateSearchExtension extends AbstractExtension implements GlobalsInterface
{
    private $formService;

    public function __construct(FormFactoryInterface $formService)
    {
        $this->formService = $formService;
    }

    public function getName()
    {
        return 'date_search_extension';
    }

    public function getGlobals()
    {
        $entity = new DateTime();
        $form = $this->formService->create(DatepickerType::class, $entity);
        return [
            'date_search' => [
                'form' => $form->createView(),
            ],
        ];
    }
}
