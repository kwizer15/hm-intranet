<?php

namespace JLM\ContactBundle\Form\DataTransformer;

use JLM\ContactBundle\Entity\Corporation;
use JLM\CoreBundle\Form\DataTransformer\ObjectToIntTransformer;

class CorporationToIntTransformer extends ObjectToIntTransformer
{
    /**
     * {@inheritdoc}
     */
    protected function getClass()
    {
        return Corporation::class;
    }
    
    /**
     * {@inheritdoc}
     */
    protected function getErrorMessage()
    {
        return 'A corporation with id "%s" does not exist!';
    }
}
