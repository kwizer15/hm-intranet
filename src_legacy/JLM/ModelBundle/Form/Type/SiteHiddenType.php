<?php

namespace JLM\ModelBundle\Form\Type;

use JLM\ModelBundle\Form\DataTransformer\SiteToIntTransformer;
use JLM\ModelBundle\Form\Type\AbstractHiddenType;

class SiteHiddenType extends AbstractHiddenType
{

    protected function getTransformerClass(): string
    {
        return SiteToIntTransformer::class;
    }
    
    protected function getTypeName(): string
    {
        return 'site';
    }
}
