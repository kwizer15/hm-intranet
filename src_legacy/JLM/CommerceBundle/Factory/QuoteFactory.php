<?php

namespace JLM\CommerceBundle\Factory;

use JLM\CommerceBundle\Builder\QuoteBuilderInterface;

class QuoteFactory
{
    /**
     *
     * @param QuoteBuilderInterface $bill
     *
     * @return QuoteInterface
     */
    public static function create(QuoteBuilderInterface $builder)
    {
        $builder->create();
        $builder->buildCreation();
        $builder->buildLines();
        $builder->buildCustomer();
        $builder->buildBusiness();
        $builder->buildIntro();
        $builder->buildConditions();
        
        return $builder->getQuote();
    }
}
