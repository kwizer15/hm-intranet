<?php

declare(strict_types=1);

namespace JLM\CoreBundle\Service;

use JLM\CoreBundle\Repository\SearchRepositoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

class SearchRenderer
{
    /**
     * @var EngineInterface
     */
    private $templating;

    public function __construct(Environment $templating)
    {
        $this->templating = $templating;
    }

    public function renderSearch(SearchRepositoryInterface $repository, Request $request, $template)
    {
        $formData = $request->get('jlm_core_search');

        if (is_array($formData) && array_key_exists('query', $formData)) {
            return $this->templating->renderResponse($template, [
                'results' => $repository->search($formData['query']),
                'query' => $formData['query'],
            ]);
        }

        return $this->templating->renderResponse($template, ['results' => [], 'query' => '']);
    }
}
